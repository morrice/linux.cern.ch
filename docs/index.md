# Recent software updates and News:

## Latest updates

* Check [AlmaLinux 9 latest updates](updates/alma9/prod/latest_updates)
* Check [AlmaLinux 8 latest updates](updates/alma8/prod/latest_updates)
* Check [Red Hat Enterprise Linux 9 latest updates](updates/rhel9/prod/latest_updates)
* Check [Red Hat Enterprise Linux 8 latest updates](updates/rhel8/prod/latest_updates)
* Check [CentOS Stream 9 latest updates](updates/cs9/prod/latest_updates)
* Check [CentOS Stream 8 latest updates](updates/cs8/prod/latest_updates)
* Check [CERN CentOS 7 latest updates](updates/cc7/prod/latest_updates)

## Latest news

See latest news [here](news/latest_news).

Archive of [(Old) News](news/old-news).
