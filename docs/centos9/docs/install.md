# CentOS Stream 9 - Install instructions

The following methods are currently available to install CentOS Stream 9

* AIMS interactive - select the menu item `Install CentOS Stream 9 (CS9) 64-bit system`
* AIMS host registration - define your host to use the `CS9_X64_64` (Stream)
* OpenStack VM - boot from the image `CS9 - x86_64 [YYYY-MM-DD` (Stream)
* ai-bs puppet managed VM - use `ai-bs --c9`
* docker image - use `docker pull gitlab-registry.cern.ch/linuxsupport/cs9-base:latest`
* Boot ISO: https://linuxsoft.cern.ch/cern/centos/s9/BaseOS/x86_64/os/images/boot.iso

For a detailed installation of a Linux Desktop, check the [step by step guide](/centos9/docs/stepbystep)
