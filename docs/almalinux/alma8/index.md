# AlmaLinux 8 Documentation
  
* [Documentation pages](/almalinux/alma8)
    * [Installation instructions](/almalinux/alma8/install)
    * [Locmap installation](/almalinux/alma8/locmap)

Please check <a href="../../docs/">Documentation</a> for additional documentation.