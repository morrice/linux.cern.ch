## 2023-02-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.rh8.cern | |
oracle-release | 1.5-2.rh8.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-1.rh8.cern | |
kmod-openafs | 1.8.9.0-1.4.18.0_425.10.1.el8_7.rh8.cern | |
openafs | 1.8.9.0-1.rh8.cern | |
openafs-authlibs | 1.8.9.0-1.rh8.cern | |
openafs-authlibs-devel | 1.8.9.0-1.rh8.cern | |
openafs-client | 1.8.9.0-1.rh8.cern | |
openafs-compat | 1.8.9.0-1.rh8.cern | |
openafs-debugsource | 1.8.9.0-1.rh8.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_425.10.1.el8_7-1.rh8.cern | |
openafs-devel | 1.8.9.0-1.rh8.cern | |
openafs-docs | 1.8.9.0-1.rh8.cern | |
openafs-kernel-source | 1.8.9.0-1.rh8.cern | |
openafs-krb5 | 1.8.9.0-1.rh8.cern | |
openafs-server | 1.8.9.0-1.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.8.29-8.el8_7.1 | |
sudo-debuginfo | 1.8.29-8.el8_7.1 | |
sudo-debugsource | 1.8.29-8.el8_7.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.7.0-1.el8_7 | |
firefox-debuginfo | 102.7.0-1.el8_7 | |
firefox-debugsource | 102.7.0-1.el8_7 | |
java-11-openjdk | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc-zip | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs | 17.0.6.0.10-3.el8_7 | |
libXpm | 3.5.12-9.el8_7 | |
libXpm-debuginfo | 3.5.12-9.el8_7 | |
libXpm-debugsource | 3.5.12-9.el8_7 | |
libXpm-devel | 3.5.12-9.el8_7 | |
libXpm-devel-debuginfo | 3.5.12-9.el8_7 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-slowdebug | 17.0.6.0.10-3.el8_7 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.rh8.cern | |
oracle-release | 1.5-2.rh8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-1.rh8.cern | |
kmod-openafs | 1.8.9.0-1.4.18.0_425.10.1.el8_7.rh8.cern | |
openafs | 1.8.9.0-1.rh8.cern | |
openafs-authlibs | 1.8.9.0-1.rh8.cern | |
openafs-authlibs-devel | 1.8.9.0-1.rh8.cern | |
openafs-client | 1.8.9.0-1.rh8.cern | |
openafs-compat | 1.8.9.0-1.rh8.cern | |
openafs-debugsource | 1.8.9.0-1.rh8.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_425.10.1.el8_7-1.rh8.cern | |
openafs-devel | 1.8.9.0-1.rh8.cern | |
openafs-docs | 1.8.9.0-1.rh8.cern | |
openafs-kernel-source | 1.8.9.0-1.rh8.cern | |
openafs-krb5 | 1.8.9.0-1.rh8.cern | |
openafs-server | 1.8.9.0-1.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.8.29-8.el8_7.1 | |
sudo-debuginfo | 1.8.29-8.el8_7.1 | |
sudo-debugsource | 1.8.29-8.el8_7.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.7.0-1.el8_7 | |
firefox-debuginfo | 102.7.0-1.el8_7 | |
firefox-debugsource | 102.7.0-1.el8_7 | |
java-11-openjdk | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-javadoc-zip | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs | 17.0.6.0.10-3.el8_7 | |
libXpm | 3.5.12-9.el8_7 | |
libXpm-debuginfo | 3.5.12-9.el8_7 | |
libXpm-debugsource | 3.5.12-9.el8_7 | |
libXpm-devel | 3.5.12-9.el8_7 | |
libXpm-devel-debuginfo | 3.5.12-9.el8_7 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el8_7 | |
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el8_7 | |
java-17-openjdk-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-debugsource | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-demo-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-devel-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-fastdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-headless-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-jmods-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-slowdebug-debuginfo | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-src-slowdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-fastdebug | 17.0.6.0.10-3.el8_7 | |
java-17-openjdk-static-libs-slowdebug | 17.0.6.0.10-3.el8_7 | |

