## 2023-04-13

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.16-1.el8_7 | |
aspnetcore-runtime-7.0 | 7.0.5-1.el8_7 | |
aspnetcore-targeting-pack-6.0 | 6.0.16-1.el8_7 | |
aspnetcore-targeting-pack-7.0 | 7.0.5-1.el8_7 | |
dotnet | 7.0.105-1.el8_7 | |
dotnet-apphost-pack-6.0 | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.5-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.16-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.5-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.16-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.5-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.16-1.el8_7 | |
dotnet-targeting-pack-7.0 | 7.0.5-1.el8_7 | |
dotnet-templates-6.0 | 6.0.116-1.el8_7 | |
dotnet-templates-7.0 | 7.0.105-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.105-1.el8_7 | |
nodejs | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-debuginfo | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-debugsource | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-devel | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-docs | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-full-i18n | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-nodemon | 2.0.20-3.module+el8.7.0+18531+81d21ca6 | |
npm | 6.14.18-1.14.21.3.1.module+el8.7.0+18531+81d21ca6 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.105-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.16-1.el8_7 | |
aspnetcore-runtime-7.0 | 7.0.5-1.el8_7 | |
aspnetcore-targeting-pack-6.0 | 6.0.16-1.el8_7 | |
aspnetcore-targeting-pack-7.0 | 7.0.5-1.el8_7 | |
dotnet | 7.0.105-1.el8_7 | |
dotnet-apphost-pack-6.0 | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0 | 7.0.5-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0 | 6.0.16-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0 | 7.0.5-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0 | 6.0.16-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0 | 7.0.5-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0 | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0 | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-targeting-pack-6.0 | 6.0.16-1.el8_7 | |
dotnet-targeting-pack-7.0 | 7.0.5-1.el8_7 | |
dotnet-templates-6.0 | 6.0.116-1.el8_7 | |
dotnet-templates-7.0 | 7.0.105-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |
netstandard-targeting-pack-2.1 | 7.0.105-1.el8_7 | |
nodejs | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-debuginfo | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-debugsource | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-devel | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-docs | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-full-i18n | 14.21.3-1.module+el8.7.0+18531+81d21ca6 | |
nodejs-nodemon | 2.0.20-3.module+el8.7.0+18531+81d21ca6 | |
npm | 6.14.18-1.14.21.3.1.module+el8.7.0+18531+81d21ca6 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-host-debuginfo | 7.0.5-1.el8_7 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-runtime-6.0-debuginfo | 6.0.16-1.el8_7 | |
dotnet-runtime-7.0-debuginfo | 7.0.5-1.el8_7 | |
dotnet-sdk-6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.116-1.el8_7 | |
dotnet-sdk-7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.105-1.el8_7 | |
dotnet6.0-debuginfo | 6.0.116-1.el8_7 | |
dotnet6.0-debugsource | 6.0.116-1.el8_7 | |
dotnet7.0-debuginfo | 7.0.105-1.el8_7 | |
dotnet7.0-debugsource | 7.0.105-1.el8_7 | |

