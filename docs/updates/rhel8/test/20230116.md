## 2023-01-16

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9pre2-3.4.18.0_425.10.1.el8_7.rh8.cern | |
openafs-debugsource | 1.8.9_4.18.0_425.10.1.el8_7pre2-3.rh8.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9pre2-3.4.18.0_425.10.1.el8_7.rh8.cern | |
openafs-debugsource | 1.8.9_4.18.0_425.10.1.el8_7pre2-3.rh8.cern | |

