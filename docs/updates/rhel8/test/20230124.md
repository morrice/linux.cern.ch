## 2023-01-24

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.8.29-8.el8_7.1 | |
sudo-debuginfo | 1.8.29-8.el8_7.1 | |
sudo-debugsource | 1.8.29-8.el8_7.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.7.0-1.el8_7 | |
firefox-debuginfo | 102.7.0-1.el8_7 | |
firefox-debugsource | 102.7.0-1.el8_7 | |
libXpm | 3.5.12-9.el8_7 | |
libXpm-debuginfo | 3.5.12-9.el8_7 | |
libXpm-debugsource | 3.5.12-9.el8_7 | |
libXpm-devel | 3.5.12-9.el8_7 | |
libXpm-devel-debuginfo | 3.5.12-9.el8_7 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.8.29-8.el8_7.1 | |
sudo-debuginfo | 1.8.29-8.el8_7.1 | |
sudo-debugsource | 1.8.29-8.el8_7.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.7.0-1.el8_7 | |
firefox-debuginfo | 102.7.0-1.el8_7 | |
firefox-debugsource | 102.7.0-1.el8_7 | |
libXpm | 3.5.12-9.el8_7 | |
libXpm-debuginfo | 3.5.12-9.el8_7 | |
libXpm-debugsource | 3.5.12-9.el8_7 | |
libXpm-devel | 3.5.12-9.el8_7 | |
libXpm-devel-debuginfo | 3.5.12-9.el8_7 | |

