## 2023-04-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-1.rh8.cern | |
cern-get-keytab | 1.5.2-1.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs-filesystem | 26.1-7.el8_7.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 26.1-7.el8_7.1 | |
emacs-common | 26.1-7.el8_7.1 | |
emacs-common-debuginfo | 26.1-7.el8_7.1 | |
emacs-debuginfo | 26.1-7.el8_7.1 | |
emacs-debugsource | 26.1-7.el8_7.1 | |
emacs-lucid | 26.1-7.el8_7.1 | |
emacs-lucid-debuginfo | 26.1-7.el8_7.1 | |
emacs-nox | 26.1-7.el8_7.1 | |
emacs-nox-debuginfo | 26.1-7.el8_7.1 | |
emacs-terminal | 26.1-7.el8_7.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-1.rh8.cern | |
cern-get-keytab | 1.5.2-1.rh8.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs-filesystem | 26.1-7.el8_7.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 26.1-7.el8_7.1 | |
emacs-common | 26.1-7.el8_7.1 | |
emacs-common-debuginfo | 26.1-7.el8_7.1 | |
emacs-debuginfo | 26.1-7.el8_7.1 | |
emacs-debugsource | 26.1-7.el8_7.1 | |
emacs-lucid | 26.1-7.el8_7.1 | |
emacs-lucid-debuginfo | 26.1-7.el8_7.1 | |
emacs-nox | 26.1-7.el8_7.1 | |
emacs-nox-debuginfo | 26.1-7.el8_7.1 | |
emacs-terminal | 26.1-7.el8_7.1 | |

