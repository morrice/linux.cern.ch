## 2023-03-21

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.9.0-3.el8_7 | |
firefox-debuginfo | 102.9.0-3.el8_7 | |
firefox-debugsource | 102.9.0-3.el8_7 | |

