## 2023-01-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-static-libs | 11.0.18.0.10-2.el9_1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el9_1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-static-libs | 11.0.18.0.10-2.el9_1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el9_1 | |

