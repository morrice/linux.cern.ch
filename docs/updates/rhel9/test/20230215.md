## 2023-02-15

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grub2-common | 2.06-46.el9_1.3 | |
grub2-debuginfo | 2.06-46.el9_1.3 | |
grub2-debugsource | 2.06-46.el9_1.3 | |
grub2-efi-aa64-modules | 2.06-46.el9_1.3 | |
grub2-efi-x64 | 2.06-46.el9_1.3 | |
grub2-efi-x64-cdboot | 2.06-46.el9_1.3 | |
grub2-efi-x64-modules | 2.06-46.el9_1.3 | |
grub2-emu-debuginfo | 2.06-46.el9_1.3 | |
grub2-pc | 2.06-46.el9_1.3 | |
grub2-pc-modules | 2.06-46.el9_1.3 | |
grub2-tools | 2.06-46.el9_1.3 | |
grub2-tools-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-efi | 2.06-46.el9_1.3 | |
grub2-tools-efi-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-extra | 2.06-46.el9_1.3 | |
grub2-tools-extra-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-minimal | 2.06-46.el9_1.3 | |
grub2-tools-minimal-debuginfo | 2.06-46.el9_1.3 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
grub2-common | 2.06-46.el9_1.3 | |
grub2-debuginfo | 2.06-46.el9_1.3 | |
grub2-debugsource | 2.06-46.el9_1.3 | |
grub2-efi-aa64 | 2.06-46.el9_1.3 | |
grub2-efi-aa64-cdboot | 2.06-46.el9_1.3 | |
grub2-efi-aa64-modules | 2.06-46.el9_1.3 | |
grub2-efi-x64-modules | 2.06-46.el9_1.3 | |
grub2-emu-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools | 2.06-46.el9_1.3 | |
grub2-tools-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-extra | 2.06-46.el9_1.3 | |
grub2-tools-extra-debuginfo | 2.06-46.el9_1.3 | |
grub2-tools-minimal | 2.06-46.el9_1.3 | |
grub2-tools-minimal-debuginfo | 2.06-46.el9_1.3 | |

