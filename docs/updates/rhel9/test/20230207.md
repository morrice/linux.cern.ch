## 2023-02-07

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.31.1-3.el9_1 | |
git-all | 2.31.1-3.el9_1 | |
git-core | 2.31.1-3.el9_1 | |
git-core-debuginfo | 2.31.1-3.el9_1 | |
git-core-doc | 2.31.1-3.el9_1 | |
git-credential-libsecret | 2.31.1-3.el9_1 | |
git-credential-libsecret-debuginfo | 2.31.1-3.el9_1 | |
git-daemon | 2.31.1-3.el9_1 | |
git-daemon-debuginfo | 2.31.1-3.el9_1 | |
git-debuginfo | 2.31.1-3.el9_1 | |
git-debugsource | 2.31.1-3.el9_1 | |
git-email | 2.31.1-3.el9_1 | |
git-gui | 2.31.1-3.el9_1 | |
git-instaweb | 2.31.1-3.el9_1 | |
git-subtree | 2.31.1-3.el9_1 | |
git-svn | 2.31.1-3.el9_1 | |
gitk | 2.31.1-3.el9_1 | |
gitweb | 2.31.1-3.el9_1 | |
perl-Git | 2.31.1-3.el9_1 | |
perl-Git-SVN | 2.31.1-3.el9_1 | |
thunderbird | 102.7.1-2.el9_1 | |
thunderbird-debuginfo | 102.7.1-2.el9_1 | |
thunderbird-debugsource | 102.7.1-2.el9_1 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.31.1-3.el9_1 | |
git-all | 2.31.1-3.el9_1 | |
git-core | 2.31.1-3.el9_1 | |
git-core-debuginfo | 2.31.1-3.el9_1 | |
git-core-doc | 2.31.1-3.el9_1 | |
git-credential-libsecret | 2.31.1-3.el9_1 | |
git-credential-libsecret-debuginfo | 2.31.1-3.el9_1 | |
git-daemon | 2.31.1-3.el9_1 | |
git-daemon-debuginfo | 2.31.1-3.el9_1 | |
git-debuginfo | 2.31.1-3.el9_1 | |
git-debugsource | 2.31.1-3.el9_1 | |
git-email | 2.31.1-3.el9_1 | |
git-gui | 2.31.1-3.el9_1 | |
git-instaweb | 2.31.1-3.el9_1 | |
git-subtree | 2.31.1-3.el9_1 | |
git-svn | 2.31.1-3.el9_1 | |
gitk | 2.31.1-3.el9_1 | |
gitweb | 2.31.1-3.el9_1 | |
perl-Git | 2.31.1-3.el9_1 | |
perl-Git-SVN | 2.31.1-3.el9_1 | |
thunderbird | 102.7.1-2.el9_1 | |
thunderbird-debuginfo | 102.7.1-2.el9_1 | |
thunderbird-debugsource | 102.7.1-2.el9_1 | |

