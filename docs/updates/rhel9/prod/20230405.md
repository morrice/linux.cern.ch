## 2023-04-05

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_162.22.2.el9_1.rh9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.22.2.el9_1-2.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-162.22.2.el9_1 | |
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel | 5.14.0-162.22.2.el9_1 | |
kernel-abi-stablelists | 5.14.0-162.22.2.el9_1 | |
kernel-core | 5.14.0-162.22.2.el9_1 | |
kernel-debug | 5.14.0-162.22.2.el9_1 | |
kernel-debug-core | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-modules | 5.14.0-162.22.2.el9_1 | |
kernel-debug-modules-extra | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.22.2.el9_1 | |
kernel-modules | 5.14.0-162.22.2.el9_1 | |
kernel-modules-extra | 5.14.0-162.22.2.el9_1 | |
kernel-tools | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs | 5.14.0-162.22.2.el9_1 | |
kpatch-patch-5_14_0-162_12_1 | 1-2.el9_1 | |
kpatch-patch-5_14_0-162_12_1-debuginfo | 1-2.el9_1 | |
kpatch-patch-5_14_0-162_12_1-debugsource | 1-2.el9_1 | |
kpatch-patch-5_14_0-162_18_1 | 1-1.el9_1 | |
kpatch-patch-5_14_0-162_18_1-debuginfo | 1-1.el9_1 | |
kpatch-patch-5_14_0-162_18_1-debugsource | 1-1.el9_1 | |
kpatch-patch-5_14_0-162_22_2 | 0-0.el9_1 | |
kpatch-patch-5_14_0-162_6_1 | 1-3.el9_1 | |
kpatch-patch-5_14_0-162_6_1-debuginfo | 1-3.el9_1 | |
kpatch-patch-5_14_0-162_6_1-debugsource | 1-3.el9_1 | |
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
tzdata | 2023b-1.el9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-devel | 5.14.0-162.22.2.el9_1 | |
kernel-debug-devel-matched | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.22.2.el9_1 | |
kernel-devel | 5.14.0-162.22.2.el9_1 | |
kernel-devel-matched | 5.14.0-162.22.2.el9_1 | |
kernel-doc | 5.14.0-162.22.2.el9_1 | |
kernel-headers | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
perf | 5.14.0-162.22.2.el9_1 | |
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
thunderbird | 102.9.0-1.el9_1 | |
thunderbird-debuginfo | 102.9.0-1.el9_1 | |
thunderbird-debugsource | 102.9.0-1.el9_1 | |
tzdata-java | 2023b-1.el9 | |

### rt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-core | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-core | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-debuginfo | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-devel | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-modules | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debug-modules-extra | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debuginfo | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-devel | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-modules | 5.14.0-162.22.2.rt21.186.el9_1 | |
kernel-rt-modules-extra | 5.14.0-162.22.2.rt21.186.el9_1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-cross-headers | 5.14.0-162.22.2.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-x86_64 | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-tools-libs-devel | 5.14.0-162.22.2.el9_1 | |
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_162.22.2.el9_1.rh9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.22.2.el9_1-2.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.4-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
bpftool-debuginfo | 5.14.0-162.22.2.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-host | 7.0.4-1.el9_1 | |
dotnet-host-debuginfo | 7.0.4-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.15-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.4-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.15-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.15-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.4-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.4-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.115-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.104-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.15-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.4-1.el9_1 | |
dotnet-templates-6.0 | 6.0.115-1.el9_1 | |
dotnet-templates-7.0 | 7.0.104-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.115-1.el9_1 | |
dotnet6.0-debugsource | 6.0.115-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.104-1.el9_1 | |
dotnet7.0-debugsource | 7.0.104-1.el9_1 | |
firefox | 102.9.0-3.el9_1 | |
firefox-debuginfo | 102.9.0-3.el9_1 | |
firefox-debugsource | 102.9.0-3.el9_1 | |
firefox-x11 | 102.9.0-3.el9_1 | |
gnutls-c++ | 3.7.6-18.el9_1 | |
gnutls-c++-debuginfo | 3.7.6-18.el9_1 | |
gnutls-dane | 3.7.6-18.el9_1 | |
gnutls-dane-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debuginfo | 3.7.6-18.el9_1 | |
gnutls-debugsource | 3.7.6-18.el9_1 | |
gnutls-devel | 3.7.6-18.el9_1 | |
gnutls-utils | 3.7.6-18.el9_1 | |
gnutls-utils-debuginfo | 3.7.6-18.el9_1 | |
kernel-debug-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debug-devel | 5.14.0-162.22.2.el9_1 | |
kernel-debug-devel-matched | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo | 5.14.0-162.22.2.el9_1 | |
kernel-debuginfo-common-aarch64 | 5.14.0-162.22.2.el9_1 | |
kernel-devel | 5.14.0-162.22.2.el9_1 | |
kernel-devel-matched | 5.14.0-162.22.2.el9_1 | |
kernel-doc | 5.14.0-162.22.2.el9_1 | |
kernel-headers | 5.14.0-162.22.2.el9_1 | |
kernel-tools-debuginfo | 5.14.0-162.22.2.el9_1 | |
libjpeg-turbo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debuginfo | 2.0.90-6.el9_1 | |
libjpeg-turbo-debugsource | 2.0.90-6.el9_1 | |
libjpeg-turbo-devel | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils | 2.0.90-6.el9_1 | |
libjpeg-turbo-utils-debuginfo | 2.0.90-6.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.104-1.el9_1 | |
nspr | 4.34.0-17.el9_1 | |
nspr-debuginfo | 4.34.0-17.el9_1 | |
nspr-devel | 4.34.0-17.el9_1 | |
nss | 3.79.0-17.el9_1 | |
nss-debuginfo | 3.79.0-17.el9_1 | |
nss-debugsource | 3.79.0-17.el9_1 | |
nss-devel | 3.79.0-17.el9_1 | |
nss-softokn | 3.79.0-17.el9_1 | |
nss-softokn-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-devel | 3.79.0-17.el9_1 | |
nss-softokn-freebl | 3.79.0-17.el9_1 | |
nss-softokn-freebl-debuginfo | 3.79.0-17.el9_1 | |
nss-softokn-freebl-devel | 3.79.0-17.el9_1 | |
nss-sysinit | 3.79.0-17.el9_1 | |
nss-sysinit-debuginfo | 3.79.0-17.el9_1 | |
nss-tools | 3.79.0-17.el9_1 | |
nss-tools-debuginfo | 3.79.0-17.el9_1 | |
nss-util | 3.79.0-17.el9_1 | |
nss-util-debuginfo | 3.79.0-17.el9_1 | |
nss-util-devel | 3.79.0-17.el9_1 | |
perf | 5.14.0-162.22.2.el9_1 | |
perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
pesign | 115-6.el9_1 | |
pesign-debuginfo | 115-6.el9_1 | |
pesign-debugsource | 115-6.el9_1 | |
python3-perf-debuginfo | 5.14.0-162.22.2.el9_1 | |
thunderbird | 102.9.0-1.el9_1 | |
thunderbird-debuginfo | 102.9.0-1.el9_1 | |
thunderbird-debugsource | 102.9.0-1.el9_1 | |
turbojpeg-debuginfo | 2.0.90-6.el9_1 | |
tzdata-java | 2023b-1.el9 | |

