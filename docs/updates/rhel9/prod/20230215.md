## 2023-02-15

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.3.1-1.rh9.cern | |
hepix | 4.10.4-0.rh9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.rh9.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba | 1.5.1-6.el9_1 | |
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.31.1-3.el9_1 | |
git-all | 2.31.1-3.el9_1 | |
git-core | 2.31.1-3.el9_1 | |
git-core-debuginfo | 2.31.1-3.el9_1 | |
git-core-doc | 2.31.1-3.el9_1 | |
git-credential-libsecret | 2.31.1-3.el9_1 | |
git-credential-libsecret-debuginfo | 2.31.1-3.el9_1 | |
git-daemon | 2.31.1-3.el9_1 | |
git-daemon-debuginfo | 2.31.1-3.el9_1 | |
git-debuginfo | 2.31.1-3.el9_1 | |
git-debugsource | 2.31.1-3.el9_1 | |
git-email | 2.31.1-3.el9_1 | |
git-gui | 2.31.1-3.el9_1 | |
git-instaweb | 2.31.1-3.el9_1 | |
git-subtree | 2.31.1-3.el9_1 | |
git-svn | 2.31.1-3.el9_1 | |
gitk | 2.31.1-3.el9_1 | |
gitweb | 2.31.1-3.el9_1 | |
perl-Git | 2.31.1-3.el9_1 | |
perl-Git-SVN | 2.31.1-3.el9_1 | |
thunderbird | 102.7.1-2.el9_1 | |
thunderbird-debuginfo | 102.7.1-2.el9_1 | |
thunderbird-debugsource | 102.7.1-2.el9_1 | |
tigervnc | 1.12.0-5.el9_1.1 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-debugsource | 1.12.0-5.el9_1.1 | |
tigervnc-icons | 1.12.0-5.el9_1.1 | |
tigervnc-license | 1.12.0-5.el9_1.1 | |
tigervnc-selinux | 1.12.0-5.el9_1.1 | |
tigervnc-server | 1.12.0-5.el9_1.1 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-module | 1.12.0-5.el9_1.1 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |
libksba-devel | 1.5.1-6.el9_1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.3.1-1.rh9.cern | |
hepix | 4.10.4-0.rh9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.rh9.cern | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba | 1.5.1-6.el9_1 | |
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
git | 2.31.1-3.el9_1 | |
git-all | 2.31.1-3.el9_1 | |
git-core | 2.31.1-3.el9_1 | |
git-core-debuginfo | 2.31.1-3.el9_1 | |
git-core-doc | 2.31.1-3.el9_1 | |
git-credential-libsecret | 2.31.1-3.el9_1 | |
git-credential-libsecret-debuginfo | 2.31.1-3.el9_1 | |
git-daemon | 2.31.1-3.el9_1 | |
git-daemon-debuginfo | 2.31.1-3.el9_1 | |
git-debuginfo | 2.31.1-3.el9_1 | |
git-debugsource | 2.31.1-3.el9_1 | |
git-email | 2.31.1-3.el9_1 | |
git-gui | 2.31.1-3.el9_1 | |
git-instaweb | 2.31.1-3.el9_1 | |
git-subtree | 2.31.1-3.el9_1 | |
git-svn | 2.31.1-3.el9_1 | |
gitk | 2.31.1-3.el9_1 | |
gitweb | 2.31.1-3.el9_1 | |
perl-Git | 2.31.1-3.el9_1 | |
perl-Git-SVN | 2.31.1-3.el9_1 | |
thunderbird | 102.7.1-2.el9_1 | |
thunderbird-debuginfo | 102.7.1-2.el9_1 | |
thunderbird-debugsource | 102.7.1-2.el9_1 | |
tigervnc | 1.12.0-5.el9_1.1 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-debugsource | 1.12.0-5.el9_1.1 | |
tigervnc-icons | 1.12.0-5.el9_1.1 | |
tigervnc-license | 1.12.0-5.el9_1.1 | |
tigervnc-selinux | 1.12.0-5.el9_1.1 | |
tigervnc-server | 1.12.0-5.el9_1.1 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-module | 1.12.0-5.el9_1.1 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |
libksba-devel | 1.5.1-6.el9_1 | |

