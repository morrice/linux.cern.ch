## 2023-02-07

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.el9.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-glance | 24.2.0-1.el9s | |
openstack-glance | 25.1.0-1.el9s | |
openstack-glance-doc | 24.2.0-1.el9s | |
openstack-glance-doc | 25.1.0-1.el9s | |
openstack-swift-account | 2.29.2-1.el9s | |
openstack-swift-account | 2.30.1-1.el9s | |
openstack-swift-container | 2.29.2-1.el9s | |
openstack-swift-container | 2.30.1-1.el9s | |
openstack-swift-doc | 2.29.2-1.el9s | |
openstack-swift-doc | 2.30.1-1.el9s | |
openstack-swift-object | 2.29.2-1.el9s | |
openstack-swift-object | 2.30.1-1.el9s | |
openstack-swift-proxy | 2.29.2-1.el9s | |
openstack-swift-proxy | 2.30.1-1.el9s | |
python3-glance | 24.2.0-1.el9s | |
python3-glance | 25.1.0-1.el9s | |
python3-glance-tests | 24.2.0-1.el9s | |
python3-glance-tests | 25.1.0-1.el9s | |
python3-swift | 2.29.2-1.el9s | |
python3-swift | 2.30.1-1.el9s | |
python3-swift-tests | 2.29.2-1.el9s | |
python3-swift-tests | 2.30.1-1.el9s | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.el9.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-glance | 24.2.0-1.el9s | |
openstack-glance | 25.1.0-1.el9s | |
openstack-glance-doc | 24.2.0-1.el9s | |
openstack-glance-doc | 25.1.0-1.el9s | |
openstack-swift-account | 2.29.2-1.el9s | |
openstack-swift-account | 2.30.1-1.el9s | |
openstack-swift-container | 2.29.2-1.el9s | |
openstack-swift-container | 2.30.1-1.el9s | |
openstack-swift-doc | 2.29.2-1.el9s | |
openstack-swift-doc | 2.30.1-1.el9s | |
openstack-swift-object | 2.29.2-1.el9s | |
openstack-swift-object | 2.30.1-1.el9s | |
openstack-swift-proxy | 2.29.2-1.el9s | |
openstack-swift-proxy | 2.30.1-1.el9s | |
python3-glance | 24.2.0-1.el9s | |
python3-glance | 25.1.0-1.el9s | |
python3-glance-tests | 24.2.0-1.el9s | |
python3-glance-tests | 25.1.0-1.el9s | |
python3-swift | 2.29.2-1.el9s | |
python3-swift | 2.30.1-1.el9s | |
python3-swift-tests | 2.29.2-1.el9s | |
python3-swift-tests | 2.30.1-1.el9s | |

