## 2021-10-14

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
msktutil | 1.1-2.el9.cern | |
msktutil-debugsource | 1.1-2.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
msktutil | 1.1-2.el9.cern | |
msktutil-debugsource | 1.1-2.el9.cern | |

