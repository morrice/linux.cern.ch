## 2023-04-28

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-nfs-ganesha5 | 1.0-1.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-nfs-ganesha5 | 1.0-1.el9s | |

