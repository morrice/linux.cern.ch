## 2022-03-02

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-gluster10 | 1.0-2.el9s | |
centos-release-gluster9 | 1.0-2.el9s | |
centos-release-virt-common | 1-3.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-gluster10 | 1.0-2.el9s | |
centos-release-gluster9 | 1.0-2.el9s | |
centos-release-virt-common | 1-3.el9s | |

