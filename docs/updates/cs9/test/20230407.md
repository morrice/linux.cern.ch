## 2023-04-07

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-2.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-openstack-yoga | 1-2.el9s | |

