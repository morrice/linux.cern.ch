## 2022-10-14

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 0.7-1.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 0.7-1.el9.cern | |

