## 2022-01-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-IO-SessionData | 1.03-16.el9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.8-1.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_30.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_34.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_39.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_41.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_43.el9.el9.cern | |
openafs | 1.8.8-1.el9.cern | |
openafs-authlibs | 1.8.8-1.el9.cern | |
openafs-authlibs-devel | 1.8.8-1.el9.cern | |
openafs-client | 1.8.8-1.el9.cern | |
openafs-compat | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_30.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_34.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_39.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_41.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_43.el9-1.el9.cern | |
openafs-devel | 1.8.8-1.el9.cern | |
openafs-docs | 1.8.8-1.el9.cern | |
openafs-kernel-source | 1.8.8-1.el9.cern | |
openafs-krb5 | 1.8.8-1.el9.cern | |
openafs-server | 1.8.8-1.el9.cern | |
pam_afs_session | 2.6-3.el9.cern | |
pam_afs_session-debugsource | 2.6-3.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-43.el9 | |
centos-gpg-keys | 9.0-8.el9 | |
centos-stream-release | 9.0-8.el9 | |
centos-stream-repos | 9.0-8.el9 | |
fuse | 2.9.9-15.el9 | |
fuse-libs | 2.9.9-15.el9 | |
kernel | 5.14.0-43.el9 | |
kernel-abi-stablelists | 5.14.0-43.el9 | |
kernel-core | 5.14.0-43.el9 | |
kernel-debug | 5.14.0-43.el9 | |
kernel-debug-core | 5.14.0-43.el9 | |
kernel-debug-modules | 5.14.0-43.el9 | |
kernel-debug-modules-extra | 5.14.0-43.el9 | |
kernel-modules | 5.14.0-43.el9 | |
kernel-modules-extra | 5.14.0-43.el9 | |
kernel-tools | 5.14.0-43.el9 | |
kernel-tools-libs | 5.14.0-43.el9 | |
libnfsidmap | 2.5.4-8.el9 | |
mdadm | 4.2-1.el9 | |
NetworkManager | 1.36.0-0.4.el9 | |
NetworkManager-adsl | 1.36.0-0.4.el9 | |
NetworkManager-bluetooth | 1.36.0-0.4.el9 | |
NetworkManager-config-server | 1.36.0-0.4.el9 | |
NetworkManager-libnm | 1.36.0-0.4.el9 | |
NetworkManager-team | 1.36.0-0.4.el9 | |
NetworkManager-tui | 1.36.0-0.4.el9 | |
NetworkManager-wifi | 1.36.0-0.4.el9 | |
NetworkManager-wwan | 1.36.0-0.4.el9 | |
nfs-utils | 2.5.4-8.el9 | |
python3 | 3.9.9-4.el9 | |
python3-ethtool | 0.15-2.el9 | |
python3-libs | 3.9.9-4.el9 | |
python3-perf | 5.14.0-43.el9 | |
tuned | 2.17.0-1.el9 | |
tuned-profiles-cpu-partitioning | 2.17.0-1.el9 | |
vim-filesystem | 8.2.2637-10.el9 | |
vim-minimal | 8.2.2637-10.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.47-1.el9 | |
annobin-annocheck | 10.47-1.el9 | |
cargo | 1.58.0-1.el9 | |
cargo-doc | 1.58.0-1.el9 | |
clippy | 1.58.0-1.el9 | |
crun | 1.4.1-1.el9 | |
edk2-ovmf | 20210527gite1999b264f1f-8.el9 | |
gnome-connections | 41.2-1.el9 | |
gnome-shell-extension-classification-banner | 40.5-4.el9 | |
kdump-anaconda-addon | 006-11.20211014git4c5a91d.el9 | |
kernel-debug-devel | 5.14.0-43.el9 | |
kernel-debug-devel-matched | 5.14.0-43.el9 | |
kernel-devel | 5.14.0-43.el9 | |
kernel-devel-matched | 5.14.0-43.el9 | |
kernel-doc | 5.14.0-43.el9 | |
kernel-headers | 5.14.0-43.el9 | |
libvirt | 8.0.0-1.el9 | |
libvirt-client | 8.0.0-1.el9 | |
libvirt-daemon | 8.0.0-1.el9 | |
libvirt-daemon-config-network | 8.0.0-1.el9 | |
libvirt-daemon-config-nwfilter | 8.0.0-1.el9 | |
libvirt-daemon-driver-interface | 8.0.0-1.el9 | |
libvirt-daemon-driver-network | 8.0.0-1.el9 | |
libvirt-daemon-driver-nodedev | 8.0.0-1.el9 | |
libvirt-daemon-driver-nwfilter | 8.0.0-1.el9 | |
libvirt-daemon-driver-qemu | 8.0.0-1.el9 | |
libvirt-daemon-driver-secret | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-core | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-disk | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-logical | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-mpath | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-rbd | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-scsi | 8.0.0-1.el9 | |
libvirt-daemon-kvm | 8.0.0-1.el9 | |
libvirt-libs | 8.0.0-1.el9 | |
libvirt-nss | 8.0.0-1.el9 | |
lorax-templates-rhel | 9.0-31.el9 | |
mariadb | 10.5.13-1.el9 | |
mariadb-backup | 10.5.13-1.el9 | |
mariadb-common | 10.5.13-1.el9 | |
mariadb-embedded | 10.5.13-1.el9 | |
mariadb-errmsg | 10.5.13-1.el9 | |
mariadb-gssapi-server | 10.5.13-1.el9 | |
mariadb-oqgraph-engine | 10.5.13-1.el9 | |
mariadb-pam | 10.5.13-1.el9 | |
mariadb-server | 10.5.13-1.el9 | |
mariadb-server-galera | 10.5.13-1.el9 | |
mariadb-server-utils | 10.5.13-1.el9 | |
mutter | 40.8-1.el9 | |
NetworkManager-cloud-setup | 1.36.0-0.4.el9 | |
NetworkManager-config-connectivity-redhat | 1.36.0-0.4.el9 | |
NetworkManager-dispatcher-routing-rules | 1.36.0-0.4.el9 | |
NetworkManager-ovs | 1.36.0-0.4.el9 | |
NetworkManager-ppp | 1.36.0-0.4.el9 | |
nfs-utils-coreos | 2.5.4-8.el9 | |
nispor | 1.2.3-1.el9 | |
nmstate | 2.0.0-0.7.alpha6.el9 | |
nmstate-libs | 2.0.0-0.7.alpha6.el9 | |
nspr | 4.32.0-7.el9 | |
nspr-devel | 4.32.0-7.el9 | |
nss | 3.71.0-5.el9 | |
nss-devel | 3.71.0-5.el9 | |
nss-softokn | 3.71.0-5.el9 | |
nss-softokn-devel | 3.71.0-5.el9 | |
nss-softokn-freebl | 3.71.0-5.el9 | |
nss-softokn-freebl-devel | 3.71.0-5.el9 | |
nss-sysinit | 3.71.0-5.el9 | |
nss-tools | 3.71.0-5.el9 | |
nss-util | 3.71.0-5.el9 | |
nss-util-devel | 3.71.0-5.el9 | |
perf | 5.14.0-43.el9 | |
python-unversioned-command | 3.9.9-4.el9 | |
python3-devel | 3.9.9-4.el9 | |
python3-libnmstate | 2.0.0-0.7.alpha6.el9 | |
python3-nispor | 1.2.3-1.el9 | |
python3-tkinter | 3.9.9-4.el9 | |
realtime-tests | 2.3-1.el9 | |
rls | 1.58.0-1.el9 | |
rpmlint | 1.11-19.el9 | |
rust | 1.58.0-1.el9 | |
rust-analysis | 1.58.0-1.el9 | |
rust-debugger-common | 1.58.0-1.el9 | |
rust-doc | 1.58.0-1.el9 | |
rust-gdb | 1.58.0-1.el9 | |
rust-lldb | 1.58.0-1.el9 | |
rust-src | 1.58.0-1.el9 | |
rust-std-static | 1.58.0-1.el9 | |
rust-std-static-wasm32-unknown-unknown | 1.58.0-1.el9 | |
rust-std-static-wasm32-wasi | 1.58.0-1.el9 | |
rust-toolset | 1.58.0-1.el9 | |
rustfmt | 1.58.0-1.el9 | |
SDL2 | 2.0.20-1.el9 | |
SDL2-devel | 2.0.20-1.el9 | |
spamassassin | 3.4.6-5.el9 | |
thunderbird | 91.5.0-2.el9 | |
tuned-gtk | 2.17.0-1.el9 | |
tuned-profiles-atomic | 2.17.0-1.el9 | |
tuned-profiles-mssql | 2.17.0-1.el9 | |
tuned-profiles-oracle | 2.17.0-1.el9 | |
tuned-profiles-spectrumscale | 2.17.0-1.el9 | |
tuned-utils | 2.17.0-1.el9 | |
valgrind | 3.18.1-8.el9 | |
valgrind-devel | 3.18.1-8.el9 | |
vim-common | 8.2.2637-10.el9 | |
vim-enhanced | 8.2.2637-10.el9 | |
vim-X11 | 8.2.2637-10.el9 | |
xorg-x11-server-Xwayland | 21.1.3-2.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-42.rt21.42.el9 | |
kernel-rt-core | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-core | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-devel | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-modules | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-42.rt21.42.el9 | |
kernel-rt-devel | 5.14.0-42.rt21.42.el9 | |
kernel-rt-modules | 5.14.0-42.rt21.42.el9 | |
kernel-rt-modules-extra | 5.14.0-42.rt21.42.el9 | |
tuned-profiles-realtime | 2.17.0-1.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fuse-devel | 2.9.9-15.el9 | |
kernel-cross-headers | 5.14.0-43.el9 | |
kernel-tools-libs-devel | 5.14.0-43.el9 | |
libmemcached-awesome-tools | 1.1.0-12.el9 | |
libnfsidmap-devel | 2.5.4-8.el9 | |
libvirt-devel | 8.0.0-1.el9 | |
libvirt-docs | 8.0.0-1.el9 | |
libvirt-lock-sanlock | 8.0.0-1.el9 | |
libzip-devel | 1.7.3-7.el9 | |
mariadb-devel | 10.5.13-1.el9 | |
mariadb-embedded-devel | 10.5.13-1.el9 | |
mariadb-test | 10.5.13-1.el9 | |
mutter-devel | 40.8-1.el9 | |
NetworkManager-libnm-devel | 1.36.0-0.4.el9 | |
nispor-devel | 1.2.3-1.el9 | |
nmstate-devel | 2.0.0-0.7.alpha6.el9 | |
python3-debug | 3.9.9-4.el9 | |
python3-idle | 3.9.9-4.el9 | |
python3-setuptools_scm+toml | 6.0.1-1.el9 | |
python3-setuptools_scm | 6.0.1-1.el9 | |
python3-test | 3.9.9-4.el9 | |
SDL2-static | 2.0.20-1.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-42.rt21.42.el9 | |
kernel-rt-core | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-core | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-devel | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-kvm | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-modules | 5.14.0-42.rt21.42.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-42.rt21.42.el9 | |
kernel-rt-devel | 5.14.0-42.rt21.42.el9 | |
kernel-rt-kvm | 5.14.0-42.rt21.42.el9 | |
kernel-rt-modules | 5.14.0-42.rt21.42.el9 | |
kernel-rt-modules-extra | 5.14.0-42.rt21.42.el9 | |
tuned-profiles-nfv | 2.17.0-1.el9 | |
tuned-profiles-nfv-guest | 2.17.0-1.el9 | |
tuned-profiles-nfv-host | 2.17.0-1.el9 | |
tuned-profiles-realtime | 2.17.0-1.el9 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-IO-SessionData | 1.03-16.el9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.8-1.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_30.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_34.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_39.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_41.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_43.el9.el9.cern | |
openafs | 1.8.8-1.el9.cern | |
openafs-authlibs | 1.8.8-1.el9.cern | |
openafs-authlibs-devel | 1.8.8-1.el9.cern | |
openafs-client | 1.8.8-1.el9.cern | |
openafs-compat | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_30.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_34.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_39.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_41.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_43.el9-1.el9.cern | |
openafs-devel | 1.8.8-1.el9.cern | |
openafs-docs | 1.8.8-1.el9.cern | |
openafs-kernel-source | 1.8.8-1.el9.cern | |
openafs-krb5 | 1.8.8-1.el9.cern | |
openafs-server | 1.8.8-1.el9.cern | |
pam_afs_session | 2.6-3.el9.cern | |
pam_afs_session-debugsource | 2.6-3.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-43.el9 | |
centos-gpg-keys | 9.0-8.el9 | |
centos-stream-release | 9.0-8.el9 | |
centos-stream-repos | 9.0-8.el9 | |
fuse | 2.9.9-15.el9 | |
fuse-libs | 2.9.9-15.el9 | |
kernel | 5.14.0-43.el9 | |
kernel-abi-stablelists | 5.14.0-43.el9 | |
kernel-core | 5.14.0-43.el9 | |
kernel-debug | 5.14.0-43.el9 | |
kernel-debug-core | 5.14.0-43.el9 | |
kernel-debug-modules | 5.14.0-43.el9 | |
kernel-debug-modules-extra | 5.14.0-43.el9 | |
kernel-modules | 5.14.0-43.el9 | |
kernel-modules-extra | 5.14.0-43.el9 | |
kernel-tools | 5.14.0-43.el9 | |
kernel-tools-libs | 5.14.0-43.el9 | |
libnfsidmap | 2.5.4-8.el9 | |
mdadm | 4.2-1.el9 | |
NetworkManager | 1.36.0-0.4.el9 | |
NetworkManager-adsl | 1.36.0-0.4.el9 | |
NetworkManager-bluetooth | 1.36.0-0.4.el9 | |
NetworkManager-config-server | 1.36.0-0.4.el9 | |
NetworkManager-libnm | 1.36.0-0.4.el9 | |
NetworkManager-team | 1.36.0-0.4.el9 | |
NetworkManager-tui | 1.36.0-0.4.el9 | |
NetworkManager-wifi | 1.36.0-0.4.el9 | |
NetworkManager-wwan | 1.36.0-0.4.el9 | |
nfs-utils | 2.5.4-8.el9 | |
python3 | 3.9.9-4.el9 | |
python3-ethtool | 0.15-2.el9 | |
python3-libs | 3.9.9-4.el9 | |
python3-perf | 5.14.0-43.el9 | |
tuned | 2.17.0-1.el9 | |
tuned-profiles-cpu-partitioning | 2.17.0-1.el9 | |
vim-filesystem | 8.2.2637-10.el9 | |
vim-minimal | 8.2.2637-10.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.47-1.el9 | |
annobin-annocheck | 10.47-1.el9 | |
cargo | 1.58.0-1.el9 | |
cargo-doc | 1.58.0-1.el9 | |
clippy | 1.58.0-1.el9 | |
crun | 1.4.1-1.el9 | |
edk2-aarch64 | 20210527gite1999b264f1f-8.el9 | |
gnome-connections | 41.2-1.el9 | |
gnome-shell-extension-classification-banner | 40.5-4.el9 | |
kdump-anaconda-addon | 006-11.20211014git4c5a91d.el9 | |
kernel-debug-devel | 5.14.0-43.el9 | |
kernel-debug-devel-matched | 5.14.0-43.el9 | |
kernel-devel | 5.14.0-43.el9 | |
kernel-devel-matched | 5.14.0-43.el9 | |
kernel-doc | 5.14.0-43.el9 | |
kernel-headers | 5.14.0-43.el9 | |
libvirt | 8.0.0-1.el9 | |
libvirt-client | 8.0.0-1.el9 | |
libvirt-daemon | 8.0.0-1.el9 | |
libvirt-daemon-config-network | 8.0.0-1.el9 | |
libvirt-daemon-config-nwfilter | 8.0.0-1.el9 | |
libvirt-daemon-driver-interface | 8.0.0-1.el9 | |
libvirt-daemon-driver-network | 8.0.0-1.el9 | |
libvirt-daemon-driver-nodedev | 8.0.0-1.el9 | |
libvirt-daemon-driver-nwfilter | 8.0.0-1.el9 | |
libvirt-daemon-driver-qemu | 8.0.0-1.el9 | |
libvirt-daemon-driver-secret | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-core | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-disk | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-logical | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-mpath | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-rbd | 8.0.0-1.el9 | |
libvirt-daemon-driver-storage-scsi | 8.0.0-1.el9 | |
libvirt-daemon-kvm | 8.0.0-1.el9 | |
libvirt-libs | 8.0.0-1.el9 | |
libvirt-nss | 8.0.0-1.el9 | |
libxdp | 1.1.1-2.el9 | |
lorax-templates-rhel | 9.0-31.el9 | |
mariadb | 10.5.13-1.el9 | |
mariadb-backup | 10.5.13-1.el9 | |
mariadb-common | 10.5.13-1.el9 | |
mariadb-embedded | 10.5.13-1.el9 | |
mariadb-errmsg | 10.5.13-1.el9 | |
mariadb-gssapi-server | 10.5.13-1.el9 | |
mariadb-oqgraph-engine | 10.5.13-1.el9 | |
mariadb-pam | 10.5.13-1.el9 | |
mariadb-server | 10.5.13-1.el9 | |
mariadb-server-galera | 10.5.13-1.el9 | |
mariadb-server-utils | 10.5.13-1.el9 | |
mutter | 40.8-1.el9 | |
NetworkManager-cloud-setup | 1.36.0-0.4.el9 | |
NetworkManager-config-connectivity-redhat | 1.36.0-0.4.el9 | |
NetworkManager-dispatcher-routing-rules | 1.36.0-0.4.el9 | |
NetworkManager-ovs | 1.36.0-0.4.el9 | |
NetworkManager-ppp | 1.36.0-0.4.el9 | |
nfs-utils-coreos | 2.5.4-8.el9 | |
nispor | 1.2.3-1.el9 | |
nmstate | 2.0.0-0.7.alpha6.el9 | |
nmstate-libs | 2.0.0-0.7.alpha6.el9 | |
nspr | 4.32.0-7.el9 | |
nspr-devel | 4.32.0-7.el9 | |
nss | 3.71.0-5.el9 | |
nss-devel | 3.71.0-5.el9 | |
nss-softokn | 3.71.0-5.el9 | |
nss-softokn-devel | 3.71.0-5.el9 | |
nss-softokn-freebl | 3.71.0-5.el9 | |
nss-softokn-freebl-devel | 3.71.0-5.el9 | |
nss-sysinit | 3.71.0-5.el9 | |
nss-tools | 3.71.0-5.el9 | |
nss-util | 3.71.0-5.el9 | |
nss-util-devel | 3.71.0-5.el9 | |
perf | 5.14.0-43.el9 | |
python-unversioned-command | 3.9.9-4.el9 | |
python3-devel | 3.9.9-4.el9 | |
python3-libnmstate | 2.0.0-0.7.alpha6.el9 | |
python3-nispor | 1.2.3-1.el9 | |
python3-tkinter | 3.9.9-4.el9 | |
rls | 1.58.0-1.el9 | |
rpmlint | 1.11-19.el9 | |
rust | 1.58.0-1.el9 | |
rust-analysis | 1.58.0-1.el9 | |
rust-debugger-common | 1.58.0-1.el9 | |
rust-doc | 1.58.0-1.el9 | |
rust-gdb | 1.58.0-1.el9 | |
rust-lldb | 1.58.0-1.el9 | |
rust-src | 1.58.0-1.el9 | |
rust-std-static | 1.58.0-1.el9 | |
rust-std-static-wasm32-unknown-unknown | 1.58.0-1.el9 | |
rust-std-static-wasm32-wasi | 1.58.0-1.el9 | |
rust-toolset | 1.58.0-1.el9 | |
rustfmt | 1.58.0-1.el9 | |
SDL2 | 2.0.20-1.el9 | |
SDL2-devel | 2.0.20-1.el9 | |
spamassassin | 3.4.6-5.el9 | |
thunderbird | 91.5.0-2.el9 | |
tuned-gtk | 2.17.0-1.el9 | |
tuned-profiles-atomic | 2.17.0-1.el9 | |
tuned-profiles-mssql | 2.17.0-1.el9 | |
tuned-profiles-oracle | 2.17.0-1.el9 | |
tuned-profiles-spectrumscale | 2.17.0-1.el9 | |
tuned-utils | 2.17.0-1.el9 | |
valgrind | 3.18.1-8.el9 | |
valgrind-devel | 3.18.1-8.el9 | |
vim-common | 8.2.2637-10.el9 | |
vim-enhanced | 8.2.2637-10.el9 | |
vim-X11 | 8.2.2637-10.el9 | |
xdp-tools | 1.1.1-2.el9 | |
xorg-x11-server-Xwayland | 21.1.3-2.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fuse-devel | 2.9.9-15.el9 | |
kernel-cross-headers | 5.14.0-43.el9 | |
kernel-tools-libs-devel | 5.14.0-43.el9 | |
libmemcached-awesome-tools | 1.1.0-12.el9 | |
libnfsidmap-devel | 2.5.4-8.el9 | |
libvirt-devel | 8.0.0-1.el9 | |
libvirt-docs | 8.0.0-1.el9 | |
libvirt-lock-sanlock | 8.0.0-1.el9 | |
libzip-devel | 1.7.3-7.el9 | |
mariadb-devel | 10.5.13-1.el9 | |
mariadb-embedded-devel | 10.5.13-1.el9 | |
mariadb-test | 10.5.13-1.el9 | |
mutter-devel | 40.8-1.el9 | |
NetworkManager-libnm-devel | 1.36.0-0.4.el9 | |
nispor-devel | 1.2.3-1.el9 | |
nmstate-devel | 2.0.0-0.7.alpha6.el9 | |
python3-debug | 3.9.9-4.el9 | |
python3-idle | 3.9.9-4.el9 | |
python3-setuptools_scm+toml | 6.0.1-1.el9 | |
python3-setuptools_scm | 6.0.1-1.el9 | |
python3-test | 3.9.9-4.el9 | |
SDL2-static | 2.0.20-1.el9 | |

