## 2021-12-10

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
yum-autoupdate | 4.6.1-2.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
yum-autoupdate | 4.6.1-2.el9.cern | |

