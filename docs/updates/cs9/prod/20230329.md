## 2023-03-29

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.el9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_289.el9.el9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_289.el9-2.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.0.0-289.el9 | |
bpftool-debuginfo | 7.0.0-289.el9 | |
device-mapper-multipath | 0.8.7-21.el9 | |
device-mapper-multipath-debuginfo | 0.8.7-21.el9 | |
device-mapper-multipath-debugsource | 0.8.7-21.el9 | |
device-mapper-multipath-libs | 0.8.7-21.el9 | |
device-mapper-multipath-libs-debuginfo | 0.8.7-21.el9 | |
gnutls | 3.7.6-20.el9 | |
gnutls-debuginfo | 3.7.6-20.el9 | |
gnutls-debugsource | 3.7.6-20.el9 | |
kernel | 5.14.0-289.el9 | |
kernel-abi-stablelists | 5.14.0-289.el9 | |
kernel-core | 5.14.0-289.el9 | |
kernel-debug | 5.14.0-289.el9 | |
kernel-debug-core | 5.14.0-289.el9 | |
kernel-debug-debuginfo | 5.14.0-289.el9 | |
kernel-debug-modules | 5.14.0-289.el9 | |
kernel-debug-modules-core | 5.14.0-289.el9 | |
kernel-debug-modules-extra | 5.14.0-289.el9 | |
kernel-debug-uki-virt | 5.14.0-289.el9 | |
kernel-debuginfo | 5.14.0-289.el9 | |
kernel-debuginfo-common-x86_64 | 5.14.0-289.el9 | |
kernel-modules | 5.14.0-289.el9 | |
kernel-modules-core | 5.14.0-289.el9 | |
kernel-modules-extra | 5.14.0-289.el9 | |
kernel-tools | 5.14.0-289.el9 | |
kernel-tools-debuginfo | 5.14.0-289.el9 | |
kernel-tools-libs | 5.14.0-289.el9 | |
kernel-uki-virt | 5.14.0-289.el9 | |
kexec-tools | 2.0.25-13.el9 | |
kexec-tools-debuginfo | 2.0.25-13.el9 | |
kexec-tools-debugsource | 2.0.25-13.el9 | |
kmod-kvdo | 8.2.1.6-75.el9 | |
kmod-kvdo-debuginfo | 8.2.1.6-75.el9 | |
kmod-kvdo-debugsource | 8.2.1.6-75.el9 | |
kpartx | 0.8.7-21.el9 | |
kpartx-debuginfo | 0.8.7-21.el9 | |
opensc | 0.23.0-1.el9 | |
opensc-debuginfo | 0.23.0-1.el9 | |
opensc-debugsource | 0.23.0-1.el9 | |
python3-perf | 5.14.0-289.el9 | |
python3-perf-debuginfo | 5.14.0-289.el9 | |
selinux-policy | 38.1.9-1.el9 | |
selinux-policy-doc | 38.1.9-1.el9 | |
selinux-policy-mls | 38.1.9-1.el9 | |
selinux-policy-sandbox | 38.1.9-1.el9 | |
selinux-policy-targeted | 38.1.9-1.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.29.1-1.el9 | |
buildah-debuginfo | 1.29.1-1.el9 | |
buildah-debugsource | 1.29.1-1.el9 | |
buildah-tests | 1.29.1-1.el9 | |
buildah-tests-debuginfo | 1.29.1-1.el9 | |
coreos-installer | 0.17.0-1.el9 | |
coreos-installer-bootinfra | 0.17.0-1.el9 | |
coreos-installer-bootinfra-debuginfo | 0.17.0-1.el9 | |
coreos-installer-debuginfo | 0.17.0-1.el9 | |
coreos-installer-dracut | 0.17.0-1.el9 | |
edk2-ovmf | 20221207gitfff6d81270b5-9.el9 | |
gnutls-c++ | 3.7.6-20.el9 | |
gnutls-c++-debuginfo | 3.7.6-20.el9 | |
gnutls-dane | 3.7.6-20.el9 | |
gnutls-dane-debuginfo | 3.7.6-20.el9 | |
gnutls-devel | 3.7.6-20.el9 | |
gnutls-utils | 3.7.6-20.el9 | |
gnutls-utils-debuginfo | 3.7.6-20.el9 | |
java-1.8.0-openjdk | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-src | 1.8.0.362.b09-4.el9 | |
kernel-debug-devel | 5.14.0-289.el9 | |
kernel-debug-devel-matched | 5.14.0-289.el9 | |
kernel-devel | 5.14.0-289.el9 | |
kernel-devel-matched | 5.14.0-289.el9 | |
kernel-doc | 5.14.0-289.el9 | |
kernel-headers | 5.14.0-289.el9 | |
libguestfs | 1.50.1-3.el9 | |
libguestfs-appliance | 1.50.1-3.el9 | |
libguestfs-bash-completion | 1.50.1-3.el9 | |
libguestfs-debuginfo | 1.50.1-3.el9 | |
libguestfs-debugsource | 1.50.1-3.el9 | |
libguestfs-inspect-icons | 1.50.1-3.el9 | |
libguestfs-rescue | 1.50.1-3.el9 | |
libguestfs-rescue-debuginfo | 1.50.1-3.el9 | |
libguestfs-rsync | 1.50.1-3.el9 | |
libguestfs-xfs | 1.50.1-3.el9 | |
libstoragemgmt | 1.9.7-1.el9 | |
libstoragemgmt-arcconf-plugin | 1.9.7-1.el9 | |
libstoragemgmt-debuginfo | 1.9.7-1.el9 | |
libstoragemgmt-debugsource | 1.9.7-1.el9 | |
libstoragemgmt-hpsa-plugin | 1.9.7-1.el9 | |
libstoragemgmt-local-plugin | 1.9.7-1.el9 | |
libstoragemgmt-megaraid-plugin | 1.9.7-1.el9 | |
libstoragemgmt-nfs-plugin | 1.9.7-1.el9 | |
libstoragemgmt-nfs-plugin-debuginfo | 1.9.7-1.el9 | |
libstoragemgmt-smis-plugin | 1.9.7-1.el9 | |
libstoragemgmt-targetd-plugin | 1.9.7-1.el9 | |
libstoragemgmt-udev | 1.9.7-1.el9 | |
libstoragemgmt-udev-debuginfo | 1.9.7-1.el9 | |
nmstate | 2.2.8-1.el9 | |
nmstate-debuginfo | 2.2.8-1.el9 | |
nmstate-debugsource | 2.2.8-1.el9 | |
nmstate-libs | 2.2.8-1.el9 | |
nmstate-libs-debuginfo | 2.2.8-1.el9 | |
nspr | 4.34.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nspr-debuginfo | 4.34.0-17.el9 | |
nspr-devel | 4.34.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-debuginfo | 3.79.0-17.el9 | |
nss-debugsource | 3.79.0-17.el9 | |
nss-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-debuginfo | 3.79.0-17.el9 | |
nss-softokn-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-debuginfo | 3.79.0-17.el9 | |
nss-softokn-freebl-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit-debuginfo | 3.79.0-17.el9 | |
nss-tools | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools-debuginfo | 3.79.0-17.el9 | |
nss-util | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-debuginfo | 3.79.0-17.el9 | |
nss-util-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
perf | 5.14.0-289.el9 | |
perf-debuginfo | 5.14.0-289.el9 | |
perl-Sys-Guestfs | 1.50.1-3.el9 | |
perl-Sys-Guestfs-debuginfo | 1.50.1-3.el9 | |
podman | 4.4.1-3.el9 | |
podman-debuginfo | 4.4.1-3.el9 | |
podman-debugsource | 4.4.1-3.el9 | |
podman-docker | 4.4.1-3.el9 | |
podman-gvproxy | 4.4.1-3.el9 | |
podman-gvproxy-debuginfo | 4.4.1-3.el9 | |
podman-plugins | 4.4.1-3.el9 | |
podman-plugins-debuginfo | 4.4.1-3.el9 | |
podman-remote | 4.4.1-3.el9 | |
podman-remote-debuginfo | 4.4.1-3.el9 | |
podman-tests | 4.4.1-3.el9 | |
python3-libguestfs | 1.50.1-3.el9 | |
python3-libguestfs-debuginfo | 1.50.1-3.el9 | |
python3-libnmstate | 2.2.8-1.el9 | |
python3-libstoragemgmt | 1.9.7-1.el9 | |
python3-libstoragemgmt-debuginfo | 1.9.7-1.el9 | |
python3-tomli | 2.0.1-5.el9 | |
python3.11-pip | 22.3.1-3.el9 | |
python3.11-pip-wheel | 22.3.1-3.el9 | |
rtla | 5.14.0-289.el9 | |
rust-coreos-installer-debuginfo | 0.17.0-1.el9 | |
rust-coreos-installer-debugsource | 0.17.0-1.el9 | |
selinux-policy-devel | 38.1.9-1.el9 | |
setroubleshoot | 3.3.31-2.el9 | |
setroubleshoot-debugsource | 3.3.31-2.el9 | |
setroubleshoot-server | 3.3.31-2.el9 | |
setroubleshoot-server-debuginfo | 3.3.31-2.el9 | |
skopeo | 1.11.2-0.1.el9 | |
skopeo-debuginfo | 1.11.2-0.1.el9 | |
skopeo-debugsource | 1.11.2-0.1.el9 | |
skopeo-tests | 1.11.2-0.1.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-289.el9 | |
kernel-rt-core | 5.14.0-289.el9 | |
kernel-rt-debug | 5.14.0-289.el9 | |
kernel-rt-debug-core | 5.14.0-289.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-289.el9 | |
kernel-rt-debug-devel | 5.14.0-289.el9 | |
kernel-rt-debug-modules | 5.14.0-289.el9 | |
kernel-rt-debug-modules-core | 5.14.0-289.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-289.el9 | |
kernel-rt-debuginfo | 5.14.0-289.el9 | |
kernel-rt-devel | 5.14.0-289.el9 | |
kernel-rt-modules | 5.14.0-289.el9 | |
kernel-rt-modules-core | 5.14.0-289.el9 | |
kernel-rt-modules-extra | 5.14.0-289.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath-devel | 0.8.7-21.el9 | |
edk2-aarch64 | 20221207gitfff6d81270b5-9.el9 | |
edk2-debugsource | 20221207gitfff6d81270b5-9.el9 | |
edk2-tools | 20221207gitfff6d81270b5-9.el9 | |
edk2-tools-debuginfo | 20221207gitfff6d81270b5-9.el9 | |
edk2-tools-doc | 20221207gitfff6d81270b5-9.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-src-slowdebug | 1.8.0.362.b09-4.el9 | |
kernel-cross-headers | 5.14.0-289.el9 | |
kernel-tools-libs-devel | 5.14.0-289.el9 | |
libguestfs-devel | 1.50.1-3.el9 | |
libguestfs-gobject | 1.50.1-3.el9 | |
libguestfs-gobject-debuginfo | 1.50.1-3.el9 | |
libguestfs-gobject-devel | 1.50.1-3.el9 | |
libguestfs-man-pages-ja | 1.50.1-3.el9 | |
libguestfs-man-pages-uk | 1.50.1-3.el9 | |
libstoragemgmt-devel | 1.9.7-1.el9 | |
lua-guestfs | 1.50.1-3.el9 | |
lua-guestfs-debuginfo | 1.50.1-3.el9 | |
nmstate-devel | 2.2.8-1.el9 | |
nmstate-static | 2.2.8-1.el9 | |
ocaml-libguestfs | 1.50.1-3.el9 | |
ocaml-libguestfs-debuginfo | 1.50.1-3.el9 | |
ocaml-libguestfs-devel | 1.50.1-3.el9 | |
php-libguestfs | 1.50.1-3.el9 | |
php-libguestfs-debuginfo | 1.50.1-3.el9 | |
ruby-libguestfs | 1.50.1-3.el9 | |
ruby-libguestfs-debuginfo | 1.50.1-3.el9 | |
systemd-boot-unsigned | 252-8.el9 | |
systemd-boot-unsigned-debuginfo | 252-8.el9 | |
WALinuxAgent-cvm | 2.7.0.6-9.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-289.el9 | |
kernel-rt-core | 5.14.0-289.el9 | |
kernel-rt-debug | 5.14.0-289.el9 | |
kernel-rt-debug-core | 5.14.0-289.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-289.el9 | |
kernel-rt-debug-devel | 5.14.0-289.el9 | |
kernel-rt-debug-kvm | 5.14.0-289.el9 | |
kernel-rt-debug-modules | 5.14.0-289.el9 | |
kernel-rt-debug-modules-core | 5.14.0-289.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-289.el9 | |
kernel-rt-debuginfo | 5.14.0-289.el9 | |
kernel-rt-devel | 5.14.0-289.el9 | |
kernel-rt-kvm | 5.14.0-289.el9 | |
kernel-rt-modules | 5.14.0-289.el9 | |
kernel-rt-modules-core | 5.14.0-289.el9 | |
kernel-rt-modules-extra | 5.14.0-289.el9 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 20.3.0-1.el9s | |
openstack-neutron | 21.1.0-1.el9s | |
openstack-neutron-bgp-dragent | 20.0.1-1.el9s | |
openstack-neutron-common | 20.3.0-1.el9s | |
openstack-neutron-common | 21.1.0-1.el9s | |
openstack-neutron-dynamic-routing-common | 20.0.1-1.el9s | |
openstack-neutron-linuxbridge | 20.3.0-1.el9s | |
openstack-neutron-linuxbridge | 21.1.0-1.el9s | |
openstack-neutron-macvtap-agent | 20.3.0-1.el9s | |
openstack-neutron-macvtap-agent | 21.1.0-1.el9s | |
openstack-neutron-metering-agent | 20.3.0-1.el9s | |
openstack-neutron-metering-agent | 21.1.0-1.el9s | |
openstack-neutron-ml2 | 20.3.0-1.el9s | |
openstack-neutron-ml2 | 21.1.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 20.3.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 21.1.0-1.el9s | |
openstack-neutron-openvswitch | 20.3.0-1.el9s | |
openstack-neutron-openvswitch | 21.1.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 20.3.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 21.1.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 20.3.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 21.1.0-1.el9s | |
openstack-neutron-rpc-server | 20.3.0-1.el9s | |
openstack-neutron-rpc-server | 21.1.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 20.3.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 21.1.0-1.el9s | |
python-neutron-lib-doc | 2.20.1-1.el9s | |
python-neutron-lib-doc | 3.1.1-1.el9s | |
python-ovsdbapp-doc | 1.15.3-1.el9s | |
python-ovsdbapp-doc | 2.1.1-1.el9s | |
python3-neutron | 20.3.0-1.el9s | |
python3-neutron | 21.1.0-1.el9s | |
python3-neutron-dynamic-routing | 20.0.1-1.el9s | |
python3-neutron-dynamic-routing-tests | 20.0.1-1.el9s | |
python3-neutron-lib | 2.20.1-1.el9s | |
python3-neutron-lib | 3.1.1-1.el9s | |
python3-neutron-lib-tests | 2.20.1-1.el9s | |
python3-neutron-lib-tests | 3.1.1-1.el9s | |
python3-neutron-tests | 20.3.0-1.el9s | |
python3-neutron-tests | 21.1.0-1.el9s | |
python3-ovn-octavia-provider | 2.1.0-1.el9s | |
python3-ovn-octavia-provider | 3.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 2.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 3.1.0-1.el9s | |
python3-ovsdbapp | 1.15.3-1.el9s | |
python3-ovsdbapp | 2.1.1-1.el9s | |
python3-ovsdbapp-tests | 1.15.3-1.el9s | |
python3-ovsdbapp-tests | 2.1.1-1.el9s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.6-0.el9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_289.el9.el9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_289.el9-2.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.0.0-289.el9 | |
bpftool-debuginfo | 7.0.0-289.el9 | |
device-mapper-multipath | 0.8.7-21.el9 | |
device-mapper-multipath-debuginfo | 0.8.7-21.el9 | |
device-mapper-multipath-debugsource | 0.8.7-21.el9 | |
device-mapper-multipath-libs | 0.8.7-21.el9 | |
device-mapper-multipath-libs-debuginfo | 0.8.7-21.el9 | |
gnutls | 3.7.6-20.el9 | |
gnutls-debuginfo | 3.7.6-20.el9 | |
gnutls-debugsource | 3.7.6-20.el9 | |
kernel | 5.14.0-289.el9 | |
kernel-64k | 5.14.0-289.el9 | |
kernel-64k-core | 5.14.0-289.el9 | |
kernel-64k-debug | 5.14.0-289.el9 | |
kernel-64k-debug-core | 5.14.0-289.el9 | |
kernel-64k-debug-debuginfo | 5.14.0-289.el9 | |
kernel-64k-debug-modules | 5.14.0-289.el9 | |
kernel-64k-debug-modules-core | 5.14.0-289.el9 | |
kernel-64k-debug-modules-extra | 5.14.0-289.el9 | |
kernel-64k-debuginfo | 5.14.0-289.el9 | |
kernel-64k-modules | 5.14.0-289.el9 | |
kernel-64k-modules-core | 5.14.0-289.el9 | |
kernel-64k-modules-extra | 5.14.0-289.el9 | |
kernel-abi-stablelists | 5.14.0-289.el9 | |
kernel-core | 5.14.0-289.el9 | |
kernel-debug | 5.14.0-289.el9 | |
kernel-debug-core | 5.14.0-289.el9 | |
kernel-debug-debuginfo | 5.14.0-289.el9 | |
kernel-debug-modules | 5.14.0-289.el9 | |
kernel-debug-modules-core | 5.14.0-289.el9 | |
kernel-debug-modules-extra | 5.14.0-289.el9 | |
kernel-debuginfo | 5.14.0-289.el9 | |
kernel-debuginfo-common-aarch64 | 5.14.0-289.el9 | |
kernel-modules | 5.14.0-289.el9 | |
kernel-modules-core | 5.14.0-289.el9 | |
kernel-modules-extra | 5.14.0-289.el9 | |
kernel-tools | 5.14.0-289.el9 | |
kernel-tools-debuginfo | 5.14.0-289.el9 | |
kernel-tools-libs | 5.14.0-289.el9 | |
kexec-tools | 2.0.25-13.el9 | |
kexec-tools-debuginfo | 2.0.25-13.el9 | |
kexec-tools-debugsource | 2.0.25-13.el9 | |
kmod-kvdo | 8.2.1.6-75.el9 | |
kmod-kvdo-debuginfo | 8.2.1.6-75.el9 | |
kmod-kvdo-debugsource | 8.2.1.6-75.el9 | |
kpartx | 0.8.7-21.el9 | |
kpartx-debuginfo | 0.8.7-21.el9 | |
opensc | 0.23.0-1.el9 | |
opensc-debuginfo | 0.23.0-1.el9 | |
opensc-debugsource | 0.23.0-1.el9 | |
python3-perf | 5.14.0-289.el9 | |
python3-perf-debuginfo | 5.14.0-289.el9 | |
selinux-policy | 38.1.9-1.el9 | |
selinux-policy-doc | 38.1.9-1.el9 | |
selinux-policy-mls | 38.1.9-1.el9 | |
selinux-policy-sandbox | 38.1.9-1.el9 | |
selinux-policy-targeted | 38.1.9-1.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
buildah | 1.29.1-1.el9 | |
buildah-debuginfo | 1.29.1-1.el9 | |
buildah-debugsource | 1.29.1-1.el9 | |
buildah-tests | 1.29.1-1.el9 | |
buildah-tests-debuginfo | 1.29.1-1.el9 | |
coreos-installer | 0.17.0-1.el9 | |
coreos-installer-bootinfra | 0.17.0-1.el9 | |
coreos-installer-bootinfra-debuginfo | 0.17.0-1.el9 | |
coreos-installer-debuginfo | 0.17.0-1.el9 | |
coreos-installer-dracut | 0.17.0-1.el9 | |
edk2-aarch64 | 20221207gitfff6d81270b5-9.el9 | |
gnutls-c++ | 3.7.6-20.el9 | |
gnutls-c++-debuginfo | 3.7.6-20.el9 | |
gnutls-dane | 3.7.6-20.el9 | |
gnutls-dane-debuginfo | 3.7.6-20.el9 | |
gnutls-devel | 3.7.6-20.el9 | |
gnutls-utils | 3.7.6-20.el9 | |
gnutls-utils-debuginfo | 3.7.6-20.el9 | |
java-1.8.0-openjdk | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-src | 1.8.0.362.b09-4.el9 | |
kernel-64k-debug-devel | 5.14.0-289.el9 | |
kernel-64k-debug-devel-matched | 5.14.0-289.el9 | |
kernel-64k-devel | 5.14.0-289.el9 | |
kernel-64k-devel-matched | 5.14.0-289.el9 | |
kernel-debug-devel | 5.14.0-289.el9 | |
kernel-debug-devel-matched | 5.14.0-289.el9 | |
kernel-devel | 5.14.0-289.el9 | |
kernel-devel-matched | 5.14.0-289.el9 | |
kernel-doc | 5.14.0-289.el9 | |
kernel-headers | 5.14.0-289.el9 | |
libguestfs | 1.50.1-3.el9 | |
libguestfs-appliance | 1.50.1-3.el9 | |
libguestfs-bash-completion | 1.50.1-3.el9 | |
libguestfs-debuginfo | 1.50.1-3.el9 | |
libguestfs-debugsource | 1.50.1-3.el9 | |
libguestfs-inspect-icons | 1.50.1-3.el9 | |
libguestfs-rescue | 1.50.1-3.el9 | |
libguestfs-rescue-debuginfo | 1.50.1-3.el9 | |
libguestfs-rsync | 1.50.1-3.el9 | |
libguestfs-xfs | 1.50.1-3.el9 | |
libstoragemgmt | 1.9.7-1.el9 | |
libstoragemgmt-arcconf-plugin | 1.9.7-1.el9 | |
libstoragemgmt-debuginfo | 1.9.7-1.el9 | |
libstoragemgmt-debugsource | 1.9.7-1.el9 | |
libstoragemgmt-hpsa-plugin | 1.9.7-1.el9 | |
libstoragemgmt-local-plugin | 1.9.7-1.el9 | |
libstoragemgmt-megaraid-plugin | 1.9.7-1.el9 | |
libstoragemgmt-nfs-plugin | 1.9.7-1.el9 | |
libstoragemgmt-nfs-plugin-debuginfo | 1.9.7-1.el9 | |
libstoragemgmt-smis-plugin | 1.9.7-1.el9 | |
libstoragemgmt-targetd-plugin | 1.9.7-1.el9 | |
libstoragemgmt-udev | 1.9.7-1.el9 | |
libstoragemgmt-udev-debuginfo | 1.9.7-1.el9 | |
nmstate | 2.2.8-1.el9 | |
nmstate-debuginfo | 2.2.8-1.el9 | |
nmstate-debugsource | 2.2.8-1.el9 | |
nmstate-libs | 2.2.8-1.el9 | |
nmstate-libs-debuginfo | 2.2.8-1.el9 | |
nspr | 4.34.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nspr-debuginfo | 4.34.0-17.el9 | |
nspr-devel | 4.34.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-debuginfo | 3.79.0-17.el9 | |
nss-debugsource | 3.79.0-17.el9 | |
nss-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-debuginfo | 3.79.0-17.el9 | |
nss-softokn-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-softokn-freebl-debuginfo | 3.79.0-17.el9 | |
nss-softokn-freebl-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-sysinit-debuginfo | 3.79.0-17.el9 | |
nss-tools | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-tools-debuginfo | 3.79.0-17.el9 | |
nss-util | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
nss-util-debuginfo | 3.79.0-17.el9 | |
nss-util-devel | 3.79.0-17.el9 | [RHSA-2023:1368](https://access.redhat.com/errata/RHSA-2023:1368) | <div class="adv_s">Security Advisory</div> ([CVE-2023-0767](https://access.redhat.com/security/cve/CVE-2023-0767))
perf | 5.14.0-289.el9 | |
perf-debuginfo | 5.14.0-289.el9 | |
perl-Sys-Guestfs | 1.50.1-3.el9 | |
perl-Sys-Guestfs-debuginfo | 1.50.1-3.el9 | |
podman | 4.4.1-3.el9 | |
podman-debuginfo | 4.4.1-3.el9 | |
podman-debugsource | 4.4.1-3.el9 | |
podman-docker | 4.4.1-3.el9 | |
podman-gvproxy | 4.4.1-3.el9 | |
podman-gvproxy-debuginfo | 4.4.1-3.el9 | |
podman-plugins | 4.4.1-3.el9 | |
podman-plugins-debuginfo | 4.4.1-3.el9 | |
podman-remote | 4.4.1-3.el9 | |
podman-remote-debuginfo | 4.4.1-3.el9 | |
podman-tests | 4.4.1-3.el9 | |
python3-libguestfs | 1.50.1-3.el9 | |
python3-libguestfs-debuginfo | 1.50.1-3.el9 | |
python3-libnmstate | 2.2.8-1.el9 | |
python3-libstoragemgmt | 1.9.7-1.el9 | |
python3-libstoragemgmt-debuginfo | 1.9.7-1.el9 | |
python3-tomli | 2.0.1-5.el9 | |
python3.11-pip | 22.3.1-3.el9 | |
python3.11-pip-wheel | 22.3.1-3.el9 | |
rtla | 5.14.0-289.el9 | |
rust-coreos-installer-debuginfo | 0.17.0-1.el9 | |
rust-coreos-installer-debugsource | 0.17.0-1.el9 | |
selinux-policy-devel | 38.1.9-1.el9 | |
setroubleshoot | 3.3.31-2.el9 | |
setroubleshoot-debugsource | 3.3.31-2.el9 | |
setroubleshoot-server | 3.3.31-2.el9 | |
setroubleshoot-server-debuginfo | 3.3.31-2.el9 | |
skopeo | 1.11.2-0.1.el9 | |
skopeo-debuginfo | 1.11.2-0.1.el9 | |
skopeo-debugsource | 1.11.2-0.1.el9 | |
skopeo-tests | 1.11.2-0.1.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath-devel | 0.8.7-21.el9 | |
edk2-debugsource | 20221207gitfff6d81270b5-9.el9 | |
edk2-ovmf | 20221207gitfff6d81270b5-9.el9 | |
edk2-tools | 20221207gitfff6d81270b5-9.el9 | |
edk2-tools-debuginfo | 20221207gitfff6d81270b5-9.el9 | |
edk2-tools-doc | 20221207gitfff6d81270b5-9.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.362.b09-4.el9 | |
java-1.8.0-openjdk-src-slowdebug | 1.8.0.362.b09-4.el9 | |
kernel-cross-headers | 5.14.0-289.el9 | |
kernel-tools-libs-devel | 5.14.0-289.el9 | |
libguestfs-devel | 1.50.1-3.el9 | |
libguestfs-gobject | 1.50.1-3.el9 | |
libguestfs-gobject-debuginfo | 1.50.1-3.el9 | |
libguestfs-gobject-devel | 1.50.1-3.el9 | |
libguestfs-man-pages-ja | 1.50.1-3.el9 | |
libguestfs-man-pages-uk | 1.50.1-3.el9 | |
libstoragemgmt-devel | 1.9.7-1.el9 | |
lua-guestfs | 1.50.1-3.el9 | |
lua-guestfs-debuginfo | 1.50.1-3.el9 | |
nmstate-devel | 2.2.8-1.el9 | |
nmstate-static | 2.2.8-1.el9 | |
ocaml-libguestfs | 1.50.1-3.el9 | |
ocaml-libguestfs-debuginfo | 1.50.1-3.el9 | |
ocaml-libguestfs-devel | 1.50.1-3.el9 | |
php-libguestfs | 1.50.1-3.el9 | |
php-libguestfs-debuginfo | 1.50.1-3.el9 | |
ruby-libguestfs | 1.50.1-3.el9 | |
ruby-libguestfs-debuginfo | 1.50.1-3.el9 | |
systemd-boot-unsigned | 252-8.el9 | |
systemd-boot-unsigned-debuginfo | 252-8.el9 | |
WALinuxAgent-cvm | 2.7.0.6-9.el9 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 20.3.0-1.el9s | |
openstack-neutron | 21.1.0-1.el9s | |
openstack-neutron-bgp-dragent | 20.0.1-1.el9s | |
openstack-neutron-common | 20.3.0-1.el9s | |
openstack-neutron-common | 21.1.0-1.el9s | |
openstack-neutron-dynamic-routing-common | 20.0.1-1.el9s | |
openstack-neutron-linuxbridge | 20.3.0-1.el9s | |
openstack-neutron-linuxbridge | 21.1.0-1.el9s | |
openstack-neutron-macvtap-agent | 20.3.0-1.el9s | |
openstack-neutron-macvtap-agent | 21.1.0-1.el9s | |
openstack-neutron-metering-agent | 20.3.0-1.el9s | |
openstack-neutron-metering-agent | 21.1.0-1.el9s | |
openstack-neutron-ml2 | 20.3.0-1.el9s | |
openstack-neutron-ml2 | 21.1.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 20.3.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 21.1.0-1.el9s | |
openstack-neutron-openvswitch | 20.3.0-1.el9s | |
openstack-neutron-openvswitch | 21.1.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 20.3.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 21.1.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 20.3.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 21.1.0-1.el9s | |
openstack-neutron-rpc-server | 20.3.0-1.el9s | |
openstack-neutron-rpc-server | 21.1.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 20.3.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 21.1.0-1.el9s | |
python-neutron-lib-doc | 2.20.1-1.el9s | |
python-neutron-lib-doc | 3.1.1-1.el9s | |
python-ovsdbapp-doc | 1.15.3-1.el9s | |
python-ovsdbapp-doc | 2.1.1-1.el9s | |
python3-neutron | 20.3.0-1.el9s | |
python3-neutron | 21.1.0-1.el9s | |
python3-neutron-dynamic-routing | 20.0.1-1.el9s | |
python3-neutron-dynamic-routing-tests | 20.0.1-1.el9s | |
python3-neutron-lib | 2.20.1-1.el9s | |
python3-neutron-lib | 3.1.1-1.el9s | |
python3-neutron-lib-tests | 2.20.1-1.el9s | |
python3-neutron-lib-tests | 3.1.1-1.el9s | |
python3-neutron-tests | 20.3.0-1.el9s | |
python3-neutron-tests | 21.1.0-1.el9s | |
python3-ovn-octavia-provider | 2.1.0-1.el9s | |
python3-ovn-octavia-provider | 3.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 2.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 3.1.0-1.el9s | |
python3-ovsdbapp | 1.15.3-1.el9s | |
python3-ovsdbapp | 2.1.1-1.el9s | |
python3-ovsdbapp-tests | 1.15.3-1.el9s | |
python3-ovsdbapp-tests | 2.1.1-1.el9s | |

