## 2021-11-04

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-yum-tool | 1.8-1.el8s.cern | |
openafs-release | 1.2-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-yum-tool | 1.8-1.el8s.cern | |
openafs-release | 1.2-1.el8s.cern | |

