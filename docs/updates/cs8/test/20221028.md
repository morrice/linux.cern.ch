## 2022-10-28

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-hosted-engine-setup | 2.6.6-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.3.2-1.el8 | |
ovirt-release-host-node | 4.5.3.2-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-hosted-engine-setup | 2.6.6-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.3.2-1.el8 | |
ovirt-release-host-node | 4.5.3.2-1.el8 | |

