## 2021-10-27

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afsconsole-accesstimes | 2.0-0.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-ironicclient | 4.6.3-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.27-2.el8 | |
ansible-doc | 2.9.27-2.el8 | |
ansible-test | 2.9.27-2.el8 | |
qemu-guest-agent | 6.0.0-33.el8s | |
qemu-img | 6.0.0-33.el8s | |
qemu-kiwi | 6.0.0-33.el8s | |
qemu-kvm | 6.0.0-33.el8s | |
qemu-kvm-block-curl | 6.0.0-33.el8s | |
qemu-kvm-block-gluster | 6.0.0-33.el8s | |
qemu-kvm-block-iscsi | 6.0.0-33.el8s | |
qemu-kvm-block-rbd | 6.0.0-33.el8s | |
qemu-kvm-block-ssh | 6.0.0-33.el8s | |
qemu-kvm-common | 6.0.0-33.el8s | |
qemu-kvm-core | 6.0.0-33.el8s | |
qemu-kvm-docs | 6.0.0-33.el8s | |
qemu-kvm-hw-usbredir | 6.0.0-33.el8s | |
qemu-kvm-tests | 6.0.0-33.el8s | |
qemu-kvm-ui-opengl | 6.0.0-33.el8s | |
qemu-kvm-ui-spice | 6.0.0-33.el8s | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afsconsole-accesstimes | 2.0-0.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-ironicclient | 4.6.3-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.27-2.el8 | |
ansible-doc | 2.9.27-2.el8 | |
ansible-test | 2.9.27-2.el8 | |
qemu-guest-agent | 6.0.0-33.el8s | |
qemu-img | 6.0.0-33.el8s | |
qemu-kiwi | 6.0.0-33.el8s | |
qemu-kvm | 6.0.0-33.el8s | |
qemu-kvm-block-curl | 6.0.0-33.el8s | |
qemu-kvm-block-iscsi | 6.0.0-33.el8s | |
qemu-kvm-block-rbd | 6.0.0-33.el8s | |
qemu-kvm-block-ssh | 6.0.0-33.el8s | |
qemu-kvm-common | 6.0.0-33.el8s | |
qemu-kvm-core | 6.0.0-33.el8s | |
qemu-kvm-docs | 6.0.0-33.el8s | |
qemu-kvm-tests | 6.0.0-33.el8s | |

