## 2023-01-31

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 20.1.0-1.el8 | |
openstack-nova | 25.1.0-1.el8 | |
openstack-nova-api | 25.1.0-1.el8 | |
openstack-nova-common | 25.1.0-1.el8 | |
openstack-nova-compute | 25.1.0-1.el8 | |
openstack-nova-conductor | 25.1.0-1.el8 | |
openstack-nova-migration | 25.1.0-1.el8 | |
openstack-nova-novncproxy | 25.1.0-1.el8 | |
openstack-nova-scheduler | 25.1.0-1.el8 | |
openstack-nova-serialproxy | 25.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 25.1.0-1.el8 | |
python3-cinder | 20.1.0-1.el8 | |
python3-cinder-common | 20.1.0-1.el8 | |
python3-cinder-tests | 20.1.0-1.el8 | |
python3-nova | 25.1.0-1.el8 | |
python3-nova-tests | 25.1.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 20.1.0-1.el8 | |
openstack-nova | 25.1.0-1.el8 | |
openstack-nova-api | 25.1.0-1.el8 | |
openstack-nova-common | 25.1.0-1.el8 | |
openstack-nova-compute | 25.1.0-1.el8 | |
openstack-nova-conductor | 25.1.0-1.el8 | |
openstack-nova-migration | 25.1.0-1.el8 | |
openstack-nova-novncproxy | 25.1.0-1.el8 | |
openstack-nova-scheduler | 25.1.0-1.el8 | |
openstack-nova-serialproxy | 25.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 25.1.0-1.el8 | |
python3-cinder | 20.1.0-1.el8 | |
python3-cinder-common | 20.1.0-1.el8 | |
python3-cinder-tests | 20.1.0-1.el8 | |
python3-nova | 25.1.0-1.el8 | |
python3-nova-tests | 25.1.0-1.el8 | |

