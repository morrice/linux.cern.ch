## 2022-03-01

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
audispd-plugins | 3.0.7-2.el8 | |
audispd-plugins-zos | 3.0.7-2.el8 | |
audit | 3.0.7-2.el8 | |
audit-libs | 3.0.7-2.el8 | |
audit-libs-devel | 3.0.7-2.el8 | |
bind-export-devel | 9.11.36-3.el8 | |
bind-export-libs | 9.11.36-3.el8 | |
cockpit | 264-1.el8 | |
cockpit-bridge | 264-1.el8 | |
cockpit-doc | 264-1.el8 | |
cockpit-system | 264-1.el8 | |
cockpit-ws | 264-1.el8 | |
cryptsetup | 2.3.7-2.el8 | |
cryptsetup-libs | 2.3.7-2.el8 | |
cryptsetup-reencrypt | 2.3.7-2.el8 | |
ctdb | 4.15.5-4.el8 | |
cyrus-sasl | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-devel | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-gs2 | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-gssapi | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-ldap | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-lib | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-md5 | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-ntlm | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-plain | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-scram | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
firewalld | 0.9.3-13.el8 | |
firewalld-filesystem | 0.9.3-13.el8 | |
fuse | 2.9.7-15.el8 | |
fuse-common | 3.3.0-15.el8 | |
fuse-devel | 2.9.7-15.el8 | |
fuse-libs | 2.9.7-15.el8 | |
fuse3 | 3.3.0-15.el8 | |
fuse3-devel | 3.3.0-15.el8 | |
fuse3-libs | 3.3.0-15.el8 | |
gawk | 4.2.1-4.el8 | |
integritysetup | 2.3.7-2.el8 | |
libblkid | 2.32.1-34.el8 | |
libblkid-devel | 2.32.1-34.el8 | |
libfdisk | 2.32.1-34.el8 | |
libfdisk-devel | 2.32.1-34.el8 | |
libmount | 2.32.1-34.el8 | |
libsemanage | 2.9-8.el8 | |
libsmartcols | 2.32.1-34.el8 | |
libsmartcols-devel | 2.32.1-34.el8 | |
libsmbclient | 4.15.5-4.el8 | |
libuuid | 2.32.1-34.el8 | |
libuuid-devel | 2.32.1-34.el8 | |
libwbclient | 4.15.5-4.el8 | |
libxml2 | 2.9.7-13.el8 | |
mdadm | 4.2-2.el8 | |
NetworkManager | 1.36.0-1.el8 | |
NetworkManager-adsl | 1.36.0-1.el8 | |
NetworkManager-bluetooth | 1.36.0-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.36.0-1.el8 | |
NetworkManager-config-server | 1.36.0-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.36.0-1.el8 | |
NetworkManager-libnm | 1.36.0-1.el8 | |
NetworkManager-ovs | 1.36.0-1.el8 | |
NetworkManager-ppp | 1.36.0-1.el8 | |
NetworkManager-team | 1.36.0-1.el8 | |
NetworkManager-tui | 1.36.0-1.el8 | |
NetworkManager-wifi | 1.36.0-1.el8 | |
NetworkManager-wwan | 1.36.0-1.el8 | |
nftables | 0.9.3-25.el8 | |
policycoreutils | 2.9-19.el8 | |
policycoreutils-dbus | 2.9-19.el8 | |
policycoreutils-devel | 2.9-19.el8 | |
policycoreutils-newrole | 2.9-19.el8 | |
policycoreutils-python-utils | 2.9-19.el8 | |
policycoreutils-restorecond | 2.9-19.el8 | |
python3-audit | 3.0.7-2.el8 | |
python3-firewall | 0.9.3-13.el8 | |
python3-libsemanage | 2.9-8.el8 | |
python3-libxml2 | 2.9.7-13.el8 | |
python3-nftables | 0.9.3-25.el8 | |
python3-policycoreutils | 2.9-19.el8 | |
python3-samba | 4.15.5-4.el8 | |
python3-samba-test | 4.15.5-4.el8 | |
samba | 4.15.5-4.el8 | |
samba-client | 4.15.5-4.el8 | |
samba-client-libs | 4.15.5-4.el8 | |
samba-common | 4.15.5-4.el8 | |
samba-common-libs | 4.15.5-4.el8 | |
samba-common-tools | 4.15.5-4.el8 | |
samba-krb5-printing | 4.15.5-4.el8 | |
samba-libs | 4.15.5-4.el8 | |
samba-pidl | 4.15.5-4.el8 | |
samba-test | 4.15.5-4.el8 | |
samba-test-libs | 4.15.5-4.el8 | |
samba-winbind | 4.15.5-4.el8 | |
samba-winbind-clients | 4.15.5-4.el8 | |
samba-winbind-krb5-locator | 4.15.5-4.el8 | |
samba-winbind-modules | 4.15.5-4.el8 | |
samba-winexe | 4.15.5-4.el8 | |
selinux-policy | 3.14.3-93.el8 | |
selinux-policy-devel | 3.14.3-93.el8 | |
selinux-policy-doc | 3.14.3-93.el8 | |
selinux-policy-minimum | 3.14.3-93.el8 | |
selinux-policy-mls | 3.14.3-93.el8 | |
selinux-policy-sandbox | 3.14.3-93.el8 | |
selinux-policy-targeted | 3.14.3-93.el8 | |
sos | 4.2-15.el8 | |
sos-audit | 4.2-15.el8 | |
util-linux | 2.32.1-34.el8 | |
util-linux-user | 2.32.1-34.el8 | |
uuidd | 2.32.1-34.el8 | |
veritysetup | 2.3.7-2.el8 | |
zsh | 5.5.1-9.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.6.6-1.el8 | |
anaconda-core | 33.16.6.6-1.el8 | |
anaconda-dracut | 33.16.6.6-1.el8 | |
anaconda-gui | 33.16.6.6-1.el8 | |
anaconda-install-env-deps | 33.16.6.6-1.el8 | |
anaconda-tui | 33.16.6.6-1.el8 | |
anaconda-widgets | 33.16.6.6-1.el8 | |
bind | 9.11.36-3.el8 | |
bind-chroot | 9.11.36-3.el8 | |
bind-devel | 9.11.36-3.el8 | |
bind-libs | 9.11.36-3.el8 | |
bind-libs-lite | 9.11.36-3.el8 | |
bind-license | 9.11.36-3.el8 | |
bind-lite-devel | 9.11.36-3.el8 | |
bind-pkcs11 | 9.11.36-3.el8 | |
bind-pkcs11-devel | 9.11.36-3.el8 | |
bind-pkcs11-libs | 9.11.36-3.el8 | |
bind-pkcs11-utils | 9.11.36-3.el8 | |
bind-sdb | 9.11.36-3.el8 | |
bind-sdb-chroot | 9.11.36-3.el8 | |
bind-utils | 9.11.36-3.el8 | |
cloud-init | 21.1-14.el8 | |
cockpit-composer | 35-1.el8 | |
cockpit-machines | 264-1.el8 | |
cockpit-packagekit | 264-1.el8 | |
cockpit-pcp | 264-1.el8 | |
cockpit-storaged | 264-1.el8 | |
coreos-installer | 0.11.0-3.el8 | |
coreos-installer-bootinfra | 0.11.0-3.el8 | |
coreos-installer-dracut | 0.11.0-3.el8 | |
cryptsetup-devel | 2.3.7-2.el8 | |
cyrus-sasl-sql | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
fapolicyd | 1.1-1.el8 | |
fapolicyd-selinux | 1.1-1.el8 | |
fdo-client | 0.4.0-8.el8 | |
fdo-init | 0.4.0-8.el8 | |
fdo-manufacturing-server | 0.4.0-8.el8 | |
fdo-owner-cli | 0.4.0-8.el8 | |
fdo-owner-onboarding-server | 0.4.0-8.el8 | |
fdo-rendezvous-server | 0.4.0-8.el8 | |
fetchmail | 6.4.24-1.el8 | |
firewall-applet | 0.9.3-13.el8 | |
firewall-config | 0.9.3-13.el8 | |
gnome-control-center | 3.28.2-33.el8 | |
gnome-control-center-filesystem | 3.28.2-33.el8 | |
go-toolset | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-bin | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-docs | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-misc | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-race | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-src | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-tests | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
libudisks2 | 2.9.0-9.el8 | |
libxml2-devel | 2.9.7-13.el8 | |
mutter | 3.32.2-63.el8 | |
network-manager-applet | 1.24.0-4.el8 | |
NetworkManager-cloud-setup | 1.36.0-1.el8 | |
nm-connection-editor | 1.24.0-4.el8 | |
osbuild | 49-1.el8 | |
osbuild-luks2 | 49-1.el8 | |
osbuild-lvm2 | 49-1.el8 | |
osbuild-ostree | 49-1.el8 | |
osbuild-selinux | 49-1.el8 | |
pixman | 0.38.4-2.el8 | |
pixman-devel | 0.38.4-2.el8 | |
plymouth | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-core-libs | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-graphics-libs | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-fade-throbber | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-label | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-script | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-space-flares | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-throbgress | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-two-step | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-scripts | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-system-theme | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-charge | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-fade-in | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-script | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-solar | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-spinfinity | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-spinner | 0.9.4-11.20200615git1e36e30.el8 | |
policycoreutils-gui | 2.9-19.el8 | |
policycoreutils-sandbox | 2.9-19.el8 | |
python3-bind | 9.11.36-3.el8 | |
python3-libmount | 2.32.1-34.el8 | |
python3-osbuild | 49-1.el8 | |
rhel-system-roles | 1.15.0-1.el8 | |
samba-vfs-iouring | 4.15.5-4.el8 | |
scap-security-guide | 0.1.60-7.el8 | |
scap-security-guide-doc | 0.1.60-7.el8 | |
udisks2 | 2.9.0-9.el8 | |
udisks2-iscsi | 2.9.0-9.el8 | |
udisks2-lsm | 2.9.0-9.el8 | |
udisks2-lvm2 | 2.9.0-9.el8 | |
zsh-html | 5.5.1-9.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glog | 0.3.5-5.el8 | |
glog-devel | 0.3.5-5.el8 | |
libmount-devel | 2.32.1-34.el8 | |
libsemanage-devel | 2.9-8.el8 | |
libsmbclient-devel | 4.15.5-4.el8 | |
libudisks2-devel | 2.9.0-9.el8 | |
libwbclient-devel | 4.15.5-4.el8 | |
mutter-devel | 3.32.2-63.el8 | |
NetworkManager-libnm-devel | 1.36.0-1.el8 | |
python3-scons | 3.1.2-1.el8 | |
samba-devel | 4.15.5-4.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-15.el8 | |
resource-agents-aliyun | 4.9.0-15.el8 | |
resource-agents-gcp | 4.9.0-15.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-15.el8 | |
resource-agents-aliyun | 4.9.0-15.el8 | |
resource-agents-gcp | 4.9.0-15.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 19.0.0-2.el8 | |
openstack-heat-api | 17.0.1-1.el8 | |
openstack-heat-api-cfn | 17.0.1-1.el8 | |
openstack-heat-common | 17.0.1-1.el8 | |
openstack-heat-engine | 17.0.1-1.el8 | |
openstack-heat-monolith | 17.0.1-1.el8 | |
python-os-service-types-doc | 1.7.0-3.el8 | |
python-oslo-utils-doc | 4.10.1-2.el8 | |
python-oslo-utils-lang | 4.10.1-2.el8 | |
python3-cinder | 19.0.0-2.el8 | |
python3-cinder-common | 19.0.0-2.el8 | |
python3-cinder-tests | 19.0.0-2.el8 | |
python3-hacking | 1.1.0-2.el8 | |
python3-heat-tests | 17.0.1-1.el8 | |
python3-os-service-types | 1.7.0-3.el8 | |
python3-oslo-utils | 4.10.1-2.el8 | |
python3-oslo-utils-tests | 4.10.1-2.el8 | |
python3-requestsexceptions | 1.4.0-3.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
audispd-plugins | 3.0.7-2.el8 | |
audispd-plugins-zos | 3.0.7-2.el8 | |
audit | 3.0.7-2.el8 | |
audit-libs | 3.0.7-2.el8 | |
audit-libs-devel | 3.0.7-2.el8 | |
bind-export-devel | 9.11.36-3.el8 | |
bind-export-libs | 9.11.36-3.el8 | |
cockpit | 264-1.el8 | |
cockpit-bridge | 264-1.el8 | |
cockpit-doc | 264-1.el8 | |
cockpit-system | 264-1.el8 | |
cockpit-ws | 264-1.el8 | |
cryptsetup | 2.3.7-2.el8 | |
cryptsetup-libs | 2.3.7-2.el8 | |
cryptsetup-reencrypt | 2.3.7-2.el8 | |
ctdb | 4.15.5-4.el8 | |
cyrus-sasl | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-devel | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-gs2 | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-gssapi | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-ldap | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-lib | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-md5 | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-ntlm | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-plain | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
cyrus-sasl-scram | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
firewalld | 0.9.3-13.el8 | |
firewalld-filesystem | 0.9.3-13.el8 | |
fuse | 2.9.7-15.el8 | |
fuse-common | 3.3.0-15.el8 | |
fuse-devel | 2.9.7-15.el8 | |
fuse-libs | 2.9.7-15.el8 | |
fuse3 | 3.3.0-15.el8 | |
fuse3-devel | 3.3.0-15.el8 | |
fuse3-libs | 3.3.0-15.el8 | |
gawk | 4.2.1-4.el8 | |
integritysetup | 2.3.7-2.el8 | |
libblkid | 2.32.1-34.el8 | |
libblkid-devel | 2.32.1-34.el8 | |
libfdisk | 2.32.1-34.el8 | |
libfdisk-devel | 2.32.1-34.el8 | |
libmount | 2.32.1-34.el8 | |
libsemanage | 2.9-8.el8 | |
libsmartcols | 2.32.1-34.el8 | |
libsmartcols-devel | 2.32.1-34.el8 | |
libsmbclient | 4.15.5-4.el8 | |
libuuid | 2.32.1-34.el8 | |
libuuid-devel | 2.32.1-34.el8 | |
libwbclient | 4.15.5-4.el8 | |
libxml2 | 2.9.7-13.el8 | |
mdadm | 4.2-2.el8 | |
NetworkManager | 1.36.0-1.el8 | |
NetworkManager-adsl | 1.36.0-1.el8 | |
NetworkManager-bluetooth | 1.36.0-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.36.0-1.el8 | |
NetworkManager-config-server | 1.36.0-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.36.0-1.el8 | |
NetworkManager-libnm | 1.36.0-1.el8 | |
NetworkManager-ovs | 1.36.0-1.el8 | |
NetworkManager-ppp | 1.36.0-1.el8 | |
NetworkManager-team | 1.36.0-1.el8 | |
NetworkManager-tui | 1.36.0-1.el8 | |
NetworkManager-wifi | 1.36.0-1.el8 | |
NetworkManager-wwan | 1.36.0-1.el8 | |
nftables | 0.9.3-25.el8 | |
policycoreutils | 2.9-19.el8 | |
policycoreutils-dbus | 2.9-19.el8 | |
policycoreutils-devel | 2.9-19.el8 | |
policycoreutils-newrole | 2.9-19.el8 | |
policycoreutils-python-utils | 2.9-19.el8 | |
policycoreutils-restorecond | 2.9-19.el8 | |
python3-audit | 3.0.7-2.el8 | |
python3-firewall | 0.9.3-13.el8 | |
python3-libsemanage | 2.9-8.el8 | |
python3-libxml2 | 2.9.7-13.el8 | |
python3-nftables | 0.9.3-25.el8 | |
python3-policycoreutils | 2.9-19.el8 | |
python3-samba | 4.15.5-4.el8 | |
python3-samba-test | 4.15.5-4.el8 | |
samba | 4.15.5-4.el8 | |
samba-client | 4.15.5-4.el8 | |
samba-client-libs | 4.15.5-4.el8 | |
samba-common | 4.15.5-4.el8 | |
samba-common-libs | 4.15.5-4.el8 | |
samba-common-tools | 4.15.5-4.el8 | |
samba-krb5-printing | 4.15.5-4.el8 | |
samba-libs | 4.15.5-4.el8 | |
samba-pidl | 4.15.5-4.el8 | |
samba-test | 4.15.5-4.el8 | |
samba-test-libs | 4.15.5-4.el8 | |
samba-winbind | 4.15.5-4.el8 | |
samba-winbind-clients | 4.15.5-4.el8 | |
samba-winbind-krb5-locator | 4.15.5-4.el8 | |
samba-winbind-modules | 4.15.5-4.el8 | |
selinux-policy | 3.14.3-93.el8 | |
selinux-policy-devel | 3.14.3-93.el8 | |
selinux-policy-doc | 3.14.3-93.el8 | |
selinux-policy-minimum | 3.14.3-93.el8 | |
selinux-policy-mls | 3.14.3-93.el8 | |
selinux-policy-sandbox | 3.14.3-93.el8 | |
selinux-policy-targeted | 3.14.3-93.el8 | |
sos | 4.2-15.el8 | |
sos-audit | 4.2-15.el8 | |
util-linux | 2.32.1-34.el8 | |
util-linux-user | 2.32.1-34.el8 | |
uuidd | 2.32.1-34.el8 | |
veritysetup | 2.3.7-2.el8 | |
zsh | 5.5.1-9.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.6.6-1.el8 | |
anaconda-core | 33.16.6.6-1.el8 | |
anaconda-dracut | 33.16.6.6-1.el8 | |
anaconda-gui | 33.16.6.6-1.el8 | |
anaconda-install-env-deps | 33.16.6.6-1.el8 | |
anaconda-tui | 33.16.6.6-1.el8 | |
anaconda-widgets | 33.16.6.6-1.el8 | |
bind | 9.11.36-3.el8 | |
bind-chroot | 9.11.36-3.el8 | |
bind-devel | 9.11.36-3.el8 | |
bind-libs | 9.11.36-3.el8 | |
bind-libs-lite | 9.11.36-3.el8 | |
bind-license | 9.11.36-3.el8 | |
bind-lite-devel | 9.11.36-3.el8 | |
bind-pkcs11 | 9.11.36-3.el8 | |
bind-pkcs11-devel | 9.11.36-3.el8 | |
bind-pkcs11-libs | 9.11.36-3.el8 | |
bind-pkcs11-utils | 9.11.36-3.el8 | |
bind-sdb | 9.11.36-3.el8 | |
bind-sdb-chroot | 9.11.36-3.el8 | |
bind-utils | 9.11.36-3.el8 | |
cloud-init | 21.1-14.el8 | |
cockpit-composer | 35-1.el8 | |
cockpit-machines | 264-1.el8 | |
cockpit-packagekit | 264-1.el8 | |
cockpit-pcp | 264-1.el8 | |
cockpit-storaged | 264-1.el8 | |
coreos-installer | 0.11.0-3.el8 | |
coreos-installer-bootinfra | 0.11.0-3.el8 | |
coreos-installer-dracut | 0.11.0-3.el8 | |
cryptsetup-devel | 2.3.7-2.el8 | |
cyrus-sasl-sql | 2.1.27-6.el8_5 | [RHSA-2022:0658](https://access.redhat.com/errata/RHSA-2022:0658) | <div class="adv_s">Security Advisory</div>
fapolicyd | 1.1-1.el8 | |
fapolicyd-selinux | 1.1-1.el8 | |
fdo-client | 0.4.0-8.el8 | |
fdo-init | 0.4.0-8.el8 | |
fdo-manufacturing-server | 0.4.0-8.el8 | |
fdo-owner-cli | 0.4.0-8.el8 | |
fdo-owner-onboarding-server | 0.4.0-8.el8 | |
fdo-rendezvous-server | 0.4.0-8.el8 | |
fetchmail | 6.4.24-1.el8 | |
firewall-applet | 0.9.3-13.el8 | |
firewall-config | 0.9.3-13.el8 | |
gnome-control-center | 3.28.2-33.el8 | |
gnome-control-center-filesystem | 3.28.2-33.el8 | |
go-toolset | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-bin | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-docs | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-misc | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-src | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
golang-tests | 1.17.7-1.module_el8.6.0+1099+24a5d718 | |
libudisks2 | 2.9.0-9.el8 | |
libxml2-devel | 2.9.7-13.el8 | |
mutter | 3.32.2-63.el8 | |
network-manager-applet | 1.24.0-4.el8 | |
NetworkManager-cloud-setup | 1.36.0-1.el8 | |
nm-connection-editor | 1.24.0-4.el8 | |
osbuild | 49-1.el8 | |
osbuild-luks2 | 49-1.el8 | |
osbuild-lvm2 | 49-1.el8 | |
osbuild-ostree | 49-1.el8 | |
osbuild-selinux | 49-1.el8 | |
pixman | 0.38.4-2.el8 | |
pixman-devel | 0.38.4-2.el8 | |
plymouth | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-core-libs | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-graphics-libs | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-fade-throbber | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-label | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-script | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-space-flares | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-throbgress | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-plugin-two-step | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-scripts | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-system-theme | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-charge | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-fade-in | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-script | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-solar | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-spinfinity | 0.9.4-11.20200615git1e36e30.el8 | |
plymouth-theme-spinner | 0.9.4-11.20200615git1e36e30.el8 | |
policycoreutils-gui | 2.9-19.el8 | |
policycoreutils-sandbox | 2.9-19.el8 | |
python3-bind | 9.11.36-3.el8 | |
python3-libmount | 2.32.1-34.el8 | |
python3-osbuild | 49-1.el8 | |
rhel-system-roles | 1.15.0-1.el8 | |
samba-vfs-iouring | 4.15.5-4.el8 | |
scap-security-guide | 0.1.60-7.el8 | |
scap-security-guide-doc | 0.1.60-7.el8 | |
udisks2 | 2.9.0-9.el8 | |
udisks2-iscsi | 2.9.0-9.el8 | |
udisks2-lsm | 2.9.0-9.el8 | |
udisks2-lvm2 | 2.9.0-9.el8 | |
zsh-html | 5.5.1-9.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
glog | 0.3.5-5.el8 | |
glog-devel | 0.3.5-5.el8 | |
libmount-devel | 2.32.1-34.el8 | |
libsemanage-devel | 2.9-8.el8 | |
libsmbclient-devel | 4.15.5-4.el8 | |
libudisks2-devel | 2.9.0-9.el8 | |
libwbclient-devel | 4.15.5-4.el8 | |
mutter-devel | 3.32.2-63.el8 | |
NetworkManager-libnm-devel | 1.36.0-1.el8 | |
python3-scons | 3.1.2-1.el8 | |
samba-devel | 4.15.5-4.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-15.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 19.0.0-2.el8 | |
openstack-heat-api | 17.0.1-1.el8 | |
openstack-heat-api-cfn | 17.0.1-1.el8 | |
openstack-heat-common | 17.0.1-1.el8 | |
openstack-heat-engine | 17.0.1-1.el8 | |
openstack-heat-monolith | 17.0.1-1.el8 | |
python-os-service-types-doc | 1.7.0-3.el8 | |
python-oslo-utils-doc | 4.10.1-2.el8 | |
python-oslo-utils-lang | 4.10.1-2.el8 | |
python3-cinder | 19.0.0-2.el8 | |
python3-cinder-common | 19.0.0-2.el8 | |
python3-cinder-tests | 19.0.0-2.el8 | |
python3-hacking | 1.1.0-2.el8 | |
python3-heat-tests | 17.0.1-1.el8 | |
python3-os-service-types | 1.7.0-3.el8 | |
python3-oslo-utils | 4.10.1-2.el8 | |
python3-oslo-utils-tests | 4.10.1-2.el8 | |
python3-requestsexceptions | 1.4.0-3.el8 | |

