## 2022-02-16

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.0-1.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-libs | 2.2.6-44.el8 | |
iproute | 5.15.0-3.el8 | |
iproute-tc | 5.15.0-3.el8 | |
kmod-kvdo | 6.2.6.14-83.el8 | |
ksc | 1.9-1.el8 | |
tuned | 2.18.0-2.el8 | |
tuned-profiles-atomic | 2.18.0-2.el8 | |
tuned-profiles-compat | 2.18.0-2.el8 | |
tuned-profiles-cpu-partitioning | 2.18.0-2.el8 | |
tuned-profiles-mssql | 2.18.0-2.el8 | |
tuned-profiles-oracle | 2.18.0-2.el8 | |
wpa_supplicant | 2.10-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.6.1-3.el8 | |
alsa-lib-devel | 1.2.6.1-3.el8 | |
alsa-ucm | 1.2.6.1-3.el8 | |
annobin | 10.29-3.el8 | |
annobin-annocheck | 10.29-3.el8 | |
ansible-core | 2.12.2-2.el8 | |
ansible-test | 2.12.2-2.el8 | |
aspnetcore-runtime-5.0 | 5.0.14-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
aspnetcore-targeting-pack-5.0 | 5.0.14-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
autocorr-af | 6.4.7.2-10.el8 | |
autocorr-bg | 6.4.7.2-10.el8 | |
autocorr-ca | 6.4.7.2-10.el8 | |
autocorr-cs | 6.4.7.2-10.el8 | |
autocorr-da | 6.4.7.2-10.el8 | |
autocorr-de | 6.4.7.2-10.el8 | |
autocorr-en | 6.4.7.2-10.el8 | |
autocorr-es | 6.4.7.2-10.el8 | |
autocorr-fa | 6.4.7.2-10.el8 | |
autocorr-fi | 6.4.7.2-10.el8 | |
autocorr-fr | 6.4.7.2-10.el8 | |
autocorr-ga | 6.4.7.2-10.el8 | |
autocorr-hr | 6.4.7.2-10.el8 | |
autocorr-hu | 6.4.7.2-10.el8 | |
autocorr-is | 6.4.7.2-10.el8 | |
autocorr-it | 6.4.7.2-10.el8 | |
autocorr-ja | 6.4.7.2-10.el8 | |
autocorr-ko | 6.4.7.2-10.el8 | |
autocorr-lb | 6.4.7.2-10.el8 | |
autocorr-lt | 6.4.7.2-10.el8 | |
autocorr-mn | 6.4.7.2-10.el8 | |
autocorr-nl | 6.4.7.2-10.el8 | |
autocorr-pl | 6.4.7.2-10.el8 | |
autocorr-pt | 6.4.7.2-10.el8 | |
autocorr-ro | 6.4.7.2-10.el8 | |
autocorr-ru | 6.4.7.2-10.el8 | |
autocorr-sk | 6.4.7.2-10.el8 | |
autocorr-sl | 6.4.7.2-10.el8 | |
autocorr-sr | 6.4.7.2-10.el8 | |
autocorr-sv | 6.4.7.2-10.el8 | |
autocorr-tr | 6.4.7.2-10.el8 | |
autocorr-vi | 6.4.7.2-10.el8 | |
autocorr-zh | 6.4.7.2-10.el8 | |
blivet-data | 3.4.0-9.el8 | |
cups | 2.2.6-44.el8 | |
cups-client | 2.2.6-44.el8 | |
cups-devel | 2.2.6-44.el8 | |
cups-filesystem | 2.2.6-44.el8 | |
cups-ipptool | 2.2.6-44.el8 | |
cups-lpd | 2.2.6-44.el8 | |
dotnet-apphost-pack-5.0 | 5.0.14-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
dotnet-hostfxr-5.0 | 5.0.14-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
dotnet-runtime-5.0 | 5.0.14-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
dotnet-sdk-5.0 | 5.0.211-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
dotnet-targeting-pack-5.0 | 5.0.14-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
dotnet-templates-5.0 | 5.0.211-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
fence-agents-all | 4.2.1-88.el8 | |
fence-agents-amt-ws | 4.2.1-88.el8 | |
fence-agents-apc | 4.2.1-88.el8 | |
fence-agents-apc-snmp | 4.2.1-88.el8 | |
fence-agents-bladecenter | 4.2.1-88.el8 | |
fence-agents-brocade | 4.2.1-88.el8 | |
fence-agents-cisco-mds | 4.2.1-88.el8 | |
fence-agents-cisco-ucs | 4.2.1-88.el8 | |
fence-agents-common | 4.2.1-88.el8 | |
fence-agents-compute | 4.2.1-88.el8 | |
fence-agents-drac5 | 4.2.1-88.el8 | |
fence-agents-eaton-snmp | 4.2.1-88.el8 | |
fence-agents-emerson | 4.2.1-88.el8 | |
fence-agents-eps | 4.2.1-88.el8 | |
fence-agents-heuristics-ping | 4.2.1-88.el8 | |
fence-agents-hpblade | 4.2.1-88.el8 | |
fence-agents-ibmblade | 4.2.1-88.el8 | |
fence-agents-ifmib | 4.2.1-88.el8 | |
fence-agents-ilo-moonshot | 4.2.1-88.el8 | |
fence-agents-ilo-mp | 4.2.1-88.el8 | |
fence-agents-ilo-ssh | 4.2.1-88.el8 | |
fence-agents-ilo2 | 4.2.1-88.el8 | |
fence-agents-intelmodular | 4.2.1-88.el8 | |
fence-agents-ipdu | 4.2.1-88.el8 | |
fence-agents-ipmilan | 4.2.1-88.el8 | |
fence-agents-kdump | 4.2.1-88.el8 | |
fence-agents-lpar | 4.2.1-88.el8 | |
fence-agents-mpath | 4.2.1-88.el8 | |
fence-agents-redfish | 4.2.1-88.el8 | |
fence-agents-rhevm | 4.2.1-88.el8 | |
fence-agents-rsa | 4.2.1-88.el8 | |
fence-agents-rsb | 4.2.1-88.el8 | |
fence-agents-sbd | 4.2.1-88.el8 | |
fence-agents-scsi | 4.2.1-88.el8 | |
fence-agents-virsh | 4.2.1-88.el8 | |
fence-agents-vmware-rest | 4.2.1-88.el8 | |
fence-agents-vmware-soap | 4.2.1-88.el8 | |
fence-agents-wti | 4.2.1-88.el8 | |
firefox | 91.6.0-1.el8_5 | [RHSA-2022:0510](https://access.redhat.com/errata/RHSA-2022:0510) | <div class="adv_s">Security Advisory</div>
libosinfo | 1.9.0-3.el8 | |
libreoffice-base | 6.4.7.2-10.el8 | |
libreoffice-calc | 6.4.7.2-10.el8 | |
libreoffice-core | 6.4.7.2-10.el8 | |
libreoffice-data | 6.4.7.2-10.el8 | |
libreoffice-draw | 6.4.7.2-10.el8 | |
libreoffice-emailmerge | 6.4.7.2-10.el8 | |
libreoffice-filters | 6.4.7.2-10.el8 | |
libreoffice-gdb-debug-support | 6.4.7.2-10.el8 | |
libreoffice-graphicfilter | 6.4.7.2-10.el8 | |
libreoffice-gtk3 | 6.4.7.2-10.el8 | |
libreoffice-help-ar | 6.4.7.2-10.el8 | |
libreoffice-help-bg | 6.4.7.2-10.el8 | |
libreoffice-help-bn | 6.4.7.2-10.el8 | |
libreoffice-help-ca | 6.4.7.2-10.el8 | |
libreoffice-help-cs | 6.4.7.2-10.el8 | |
libreoffice-help-da | 6.4.7.2-10.el8 | |
libreoffice-help-de | 6.4.7.2-10.el8 | |
libreoffice-help-dz | 6.4.7.2-10.el8 | |
libreoffice-help-el | 6.4.7.2-10.el8 | |
libreoffice-help-en | 6.4.7.2-10.el8 | |
libreoffice-help-es | 6.4.7.2-10.el8 | |
libreoffice-help-et | 6.4.7.2-10.el8 | |
libreoffice-help-eu | 6.4.7.2-10.el8 | |
libreoffice-help-fi | 6.4.7.2-10.el8 | |
libreoffice-help-fr | 6.4.7.2-10.el8 | |
libreoffice-help-gl | 6.4.7.2-10.el8 | |
libreoffice-help-gu | 6.4.7.2-10.el8 | |
libreoffice-help-he | 6.4.7.2-10.el8 | |
libreoffice-help-hi | 6.4.7.2-10.el8 | |
libreoffice-help-hr | 6.4.7.2-10.el8 | |
libreoffice-help-hu | 6.4.7.2-10.el8 | |
libreoffice-help-id | 6.4.7.2-10.el8 | |
libreoffice-help-it | 6.4.7.2-10.el8 | |
libreoffice-help-ja | 6.4.7.2-10.el8 | |
libreoffice-help-ko | 6.4.7.2-10.el8 | |
libreoffice-help-lt | 6.4.7.2-10.el8 | |
libreoffice-help-lv | 6.4.7.2-10.el8 | |
libreoffice-help-nb | 6.4.7.2-10.el8 | |
libreoffice-help-nl | 6.4.7.2-10.el8 | |
libreoffice-help-nn | 6.4.7.2-10.el8 | |
libreoffice-help-pl | 6.4.7.2-10.el8 | |
libreoffice-help-pt-BR | 6.4.7.2-10.el8 | |
libreoffice-help-pt-PT | 6.4.7.2-10.el8 | |
libreoffice-help-ro | 6.4.7.2-10.el8 | |
libreoffice-help-ru | 6.4.7.2-10.el8 | |
libreoffice-help-si | 6.4.7.2-10.el8 | |
libreoffice-help-sk | 6.4.7.2-10.el8 | |
libreoffice-help-sl | 6.4.7.2-10.el8 | |
libreoffice-help-sv | 6.4.7.2-10.el8 | |
libreoffice-help-ta | 6.4.7.2-10.el8 | |
libreoffice-help-tr | 6.4.7.2-10.el8 | |
libreoffice-help-uk | 6.4.7.2-10.el8 | |
libreoffice-help-zh-Hans | 6.4.7.2-10.el8 | |
libreoffice-help-zh-Hant | 6.4.7.2-10.el8 | |
libreoffice-impress | 6.4.7.2-10.el8 | |
libreoffice-langpack-af | 6.4.7.2-10.el8 | |
libreoffice-langpack-ar | 6.4.7.2-10.el8 | |
libreoffice-langpack-as | 6.4.7.2-10.el8 | |
libreoffice-langpack-bg | 6.4.7.2-10.el8 | |
libreoffice-langpack-bn | 6.4.7.2-10.el8 | |
libreoffice-langpack-br | 6.4.7.2-10.el8 | |
libreoffice-langpack-ca | 6.4.7.2-10.el8 | |
libreoffice-langpack-cs | 6.4.7.2-10.el8 | |
libreoffice-langpack-cy | 6.4.7.2-10.el8 | |
libreoffice-langpack-da | 6.4.7.2-10.el8 | |
libreoffice-langpack-de | 6.4.7.2-10.el8 | |
libreoffice-langpack-dz | 6.4.7.2-10.el8 | |
libreoffice-langpack-el | 6.4.7.2-10.el8 | |
libreoffice-langpack-en | 6.4.7.2-10.el8 | |
libreoffice-langpack-es | 6.4.7.2-10.el8 | |
libreoffice-langpack-et | 6.4.7.2-10.el8 | |
libreoffice-langpack-eu | 6.4.7.2-10.el8 | |
libreoffice-langpack-fa | 6.4.7.2-10.el8 | |
libreoffice-langpack-fi | 6.4.7.2-10.el8 | |
libreoffice-langpack-fr | 6.4.7.2-10.el8 | |
libreoffice-langpack-ga | 6.4.7.2-10.el8 | |
libreoffice-langpack-gl | 6.4.7.2-10.el8 | |
libreoffice-langpack-gu | 6.4.7.2-10.el8 | |
libreoffice-langpack-he | 6.4.7.2-10.el8 | |
libreoffice-langpack-hi | 6.4.7.2-10.el8 | |
libreoffice-langpack-hr | 6.4.7.2-10.el8 | |
libreoffice-langpack-hu | 6.4.7.2-10.el8 | |
libreoffice-langpack-id | 6.4.7.2-10.el8 | |
libreoffice-langpack-it | 6.4.7.2-10.el8 | |
libreoffice-langpack-ja | 6.4.7.2-10.el8 | |
libreoffice-langpack-kk | 6.4.7.2-10.el8 | |
libreoffice-langpack-kn | 6.4.7.2-10.el8 | |
libreoffice-langpack-ko | 6.4.7.2-10.el8 | |
libreoffice-langpack-lt | 6.4.7.2-10.el8 | |
libreoffice-langpack-lv | 6.4.7.2-10.el8 | |
libreoffice-langpack-mai | 6.4.7.2-10.el8 | |
libreoffice-langpack-ml | 6.4.7.2-10.el8 | |
libreoffice-langpack-mr | 6.4.7.2-10.el8 | |
libreoffice-langpack-nb | 6.4.7.2-10.el8 | |
libreoffice-langpack-nl | 6.4.7.2-10.el8 | |
libreoffice-langpack-nn | 6.4.7.2-10.el8 | |
libreoffice-langpack-nr | 6.4.7.2-10.el8 | |
libreoffice-langpack-nso | 6.4.7.2-10.el8 | |
libreoffice-langpack-or | 6.4.7.2-10.el8 | |
libreoffice-langpack-pa | 6.4.7.2-10.el8 | |
libreoffice-langpack-pl | 6.4.7.2-10.el8 | |
libreoffice-langpack-pt-BR | 6.4.7.2-10.el8 | |
libreoffice-langpack-pt-PT | 6.4.7.2-10.el8 | |
libreoffice-langpack-ro | 6.4.7.2-10.el8 | |
libreoffice-langpack-ru | 6.4.7.2-10.el8 | |
libreoffice-langpack-si | 6.4.7.2-10.el8 | |
libreoffice-langpack-sk | 6.4.7.2-10.el8 | |
libreoffice-langpack-sl | 6.4.7.2-10.el8 | |
libreoffice-langpack-sr | 6.4.7.2-10.el8 | |
libreoffice-langpack-ss | 6.4.7.2-10.el8 | |
libreoffice-langpack-st | 6.4.7.2-10.el8 | |
libreoffice-langpack-sv | 6.4.7.2-10.el8 | |
libreoffice-langpack-ta | 6.4.7.2-10.el8 | |
libreoffice-langpack-te | 6.4.7.2-10.el8 | |
libreoffice-langpack-th | 6.4.7.2-10.el8 | |
libreoffice-langpack-tn | 6.4.7.2-10.el8 | |
libreoffice-langpack-tr | 6.4.7.2-10.el8 | |
libreoffice-langpack-ts | 6.4.7.2-10.el8 | |
libreoffice-langpack-uk | 6.4.7.2-10.el8 | |
libreoffice-langpack-ve | 6.4.7.2-10.el8 | |
libreoffice-langpack-xh | 6.4.7.2-10.el8 | |
libreoffice-langpack-zh-Hans | 6.4.7.2-10.el8 | |
libreoffice-langpack-zh-Hant | 6.4.7.2-10.el8 | |
libreoffice-langpack-zu | 6.4.7.2-10.el8 | |
libreoffice-math | 6.4.7.2-10.el8 | |
libreoffice-ogltrans | 6.4.7.2-10.el8 | |
libreoffice-opensymbol-fonts | 6.4.7.2-10.el8 | |
libreoffice-pdfimport | 6.4.7.2-10.el8 | |
libreoffice-pyuno | 6.4.7.2-10.el8 | |
libreoffice-ure | 6.4.7.2-10.el8 | |
libreoffice-ure-common | 6.4.7.2-10.el8 | |
libreoffice-wiki-publisher | 6.4.7.2-10.el8 | |
libreoffice-writer | 6.4.7.2-10.el8 | |
libreoffice-x11 | 6.4.7.2-10.el8 | |
libreoffice-xsltfilter | 6.4.7.2-10.el8 | |
libreofficekit | 6.4.7.2-10.el8 | |
nmstate | 1.2.1-1.el8 | |
nmstate-libs | 1.2.1-1.el8 | |
nmstate-plugin-ovsdb | 1.2.1-1.el8 | |
python2 | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-debug | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-devel | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-funcsigs | 1.0.2-13.module_el8.6.0+1079+8d338a31 | |
python2-libs | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-lxml | 4.2.3-6.module_el8.6.0+1079+8d338a31 | |
python2-test | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-tkinter | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-tools | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python3-blivet | 3.4.0-9.el8 | |
python3-libnmstate | 1.2.1-1.el8 | |
python38-resolvelib | 0.5.4-5.el8 | |
rhel-system-roles | 1.13.1-1.el8 | |
sevctl | 0.2.0-1.el8 | |
squid | 4.15-3.module_el8.6.0+1091+f66ed1aa | |
thunderbird | 91.6.0-1.el8_5 | [RHSA-2022:0535](https://access.redhat.com/errata/RHSA-2022:0535) | <div class="adv_s">Security Advisory</div>
tuned-gtk | 2.18.0-2.el8 | |
tuned-utils | 2.18.0-2.el8 | |
tuned-utils-systemtap | 2.18.0-2.el8 | |
valgrind | 3.18.1-7.el8 | |
valgrind-devel | 3.18.1-7.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-5.0-source-built-artifacts | 5.0.211-1.el8_5 | [RHSA-2022:0495](https://access.redhat.com/errata/RHSA-2022:0495) | <div class="adv_s">Security Advisory</div>
gflags | 2.2.2-1.el8 | |
gflags-devel | 2.2.2-1.el8 | |
glog | 0.3.5-4.el8 | |
glog-devel | 0.3.5-4.el8 | |
iproute-devel | 5.15.0-3.el8 | |
libreoffice-sdk | 6.4.7.2-10.el8 | |
libreoffice-sdk-doc | 6.4.7.2-10.el8 | |
nmstate-devel | 1.2.1-1.el8 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-nautilus | 1.3-2.el8 | |
centos-release-ceph-octopus | 1.1-2.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-88.el8 | |
fence-agents-aws | 4.2.1-88.el8 | |
fence-agents-azure-arm | 4.2.1-88.el8 | |
fence-agents-gce | 4.2.1-88.el8 | |
pcs | 0.10.12-6.el8 | |
pcs-snmp | 0.10.12-6.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tuned-profiles-realtime | 2.18.0-2.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-88.el8 | |
fence-agents-aws | 4.2.1-88.el8 | |
fence-agents-azure-arm | 4.2.1-88.el8 | |
fence-agents-gce | 4.2.1-88.el8 | |
pcs | 0.10.12-6.el8 | |
pcs-snmp | 0.10.12-6.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-dracut-conf | 1.0-1.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-libs | 2.2.6-44.el8 | |
iproute | 5.15.0-3.el8 | |
iproute-tc | 5.15.0-3.el8 | |
kmod-kvdo | 6.2.6.14-83.el8 | |
ksc | 1.9-1.el8 | |
tuned | 2.18.0-2.el8 | |
tuned-profiles-atomic | 2.18.0-2.el8 | |
tuned-profiles-compat | 2.18.0-2.el8 | |
tuned-profiles-cpu-partitioning | 2.18.0-2.el8 | |
tuned-profiles-mssql | 2.18.0-2.el8 | |
tuned-profiles-oracle | 2.18.0-2.el8 | |
wpa_supplicant | 2.10-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.6.1-3.el8 | |
alsa-lib-devel | 1.2.6.1-3.el8 | |
alsa-ucm | 1.2.6.1-3.el8 | |
annobin | 10.29-3.el8 | |
annobin-annocheck | 10.29-3.el8 | |
ansible-core | 2.12.2-2.el8 | |
ansible-test | 2.12.2-2.el8 | |
autocorr-en | 6.4.7.2-10.el8 | |
blivet-data | 3.4.0-9.el8 | |
cups | 2.2.6-44.el8 | |
cups-client | 2.2.6-44.el8 | |
cups-devel | 2.2.6-44.el8 | |
cups-filesystem | 2.2.6-44.el8 | |
cups-ipptool | 2.2.6-44.el8 | |
cups-lpd | 2.2.6-44.el8 | |
fence-agents-all | 4.2.1-88.el8 | |
fence-agents-amt-ws | 4.2.1-88.el8 | |
fence-agents-apc | 4.2.1-88.el8 | |
fence-agents-apc-snmp | 4.2.1-88.el8 | |
fence-agents-bladecenter | 4.2.1-88.el8 | |
fence-agents-brocade | 4.2.1-88.el8 | |
fence-agents-cisco-mds | 4.2.1-88.el8 | |
fence-agents-cisco-ucs | 4.2.1-88.el8 | |
fence-agents-common | 4.2.1-88.el8 | |
fence-agents-compute | 4.2.1-88.el8 | |
fence-agents-drac5 | 4.2.1-88.el8 | |
fence-agents-eaton-snmp | 4.2.1-88.el8 | |
fence-agents-emerson | 4.2.1-88.el8 | |
fence-agents-eps | 4.2.1-88.el8 | |
fence-agents-heuristics-ping | 4.2.1-88.el8 | |
fence-agents-hpblade | 4.2.1-88.el8 | |
fence-agents-ibmblade | 4.2.1-88.el8 | |
fence-agents-ifmib | 4.2.1-88.el8 | |
fence-agents-ilo-moonshot | 4.2.1-88.el8 | |
fence-agents-ilo-mp | 4.2.1-88.el8 | |
fence-agents-ilo-ssh | 4.2.1-88.el8 | |
fence-agents-ilo2 | 4.2.1-88.el8 | |
fence-agents-intelmodular | 4.2.1-88.el8 | |
fence-agents-ipdu | 4.2.1-88.el8 | |
fence-agents-ipmilan | 4.2.1-88.el8 | |
fence-agents-kdump | 4.2.1-88.el8 | |
fence-agents-mpath | 4.2.1-88.el8 | |
fence-agents-redfish | 4.2.1-88.el8 | |
fence-agents-rhevm | 4.2.1-88.el8 | |
fence-agents-rsa | 4.2.1-88.el8 | |
fence-agents-rsb | 4.2.1-88.el8 | |
fence-agents-sbd | 4.2.1-88.el8 | |
fence-agents-scsi | 4.2.1-88.el8 | |
fence-agents-virsh | 4.2.1-88.el8 | |
fence-agents-vmware-rest | 4.2.1-88.el8 | |
fence-agents-vmware-soap | 4.2.1-88.el8 | |
fence-agents-wti | 4.2.1-88.el8 | |
firefox | 91.6.0-1.el8_5 | [RHSA-2022:0510](https://access.redhat.com/errata/RHSA-2022:0510) | <div class="adv_s">Security Advisory</div>
libabw | 0.1.2-2.el8 | |
libcdr | 0.1.4-4.el8 | |
libepubgen | 0.1.0-3.el8 | [RHSA-2021:1586](https://access.redhat.com/errata/RHSA-2021:1586) | <div class="adv_s">Security Advisory</div>
libetonyek | 0.1.8-1.el8 | |
libfreehand | 0.1.2-2.el8 | |
libmspub | 0.1.4-1.el8 | |
libmwaw | 0.3.14-1.el8 | |
libosinfo | 1.9.0-3.el8 | |
libpagemaker | 0.0.4-3.el8 | |
libqxp | 0.0.1-2.el8 | |
libreoffice-calc | 6.4.7.2-10.el8 | |
libreoffice-core | 6.4.7.2-10.el8 | |
libreoffice-data | 6.4.7.2-10.el8 | |
libreoffice-gdb-debug-support | 6.4.7.2-10.el8 | |
libreoffice-graphicfilter | 6.4.7.2-10.el8 | |
libreoffice-help-en | 6.4.7.2-10.el8 | |
libreoffice-impress | 6.4.7.2-10.el8 | |
libreoffice-langpack-en | 6.4.7.2-10.el8 | |
libreoffice-ogltrans | 6.4.7.2-10.el8 | |
libreoffice-opensymbol-fonts | 6.4.7.2-10.el8 | |
libreoffice-pdfimport | 6.4.7.2-10.el8 | |
libreoffice-pyuno | 6.4.7.2-10.el8 | |
libreoffice-ure | 6.4.7.2-10.el8 | |
libreoffice-ure-common | 6.4.7.2-10.el8 | |
libreoffice-writer | 6.4.7.2-10.el8 | |
libreoffice-x11 | 6.4.7.2-10.el8 | |
libstaroffice | 0.0.6-1.el8 | |
libvisio | 0.1.6-2.el8 | |
libwps | 0.4.9-1.el8 | |
libzmf | 0.0.2-3.el8 | |
lpsolve | 5.5.2.0-21.el8 | [RHEA-2021:4286](https://access.redhat.com/errata/RHEA-2021:4286) | <div class="adv_e">Product Enhancement Advisory</div>
nmstate | 1.2.1-1.el8 | |
nmstate-libs | 1.2.1-1.el8 | |
nmstate-plugin-ovsdb | 1.2.1-1.el8 | |
python2 | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-debug | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-devel | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-funcsigs | 1.0.2-13.module_el8.6.0+1079+8d338a31 | |
python2-libs | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-lxml | 4.2.3-6.module_el8.6.0+1079+8d338a31 | |
python2-test | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-tkinter | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python2-tools | 2.7.18-10.module_el8.6.0+1092+a03304bb | |
python3-blivet | 3.4.0-9.el8 | |
python3-libnmstate | 1.2.1-1.el8 | |
python38-resolvelib | 0.5.4-5.el8 | |
rhel-system-roles | 1.13.1-1.el8 | |
squid | 4.15-3.module_el8.6.0+1091+f66ed1aa | |
thunderbird | 91.6.0-1.el8_5 | [RHSA-2022:0535](https://access.redhat.com/errata/RHSA-2022:0535) | <div class="adv_s">Security Advisory</div>
tuned-gtk | 2.18.0-2.el8 | |
tuned-utils | 2.18.0-2.el8 | |
tuned-utils-systemtap | 2.18.0-2.el8 | |
valgrind | 3.18.1-7.el8 | |
valgrind-devel | 3.18.1-7.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gflags | 2.2.2-1.el8 | |
gflags-devel | 2.2.2-1.el8 | |
glog | 0.3.5-4.el8 | |
glog-devel | 0.3.5-4.el8 | |
iproute-devel | 5.15.0-3.el8 | |
nmstate-devel | 1.2.1-1.el8 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-nautilus | 1.3-2.el8 | |
centos-release-ceph-octopus | 1.1-2.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-azure-arm | 4.2.1-88.el8 | |
fence-agents-gce | 4.2.1-88.el8 | |
pcs | 0.10.12-6.el8 | |
pcs-snmp | 0.10.12-6.el8 | |

