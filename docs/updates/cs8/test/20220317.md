## 2022-03-17

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic-inspector | 10.1.3-1.el8 | |
openstack-ironic-inspector-api | 10.1.3-1.el8 | |
openstack-ironic-inspector-conductor | 10.1.3-1.el8 | |
openstack-ironic-inspector-dnsmasq | 10.1.3-1.el8 | |
openstack-ironic-inspector-doc | 10.1.3-1.el8 | |
openstack-ironic-python-agent | 6.1.3-1.el8 | |
openstack-ironic-ui | 4.0.1-1.el8 | |
openstack-ironic-ui-doc | 4.0.1-1.el8 | |
python-ironic-python-agent-doc | 6.1.3-1.el8 | |
python3-ironic-inspector-tests | 10.1.3-1.el8 | |
python3-ironic-python-agent | 6.1.3-1.el8 | |
python3-os-brick | 4.0.5-1.el8 | |
python3-validations-libs | 1.4.0-1.el8 | |
validations-common | 1.2.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic-inspector | 10.1.3-1.el8 | |
openstack-ironic-inspector-api | 10.1.3-1.el8 | |
openstack-ironic-inspector-conductor | 10.1.3-1.el8 | |
openstack-ironic-inspector-dnsmasq | 10.1.3-1.el8 | |
openstack-ironic-inspector-doc | 10.1.3-1.el8 | |
openstack-ironic-python-agent | 6.1.3-1.el8 | |
openstack-ironic-ui | 4.0.1-1.el8 | |
openstack-ironic-ui-doc | 4.0.1-1.el8 | |
python-ironic-python-agent-doc | 6.1.3-1.el8 | |
python3-ironic-inspector-tests | 10.1.3-1.el8 | |
python3-ironic-python-agent | 6.1.3-1.el8 | |
python3-os-brick | 4.0.5-1.el8 | |
python3-validations-libs | 1.4.0-1.el8 | |
validations-common | 1.2.0-1.el8 | |

