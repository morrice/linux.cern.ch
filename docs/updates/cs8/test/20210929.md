## 2021-09-29

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 16.4.1-1.el8 | |
python3-cinder | 16.4.1-1.el8 | |
python3-cinder-tests | 16.4.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 16.4.1-1.el8 | |
python3-cinder | 16.4.1-1.el8 | |
python3-cinder-tests | 16.4.1-1.el8 | |

