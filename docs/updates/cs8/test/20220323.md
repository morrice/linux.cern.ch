## 2022-03-23

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-373.el8 | |
kernel | 4.18.0-373.el8 | |
kernel-abi-stablelists | 4.18.0-373.el8 | |
kernel-core | 4.18.0-373.el8 | |
kernel-cross-headers | 4.18.0-373.el8 | |
kernel-debug | 4.18.0-373.el8 | |
kernel-debug-core | 4.18.0-373.el8 | |
kernel-debug-devel | 4.18.0-373.el8 | |
kernel-debug-modules | 4.18.0-373.el8 | |
kernel-debug-modules-extra | 4.18.0-373.el8 | |
kernel-devel | 4.18.0-373.el8 | |
kernel-doc | 4.18.0-373.el8 | |
kernel-headers | 4.18.0-373.el8 | |
kernel-modules | 4.18.0-373.el8 | |
kernel-modules-extra | 4.18.0-373.el8 | |
kernel-tools | 4.18.0-373.el8 | |
kernel-tools-libs | 4.18.0-373.el8 | |
perf | 4.18.0-373.el8 | |
polkit | 0.115-13.el8.2 | |
polkit-devel | 0.115-13.el8.2 | |
polkit-docs | 0.115-13.el8.2 | |
polkit-libs | 0.115-13.el8.2 | |
python3-perf | 4.18.0-373.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-373.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-373.el8 | |
kernel | 4.18.0-373.el8 | |
kernel-abi-stablelists | 4.18.0-373.el8 | |
kernel-core | 4.18.0-373.el8 | |
kernel-cross-headers | 4.18.0-373.el8 | |
kernel-debug | 4.18.0-373.el8 | |
kernel-debug-core | 4.18.0-373.el8 | |
kernel-debug-devel | 4.18.0-373.el8 | |
kernel-debug-modules | 4.18.0-373.el8 | |
kernel-debug-modules-extra | 4.18.0-373.el8 | |
kernel-devel | 4.18.0-373.el8 | |
kernel-doc | 4.18.0-373.el8 | |
kernel-headers | 4.18.0-373.el8 | |
kernel-modules | 4.18.0-373.el8 | |
kernel-modules-extra | 4.18.0-373.el8 | |
kernel-tools | 4.18.0-373.el8 | |
kernel-tools-libs | 4.18.0-373.el8 | |
perf | 4.18.0-373.el8 | |
polkit | 0.115-13.el8.2 | |
polkit-devel | 0.115-13.el8.2 | |
polkit-docs | 0.115-13.el8.2 | |
polkit-libs | 0.115-13.el8.2 | |
python3-perf | 4.18.0-373.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-373.el8 | |

