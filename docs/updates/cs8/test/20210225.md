## 2021-02-25

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openafs-release | 1.1-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openafs-release | 1.1-1.el8s.cern | |

