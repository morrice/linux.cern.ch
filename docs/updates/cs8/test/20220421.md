## 2022-04-21

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 48-1.3.el8s | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-containers-podman | 1.9.3-1.el8 | |
openstack-cinder | 19.1.0-1.el8 | |
openstack-manila | 11.1.2-1.el8 | |
openstack-manila | 13.0.3-1.el8 | |
openstack-manila-doc | 11.1.2-1.el8 | |
openstack-manila-doc | 13.0.3-1.el8 | |
openstack-manila-share | 11.1.2-1.el8 | |
openstack-manila-share | 13.0.3-1.el8 | |
openstack-nova | 24.1.0-1.el8 | |
openstack-nova-api | 24.1.0-1.el8 | |
openstack-nova-common | 24.1.0-1.el8 | |
openstack-nova-compute | 24.1.0-1.el8 | |
openstack-nova-conductor | 24.1.0-1.el8 | |
openstack-nova-migration | 24.1.0-1.el8 | |
openstack-nova-novncproxy | 24.1.0-1.el8 | |
openstack-nova-scheduler | 24.1.0-1.el8 | |
openstack-nova-serialproxy | 24.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 24.1.0-1.el8 | |
openstack-tempest | 30.1.0-1.el8 | |
openstack-tempest-all | 30.1.0-1.el8 | |
openstack-tempest-doc | 30.1.0-1.el8 | |
puppet-tempest | 19.5.0-1.el8 | |
python-manila-tests-tempest-doc | 1.7.0-1.el8 | |
python-oslo-cache-doc | 2.8.2-1.el8 | |
python-oslo-cache-lang | 2.8.2-1.el8 | |
python-oslo-messaging-doc | 12.9.3-1.el8 | |
python-testtools-doc | 2.5.0-2.el8 | |
python3-barbican-tests-tempest | 1.6.0-1.el8 | |
python3-cinder | 19.1.0-1.el8 | |
python3-cinder-common | 19.1.0-1.el8 | |
python3-cinder-tests | 19.1.0-1.el8 | |
python3-cinder-tests-tempest | 1.6.0-1.el8 | |
python3-glance-tests-tempest | 0.3.0-1.el8 | |
python3-manila | 11.1.2-1.el8 | |
python3-manila | 13.0.3-1.el8 | |
python3-manila-tests | 11.1.2-1.el8 | |
python3-manila-tests | 13.0.3-1.el8 | |
python3-manila-tests-tempest | 1.7.0-1.el8 | |
python3-nova | 24.1.0-1.el8 | |
python3-nova-tests | 24.1.0-1.el8 | |
python3-os-brick | 5.0.2-1.el8 | |
python3-oslo-cache | 2.8.2-1.el8 | |
python3-oslo-cache-tests | 2.8.2-1.el8 | |
python3-oslo-messaging | 12.9.3-1.el8 | |
python3-oslo-messaging-tests | 12.9.3-1.el8 | |
python3-tempest | 30.1.0-1.el8 | |
python3-tempest-tests | 30.1.0-1.el8 | |
python3-testtools | 2.5.0-2.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 48-1.3.el8s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-containers-podman | 1.9.3-1.el8 | |
openstack-cinder | 19.1.0-1.el8 | |
openstack-manila | 11.1.2-1.el8 | |
openstack-manila | 13.0.3-1.el8 | |
openstack-manila-doc | 11.1.2-1.el8 | |
openstack-manila-doc | 13.0.3-1.el8 | |
openstack-manila-share | 11.1.2-1.el8 | |
openstack-manila-share | 13.0.3-1.el8 | |
openstack-nova | 24.1.0-1.el8 | |
openstack-nova-api | 24.1.0-1.el8 | |
openstack-nova-common | 24.1.0-1.el8 | |
openstack-nova-compute | 24.1.0-1.el8 | |
openstack-nova-conductor | 24.1.0-1.el8 | |
openstack-nova-migration | 24.1.0-1.el8 | |
openstack-nova-novncproxy | 24.1.0-1.el8 | |
openstack-nova-scheduler | 24.1.0-1.el8 | |
openstack-nova-serialproxy | 24.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 24.1.0-1.el8 | |
openstack-tempest | 30.1.0-1.el8 | |
openstack-tempest-all | 30.1.0-1.el8 | |
openstack-tempest-doc | 30.1.0-1.el8 | |
puppet-tempest | 19.5.0-1.el8 | |
python-manila-tests-tempest-doc | 1.7.0-1.el8 | |
python-oslo-cache-doc | 2.8.2-1.el8 | |
python-oslo-cache-lang | 2.8.2-1.el8 | |
python-oslo-messaging-doc | 12.9.3-1.el8 | |
python-testtools-doc | 2.5.0-2.el8 | |
python3-barbican-tests-tempest | 1.6.0-1.el8 | |
python3-cinder | 19.1.0-1.el8 | |
python3-cinder-common | 19.1.0-1.el8 | |
python3-cinder-tests | 19.1.0-1.el8 | |
python3-cinder-tests-tempest | 1.6.0-1.el8 | |
python3-glance-tests-tempest | 0.3.0-1.el8 | |
python3-manila | 11.1.2-1.el8 | |
python3-manila | 13.0.3-1.el8 | |
python3-manila-tests | 11.1.2-1.el8 | |
python3-manila-tests | 13.0.3-1.el8 | |
python3-manila-tests-tempest | 1.7.0-1.el8 | |
python3-nova | 24.1.0-1.el8 | |
python3-nova-tests | 24.1.0-1.el8 | |
python3-os-brick | 5.0.2-1.el8 | |
python3-oslo-cache | 2.8.2-1.el8 | |
python3-oslo-cache-tests | 2.8.2-1.el8 | |
python3-oslo-messaging | 12.9.3-1.el8 | |
python3-oslo-messaging-tests | 12.9.3-1.el8 | |
python3-tempest | 30.1.0-1.el8 | |
python3-tempest-tests | 30.1.0-1.el8 | |
python3-testtools | 2.5.0-2.el8 | |

