## 2023-02-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.el8s.cern | |
pyphonebook | 2.1.5-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-glance | 23.1.0-1.el8 | |
openstack-glance-doc | 23.1.0-1.el8 | |
openstack-neutron | 19.5.0-1.el8 | |
openstack-neutron-common | 19.5.0-1.el8 | |
openstack-neutron-linuxbridge | 19.5.0-1.el8 | |
openstack-neutron-macvtap-agent | 19.5.0-1.el8 | |
openstack-neutron-metering-agent | 19.5.0-1.el8 | |
openstack-neutron-ml2 | 19.5.0-1.el8 | |
openstack-neutron-ml2ovn-trace | 19.5.0-1.el8 | |
openstack-neutron-openvswitch | 19.5.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 19.5.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 19.5.0-1.el8 | |
openstack-neutron-rpc-server | 19.5.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 19.5.0-1.el8 | |
openstack-trove | 16.0.1-1.el8 | |
openstack-trove-api | 16.0.1-1.el8 | |
openstack-trove-common | 16.0.1-1.el8 | |
openstack-trove-conductor | 16.0.1-1.el8 | |
openstack-trove-guestagent | 16.0.1-1.el8 | |
openstack-trove-taskmanager | 16.0.1-1.el8 | |
python-ironic-tests-tempest-doc | 2.6.0-1.el8 | |
python-openstackclient-doc | 5.6.2-1.el8 | |
python-openstackclient-lang | 5.6.2-1.el8 | |
python-ovsdbapp-doc | 1.12.3-1.el8 | |
python3-cinder-tests-tempest | 1.8.0-1.el8 | |
python3-glance | 23.1.0-1.el8 | |
python3-glance-tests | 23.1.0-1.el8 | |
python3-ironic-tests-tempest | 2.6.0-1.el8 | |
python3-neutron | 19.5.0-1.el8 | |
python3-neutron-tests | 19.5.0-1.el8 | |
python3-openstackclient | 5.6.2-1.el8 | |
python3-ovsdbapp | 1.12.3-1.el8 | |
python3-ovsdbapp-tests | 1.12.3-1.el8 | |
python3-trove | 16.0.1-1.el8 | |
python3-trove-tests | 16.0.1-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.4-1.el8s.cern | |
pyphonebook | 2.1.5-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-glance | 23.1.0-1.el8 | |
openstack-glance-doc | 23.1.0-1.el8 | |
openstack-neutron | 19.5.0-1.el8 | |
openstack-neutron-common | 19.5.0-1.el8 | |
openstack-neutron-linuxbridge | 19.5.0-1.el8 | |
openstack-neutron-macvtap-agent | 19.5.0-1.el8 | |
openstack-neutron-metering-agent | 19.5.0-1.el8 | |
openstack-neutron-ml2 | 19.5.0-1.el8 | |
openstack-neutron-ml2ovn-trace | 19.5.0-1.el8 | |
openstack-neutron-openvswitch | 19.5.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 19.5.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 19.5.0-1.el8 | |
openstack-neutron-rpc-server | 19.5.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 19.5.0-1.el8 | |
openstack-trove | 16.0.1-1.el8 | |
openstack-trove-api | 16.0.1-1.el8 | |
openstack-trove-common | 16.0.1-1.el8 | |
openstack-trove-conductor | 16.0.1-1.el8 | |
openstack-trove-guestagent | 16.0.1-1.el8 | |
openstack-trove-taskmanager | 16.0.1-1.el8 | |
python-ironic-tests-tempest-doc | 2.6.0-1.el8 | |
python-openstackclient-doc | 5.6.2-1.el8 | |
python-openstackclient-lang | 5.6.2-1.el8 | |
python-ovsdbapp-doc | 1.12.3-1.el8 | |
python3-cinder-tests-tempest | 1.8.0-1.el8 | |
python3-glance | 23.1.0-1.el8 | |
python3-glance-tests | 23.1.0-1.el8 | |
python3-ironic-tests-tempest | 2.6.0-1.el8 | |
python3-neutron | 19.5.0-1.el8 | |
python3-neutron-tests | 19.5.0-1.el8 | |
python3-openstackclient | 5.6.2-1.el8 | |
python3-ovsdbapp | 1.12.3-1.el8 | |
python3-ovsdbapp-tests | 1.12.3-1.el8 | |
python3-trove | 16.0.1-1.el8 | |
python3-trove-tests | 16.0.1-1.el8 | |

