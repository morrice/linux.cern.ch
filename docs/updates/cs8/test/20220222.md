## 2022-02-22

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.27-4.el8 | |
ansible-doc | 2.9.27-4.el8 | |
ansible-test | 2.9.27-4.el8 | |
openstack-tempest | 28.1.0-2.el8 | |
openstack-tempest-all | 28.1.0-2.el8 | |
openstack-tempest-doc | 28.1.0-2.el8 | |
puppet-aodh | 18.4.1-1.el8 | |
puppet-barbican | 18.4.1-1.el8 | |
puppet-ceilometer | 18.4.1-1.el8 | |
puppet-cinder | 18.5.0-1.el8 | |
puppet-designate | 18.5.0-1.el8 | |
puppet-glance | 18.5.0-1.el8 | |
puppet-gnocchi | 18.4.1-1.el8 | |
puppet-horizon | 18.5.0-1.el8 | |
puppet-ironic | 18.6.0-1.el8 | |
puppet-keystone | 18.5.0-1.el8 | |
puppet-magnum | 18.4.1-1.el8 | |
puppet-manila | 18.5.0-1.el8 | |
puppet-neutron | 18.5.0-1.el8 | |
puppet-nova | 18.5.0-1.el8 | |
puppet-octavia | 18.4.1-1.el8 | |
puppet-openstacklib | 18.5.0-1.el8 | |
puppet-openstack_extras | 18.5.0-1.el8 | |
puppet-oslo | 18.4.1-1.el8 | |
puppet-ovn | 18.5.0-1.el8 | |
puppet-placement | 5.4.1-1.el8 | |
puppet-swift | 18.5.0-1.el8 | |
puppet-vswitch | 14.4.1-1.el8 | |
python-oslo-cache-lang | 2.6.2-1.el8 | |
python3-oslo-cache | 2.6.2-1.el8 | |
python3-oslo-cache-tests | 2.6.2-1.el8 | |
python3-tempest | 28.1.0-2.el8 | |
python3-tempest-tests | 28.1.0-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.27-4.el8 | |
ansible-doc | 2.9.27-4.el8 | |
ansible-test | 2.9.27-4.el8 | |
openstack-tempest | 28.1.0-2.el8 | |
openstack-tempest-all | 28.1.0-2.el8 | |
openstack-tempest-doc | 28.1.0-2.el8 | |
puppet-aodh | 18.4.1-1.el8 | |
puppet-barbican | 18.4.1-1.el8 | |
puppet-ceilometer | 18.4.1-1.el8 | |
puppet-cinder | 18.5.0-1.el8 | |
puppet-designate | 18.5.0-1.el8 | |
puppet-glance | 18.5.0-1.el8 | |
puppet-gnocchi | 18.4.1-1.el8 | |
puppet-horizon | 18.5.0-1.el8 | |
puppet-ironic | 18.6.0-1.el8 | |
puppet-keystone | 18.5.0-1.el8 | |
puppet-magnum | 18.4.1-1.el8 | |
puppet-manila | 18.5.0-1.el8 | |
puppet-neutron | 18.5.0-1.el8 | |
puppet-nova | 18.5.0-1.el8 | |
puppet-octavia | 18.4.1-1.el8 | |
puppet-openstacklib | 18.5.0-1.el8 | |
puppet-openstack_extras | 18.5.0-1.el8 | |
puppet-oslo | 18.4.1-1.el8 | |
puppet-ovn | 18.5.0-1.el8 | |
puppet-placement | 5.4.1-1.el8 | |
puppet-swift | 18.5.0-1.el8 | |
puppet-vswitch | 14.4.1-1.el8 | |
python-oslo-cache-lang | 2.6.2-1.el8 | |
python3-oslo-cache | 2.6.2-1.el8 | |
python3-oslo-cache-tests | 2.6.2-1.el8 | |
python3-tempest | 28.1.0-2.el8 | |
python3-tempest-tests | 28.1.0-2.el8 | |

