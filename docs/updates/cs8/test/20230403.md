## 2023-04-03

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_481.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_481.el8-2.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-481.el8 | |
cockpit | 288.2-1.el8 | |
cockpit-bridge | 288.2-1.el8 | |
cockpit-doc | 288.2-1.el8 | |
cockpit-system | 288.2-1.el8 | |
cockpit-ws | 288.2-1.el8 | |
kernel | 4.18.0-481.el8 | |
kernel-abi-stablelists | 4.18.0-481.el8 | |
kernel-core | 4.18.0-481.el8 | |
kernel-cross-headers | 4.18.0-481.el8 | |
kernel-debug | 4.18.0-481.el8 | |
kernel-debug-core | 4.18.0-481.el8 | |
kernel-debug-devel | 4.18.0-481.el8 | |
kernel-debug-modules | 4.18.0-481.el8 | |
kernel-debug-modules-extra | 4.18.0-481.el8 | |
kernel-devel | 4.18.0-481.el8 | |
kernel-doc | 4.18.0-481.el8 | |
kernel-headers | 4.18.0-481.el8 | |
kernel-modules | 4.18.0-481.el8 | |
kernel-modules-extra | 4.18.0-481.el8 | |
kernel-tools | 4.18.0-481.el8 | |
kernel-tools-libs | 4.18.0-481.el8 | |
kmod-kvdo | 6.2.8.7-89.el8 | |
NetworkManager | 1.40.16-2.el8 | |
NetworkManager-adsl | 1.40.16-2.el8 | |
NetworkManager-bluetooth | 1.40.16-2.el8 | |
NetworkManager-config-connectivity-redhat | 1.40.16-2.el8 | |
NetworkManager-config-server | 1.40.16-2.el8 | |
NetworkManager-dispatcher-routing-rules | 1.40.16-2.el8 | |
NetworkManager-initscripts-updown | 1.40.16-2.el8 | |
NetworkManager-libnm | 1.40.16-2.el8 | |
NetworkManager-ovs | 1.40.16-2.el8 | |
NetworkManager-ppp | 1.40.16-2.el8 | |
NetworkManager-team | 1.40.16-2.el8 | |
NetworkManager-tui | 1.40.16-2.el8 | |
NetworkManager-wifi | 1.40.16-2.el8 | |
NetworkManager-wwan | 1.40.16-2.el8 | |
perf | 4.18.0-481.el8 | |
python3-perf | 4.18.0-481.el8 | |
samba-usershares | 4.17.5-0.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-composer | 45-1.el8 | |
libreoffice | 6.4.7.2-13.el8 | |
libsndfile-utils | 1.0.28-13.el8 | |
NetworkManager-cloud-setup | 1.40.16-2.el8 | |
nmap | 7.92-1.el8 | |
nmap-ncat | 7.92-1.el8 | |
osbuild | 82-1.el8 | |
osbuild-luks2 | 82-1.el8 | |
osbuild-lvm2 | 82-1.el8 | |
osbuild-ostree | 82-1.el8 | |
osbuild-selinux | 82-1.el8 | |
python3-osbuild | 82-1.el8 | |
python3.11-charset-normalizer | 2.1.0-1.el8 | |
python3.11-devel | 3.11.2-2.el8 | |
python3.11-idna | 3.4-1.el8 | |
python3.11-lxml | 4.9.2-3.el8 | |
python3.11-mod_wsgi | 4.9.4-1.el8 | |
python3.11-numpy | 1.23.5-1.el8 | |
python3.11-numpy-f2py | 1.23.5-1.el8 | |
python3.11-pip | 22.3.1-3.el8 | |
python3.11-pip-wheel | 22.3.1-3.el8 | |
python3.11-psycopg2 | 2.9.3-1.el8 | |
python3.11-PyMySQL | 1.0.2-1.el8 | |
python3.11-pysocks | 1.7.1-1.el8 | |
python3.11-requests | 2.28.1-1.el8 | |
python3.11-rpm-macros | 3.11.2-2.el8 | |
python3.11-scipy | 1.10.0-1.el8 | |
python3.11-setuptools | 65.5.1-2.el8 | |
python3.11-tkinter | 3.11.2-2.el8 | |
python3.11-urllib3 | 1.26.12-1.el8 | |
python3.11-wheel | 0.38.4-3.el8 | |
tigervnc | 1.13.1-1.el8 | |
tigervnc-icons | 1.13.1-1.el8 | |
tigervnc-license | 1.13.1-1.el8 | |
tigervnc-selinux | 1.13.1-1.el8 | |
tigervnc-server | 1.13.1-1.el8 | |
tigervnc-server-minimal | 1.13.1-1.el8 | |
tigervnc-server-module | 1.13.1-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fstrm-utils | 0.6.1-3.el8 | [RHBA-2022:7641](https://access.redhat.com/errata/RHBA-2022:7641) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b08-3.el8 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b08-3.el8 | |
kernel-tools-libs-devel | 4.18.0-481.el8 | |
NetworkManager-libnm-devel | 1.40.16-2.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-41.el8 | |
resource-agents-aliyun | 4.9.0-41.el8 | |
resource-agents-gcp | 4.9.0-41.el8 | |
resource-agents-paf | 4.9.0-41.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rt-setup | 2.1-5.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-41.el8 | |
resource-agents-aliyun | 4.9.0-41.el8 | |
resource-agents-gcp | 4.9.0-41.el8 | |
resource-agents-paf | 4.9.0-41.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_481.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_481.el8-2.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-481.el8 | |
cockpit | 288.2-1.el8 | |
cockpit-bridge | 288.2-1.el8 | |
cockpit-doc | 288.2-1.el8 | |
cockpit-system | 288.2-1.el8 | |
cockpit-ws | 288.2-1.el8 | |
kernel | 4.18.0-481.el8 | |
kernel-abi-stablelists | 4.18.0-481.el8 | |
kernel-core | 4.18.0-481.el8 | |
kernel-cross-headers | 4.18.0-481.el8 | |
kernel-debug | 4.18.0-481.el8 | |
kernel-debug-core | 4.18.0-481.el8 | |
kernel-debug-devel | 4.18.0-481.el8 | |
kernel-debug-modules | 4.18.0-481.el8 | |
kernel-debug-modules-extra | 4.18.0-481.el8 | |
kernel-devel | 4.18.0-481.el8 | |
kernel-doc | 4.18.0-481.el8 | |
kernel-headers | 4.18.0-481.el8 | |
kernel-modules | 4.18.0-481.el8 | |
kernel-modules-extra | 4.18.0-481.el8 | |
kernel-tools | 4.18.0-481.el8 | |
kernel-tools-libs | 4.18.0-481.el8 | |
kmod-kvdo | 6.2.8.7-89.el8 | |
NetworkManager | 1.40.16-2.el8 | |
NetworkManager-adsl | 1.40.16-2.el8 | |
NetworkManager-bluetooth | 1.40.16-2.el8 | |
NetworkManager-config-connectivity-redhat | 1.40.16-2.el8 | |
NetworkManager-config-server | 1.40.16-2.el8 | |
NetworkManager-dispatcher-routing-rules | 1.40.16-2.el8 | |
NetworkManager-libnm | 1.40.16-2.el8 | |
NetworkManager-ovs | 1.40.16-2.el8 | |
NetworkManager-ppp | 1.40.16-2.el8 | |
NetworkManager-team | 1.40.16-2.el8 | |
NetworkManager-tui | 1.40.16-2.el8 | |
NetworkManager-wifi | 1.40.16-2.el8 | |
NetworkManager-wwan | 1.40.16-2.el8 | |
perf | 4.18.0-481.el8 | |
python3-perf | 4.18.0-481.el8 | |
samba-usershares | 4.17.5-0.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-composer | 45-1.el8 | |
libsndfile-utils | 1.0.28-13.el8 | |
NetworkManager-cloud-setup | 1.40.16-2.el8 | |
nmap | 7.92-1.el8 | |
nmap-ncat | 7.92-1.el8 | |
osbuild | 82-1.el8 | |
osbuild-luks2 | 82-1.el8 | |
osbuild-lvm2 | 82-1.el8 | |
osbuild-ostree | 82-1.el8 | |
osbuild-selinux | 82-1.el8 | |
python3-osbuild | 82-1.el8 | |
python3.11-charset-normalizer | 2.1.0-1.el8 | |
python3.11-devel | 3.11.2-2.el8 | |
python3.11-idna | 3.4-1.el8 | |
python3.11-lxml | 4.9.2-3.el8 | |
python3.11-mod_wsgi | 4.9.4-1.el8 | |
python3.11-numpy | 1.23.5-1.el8 | |
python3.11-numpy-f2py | 1.23.5-1.el8 | |
python3.11-pip | 22.3.1-3.el8 | |
python3.11-pip-wheel | 22.3.1-3.el8 | |
python3.11-psycopg2 | 2.9.3-1.el8 | |
python3.11-PyMySQL | 1.0.2-1.el8 | |
python3.11-pysocks | 1.7.1-1.el8 | |
python3.11-requests | 2.28.1-1.el8 | |
python3.11-rpm-macros | 3.11.2-2.el8 | |
python3.11-scipy | 1.10.0-1.el8 | |
python3.11-setuptools | 65.5.1-2.el8 | |
python3.11-tkinter | 3.11.2-2.el8 | |
python3.11-urllib3 | 1.26.12-1.el8 | |
python3.11-wheel | 0.38.4-3.el8 | |
tigervnc | 1.13.1-1.el8 | |
tigervnc-icons | 1.13.1-1.el8 | |
tigervnc-license | 1.13.1-1.el8 | |
tigervnc-selinux | 1.13.1-1.el8 | |
tigervnc-server | 1.13.1-1.el8 | |
tigervnc-server-minimal | 1.13.1-1.el8 | |
tigervnc-server-module | 1.13.1-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fstrm-utils | 0.6.1-3.el8 | [RHBA-2022:7641](https://access.redhat.com/errata/RHBA-2022:7641) | <div class="adv_b">Bug Fix Advisory</div>
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.362.b08-3.el8 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.362.b08-3.el8 | |
kernel-tools-libs-devel | 4.18.0-481.el8 | |
NetworkManager-libnm-devel | 1.40.16-2.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-41.el8 | |
resource-agents-paf | 4.9.0-41.el8 | |

