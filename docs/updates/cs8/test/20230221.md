## 2023-02-21

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-os-vif-doc | 2.6.1-1.el8 | |
python3-os-vif | 2.6.1-1.el8 | |
python3-os-vif-tests | 2.6.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-os-vif-doc | 2.6.1-1.el8 | |
python3-os-vif | 2.6.1-1.el8 | |
python3-os-vif-tests | 2.6.1-1.el8 | |

