## 2022-03-30

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.el8s.cern | |
centos-linux-repos | 8-6.el8s.cern | |
centos-stream-repos | 8-6.el8s.cern | |
cern-anaconda-addon | 1.8-5.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.el8 | |
centos-stream-repos | 8-6.el8 | |
iproute | 5.15.0-4.el8 | |
iproute-tc | 5.15.0-4.el8 | |
iputils | 20180629-10.el8 | |
iputils-ninfod | 20180629-10.el8 | |
openssl | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
openssl-devel | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
openssl-libs | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
openssl-perl | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
platform-python | 3.6.8-46.el8 | |
procps-ng | 3.3.15-7.el8 | |
procps-ng-i18n | 3.3.15-7.el8 | |
python3-libs | 3.6.8-46.el8 | |
python3-test | 3.6.8-46.el8 | |
selinux-policy | 3.14.3-95.el8 | |
selinux-policy-devel | 3.14.3-95.el8 | |
selinux-policy-doc | 3.14.3-95.el8 | |
selinux-policy-minimum | 3.14.3-95.el8 | |
selinux-policy-mls | 3.14.3-95.el8 | |
selinux-policy-sandbox | 3.14.3-95.el8 | |
selinux-policy-targeted | 3.14.3-95.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.58-1.el8 | |
annobin-annocheck | 10.58-1.el8 | |
aspnetcore-runtime-5.0 | 5.0.15-2.el8 | |
aspnetcore-targeting-pack-5.0 | 5.0.15-2.el8 | |
dotnet-apphost-pack-5.0 | 5.0.15-2.el8 | |
dotnet-hostfxr-5.0 | 5.0.15-2.el8 | |
dotnet-runtime-5.0 | 5.0.15-2.el8 | |
dotnet-sdk-5.0 | 5.0.212-2.el8 | |
dotnet-targeting-pack-5.0 | 5.0.15-2.el8 | |
dotnet-templates-5.0 | 5.0.212-2.el8 | |
libnma | 1.8.36-2.el8 | |
platform-python-debug | 3.6.8-46.el8 | |
platform-python-devel | 3.6.8-46.el8 | |
python3-idle | 3.6.8-46.el8 | |
python3-tkinter | 3.6.8-46.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-5.0-source-built-artifacts | 5.0.212-2.el8 | |
iproute-devel | 5.15.0-4.el8 | |
libnma-devel | 1.8.36-2.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.el8s.cern | |
centos-linux-repos | 8-6.el8s.cern | |
centos-stream-repos | 8-6.el8s.cern | |
cern-anaconda-addon | 1.8-5.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-6.el8 | |
centos-stream-repos | 8-6.el8 | |
iproute | 5.15.0-4.el8 | |
iproute-tc | 5.15.0-4.el8 | |
iputils | 20180629-10.el8 | |
iputils-ninfod | 20180629-10.el8 | |
openssl | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
openssl-devel | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
openssl-libs | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
openssl-perl | 1.1.1k-6.el8 | [RHSA-2022:1065](https://access.redhat.com/errata/RHSA-2022:1065) | <div class="adv_s">Security Advisory</div>
platform-python | 3.6.8-46.el8 | |
procps-ng | 3.3.15-7.el8 | |
procps-ng-i18n | 3.3.15-7.el8 | |
python3-libs | 3.6.8-46.el8 | |
python3-test | 3.6.8-46.el8 | |
selinux-policy | 3.14.3-95.el8 | |
selinux-policy-devel | 3.14.3-95.el8 | |
selinux-policy-doc | 3.14.3-95.el8 | |
selinux-policy-minimum | 3.14.3-95.el8 | |
selinux-policy-mls | 3.14.3-95.el8 | |
selinux-policy-sandbox | 3.14.3-95.el8 | |
selinux-policy-targeted | 3.14.3-95.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.58-1.el8 | |
annobin-annocheck | 10.58-1.el8 | |
libnma | 1.8.36-2.el8 | |
platform-python-debug | 3.6.8-46.el8 | |
platform-python-devel | 3.6.8-46.el8 | |
python3-idle | 3.6.8-46.el8 | |
python3-tkinter | 3.6.8-46.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iproute-devel | 5.15.0-4.el8 | |
libnma-devel | 1.8.36-2.el8 | |

