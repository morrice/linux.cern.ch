## 2022-01-27

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bsdtar | 3.3.3-3.el8_5 | |
dnf-plugin-subscription-manager | 1.28.25-1.el8 | |
libarchive | 3.3.3-3.el8_5 | |
libqrtr-glib | 1.0.0-1.el8 | |
polkit | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
polkit-devel | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
polkit-docs | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
polkit-libs | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
python3-cloud-what | 1.28.25-1.el8 | |
python3-subscription-manager-rhsm | 1.28.25-1.el8 | |
python3-syspurpose | 1.28.25-1.el8 | |
rhsm-icons | 1.28.25-1.el8 | |
subscription-manager | 1.28.25-1.el8 | |
subscription-manager-cockpit | 1.28.25-1.el8 | |
subscription-manager-plugin-ostree | 1.28.25-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.25-1.el8 | |
vim-minimal | 8.0.1763-16.el8_5.4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16 | 9.16.23-0.5.el8 | |
bind9.16-chroot | 9.16.23-0.5.el8 | |
bind9.16-dnssec-utils | 9.16.23-0.5.el8 | |
bind9.16-libs | 9.16.23-0.5.el8 | |
bind9.16-license | 9.16.23-0.5.el8 | |
bind9.16-utils | 9.16.23-0.5.el8 | |
java-11-openjdk | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc-zip | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
python3-bind9.16 | 9.16.23-0.5.el8 | |
subscription-manager-migration | 1.28.25-1.el8 | |
vim-common | 8.0.1763-16.el8_5.4 | |
vim-enhanced | 8.0.1763-16.el8_5.4 | |
vim-filesystem | 8.0.1763-16.el8_5.4 | |
vim-X11 | 8.0.1763-16.el8_5.4 | |
virtio-win | 1.9.24-2.el8_5 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
asio-devel | 1.10.8-7.module_el8.3.0+757+d382997d | |
bind9.16-devel | 9.16.23-0.5.el8 | |
bind9.16-doc | 9.16.23-0.5.el8 | |
dotnet-build-reference-packages | 0-11.20211215git045b288.el8_5 | |
java-11-openjdk-demo-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
Judy-devel | 1.0.5-18.module_el8.3.0+757+d382997d | |
libarchive-devel | 3.3.3-3.el8_5 | |
lmdb-devel | 0.9.24-1.el8 | |
mingw32-glib2 | 2.70.1-1.el8_5 | |
mingw32-glib2-static | 2.70.1-1.el8_5 | |
mingw64-glib2 | 2.70.1-1.el8_5 | |
mingw64-glib2-static | 2.70.1-1.el8_5 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ceilometer-central | 15.1.0-1.el8 | |
openstack-ceilometer-common | 15.1.0-1.el8 | |
openstack-ceilometer-compute | 15.1.0-1.el8 | |
openstack-ceilometer-ipmi | 15.1.0-1.el8 | |
openstack-ceilometer-notification | 15.1.0-1.el8 | |
openstack-ceilometer-polling | 15.1.0-1.el8 | |
openstack-cloudkitty-api | 13.0.2-1.el8 | |
openstack-cloudkitty-common | 13.0.2-1.el8 | |
openstack-cloudkitty-processor | 13.0.2-1.el8 | |
openstack-cloudkitty-ui | 11.0.1-1.el8 | |
openstack-cloudkitty-ui-doc | 11.0.1-1.el8 | |
openstack-ec2-api | 11.1.0-1.el8 | |
openstack-kolla | 11.2.1-1.el8 | |
openstack-magnum-api | 11.2.1-1.el8 | |
openstack-magnum-common | 11.2.1-1.el8 | |
openstack-magnum-conductor | 11.2.1-1.el8 | |
openstack-magnum-doc | 11.2.1-1.el8 | |
openstack-octavia-ui | 6.0.1-1.el8 | |
python-networking-generic-switch-doc | 4.0.1-1.el8 | |
python-oslo-utils-doc | 4.10.1-1.el8 | |
python-oslo-utils-lang | 4.10.1-1.el8 | |
python3-ceilometer | 15.1.0-1.el8 | |
python3-ceilometer-tests | 15.1.0-1.el8 | |
python3-cloudkitty-tests | 13.0.2-1.el8 | |
python3-ec2-api | 11.1.0-1.el8 | |
python3-ec2-api-doc | 11.1.0-1.el8 | |
python3-ec2-api-tests | 11.1.0-1.el8 | |
python3-magnum | 11.2.1-1.el8 | |
python3-magnum-tests | 11.2.1-1.el8 | |
python3-networking-generic-switch | 4.0.1-1.el8 | |
python3-networking-generic-switch-tests | 4.0.1-1.el8 | |
python3-oslo-utils | 4.10.1-1.el8 | |
python3-oslo-utils-tests | 4.10.1-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-java-ceilometer-client | 3.2.9-9.el8 | |
openstack-java-ceilometer-model | 3.2.9-9.el8 | |
openstack-java-cinder-client | 3.2.9-9.el8 | |
openstack-java-cinder-model | 3.2.9-9.el8 | |
openstack-java-client | 3.2.9-9.el8 | |
openstack-java-glance-client | 3.2.9-9.el8 | |
openstack-java-glance-model | 3.2.9-9.el8 | |
openstack-java-heat-client | 3.2.9-9.el8 | |
openstack-java-heat-model | 3.2.9-9.el8 | |
openstack-java-keystone-client | 3.2.9-9.el8 | |
openstack-java-keystone-model | 3.2.9-9.el8 | |
openstack-java-nova-client | 3.2.9-9.el8 | |
openstack-java-nova-model | 3.2.9-9.el8 | |
openstack-java-quantum-client | 3.2.9-9.el8 | |
openstack-java-quantum-model | 3.2.9-9.el8 | |
openstack-java-resteasy-connector | 3.2.9-9.el8 | |
openstack-java-sdk-javadoc | 3.2.9-9.el8 | |
openstack-java-swift-client | 3.2.9-9.el8 | |
openstack-java-swift-model | 3.2.9-9.el8 | |
snmp4j | 3.6.4-0.1.el8 | |
snmp4j-javadoc | 3.6.4-0.1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bsdtar | 3.3.3-3.el8_5 | |
dnf-plugin-subscription-manager | 1.28.25-1.el8 | |
libarchive | 3.3.3-3.el8_5 | |
libqrtr-glib | 1.0.0-1.el8 | |
polkit | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
polkit-devel | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
polkit-docs | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
polkit-libs | 0.115-13.el8_5.1 | [RHSA-2022:0267](https://access.redhat.com/errata/RHSA-2022:0267) | <div class="adv_s">Security Advisory</div>
python3-cloud-what | 1.28.25-1.el8 | |
python3-subscription-manager-rhsm | 1.28.25-1.el8 | |
python3-syspurpose | 1.28.25-1.el8 | |
rhsm-icons | 1.28.25-1.el8 | |
subscription-manager | 1.28.25-1.el8 | |
subscription-manager-cockpit | 1.28.25-1.el8 | |
subscription-manager-plugin-ostree | 1.28.25-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.25-1.el8 | |
vim-minimal | 8.0.1763-16.el8_5.4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind9.16 | 9.16.23-0.5.el8 | |
bind9.16-chroot | 9.16.23-0.5.el8 | |
bind9.16-dnssec-utils | 9.16.23-0.5.el8 | |
bind9.16-libs | 9.16.23-0.5.el8 | |
bind9.16-license | 9.16.23-0.5.el8 | |
bind9.16-utils | 9.16.23-0.5.el8 | |
java-11-openjdk | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc-zip | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
python3-bind9.16 | 9.16.23-0.5.el8 | |
subscription-manager-migration | 1.28.25-1.el8 | |
vim-common | 8.0.1763-16.el8_5.4 | |
vim-enhanced | 8.0.1763-16.el8_5.4 | |
vim-filesystem | 8.0.1763-16.el8_5.4 | |
vim-X11 | 8.0.1763-16.el8_5.4 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
asio-devel | 1.10.8-7.module_el8.3.0+757+d382997d | |
bind9.16-devel | 9.16.23-0.5.el8 | |
bind9.16-doc | 9.16.23-0.5.el8 | |
java-11-openjdk-demo-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-fastdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-slowdebug | 11.0.14.0.9-2.el8_5 | [RHSA-2022:0185](https://access.redhat.com/errata/RHSA-2022:0185) | <div class="adv_s">Security Advisory</div>
Judy-devel | 1.0.5-18.module_el8.3.0+757+d382997d | |
libarchive-devel | 3.3.3-3.el8_5 | |
lmdb-devel | 0.9.24-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ceilometer-central | 15.1.0-1.el8 | |
openstack-ceilometer-common | 15.1.0-1.el8 | |
openstack-ceilometer-compute | 15.1.0-1.el8 | |
openstack-ceilometer-ipmi | 15.1.0-1.el8 | |
openstack-ceilometer-notification | 15.1.0-1.el8 | |
openstack-ceilometer-polling | 15.1.0-1.el8 | |
openstack-cloudkitty-api | 13.0.2-1.el8 | |
openstack-cloudkitty-common | 13.0.2-1.el8 | |
openstack-cloudkitty-processor | 13.0.2-1.el8 | |
openstack-cloudkitty-ui | 11.0.1-1.el8 | |
openstack-cloudkitty-ui-doc | 11.0.1-1.el8 | |
openstack-ec2-api | 11.1.0-1.el8 | |
openstack-kolla | 11.2.1-1.el8 | |
openstack-magnum-api | 11.2.1-1.el8 | |
openstack-magnum-common | 11.2.1-1.el8 | |
openstack-magnum-conductor | 11.2.1-1.el8 | |
openstack-magnum-doc | 11.2.1-1.el8 | |
openstack-octavia-ui | 6.0.1-1.el8 | |
python-networking-generic-switch-doc | 4.0.1-1.el8 | |
python-oslo-utils-doc | 4.10.1-1.el8 | |
python-oslo-utils-lang | 4.10.1-1.el8 | |
python3-ceilometer | 15.1.0-1.el8 | |
python3-ceilometer-tests | 15.1.0-1.el8 | |
python3-cloudkitty-tests | 13.0.2-1.el8 | |
python3-ec2-api | 11.1.0-1.el8 | |
python3-ec2-api-doc | 11.1.0-1.el8 | |
python3-ec2-api-tests | 11.1.0-1.el8 | |
python3-magnum | 11.2.1-1.el8 | |
python3-magnum-tests | 11.2.1-1.el8 | |
python3-networking-generic-switch | 4.0.1-1.el8 | |
python3-networking-generic-switch-tests | 4.0.1-1.el8 | |
python3-oslo-utils | 4.10.1-1.el8 | |
python3-oslo-utils-tests | 4.10.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-java-ceilometer-client | 3.2.9-9.el8 | |
openstack-java-ceilometer-model | 3.2.9-9.el8 | |
openstack-java-cinder-client | 3.2.9-9.el8 | |
openstack-java-cinder-model | 3.2.9-9.el8 | |
openstack-java-client | 3.2.9-9.el8 | |
openstack-java-glance-client | 3.2.9-9.el8 | |
openstack-java-glance-model | 3.2.9-9.el8 | |
openstack-java-heat-client | 3.2.9-9.el8 | |
openstack-java-heat-model | 3.2.9-9.el8 | |
openstack-java-keystone-client | 3.2.9-9.el8 | |
openstack-java-keystone-model | 3.2.9-9.el8 | |
openstack-java-nova-client | 3.2.9-9.el8 | |
openstack-java-nova-model | 3.2.9-9.el8 | |
openstack-java-quantum-client | 3.2.9-9.el8 | |
openstack-java-quantum-model | 3.2.9-9.el8 | |
openstack-java-resteasy-connector | 3.2.9-9.el8 | |
openstack-java-sdk-javadoc | 3.2.9-9.el8 | |
openstack-java-swift-client | 3.2.9-9.el8 | |
openstack-java-swift-model | 3.2.9-9.el8 | |
snmp4j | 3.6.4-0.1.el8 | |
snmp4j-javadoc | 3.6.4-0.1.el8 | |

