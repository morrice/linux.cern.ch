## 2022-02-23

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-heat-api | 15.1.0-1.el8 | |
openstack-heat-api-cfn | 15.1.0-1.el8 | |
openstack-heat-common | 15.1.0-1.el8 | |
openstack-heat-engine | 15.1.0-1.el8 | |
openstack-heat-monolith | 15.1.0-1.el8 | |
openstack-heat-ui | 4.0.1-1.el8 | |
openstack-kuryr-kubernetes-cni | 3.1.0-1.el8 | |
openstack-kuryr-kubernetes-common | 3.1.0-1.el8 | |
openstack-kuryr-kubernetes-controller | 3.1.0-1.el8 | |
openstack-kuryr-kubernetes-doc | 3.1.0-1.el8 | |
python3-heat-tests | 15.1.0-1.el8 | |
python3-heat-ui-doc | 4.0.1-1.el8 | |
python3-kuryr-kubernetes | 3.1.0-1.el8 | |
python3-kuryr-kubernetes-tests | 3.1.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-heat-api | 15.1.0-1.el8 | |
openstack-heat-api-cfn | 15.1.0-1.el8 | |
openstack-heat-common | 15.1.0-1.el8 | |
openstack-heat-engine | 15.1.0-1.el8 | |
openstack-heat-monolith | 15.1.0-1.el8 | |
openstack-heat-ui | 4.0.1-1.el8 | |
openstack-kuryr-kubernetes-cni | 3.1.0-1.el8 | |
openstack-kuryr-kubernetes-common | 3.1.0-1.el8 | |
openstack-kuryr-kubernetes-controller | 3.1.0-1.el8 | |
openstack-kuryr-kubernetes-doc | 3.1.0-1.el8 | |
python3-heat-tests | 15.1.0-1.el8 | |
python3-heat-ui-doc | 4.0.1-1.el8 | |
python3-kuryr-kubernetes | 3.1.0-1.el8 | |
python3-kuryr-kubernetes-tests | 3.1.0-1.el8 | |

