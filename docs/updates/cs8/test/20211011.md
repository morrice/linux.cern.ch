## 2021-10-11

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libguestfs-winsupport | 8.2-2.el8s | |
libtpms | 0.7.4-6.20201106git2452a24dab.el8s | |
libtpms-devel | 0.7.4-6.20201106git2452a24dab.el8s | |
libvirt | 7.6.0-4.el8s | |
libvirt-client | 7.6.0-4.el8s | |
libvirt-daemon | 7.6.0-4.el8s | |
libvirt-daemon-config-network | 7.6.0-4.el8s | |
libvirt-daemon-config-nwfilter | 7.6.0-4.el8s | |
libvirt-daemon-driver-interface | 7.6.0-4.el8s | |
libvirt-daemon-driver-network | 7.6.0-4.el8s | |
libvirt-daemon-driver-nodedev | 7.6.0-4.el8s | |
libvirt-daemon-driver-nwfilter | 7.6.0-4.el8s | |
libvirt-daemon-driver-qemu | 7.6.0-4.el8s | |
libvirt-daemon-driver-secret | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-core | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-disk | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-gluster | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-logical | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-mpath | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-rbd | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-scsi | 7.6.0-4.el8s | |
libvirt-daemon-kvm | 7.6.0-4.el8s | |
libvirt-devel | 7.6.0-4.el8s | |
libvirt-docs | 7.6.0-4.el8s | |
libvirt-libs | 7.6.0-4.el8s | |
libvirt-lock-sanlock | 7.6.0-4.el8s | |
libvirt-nss | 7.6.0-4.el8s | |
libvirt-wireshark | 7.6.0-4.el8s | |
qemu-guest-agent | 6.0.0-31.el8s | |
qemu-img | 6.0.0-31.el8s | |
qemu-kiwi | 6.0.0-31.el8s | |
qemu-kvm | 6.0.0-31.el8s | |
qemu-kvm-block-curl | 6.0.0-31.el8s | |
qemu-kvm-block-gluster | 6.0.0-31.el8s | |
qemu-kvm-block-iscsi | 6.0.0-31.el8s | |
qemu-kvm-block-rbd | 6.0.0-31.el8s | |
qemu-kvm-block-ssh | 6.0.0-31.el8s | |
qemu-kvm-common | 6.0.0-31.el8s | |
qemu-kvm-core | 6.0.0-31.el8s | |
qemu-kvm-docs | 6.0.0-31.el8s | |
qemu-kvm-hw-usbredir | 6.0.0-31.el8s | |
qemu-kvm-tests | 6.0.0-31.el8s | |
qemu-kvm-ui-opengl | 6.0.0-31.el8s | |
qemu-kvm-ui-spice | 6.0.0-31.el8s | |
swtpm | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-devel | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-libs | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-tools | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-tools-pkcs11 | 0.6.0-2.20210607gitea627b3.el8s | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libguestfs-winsupport | 8.2-2.el8s | |
libtpms | 0.7.4-6.20201106git2452a24dab.el8s | |
libtpms-devel | 0.7.4-6.20201106git2452a24dab.el8s | |
libvirt | 7.6.0-4.el8s | |
libvirt-client | 7.6.0-4.el8s | |
libvirt-daemon | 7.6.0-4.el8s | |
libvirt-daemon-config-network | 7.6.0-4.el8s | |
libvirt-daemon-config-nwfilter | 7.6.0-4.el8s | |
libvirt-daemon-driver-interface | 7.6.0-4.el8s | |
libvirt-daemon-driver-network | 7.6.0-4.el8s | |
libvirt-daemon-driver-nodedev | 7.6.0-4.el8s | |
libvirt-daemon-driver-nwfilter | 7.6.0-4.el8s | |
libvirt-daemon-driver-qemu | 7.6.0-4.el8s | |
libvirt-daemon-driver-secret | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-core | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-disk | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-gluster | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-logical | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-mpath | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-rbd | 7.6.0-4.el8s | |
libvirt-daemon-driver-storage-scsi | 7.6.0-4.el8s | |
libvirt-daemon-kvm | 7.6.0-4.el8s | |
libvirt-devel | 7.6.0-4.el8s | |
libvirt-docs | 7.6.0-4.el8s | |
libvirt-libs | 7.6.0-4.el8s | |
libvirt-lock-sanlock | 7.6.0-4.el8s | |
libvirt-nss | 7.6.0-4.el8s | |
libvirt-wireshark | 7.6.0-4.el8s | |
qemu-guest-agent | 6.0.0-31.el8s | |
qemu-img | 6.0.0-31.el8s | |
qemu-kiwi | 6.0.0-31.el8s | |
qemu-kvm | 6.0.0-31.el8s | |
qemu-kvm-block-curl | 6.0.0-31.el8s | |
qemu-kvm-block-iscsi | 6.0.0-31.el8s | |
qemu-kvm-block-rbd | 6.0.0-31.el8s | |
qemu-kvm-block-ssh | 6.0.0-31.el8s | |
qemu-kvm-common | 6.0.0-31.el8s | |
qemu-kvm-core | 6.0.0-31.el8s | |
qemu-kvm-docs | 6.0.0-31.el8s | |
qemu-kvm-tests | 6.0.0-31.el8s | |
swtpm | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-devel | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-libs | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-tools | 0.6.0-2.20210607gitea627b3.el8s | |
swtpm-tools-pkcs11 | 0.6.0-2.20210607gitea627b3.el8s | |

