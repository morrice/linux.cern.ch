## 2022-09-29

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 19.4.0-2.el8 | |
openstack-neutron-common | 19.4.0-2.el8 | |
openstack-neutron-linuxbridge | 19.4.0-2.el8 | |
openstack-neutron-macvtap-agent | 19.4.0-2.el8 | |
openstack-neutron-metering-agent | 19.4.0-2.el8 | |
openstack-neutron-ml2 | 19.4.0-2.el8 | |
openstack-neutron-ml2ovn-trace | 19.4.0-2.el8 | |
openstack-neutron-openvswitch | 19.4.0-2.el8 | |
openstack-neutron-ovn-metadata-agent | 19.4.0-2.el8 | |
openstack-neutron-ovn-migration-tool | 19.4.0-2.el8 | |
openstack-neutron-rpc-server | 19.4.0-2.el8 | |
openstack-neutron-sriov-nic-agent | 19.4.0-2.el8 | |
python3-neutron | 19.4.0-2.el8 | |
python3-neutron-tests | 19.4.0-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 19.4.0-2.el8 | |
openstack-neutron-common | 19.4.0-2.el8 | |
openstack-neutron-linuxbridge | 19.4.0-2.el8 | |
openstack-neutron-macvtap-agent | 19.4.0-2.el8 | |
openstack-neutron-metering-agent | 19.4.0-2.el8 | |
openstack-neutron-ml2 | 19.4.0-2.el8 | |
openstack-neutron-ml2ovn-trace | 19.4.0-2.el8 | |
openstack-neutron-openvswitch | 19.4.0-2.el8 | |
openstack-neutron-ovn-metadata-agent | 19.4.0-2.el8 | |
openstack-neutron-ovn-migration-tool | 19.4.0-2.el8 | |
openstack-neutron-rpc-server | 19.4.0-2.el8 | |
openstack-neutron-sriov-nic-agent | 19.4.0-2.el8 | |
python3-neutron | 19.4.0-2.el8 | |
python3-neutron-tests | 19.4.0-2.el8 | |

