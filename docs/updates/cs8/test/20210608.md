## 2021-06-08

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-openvswitch | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-devel | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-central | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-common | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-host | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-vtep | 2.11-0.2021060703.el8 | |
ovirt-python-openvswitch | 2.11-0.2021060703.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-openvswitch | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-devel | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-central | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-common | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-host | 2.11-0.2021060703.el8 | |
ovirt-openvswitch-ovn-vtep | 2.11-0.2021060703.el8 | |
ovirt-python-openvswitch | 2.11-0.2021060703.el8 | |

