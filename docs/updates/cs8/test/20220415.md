## 2022-04-15

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accel-config | 3.4.6.3-1.el8 | |
accel-config-libs | 3.4.6.3-1.el8 | |
selinux-policy | 3.14.3-96.el8 | |
selinux-policy-devel | 3.14.3-96.el8 | |
selinux-policy-doc | 3.14.3-96.el8 | |
selinux-policy-minimum | 3.14.3-96.el8 | |
selinux-policy-mls | 3.14.3-96.el8 | |
selinux-policy-sandbox | 3.14.3-96.el8 | |
selinux-policy-targeted | 3.14.3-96.el8 | |
tzdata | 2022a-2.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-spawn | 1.0.5-1.el8 | |
flatpak-xdg-utils | 1.0.5-1.el8 | |
ipxe-bootimgs-aarch64 | 20181214-9.git133f4c47.el8 | |
libnma | 1.8.38-1.el8 | |
motif | 2.3.4-17.el8 | |
motif-devel | 2.3.4-17.el8 | |
motif-static | 2.3.4-17.el8 | |
stalld | 1.16-1.el8 | |
tuned-profiles-postgresql | 2.18.0-2.el8 | |
tzdata-java | 2022a-2.el8 | |
xdg-desktop-portal | 1.8.1-1.el8 | |
xdg-desktop-portal-gtk | 1.8.0-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
accel-config-devel | 3.4.6.3-1.el8 | |
libnma-devel | 1.8.38-1.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-containers-podman | 1.9.3-1.el8 | |
openstack-cinder | 18.2.0-1.el8 | |
openstack-designate-agent | 11.0.2-1.el8 | |
openstack-designate-api | 11.0.2-1.el8 | |
openstack-designate-central | 11.0.2-1.el8 | |
openstack-designate-common | 11.0.2-1.el8 | |
openstack-designate-mdns | 11.0.2-1.el8 | |
openstack-designate-producer | 11.0.2-1.el8 | |
openstack-designate-sink | 11.0.2-1.el8 | |
openstack-designate-worker | 11.0.2-1.el8 | |
openstack-ironic-python-agent-builder | 2.8.0-1.el8 | |
openstack-nova | 22.4.0-1.el8 | |
openstack-nova | 23.2.0-1.el8 | |
openstack-nova-api | 22.4.0-1.el8 | |
openstack-nova-api | 23.2.0-1.el8 | |
openstack-nova-common | 22.4.0-1.el8 | |
openstack-nova-common | 23.2.0-1.el8 | |
openstack-nova-compute | 22.4.0-1.el8 | |
openstack-nova-compute | 23.2.0-1.el8 | |
openstack-nova-conductor | 22.4.0-1.el8 | |
openstack-nova-conductor | 23.2.0-1.el8 | |
openstack-nova-migration | 22.4.0-1.el8 | |
openstack-nova-migration | 23.2.0-1.el8 | |
openstack-nova-novncproxy | 22.4.0-1.el8 | |
openstack-nova-novncproxy | 23.2.0-1.el8 | |
openstack-nova-scheduler | 22.4.0-1.el8 | |
openstack-nova-scheduler | 23.2.0-1.el8 | |
openstack-nova-serialproxy | 22.4.0-1.el8 | |
openstack-nova-serialproxy | 23.2.0-1.el8 | |
openstack-nova-spicehtml5proxy | 22.4.0-1.el8 | |
openstack-nova-spicehtml5proxy | 23.2.0-1.el8 | |
puppet-tempest | 18.5.0-1.el8 | |
python-heatclient-doc | 2.3.1-1.el8 | |
python-manila-tests-tempest-doc | 1.7.0-1.el8 | |
python3-barbican-tests-tempest | 1.6.0-1.el8 | |
python3-cinder | 18.2.0-1.el8 | |
python3-cinder-common | 18.2.0-1.el8 | |
python3-cinder-tests | 18.2.0-1.el8 | |
python3-cinder-tests-tempest | 1.6.0-1.el8 | |
python3-designate | 11.0.2-1.el8 | |
python3-designate-tests | 11.0.2-1.el8 | |
python3-glance-tests-tempest | 0.3.0-1.el8 | |
python3-heatclient | 2.3.1-1.el8 | |
python3-manila-tests-tempest | 1.7.0-1.el8 | |
python3-nova | 22.4.0-1.el8 | |
python3-nova | 23.2.0-1.el8 | |
python3-nova-tests | 22.4.0-1.el8 | |
python3-nova-tests | 23.2.0-1.el8 | |
python3-openstacksdk | 0.55.1-1.el8 | |
python3-openstacksdk-tests | 0.55.1-1.el8 | |
python3-os-brick | 4.3.3-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-96.el8 | |
selinux-policy-devel | 3.14.3-96.el8 | |
selinux-policy-doc | 3.14.3-96.el8 | |
selinux-policy-minimum | 3.14.3-96.el8 | |
selinux-policy-mls | 3.14.3-96.el8 | |
selinux-policy-sandbox | 3.14.3-96.el8 | |
selinux-policy-targeted | 3.14.3-96.el8 | |
tzdata | 2022a-2.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet | 6.0.0-0.6.28be3e9a006d90d8c6e87d4353b77882829df718.el8 | [RHEA-2021:4351](https://access.redhat.com/errata/RHEA-2021:4351) | <div class="adv_e">Product Enhancement Advisory</div>
flatpak-spawn | 1.0.5-1.el8 | |
flatpak-xdg-utils | 1.0.5-1.el8 | |
ipxe-bootimgs-aarch64 | 20181214-9.git133f4c47.el8 | |
libnma | 1.8.38-1.el8 | |
motif | 2.3.4-17.el8 | |
motif-devel | 2.3.4-17.el8 | |
motif-static | 2.3.4-17.el8 | |
stalld | 1.16-1.el8 | |
tuned-profiles-postgresql | 2.18.0-2.el8 | |
tzdata-java | 2022a-2.el8 | |
xdg-desktop-portal | 1.8.1-1.el8 | |
xdg-desktop-portal-gtk | 1.8.0-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnma-devel | 1.8.38-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-containers-podman | 1.9.3-1.el8 | |
openstack-cinder | 18.2.0-1.el8 | |
openstack-designate-agent | 11.0.2-1.el8 | |
openstack-designate-api | 11.0.2-1.el8 | |
openstack-designate-central | 11.0.2-1.el8 | |
openstack-designate-common | 11.0.2-1.el8 | |
openstack-designate-mdns | 11.0.2-1.el8 | |
openstack-designate-producer | 11.0.2-1.el8 | |
openstack-designate-sink | 11.0.2-1.el8 | |
openstack-designate-worker | 11.0.2-1.el8 | |
openstack-ironic-python-agent-builder | 2.8.0-1.el8 | |
openstack-nova | 22.4.0-1.el8 | |
openstack-nova | 23.2.0-1.el8 | |
openstack-nova-api | 22.4.0-1.el8 | |
openstack-nova-api | 23.2.0-1.el8 | |
openstack-nova-common | 22.4.0-1.el8 | |
openstack-nova-common | 23.2.0-1.el8 | |
openstack-nova-compute | 22.4.0-1.el8 | |
openstack-nova-compute | 23.2.0-1.el8 | |
openstack-nova-conductor | 22.4.0-1.el8 | |
openstack-nova-conductor | 23.2.0-1.el8 | |
openstack-nova-migration | 22.4.0-1.el8 | |
openstack-nova-migration | 23.2.0-1.el8 | |
openstack-nova-novncproxy | 22.4.0-1.el8 | |
openstack-nova-novncproxy | 23.2.0-1.el8 | |
openstack-nova-scheduler | 22.4.0-1.el8 | |
openstack-nova-scheduler | 23.2.0-1.el8 | |
openstack-nova-serialproxy | 22.4.0-1.el8 | |
openstack-nova-serialproxy | 23.2.0-1.el8 | |
openstack-nova-spicehtml5proxy | 22.4.0-1.el8 | |
openstack-nova-spicehtml5proxy | 23.2.0-1.el8 | |
puppet-tempest | 18.5.0-1.el8 | |
python-heatclient-doc | 2.3.1-1.el8 | |
python-manila-tests-tempest-doc | 1.7.0-1.el8 | |
python3-barbican-tests-tempest | 1.6.0-1.el8 | |
python3-cinder | 18.2.0-1.el8 | |
python3-cinder-common | 18.2.0-1.el8 | |
python3-cinder-tests | 18.2.0-1.el8 | |
python3-cinder-tests-tempest | 1.6.0-1.el8 | |
python3-designate | 11.0.2-1.el8 | |
python3-designate-tests | 11.0.2-1.el8 | |
python3-glance-tests-tempest | 0.3.0-1.el8 | |
python3-heatclient | 2.3.1-1.el8 | |
python3-manila-tests-tempest | 1.7.0-1.el8 | |
python3-nova | 22.4.0-1.el8 | |
python3-nova | 23.2.0-1.el8 | |
python3-nova-tests | 22.4.0-1.el8 | |
python3-nova-tests | 23.2.0-1.el8 | |
python3-openstacksdk | 0.55.1-1.el8 | |
python3-openstacksdk-tests | 0.55.1-1.el8 | |
python3-os-brick | 4.3.3-1.el8 | |

