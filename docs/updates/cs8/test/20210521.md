## 2021-05-21

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.1.5-14.el8s.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 48-1.1.el8s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.1.5-14.el8s.cern | |

