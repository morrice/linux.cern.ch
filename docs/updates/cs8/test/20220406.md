## 2022-04-06

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-stevedore | 3.5.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-stevedore | 3.5.0-1.el8 | |

