## 2021-08-27

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 1.2.0-1.el8 | |
python-oslo-db-doc | 8.4.1-1.el8 | |
python-oslo-db-lang | 8.4.1-1.el8 | |
python3-neutron-tests-tempest | 1.2.0-1.el8 | |
python3-oslo-db | 8.4.1-1.el8 | |
python3-oslo-db-tests | 8.4.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 1.2.0-1.el8 | |
python-oslo-db-doc | 8.4.1-1.el8 | |
python-oslo-db-lang | 8.4.1-1.el8 | |
python3-neutron-tests-tempest | 1.2.0-1.el8 | |
python3-oslo-db | 8.4.1-1.el8 | |
python3-oslo-db-tests | 8.4.1-1.el8 | |

