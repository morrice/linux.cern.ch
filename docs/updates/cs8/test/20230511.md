## 2023-05-11

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_490.el8.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-490.el8 | |
cockpit | 291-1.el8 | |
cockpit-bridge | 291-1.el8 | |
cockpit-doc | 291-1.el8 | |
cockpit-system | 291-1.el8 | |
cockpit-ws | 291-1.el8 | |
elfutils | 0.189-2.el8 | |
elfutils-debuginfod | 0.189-2.el8 | |
elfutils-debuginfod-client | 0.189-2.el8 | |
elfutils-debuginfod-client-devel | 0.189-2.el8 | |
elfutils-default-yama-scope | 0.189-2.el8 | |
elfutils-devel | 0.189-2.el8 | |
elfutils-libelf | 0.189-2.el8 | |
elfutils-libelf-devel | 0.189-2.el8 | |
elfutils-libs | 0.189-2.el8 | |
kernel | 4.18.0-490.el8 | |
kernel-abi-stablelists | 4.18.0-490.el8 | |
kernel-core | 4.18.0-490.el8 | |
kernel-cross-headers | 4.18.0-490.el8 | |
kernel-debug | 4.18.0-490.el8 | |
kernel-debug-core | 4.18.0-490.el8 | |
kernel-debug-devel | 4.18.0-490.el8 | |
kernel-debug-modules | 4.18.0-490.el8 | |
kernel-debug-modules-extra | 4.18.0-490.el8 | |
kernel-devel | 4.18.0-490.el8 | |
kernel-doc | 4.18.0-490.el8 | |
kernel-headers | 4.18.0-490.el8 | |
kernel-modules | 4.18.0-490.el8 | |
kernel-modules-extra | 4.18.0-490.el8 | |
kernel-tools | 4.18.0-490.el8 | |
kernel-tools-libs | 4.18.0-490.el8 | |
libtracefs | 1.3.1-2.el8 | |
perf | 4.18.0-490.el8 | |
python3-perf | 4.18.0-490.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cloud-init | 23.1.1-3.el8 | |
cockpit-machines | 291-1.el8 | |
cockpit-packagekit | 291-1.el8 | |
cockpit-pcp | 291-1.el8 | |
cockpit-storaged | 291-1.el8 | |
crash | 7.3.2-6.el8 | |
fence-agents-all | 4.2.1-114.el8 | |
fence-agents-all | 4.2.1-115.el8 | |
fence-agents-amt-ws | 4.2.1-114.el8 | |
fence-agents-amt-ws | 4.2.1-115.el8 | |
fence-agents-apc | 4.2.1-114.el8 | |
fence-agents-apc | 4.2.1-115.el8 | |
fence-agents-apc-snmp | 4.2.1-114.el8 | |
fence-agents-apc-snmp | 4.2.1-115.el8 | |
fence-agents-bladecenter | 4.2.1-114.el8 | |
fence-agents-bladecenter | 4.2.1-115.el8 | |
fence-agents-brocade | 4.2.1-114.el8 | |
fence-agents-brocade | 4.2.1-115.el8 | |
fence-agents-cisco-mds | 4.2.1-114.el8 | |
fence-agents-cisco-mds | 4.2.1-115.el8 | |
fence-agents-cisco-ucs | 4.2.1-114.el8 | |
fence-agents-cisco-ucs | 4.2.1-115.el8 | |
fence-agents-common | 4.2.1-114.el8 | |
fence-agents-common | 4.2.1-115.el8 | |
fence-agents-compute | 4.2.1-114.el8 | |
fence-agents-compute | 4.2.1-115.el8 | |
fence-agents-drac5 | 4.2.1-114.el8 | |
fence-agents-drac5 | 4.2.1-115.el8 | |
fence-agents-eaton-snmp | 4.2.1-114.el8 | |
fence-agents-eaton-snmp | 4.2.1-115.el8 | |
fence-agents-emerson | 4.2.1-114.el8 | |
fence-agents-emerson | 4.2.1-115.el8 | |
fence-agents-eps | 4.2.1-114.el8 | |
fence-agents-eps | 4.2.1-115.el8 | |
fence-agents-heuristics-ping | 4.2.1-114.el8 | |
fence-agents-heuristics-ping | 4.2.1-115.el8 | |
fence-agents-hpblade | 4.2.1-114.el8 | |
fence-agents-hpblade | 4.2.1-115.el8 | |
fence-agents-ibm-powervs | 4.2.1-114.el8 | |
fence-agents-ibm-powervs | 4.2.1-115.el8 | |
fence-agents-ibm-vpc | 4.2.1-114.el8 | |
fence-agents-ibm-vpc | 4.2.1-115.el8 | |
fence-agents-ibmblade | 4.2.1-114.el8 | |
fence-agents-ibmblade | 4.2.1-115.el8 | |
fence-agents-ifmib | 4.2.1-114.el8 | |
fence-agents-ifmib | 4.2.1-115.el8 | |
fence-agents-ilo-moonshot | 4.2.1-114.el8 | |
fence-agents-ilo-moonshot | 4.2.1-115.el8 | |
fence-agents-ilo-mp | 4.2.1-114.el8 | |
fence-agents-ilo-mp | 4.2.1-115.el8 | |
fence-agents-ilo-ssh | 4.2.1-114.el8 | |
fence-agents-ilo-ssh | 4.2.1-115.el8 | |
fence-agents-ilo2 | 4.2.1-114.el8 | |
fence-agents-ilo2 | 4.2.1-115.el8 | |
fence-agents-intelmodular | 4.2.1-114.el8 | |
fence-agents-intelmodular | 4.2.1-115.el8 | |
fence-agents-ipdu | 4.2.1-114.el8 | |
fence-agents-ipdu | 4.2.1-115.el8 | |
fence-agents-ipmilan | 4.2.1-114.el8 | |
fence-agents-ipmilan | 4.2.1-115.el8 | |
fence-agents-kdump | 4.2.1-114.el8 | |
fence-agents-kdump | 4.2.1-115.el8 | |
fence-agents-kubevirt | 4.2.1-114.el8 | |
fence-agents-kubevirt | 4.2.1-115.el8 | |
fence-agents-lpar | 4.2.1-114.el8 | |
fence-agents-lpar | 4.2.1-115.el8 | |
fence-agents-mpath | 4.2.1-114.el8 | |
fence-agents-mpath | 4.2.1-115.el8 | |
fence-agents-redfish | 4.2.1-114.el8 | |
fence-agents-redfish | 4.2.1-115.el8 | |
fence-agents-rhevm | 4.2.1-114.el8 | |
fence-agents-rhevm | 4.2.1-115.el8 | |
fence-agents-rsa | 4.2.1-114.el8 | |
fence-agents-rsa | 4.2.1-115.el8 | |
fence-agents-rsb | 4.2.1-114.el8 | |
fence-agents-rsb | 4.2.1-115.el8 | |
fence-agents-sbd | 4.2.1-114.el8 | |
fence-agents-sbd | 4.2.1-115.el8 | |
fence-agents-scsi | 4.2.1-114.el8 | |
fence-agents-scsi | 4.2.1-115.el8 | |
fence-agents-virsh | 4.2.1-114.el8 | |
fence-agents-virsh | 4.2.1-115.el8 | |
fence-agents-vmware-rest | 4.2.1-114.el8 | |
fence-agents-vmware-rest | 4.2.1-115.el8 | |
fence-agents-vmware-soap | 4.2.1-114.el8 | |
fence-agents-vmware-soap | 4.2.1-115.el8 | |
fence-agents-wti | 4.2.1-114.el8 | |
fence-agents-wti | 4.2.1-115.el8 | |
gnome-terminal | 3.28.3-4.el8 | |
gnome-terminal-nautilus | 3.28.3-4.el8 | |
libwebp | 1.0.0-9.el8 | |
libwebp-devel | 1.0.0-9.el8 | |
marisa | 0.2.4-38.el8 | |
open-vm-tools | 12.2.0-1.el8 | |
open-vm-tools-desktop | 12.2.0-1.el8 | |
open-vm-tools-sdmp | 12.2.0-1.el8 | |
perl-DBI | 1.641-4.module_el8+332+132e4365 | |
perl-DBI | 1.641-4.module_el8+332+233a43e2 | |
perl-DBI | 1.641-4.module_el8+332+68e746e8 | |
perl-DBI | 1.641-4.module_el8+332+f50ba9bb | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+124f1251 | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+1755b796 | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+1ec643e0 | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+30acecc7 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+124f1251 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+1755b796 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+1ec643e0 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+30acecc7 | |
protobuf-c | 1.3.0-8.el8 | |
protobuf-c-compiler | 1.3.0-8.el8 | |
protobuf-c-devel | 1.3.0-8.el8 | |
pykickstart | 3.16.16-1.el8 | |
python3-kickstart | 3.16.16-1.el8 | |
rhel-system-roles | 1.22.0-0.6.el8 | |
rpm-ostree | 2022.10.117.g52714b51-1.el8 | |
rpm-ostree-libs | 2022.10.117.g52714b51-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
crash-devel | 7.3.2-6.el8 | |
elfutils-devel-static | 0.189-2.el8 | |
elfutils-libelf-devel-static | 0.189-2.el8 | |
kernel-tools-libs-devel | 4.18.0-490.el8 | |
libtracefs-devel | 1.3.1-2.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-114.el8 | |
fence-agents-aliyun | 4.2.1-115.el8 | |
fence-agents-aws | 4.2.1-114.el8 | |
fence-agents-aws | 4.2.1-115.el8 | |
fence-agents-azure-arm | 4.2.1-114.el8 | |
fence-agents-azure-arm | 4.2.1-115.el8 | |
fence-agents-gce | 4.2.1-114.el8 | |
fence-agents-gce | 4.2.1-115.el8 | |
fence-agents-openstack | 4.2.1-114.el8 | |
fence-agents-openstack | 4.2.1-115.el8 | |
resource-agents | 4.9.0-42.el8 | |
resource-agents-aliyun | 4.9.0-42.el8 | |
resource-agents-gcp | 4.9.0-42.el8 | |
resource-agents-paf | 4.9.0-42.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-114.el8 | |
fence-agents-aliyun | 4.2.1-115.el8 | |
fence-agents-aws | 4.2.1-114.el8 | |
fence-agents-aws | 4.2.1-115.el8 | |
fence-agents-azure-arm | 4.2.1-114.el8 | |
fence-agents-azure-arm | 4.2.1-115.el8 | |
fence-agents-gce | 4.2.1-114.el8 | |
fence-agents-gce | 4.2.1-115.el8 | |
fence-agents-openstack | 4.2.1-114.el8 | |
fence-agents-openstack | 4.2.1-115.el8 | |
resource-agents | 4.9.0-42.el8 | |
resource-agents-aliyun | 4.9.0-42.el8 | |
resource-agents-gcp | 4.9.0-42.el8 | |
resource-agents-paf | 4.9.0-42.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 25.1.1-1.el8 | |
openstack-nova-api | 25.1.1-1.el8 | |
openstack-nova-common | 25.1.1-1.el8 | |
openstack-nova-compute | 25.1.1-1.el8 | |
openstack-nova-conductor | 25.1.1-1.el8 | |
openstack-nova-migration | 25.1.1-1.el8 | |
openstack-nova-novncproxy | 25.1.1-1.el8 | |
openstack-nova-scheduler | 25.1.1-1.el8 | |
openstack-nova-serialproxy | 25.1.1-1.el8 | |
openstack-nova-spicehtml5proxy | 25.1.1-1.el8 | |
python3-nova | 25.1.1-1.el8 | |
python3-nova-tests | 25.1.1-1.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_490.el8.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-490.el8 | |
cockpit | 291-1.el8 | |
cockpit-bridge | 291-1.el8 | |
cockpit-doc | 291-1.el8 | |
cockpit-system | 291-1.el8 | |
cockpit-ws | 291-1.el8 | |
elfutils | 0.189-2.el8 | |
elfutils-debuginfod | 0.189-2.el8 | |
elfutils-debuginfod-client | 0.189-2.el8 | |
elfutils-debuginfod-client-devel | 0.189-2.el8 | |
elfutils-default-yama-scope | 0.189-2.el8 | |
elfutils-devel | 0.189-2.el8 | |
elfutils-libelf | 0.189-2.el8 | |
elfutils-libelf-devel | 0.189-2.el8 | |
elfutils-libs | 0.189-2.el8 | |
kernel | 4.18.0-490.el8 | |
kernel-abi-stablelists | 4.18.0-490.el8 | |
kernel-core | 4.18.0-490.el8 | |
kernel-cross-headers | 4.18.0-490.el8 | |
kernel-debug | 4.18.0-490.el8 | |
kernel-debug-core | 4.18.0-490.el8 | |
kernel-debug-devel | 4.18.0-490.el8 | |
kernel-debug-modules | 4.18.0-490.el8 | |
kernel-debug-modules-extra | 4.18.0-490.el8 | |
kernel-devel | 4.18.0-490.el8 | |
kernel-doc | 4.18.0-490.el8 | |
kernel-headers | 4.18.0-490.el8 | |
kernel-modules | 4.18.0-490.el8 | |
kernel-modules-extra | 4.18.0-490.el8 | |
kernel-tools | 4.18.0-490.el8 | |
kernel-tools-libs | 4.18.0-490.el8 | |
libtracefs | 1.3.1-2.el8 | |
perf | 4.18.0-490.el8 | |
protobuf-c | 1.3.0-8.el8 | |
python3-perf | 4.18.0-490.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cloud-init | 23.1.1-3.el8 | |
cockpit-machines | 291-1.el8 | |
cockpit-packagekit | 291-1.el8 | |
cockpit-pcp | 291-1.el8 | |
cockpit-storaged | 291-1.el8 | |
crash | 7.3.2-6.el8 | |
fence-agents-all | 4.2.1-114.el8 | |
fence-agents-all | 4.2.1-115.el8 | |
fence-agents-amt-ws | 4.2.1-114.el8 | |
fence-agents-amt-ws | 4.2.1-115.el8 | |
fence-agents-apc | 4.2.1-114.el8 | |
fence-agents-apc | 4.2.1-115.el8 | |
fence-agents-apc-snmp | 4.2.1-114.el8 | |
fence-agents-apc-snmp | 4.2.1-115.el8 | |
fence-agents-bladecenter | 4.2.1-114.el8 | |
fence-agents-bladecenter | 4.2.1-115.el8 | |
fence-agents-brocade | 4.2.1-114.el8 | |
fence-agents-brocade | 4.2.1-115.el8 | |
fence-agents-cisco-mds | 4.2.1-114.el8 | |
fence-agents-cisco-mds | 4.2.1-115.el8 | |
fence-agents-cisco-ucs | 4.2.1-114.el8 | |
fence-agents-cisco-ucs | 4.2.1-115.el8 | |
fence-agents-common | 4.2.1-114.el8 | |
fence-agents-common | 4.2.1-115.el8 | |
fence-agents-compute | 4.2.1-114.el8 | |
fence-agents-compute | 4.2.1-115.el8 | |
fence-agents-drac5 | 4.2.1-114.el8 | |
fence-agents-drac5 | 4.2.1-115.el8 | |
fence-agents-eaton-snmp | 4.2.1-114.el8 | |
fence-agents-eaton-snmp | 4.2.1-115.el8 | |
fence-agents-emerson | 4.2.1-114.el8 | |
fence-agents-emerson | 4.2.1-115.el8 | |
fence-agents-eps | 4.2.1-114.el8 | |
fence-agents-eps | 4.2.1-115.el8 | |
fence-agents-heuristics-ping | 4.2.1-114.el8 | |
fence-agents-heuristics-ping | 4.2.1-115.el8 | |
fence-agents-hpblade | 4.2.1-114.el8 | |
fence-agents-hpblade | 4.2.1-115.el8 | |
fence-agents-ibm-powervs | 4.2.1-114.el8 | |
fence-agents-ibm-powervs | 4.2.1-115.el8 | |
fence-agents-ibm-vpc | 4.2.1-114.el8 | |
fence-agents-ibm-vpc | 4.2.1-115.el8 | |
fence-agents-ibmblade | 4.2.1-114.el8 | |
fence-agents-ibmblade | 4.2.1-115.el8 | |
fence-agents-ifmib | 4.2.1-114.el8 | |
fence-agents-ifmib | 4.2.1-115.el8 | |
fence-agents-ilo-moonshot | 4.2.1-114.el8 | |
fence-agents-ilo-moonshot | 4.2.1-115.el8 | |
fence-agents-ilo-mp | 4.2.1-114.el8 | |
fence-agents-ilo-mp | 4.2.1-115.el8 | |
fence-agents-ilo-ssh | 4.2.1-114.el8 | |
fence-agents-ilo-ssh | 4.2.1-115.el8 | |
fence-agents-ilo2 | 4.2.1-114.el8 | |
fence-agents-ilo2 | 4.2.1-115.el8 | |
fence-agents-intelmodular | 4.2.1-114.el8 | |
fence-agents-intelmodular | 4.2.1-115.el8 | |
fence-agents-ipdu | 4.2.1-114.el8 | |
fence-agents-ipdu | 4.2.1-115.el8 | |
fence-agents-ipmilan | 4.2.1-114.el8 | |
fence-agents-ipmilan | 4.2.1-115.el8 | |
fence-agents-kdump | 4.2.1-114.el8 | |
fence-agents-kdump | 4.2.1-115.el8 | |
fence-agents-kubevirt | 4.2.1-114.el8 | |
fence-agents-kubevirt | 4.2.1-115.el8 | |
fence-agents-mpath | 4.2.1-114.el8 | |
fence-agents-mpath | 4.2.1-115.el8 | |
fence-agents-redfish | 4.2.1-114.el8 | |
fence-agents-redfish | 4.2.1-115.el8 | |
fence-agents-rhevm | 4.2.1-114.el8 | |
fence-agents-rhevm | 4.2.1-115.el8 | |
fence-agents-rsa | 4.2.1-114.el8 | |
fence-agents-rsa | 4.2.1-115.el8 | |
fence-agents-rsb | 4.2.1-114.el8 | |
fence-agents-rsb | 4.2.1-115.el8 | |
fence-agents-sbd | 4.2.1-114.el8 | |
fence-agents-sbd | 4.2.1-115.el8 | |
fence-agents-scsi | 4.2.1-114.el8 | |
fence-agents-scsi | 4.2.1-115.el8 | |
fence-agents-virsh | 4.2.1-114.el8 | |
fence-agents-virsh | 4.2.1-115.el8 | |
fence-agents-vmware-rest | 4.2.1-114.el8 | |
fence-agents-vmware-rest | 4.2.1-115.el8 | |
fence-agents-vmware-soap | 4.2.1-114.el8 | |
fence-agents-vmware-soap | 4.2.1-115.el8 | |
fence-agents-wti | 4.2.1-114.el8 | |
fence-agents-wti | 4.2.1-115.el8 | |
gnome-terminal | 3.28.3-4.el8 | |
gnome-terminal-nautilus | 3.28.3-4.el8 | |
libwebp | 1.0.0-9.el8 | |
libwebp-devel | 1.0.0-9.el8 | |
marisa | 0.2.4-38.el8 | |
perl-DBI | 1.641-4.module_el8+332+132e4365 | |
perl-DBI | 1.641-4.module_el8+332+233a43e2 | |
perl-DBI | 1.641-4.module_el8+332+68e746e8 | |
perl-DBI | 1.641-4.module_el8+332+f50ba9bb | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+124f1251 | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+1755b796 | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+1ec643e0 | |
perl-IO-Socket-SSL | 2.066-4.module_el8+339+30acecc7 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+124f1251 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+1755b796 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+1ec643e0 | |
perl-Net-SSLeay | 1.88-2.module_el8+339+30acecc7 | |
protobuf-c-compiler | 1.3.0-8.el8 | |
protobuf-c-devel | 1.3.0-8.el8 | |
pykickstart | 3.16.16-1.el8 | |
python3-kickstart | 3.16.16-1.el8 | |
rhel-system-roles | 1.22.0-0.6.el8 | |
rpm-ostree | 2022.10.117.g52714b51-1.el8 | |
rpm-ostree-libs | 2022.10.117.g52714b51-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
crash-devel | 7.3.2-6.el8 | |
elfutils-devel-static | 0.189-2.el8 | |
elfutils-libelf-devel-static | 0.189-2.el8 | |
kernel-tools-libs-devel | 4.18.0-490.el8 | |
libtracefs-devel | 1.3.1-2.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-azure-arm | 4.2.1-114.el8 | |
fence-agents-azure-arm | 4.2.1-115.el8 | |
fence-agents-gce | 4.2.1-114.el8 | |
fence-agents-gce | 4.2.1-115.el8 | |
resource-agents | 4.9.0-42.el8 | |
resource-agents-paf | 4.9.0-42.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 25.1.1-1.el8 | |
openstack-nova-api | 25.1.1-1.el8 | |
openstack-nova-common | 25.1.1-1.el8 | |
openstack-nova-compute | 25.1.1-1.el8 | |
openstack-nova-conductor | 25.1.1-1.el8 | |
openstack-nova-migration | 25.1.1-1.el8 | |
openstack-nova-novncproxy | 25.1.1-1.el8 | |
openstack-nova-scheduler | 25.1.1-1.el8 | |
openstack-nova-serialproxy | 25.1.1-1.el8 | |
openstack-nova-spicehtml5proxy | 25.1.1-1.el8 | |
python3-nova | 25.1.1-1.el8 | |
python3-nova-tests | 25.1.1-1.el8 | |

