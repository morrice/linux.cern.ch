## 2021-12-03

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.4-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.4-1.el8s.cern | |

