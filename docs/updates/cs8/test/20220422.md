## 2022-04-22

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8-1.4.18.0_383.el8.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_383.el8-1.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-114.el8 | |
bpftool | 4.18.0-383.el8 | |
kernel | 4.18.0-383.el8 | |
kernel-abi-stablelists | 4.18.0-383.el8 | |
kernel-core | 4.18.0-383.el8 | |
kernel-cross-headers | 4.18.0-383.el8 | |
kernel-debug | 4.18.0-383.el8 | |
kernel-debug-core | 4.18.0-383.el8 | |
kernel-debug-devel | 4.18.0-383.el8 | |
kernel-debug-modules | 4.18.0-383.el8 | |
kernel-debug-modules-extra | 4.18.0-383.el8 | |
kernel-devel | 4.18.0-383.el8 | |
kernel-doc | 4.18.0-383.el8 | |
kernel-headers | 4.18.0-383.el8 | |
kernel-modules | 4.18.0-383.el8 | |
kernel-modules-extra | 4.18.0-383.el8 | |
kernel-tools | 4.18.0-383.el8 | |
kernel-tools-libs | 4.18.0-383.el8 | |
perf | 4.18.0-383.el8 | |
python3-perf | 4.18.0-383.el8 | |
tuna | 0.17-2.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
adwaita-qt5 | 1.2.1-4.el8 | |
binutils-devel | 2.30-114.el8 | |
crash-gcore-command | 1.6.3-2.el8 | |
fribidi | 1.0.4-9.el8 | |
fribidi-devel | 1.0.4-9.el8 | |
libadwaita-qt5 | 1.2.1-4.el8 | |
mod_auth_openidc | 2.4.9.4-1.module_el8.7.0+1136+d8f380b8 | |
postgresql-jdbc | 42.2.14-1.el8 | |
postgresql-jdbc-javadoc | 42.2.14-1.el8 | |
postgresql-odbc | 10.03.0000-3.el8 | |
postgresql-odbc-tests | 10.03.0000-3.el8 | |
python-qt5-rpm-macros | 5.15.0-3.el8 | |
python3-pyqt5-sip | 4.19.25-1.el8 | |
python3-qt5 | 5.15.0-3.el8 | |
python3-qt5-base | 5.15.0-3.el8 | |
python3-wx-siplib | 4.19.25-1.el8 | |
qgnomeplatform | 0.7.1-3.0.1.el8 | |
qgnomeplatform | 0.7.1-3.el8 | |
qt5-assistant | 5.15.3-1.el8 | |
qt5-designer | 5.15.3-1.el8 | |
qt5-doctools | 5.15.3-1.el8 | |
qt5-linguist | 5.15.3-1.el8 | |
qt5-qdbusviewer | 5.15.3-1.el8 | |
qt5-qt3d | 5.15.3-1.el8 | |
qt5-qt3d-devel | 5.15.3-1.el8 | |
qt5-qt3d-examples | 5.15.3-1.el8 | |
qt5-qtbase | 5.15.3-1.el8 | |
qt5-qtbase-common | 5.15.3-1.el8 | |
qt5-qtbase-devel | 5.15.3-1.el8 | |
qt5-qtbase-examples | 5.15.3-1.el8 | |
qt5-qtbase-gui | 5.15.3-1.el8 | |
qt5-qtbase-mysql | 5.15.3-1.el8 | |
qt5-qtbase-odbc | 5.15.3-1.el8 | |
qt5-qtbase-postgresql | 5.15.3-1.el8 | |
qt5-qtbase-private-devel | 5.15.3-1.el8 | |
qt5-qtcanvas3d | 5.12.5-4.el8 | |
qt5-qtcanvas3d-examples | 5.12.5-4.el8 | |
qt5-qtconnectivity | 5.15.3-1.el8 | |
qt5-qtconnectivity-devel | 5.15.3-1.el8 | |
qt5-qtconnectivity-examples | 5.15.3-1.el8 | |
qt5-qtdeclarative | 5.15.3-1.el8 | |
qt5-qtdeclarative-devel | 5.15.3-1.el8 | |
qt5-qtdeclarative-examples | 5.15.3-1.el8 | |
qt5-qtdoc | 5.15.3-1.el8 | |
qt5-qtgraphicaleffects | 5.15.3-1.el8 | |
qt5-qtimageformats | 5.15.3-1.el8 | |
qt5-qtlocation | 5.15.3-1.el8 | |
qt5-qtlocation-devel | 5.15.3-1.el8 | |
qt5-qtlocation-examples | 5.15.3-1.el8 | |
qt5-qtmultimedia | 5.15.3-1.el8 | |
qt5-qtmultimedia-devel | 5.15.3-1.el8 | |
qt5-qtmultimedia-examples | 5.15.3-1.el8 | |
qt5-qtquickcontrols | 5.15.3-1.el8 | |
qt5-qtquickcontrols-examples | 5.15.3-1.el8 | |
qt5-qtquickcontrols2 | 5.15.3-1.el8 | |
qt5-qtquickcontrols2-examples | 5.15.3-1.el8 | |
qt5-qtscript | 5.15.3-1.el8 | |
qt5-qtscript-devel | 5.15.3-1.el8 | |
qt5-qtscript-examples | 5.15.3-1.el8 | |
qt5-qtsensors | 5.15.3-1.el8 | |
qt5-qtsensors-devel | 5.15.3-1.el8 | |
qt5-qtsensors-examples | 5.15.3-1.el8 | |
qt5-qtserialbus | 5.15.3-1.el8 | |
qt5-qtserialbus-examples | 5.15.3-1.el8 | |
qt5-qtserialport | 5.15.3-1.el8 | |
qt5-qtserialport-devel | 5.15.3-1.el8 | |
qt5-qtserialport-examples | 5.15.3-1.el8 | |
qt5-qtsvg | 5.15.3-1.el8 | |
qt5-qtsvg-devel | 5.15.3-1.el8 | |
qt5-qtsvg-examples | 5.15.3-1.el8 | |
qt5-qttools | 5.15.3-1.el8 | |
qt5-qttools-common | 5.15.3-1.el8 | |
qt5-qttools-devel | 5.15.3-1.el8 | |
qt5-qttools-examples | 5.15.3-1.el8 | |
qt5-qttools-libs-designer | 5.15.3-1.el8 | |
qt5-qttools-libs-designercomponents | 5.15.3-1.el8 | |
qt5-qttools-libs-help | 5.15.3-1.el8 | |
qt5-qttranslations | 5.15.3-1.el8 | |
qt5-qtwayland | 5.15.3-1.el8 | |
qt5-qtwayland-examples | 5.15.3-1.el8 | |
qt5-qtwebchannel | 5.15.3-1.el8 | |
qt5-qtwebchannel-devel | 5.15.3-1.el8 | |
qt5-qtwebchannel-examples | 5.15.3-1.el8 | |
qt5-qtwebsockets | 5.15.3-1.el8 | |
qt5-qtwebsockets-devel | 5.15.3-1.el8 | |
qt5-qtwebsockets-examples | 5.15.3-1.el8 | |
qt5-qtx11extras | 5.15.3-1.el8 | |
qt5-qtx11extras-devel | 5.15.3-1.el8 | |
qt5-qtxmlpatterns | 5.15.3-1.el8 | |
qt5-qtxmlpatterns-devel | 5.15.3-1.el8 | |
qt5-qtxmlpatterns-examples | 5.15.3-1.el8 | |
qt5-rpm-macros | 5.15.3-1.el8 | |
qt5-srpm-macros | 5.15.3-1.el8 | |
rsyslog | 8.2102.0-9.el8 | |
rsyslog-crypto | 8.2102.0-9.el8 | |
rsyslog-doc | 8.2102.0-9.el8 | |
rsyslog-elasticsearch | 8.2102.0-9.el8 | |
rsyslog-gnutls | 8.2102.0-9.el8 | |
rsyslog-gssapi | 8.2102.0-9.el8 | |
rsyslog-kafka | 8.2102.0-9.el8 | |
rsyslog-mmaudit | 8.2102.0-9.el8 | |
rsyslog-mmfields | 8.2102.0-9.el8 | |
rsyslog-mmjsonparse | 8.2102.0-9.el8 | |
rsyslog-mmkubernetes | 8.2102.0-9.el8 | |
rsyslog-mmnormalize | 8.2102.0-9.el8 | |
rsyslog-mmsnmptrapd | 8.2102.0-9.el8 | |
rsyslog-mysql | 8.2102.0-9.el8 | |
rsyslog-omamqp1 | 8.2102.0-9.el8 | |
rsyslog-openssl | 8.2102.0-9.el8 | |
rsyslog-pgsql | 8.2102.0-9.el8 | |
rsyslog-relp | 8.2102.0-9.el8 | |
rsyslog-snmp | 8.2102.0-9.el8 | |
rsyslog-udpspoof | 8.2102.0-9.el8 | |
sip | 4.19.25-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-383.el8 | |
python3-qt5-devel | 5.15.0-3.el8 | |
python3-sip-devel | 4.19.25-1.el8 | |
qt5-devel | 5.15.3-1.el8 | |
qt5-qtbase-static | 5.15.3-1.el8 | |
qt5-qtdeclarative-static | 5.15.3-1.el8 | |
qt5-qtquickcontrols2-devel | 5.15.3-1.el8 | |
qt5-qtserialbus-devel | 5.15.3-1.el8 | |
qt5-qttools-static | 5.15.3-1.el8 | |
qt5-qtwayland-devel | 5.15.3-1.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-383.rt7.168.el8 | |
kernel-rt-core | 4.18.0-383.rt7.168.el8 | |
kernel-rt-debug | 4.18.0-383.rt7.168.el8 | |
kernel-rt-debug-core | 4.18.0-383.rt7.168.el8 | |
kernel-rt-debug-devel | 4.18.0-383.rt7.168.el8 | |
kernel-rt-debug-modules | 4.18.0-383.rt7.168.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-383.rt7.168.el8 | |
kernel-rt-devel | 4.18.0-383.rt7.168.el8 | |
kernel-rt-modules | 4.18.0-383.rt7.168.el8 | |
kernel-rt-modules-extra | 4.18.0-383.rt7.168.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.7.1-1.938abd0git.el8 | |
ansible-config_template | 1.2.1-1.bd7543agit.el8 | |
ansible-pacemaker | 1.0.4-2.666f706git.el8 | |
ansible-role-atos-hsm | 3.0.0-1.el8 | |
ansible-role-chrony | 1.2.0-1.el8 | |
ansible-role-collect-logs | 1.5.0-1.el8 | |
ansible-role-collectd-config | 0.0.2-1.el8 | |
ansible-role-container-registry | 1.4.0-1.el8 | |
ansible-role-lunasa-hsm | 3.0.0-1.el8 | |
ansible-role-metalsmith-deployment | 1.6.2-1.el8 | |
ansible-role-openstack-operations | 0.0.1-0.2.2ab288fgit.el8 | |
ansible-role-qdr-config | 0.0.1-1.el8 | |
ansible-role-redhat-subscription | 1.2.0-1.el8 | |
ansible-role-thales-hsm | 3.0.0-1.el8 | |
ansible-role-tripleo-modify-image | 1.4.0-1.el8 | |
ansible-tripleo-ipa | 0.2.2-1.el8 | |
ansible-tripleo-ipsec | 11.0.1-1.el8 | |
openstack-packstack | 20.0.0-1.el8 | |
openstack-packstack-doc | 20.0.0-1.el8 | |
openstack-packstack-puppet | 20.0.0-1.el8 | |
openstack-tempest | 30.1.0-2.el8 | |
openstack-tempest-all | 30.1.0-2.el8 | |
openstack-tripleo-common | 16.4.0-1.el8 | |
openstack-tripleo-common-container-base | 16.4.0-1.el8 | |
openstack-tripleo-common-containers | 16.4.0-1.el8 | |
openstack-tripleo-common-devtools | 16.4.0-1.el8 | |
openstack-tripleo-heat-templates | 16.0.0-1.el8 | |
openstack-tripleo-image-elements | 14.2.0-1.el8 | |
openstack-tripleo-puppet-elements | 15.2.0-1.el8 | |
openstack-tripleo-validations | 15.2.0-1.el8 | |
openstack-tripleo-validations-doc | 15.2.0-1.el8 | |
openstack-tripleo-validations-tests | 15.2.0-1.el8 | |
os-net-config | 15.2.0-1.el8 | |
os-refresh-config | 13.1.0-1.el8 | |
ovn-bgp-agent | 0.2.0-1.el8 | |
ovn-bgp-agent-doc | 0.2.0-1.el8 | |
puppet-midonet | 1.1.0-2.4a44c33git.el8 | |
puppet-pacemaker | 1.4.0-1.el8 | |
puppet-rsyslog | 5.2.1-0.2.0rc0.dfec146git.el8 | |
puppet-tripleo | 16.1.0-1.el8 | |
python-metalsmith-doc | 1.6.2-1.el8 | |
python3-metalsmith | 1.6.2-1.el8 | |
python3-metalsmith-tests | 1.6.2-1.el8 | |
python3-tempest | 30.1.0-2.el8 | |
python3-tempest-tests | 30.1.0-2.el8 | |
python3-tripleo-common | 16.4.0-1.el8 | |
python3-tripleoclient | 18.0.0-1.el8 | |
python3-validations-libs | 1.7.0-1.el8 | |
tripleo-ansible | 4.2.0-1.el8 | |
tripleo-operator-ansible | 0.8.0-1.el8 | |
validations-common | 1.7.0-1.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8-1.4.18.0_383.el8.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_383.el8-1.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
binutils | 2.30-114.el8 | |
bpftool | 4.18.0-383.el8 | |
kernel | 4.18.0-383.el8 | |
kernel-abi-stablelists | 4.18.0-383.el8 | |
kernel-core | 4.18.0-383.el8 | |
kernel-cross-headers | 4.18.0-383.el8 | |
kernel-debug | 4.18.0-383.el8 | |
kernel-debug-core | 4.18.0-383.el8 | |
kernel-debug-devel | 4.18.0-383.el8 | |
kernel-debug-modules | 4.18.0-383.el8 | |
kernel-debug-modules-extra | 4.18.0-383.el8 | |
kernel-devel | 4.18.0-383.el8 | |
kernel-doc | 4.18.0-383.el8 | |
kernel-headers | 4.18.0-383.el8 | |
kernel-modules | 4.18.0-383.el8 | |
kernel-modules-extra | 4.18.0-383.el8 | |
kernel-tools | 4.18.0-383.el8 | |
kernel-tools-libs | 4.18.0-383.el8 | |
perf | 4.18.0-383.el8 | |
python3-perf | 4.18.0-383.el8 | |
tuna | 0.17-2.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
adwaita-qt5 | 1.2.1-4.el8 | |
binutils-devel | 2.30-114.el8 | |
crash-gcore-command | 1.6.3-2.el8 | |
fribidi | 1.0.4-9.el8 | |
fribidi-devel | 1.0.4-9.el8 | |
libadwaita-qt5 | 1.2.1-4.el8 | |
mod_auth_openidc | 2.4.9.4-1.module_el8.7.0+1136+d8f380b8 | |
postgresql-jdbc | 42.2.14-1.el8 | |
postgresql-jdbc-javadoc | 42.2.14-1.el8 | |
postgresql-odbc | 10.03.0000-3.el8 | |
postgresql-odbc-tests | 10.03.0000-3.el8 | |
python-qt5-rpm-macros | 5.15.0-3.el8 | |
python3-pyqt5-sip | 4.19.25-1.el8 | |
python3-qt5 | 5.15.0-3.el8 | |
python3-qt5-base | 5.15.0-3.el8 | |
python3-wx-siplib | 4.19.25-1.el8 | |
qgnomeplatform | 0.7.1-3.0.1.el8 | |
qgnomeplatform | 0.7.1-3.el8 | |
qt5-assistant | 5.15.3-1.el8 | |
qt5-designer | 5.15.3-1.el8 | |
qt5-doctools | 5.15.3-1.el8 | |
qt5-linguist | 5.15.3-1.el8 | |
qt5-qdbusviewer | 5.15.3-1.el8 | |
qt5-qt3d | 5.15.3-1.el8 | |
qt5-qt3d-devel | 5.15.3-1.el8 | |
qt5-qt3d-examples | 5.15.3-1.el8 | |
qt5-qtbase | 5.15.3-1.el8 | |
qt5-qtbase-common | 5.15.3-1.el8 | |
qt5-qtbase-devel | 5.15.3-1.el8 | |
qt5-qtbase-examples | 5.15.3-1.el8 | |
qt5-qtbase-gui | 5.15.3-1.el8 | |
qt5-qtbase-mysql | 5.15.3-1.el8 | |
qt5-qtbase-odbc | 5.15.3-1.el8 | |
qt5-qtbase-postgresql | 5.15.3-1.el8 | |
qt5-qtbase-private-devel | 5.15.3-1.el8 | |
qt5-qtcanvas3d | 5.12.5-4.el8 | |
qt5-qtcanvas3d-examples | 5.12.5-4.el8 | |
qt5-qtconnectivity | 5.15.3-1.el8 | |
qt5-qtconnectivity-devel | 5.15.3-1.el8 | |
qt5-qtconnectivity-examples | 5.15.3-1.el8 | |
qt5-qtdeclarative | 5.15.3-1.el8 | |
qt5-qtdeclarative-devel | 5.15.3-1.el8 | |
qt5-qtdeclarative-examples | 5.15.3-1.el8 | |
qt5-qtdoc | 5.15.3-1.el8 | |
qt5-qtgraphicaleffects | 5.15.3-1.el8 | |
qt5-qtimageformats | 5.15.3-1.el8 | |
qt5-qtlocation | 5.15.3-1.el8 | |
qt5-qtlocation-devel | 5.15.3-1.el8 | |
qt5-qtlocation-examples | 5.15.3-1.el8 | |
qt5-qtmultimedia | 5.15.3-1.el8 | |
qt5-qtmultimedia-devel | 5.15.3-1.el8 | |
qt5-qtmultimedia-examples | 5.15.3-1.el8 | |
qt5-qtquickcontrols | 5.15.3-1.el8 | |
qt5-qtquickcontrols-examples | 5.15.3-1.el8 | |
qt5-qtquickcontrols2 | 5.15.3-1.el8 | |
qt5-qtquickcontrols2-examples | 5.15.3-1.el8 | |
qt5-qtscript | 5.15.3-1.el8 | |
qt5-qtscript-devel | 5.15.3-1.el8 | |
qt5-qtscript-examples | 5.15.3-1.el8 | |
qt5-qtsensors | 5.15.3-1.el8 | |
qt5-qtsensors-devel | 5.15.3-1.el8 | |
qt5-qtsensors-examples | 5.15.3-1.el8 | |
qt5-qtserialbus | 5.15.3-1.el8 | |
qt5-qtserialbus-examples | 5.15.3-1.el8 | |
qt5-qtserialport | 5.15.3-1.el8 | |
qt5-qtserialport-devel | 5.15.3-1.el8 | |
qt5-qtserialport-examples | 5.15.3-1.el8 | |
qt5-qtsvg | 5.15.3-1.el8 | |
qt5-qtsvg-devel | 5.15.3-1.el8 | |
qt5-qtsvg-examples | 5.15.3-1.el8 | |
qt5-qttools | 5.15.3-1.el8 | |
qt5-qttools-common | 5.15.3-1.el8 | |
qt5-qttools-devel | 5.15.3-1.el8 | |
qt5-qttools-examples | 5.15.3-1.el8 | |
qt5-qttools-libs-designer | 5.15.3-1.el8 | |
qt5-qttools-libs-designercomponents | 5.15.3-1.el8 | |
qt5-qttools-libs-help | 5.15.3-1.el8 | |
qt5-qttranslations | 5.15.3-1.el8 | |
qt5-qtwayland | 5.15.3-1.el8 | |
qt5-qtwayland-examples | 5.15.3-1.el8 | |
qt5-qtwebchannel | 5.15.3-1.el8 | |
qt5-qtwebchannel-devel | 5.15.3-1.el8 | |
qt5-qtwebchannel-examples | 5.15.3-1.el8 | |
qt5-qtwebsockets | 5.15.3-1.el8 | |
qt5-qtwebsockets-devel | 5.15.3-1.el8 | |
qt5-qtwebsockets-examples | 5.15.3-1.el8 | |
qt5-qtx11extras | 5.15.3-1.el8 | |
qt5-qtx11extras-devel | 5.15.3-1.el8 | |
qt5-qtxmlpatterns | 5.15.3-1.el8 | |
qt5-qtxmlpatterns-devel | 5.15.3-1.el8 | |
qt5-qtxmlpatterns-examples | 5.15.3-1.el8 | |
qt5-rpm-macros | 5.15.3-1.el8 | |
qt5-srpm-macros | 5.15.3-1.el8 | |
rsyslog | 8.2102.0-9.el8 | |
rsyslog-crypto | 8.2102.0-9.el8 | |
rsyslog-doc | 8.2102.0-9.el8 | |
rsyslog-elasticsearch | 8.2102.0-9.el8 | |
rsyslog-gnutls | 8.2102.0-9.el8 | |
rsyslog-gssapi | 8.2102.0-9.el8 | |
rsyslog-kafka | 8.2102.0-9.el8 | |
rsyslog-mmaudit | 8.2102.0-9.el8 | |
rsyslog-mmfields | 8.2102.0-9.el8 | |
rsyslog-mmjsonparse | 8.2102.0-9.el8 | |
rsyslog-mmkubernetes | 8.2102.0-9.el8 | |
rsyslog-mmnormalize | 8.2102.0-9.el8 | |
rsyslog-mmsnmptrapd | 8.2102.0-9.el8 | |
rsyslog-mysql | 8.2102.0-9.el8 | |
rsyslog-omamqp1 | 8.2102.0-9.el8 | |
rsyslog-openssl | 8.2102.0-9.el8 | |
rsyslog-pgsql | 8.2102.0-9.el8 | |
rsyslog-relp | 8.2102.0-9.el8 | |
rsyslog-snmp | 8.2102.0-9.el8 | |
rsyslog-udpspoof | 8.2102.0-9.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-383.el8 | |
python3-qt5-devel | 5.15.0-3.el8 | |
python3-sip-devel | 4.19.25-1.el8 | |
qt5-devel | 5.15.3-1.el8 | |
qt5-qtbase-static | 5.15.3-1.el8 | |
qt5-qtdeclarative-static | 5.15.3-1.el8 | |
qt5-qtquickcontrols2-devel | 5.15.3-1.el8 | |
qt5-qtserialbus-devel | 5.15.3-1.el8 | |
qt5-qttools-static | 5.15.3-1.el8 | |
qt5-qtwayland-devel | 5.15.3-1.el8 | |
sip | 4.19.25-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.7.1-1.938abd0git.el8 | |
ansible-config_template | 1.2.1-1.bd7543agit.el8 | |
ansible-pacemaker | 1.0.4-2.666f706git.el8 | |
ansible-role-atos-hsm | 3.0.0-1.el8 | |
ansible-role-chrony | 1.2.0-1.el8 | |
ansible-role-collect-logs | 1.5.0-1.el8 | |
ansible-role-collectd-config | 0.0.2-1.el8 | |
ansible-role-container-registry | 1.4.0-1.el8 | |
ansible-role-lunasa-hsm | 3.0.0-1.el8 | |
ansible-role-metalsmith-deployment | 1.6.2-1.el8 | |
ansible-role-openstack-operations | 0.0.1-0.2.2ab288fgit.el8 | |
ansible-role-qdr-config | 0.0.1-1.el8 | |
ansible-role-redhat-subscription | 1.2.0-1.el8 | |
ansible-role-thales-hsm | 3.0.0-1.el8 | |
ansible-role-tripleo-modify-image | 1.4.0-1.el8 | |
ansible-tripleo-ipa | 0.2.2-1.el8 | |
ansible-tripleo-ipsec | 11.0.1-1.el8 | |
openstack-packstack | 20.0.0-1.el8 | |
openstack-packstack-doc | 20.0.0-1.el8 | |
openstack-packstack-puppet | 20.0.0-1.el8 | |
openstack-tempest | 30.1.0-2.el8 | |
openstack-tempest-all | 30.1.0-2.el8 | |
openstack-tripleo-common | 16.4.0-1.el8 | |
openstack-tripleo-common-container-base | 16.4.0-1.el8 | |
openstack-tripleo-common-containers | 16.4.0-1.el8 | |
openstack-tripleo-common-devtools | 16.4.0-1.el8 | |
openstack-tripleo-heat-templates | 16.0.0-1.el8 | |
openstack-tripleo-image-elements | 14.2.0-1.el8 | |
openstack-tripleo-puppet-elements | 15.2.0-1.el8 | |
openstack-tripleo-validations | 15.2.0-1.el8 | |
openstack-tripleo-validations-doc | 15.2.0-1.el8 | |
openstack-tripleo-validations-tests | 15.2.0-1.el8 | |
os-net-config | 15.2.0-1.el8 | |
os-refresh-config | 13.1.0-1.el8 | |
ovn-bgp-agent | 0.2.0-1.el8 | |
ovn-bgp-agent-doc | 0.2.0-1.el8 | |
puppet-midonet | 1.1.0-2.4a44c33git.el8 | |
puppet-pacemaker | 1.4.0-1.el8 | |
puppet-rsyslog | 5.2.1-0.2.0rc0.dfec146git.el8 | |
puppet-tripleo | 16.1.0-1.el8 | |
python-metalsmith-doc | 1.6.2-1.el8 | |
python3-metalsmith | 1.6.2-1.el8 | |
python3-metalsmith-tests | 1.6.2-1.el8 | |
python3-tempest | 30.1.0-2.el8 | |
python3-tempest-tests | 30.1.0-2.el8 | |
python3-tripleo-common | 16.4.0-1.el8 | |
python3-tripleoclient | 18.0.0-1.el8 | |
python3-validations-libs | 1.7.0-1.el8 | |
tripleo-ansible | 4.2.0-1.el8 | |
tripleo-operator-ansible | 0.8.0-1.el8 | |
validations-common | 1.7.0-1.el8 | |

