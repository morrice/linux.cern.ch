## 2021-07-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-Convert-ASN1 | 0.27-16.el8s.cern | |
perl-Convert-ASN1-tests | 0.27-16.el8s.cern | |
perl-MIME-tools | 5.509-9.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.5.0-1.el8 | |
openstack-ironic-api | 17.0.3-1.el8 | |
openstack-ironic-common | 17.0.3-1.el8 | |
openstack-ironic-conductor | 17.0.3-1.el8 | |
openstack-kolla | 12.0.0-1.el8 | |
openstack-nova | 23.0.1-1.el8 | |
openstack-nova-api | 23.0.1-1.el8 | |
openstack-nova-common | 23.0.1-1.el8 | |
openstack-nova-compute | 23.0.1-1.el8 | |
openstack-nova-conductor | 23.0.1-1.el8 | |
openstack-nova-migration | 23.0.1-1.el8 | |
openstack-nova-novncproxy | 23.0.1-1.el8 | |
openstack-nova-scheduler | 23.0.1-1.el8 | |
openstack-nova-serialproxy | 23.0.1-1.el8 | |
openstack-nova-spicehtml5proxy | 23.0.1-1.el8 | |
openstack-packstack | 18.0.0-1.el8 | |
openstack-packstack-doc | 18.0.0-1.el8 | |
openstack-packstack-puppet | 18.0.0-1.el8 | |
openstack-placement-api | 5.0.1-1.el8 | |
openstack-placement-common | 5.0.1-1.el8 | |
openstack-placement-doc | 5.0.1-1.el8 | |
openstack-selinux | 0.8.27-1.el8 | |
openstack-selinux-devel | 0.8.27-1.el8 | |
openstack-selinux-test | 0.8.27-1.el8 | |
openstack-tripleo-common | 15.2.1-1.el8 | |
openstack-tripleo-common-container-base | 15.2.1-1.el8 | |
openstack-tripleo-common-containers | 15.2.1-1.el8 | |
openstack-tripleo-common-devtools | 15.2.1-1.el8 | |
openstack-tripleo-heat-templates | 14.1.2-1.el8 | |
openstack-tripleo-image-elements | 13.1.1-1.el8 | |
openstack-tripleo-puppet-elements | 14.1.1-1.el8 | |
openstack-tripleo-validations | 14.1.1-1.el8 | |
openstack-tripleo-validations-doc | 14.1.1-1.el8 | |
openstack-tripleo-validations-tests | 14.1.1-1.el8 | |
puppet-tripleo | 14.2.0-1.el8 | |
python-magnumclient-doc | 3.4.1-1.el8 | |
python-octavia-tests-tempest-doc | 1.7.0-1.el8 | |
python-sushy-doc | 3.7.2-1.el8 | |
python3-ironic-tests | 17.0.3-1.el8 | |
python3-magnumclient | 3.4.1-1.el8 | |
python3-magnumclient-tests | 3.4.1-1.el8 | |
python3-nova | 23.0.1-1.el8 | |
python3-nova-tests | 23.0.1-1.el8 | |
python3-octavia-tests-tempest | 1.7.0-1.el8 | |
python3-octavia-tests-tempest-golang | 1.7.0-1.el8 | |
python3-placement | 5.0.1-1.el8 | |
python3-placement-tests | 5.0.1-1.el8 | |
python3-sushy | 3.7.2-1.el8 | |
python3-sushy-tests | 3.7.2-1.el8 | |
python3-tooz | 2.8.1-1.el8 | |
python3-tripleo-common | 15.2.1-1.el8 | |
python3-tripleoclient | 16.2.1-1.el8 | |
tripleo-ansible | 3.1.2-1.el8 | |
tripleo-operator-ansible | 0.5.1-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-Convert-ASN1 | 0.27-16.el8s.cern | |
perl-Convert-ASN1-tests | 0.27-16.el8s.cern | |
perl-MIME-tools | 5.509-9.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.5.0-1.el8 | |
openstack-ironic-api | 17.0.3-1.el8 | |
openstack-ironic-common | 17.0.3-1.el8 | |
openstack-ironic-conductor | 17.0.3-1.el8 | |
openstack-kolla | 12.0.0-1.el8 | |
openstack-nova | 23.0.1-1.el8 | |
openstack-nova-api | 23.0.1-1.el8 | |
openstack-nova-common | 23.0.1-1.el8 | |
openstack-nova-compute | 23.0.1-1.el8 | |
openstack-nova-conductor | 23.0.1-1.el8 | |
openstack-nova-migration | 23.0.1-1.el8 | |
openstack-nova-novncproxy | 23.0.1-1.el8 | |
openstack-nova-scheduler | 23.0.1-1.el8 | |
openstack-nova-serialproxy | 23.0.1-1.el8 | |
openstack-nova-spicehtml5proxy | 23.0.1-1.el8 | |
openstack-packstack | 18.0.0-1.el8 | |
openstack-packstack-doc | 18.0.0-1.el8 | |
openstack-packstack-puppet | 18.0.0-1.el8 | |
openstack-placement-api | 5.0.1-1.el8 | |
openstack-placement-common | 5.0.1-1.el8 | |
openstack-placement-doc | 5.0.1-1.el8 | |
openstack-selinux | 0.8.27-1.el8 | |
openstack-selinux-devel | 0.8.27-1.el8 | |
openstack-selinux-test | 0.8.27-1.el8 | |
openstack-tripleo-common | 15.2.1-1.el8 | |
openstack-tripleo-common-container-base | 15.2.1-1.el8 | |
openstack-tripleo-common-containers | 15.2.1-1.el8 | |
openstack-tripleo-common-devtools | 15.2.1-1.el8 | |
openstack-tripleo-heat-templates | 14.1.2-1.el8 | |
openstack-tripleo-image-elements | 13.1.1-1.el8 | |
openstack-tripleo-puppet-elements | 14.1.1-1.el8 | |
openstack-tripleo-validations | 14.1.1-1.el8 | |
openstack-tripleo-validations-doc | 14.1.1-1.el8 | |
openstack-tripleo-validations-tests | 14.1.1-1.el8 | |
puppet-tripleo | 14.2.0-1.el8 | |
python-magnumclient-doc | 3.4.1-1.el8 | |
python-octavia-tests-tempest-doc | 1.7.0-1.el8 | |
python-sushy-doc | 3.7.2-1.el8 | |
python3-ironic-tests | 17.0.3-1.el8 | |
python3-magnumclient | 3.4.1-1.el8 | |
python3-magnumclient-tests | 3.4.1-1.el8 | |
python3-nova | 23.0.1-1.el8 | |
python3-nova-tests | 23.0.1-1.el8 | |
python3-octavia-tests-tempest | 1.7.0-1.el8 | |
python3-octavia-tests-tempest-golang | 1.7.0-1.el8 | |
python3-placement | 5.0.1-1.el8 | |
python3-placement-tests | 5.0.1-1.el8 | |
python3-sushy | 3.7.2-1.el8 | |
python3-sushy-tests | 3.7.2-1.el8 | |
python3-tooz | 2.8.1-1.el8 | |
python3-tripleo-common | 15.2.1-1.el8 | |
python3-tripleoclient | 16.2.1-1.el8 | |
tripleo-ansible | 3.1.2-1.el8 | |
tripleo-operator-ansible | 0.5.1-1.el8 | |

