## 2022-06-28

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-pyroute2 | 0.6.6-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gluster-ansible-features | 1.0.5-15.el8 | |
gluster-ansible-infra | 1.0.4-22.el8 | |
gluster-ansible-roles | 1.0.5-28.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-pyroute2 | 0.6.6-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gluster-ansible-features | 1.0.5-15.el8 | |
gluster-ansible-infra | 1.0.4-22.el8 | |
gluster-ansible-roles | 1.0.5-28.el8 | |

