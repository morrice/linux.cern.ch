## 2021-03-09

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-role-chrony | 1.0.3-1.el8 | |
ansible-role-container-registry | 1.3.0-1.el8 | |
ansible-role-redhat-subscription | 1.1.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-role-chrony | 1.0.3-1.el8 | |
ansible-role-container-registry | 1.3.0-1.el8 | |
ansible-role-redhat-subscription | 1.1.2-1.el8 | |

