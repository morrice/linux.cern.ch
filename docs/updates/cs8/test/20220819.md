## 2022-08-19

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cloudkitty-ui | 14.0.1-1.el8 | |
openstack-cloudkitty-ui-doc | 14.0.1-1.el8 | |
openstack-octavia-ui | 7.0.1-1.el8 | |
openstack-tripleo-validations | 14.3.1-1.el8 | |
openstack-tripleo-validations-doc | 14.3.1-1.el8 | |
openstack-tripleo-validations-tests | 14.3.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cloudkitty-ui | 14.0.1-1.el8 | |
openstack-cloudkitty-ui-doc | 14.0.1-1.el8 | |
openstack-octavia-ui | 7.0.1-1.el8 | |
openstack-tripleo-validations | 14.3.1-1.el8 | |
openstack-tripleo-validations-doc | 14.3.1-1.el8 | |
openstack-tripleo-validations-tests | 14.3.1-1.el8 | |

