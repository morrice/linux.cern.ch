## 2021-01-28

### BaseOS x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.8.29-6.el8_3.1 | | 
tzdata | 2021a-1.el8 | | 

### AppStream x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
podman | 1.0.0-6.git921f98f.module_el8.4.0+653+7095eaa9 | | 
podman-docker | 1.0.0-6.git921f98f.module_el8.4.0+653+7095eaa9 | | 
tzdata-java | 2021a-1.el8 | | 

### BaseOS aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sudo | 1.8.29-6.el8_3.1 | | 
tzdata | 2021a-1.el8 | | 

### AppStream aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
podman | 1.0.0-6.git921f98f.module_el8.4.0+653+7095eaa9 | | 
podman-docker | 1.0.0-6.git921f98f.module_el8.4.0+653+7095eaa9 | | 
tzdata-java | 2021a-1.el8 | | 

