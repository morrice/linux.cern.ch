## 2022-08-11

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 12.4.0-1.el8 | |
openstack-kolla | 13.3.0-1.el8 | |
openstack-kolla | 14.3.0-1.el8 | |
python-networking-generic-switch-doc | 5.0.1-1.el8 | |
python-oslo-vmware-doc | 3.8.2-1.el8 | |
python-oslo-vmware-doc | 3.9.3-1.el8 | |
python-oslo-vmware-lang | 3.8.2-1.el8 | |
python-oslo-vmware-lang | 3.9.3-1.el8 | |
python3-ironic-lib | 4.6.4-1.el8 | |
python3-networking-generic-switch | 5.0.1-1.el8 | |
python3-networking-generic-switch-tests | 5.0.1-1.el8 | |
python3-oslo-vmware | 3.8.2-1.el8 | |
python3-oslo-vmware | 3.9.3-1.el8 | |
python3-oslo-vmware-tests | 3.8.2-1.el8 | |
python3-oslo-vmware-tests | 3.9.3-1.el8 | |
python3-stevedore | 3.4.1-1.el8 | |
python3-tooz | 2.8.3-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-ovirt-dashboard | 0.16.2-1.el8 | |
mom | 0.6.3-1.el8 | |
otopi-common | 1.10.2-1.el8 | |
otopi-debug-plugins | 1.10.2-1.el8 | |
ovirt-ansible-collection | 2.2.2-1.el8 | |
ovirt-engine-api-metamodel | 1.3.9-1.el8 | |
ovirt-engine-api-metamodel-server | 1.3.9-1.el8 | |
ovirt-engine-api-model | 4.5.11-1.el8 | |
ovirt-engine-dwh | 4.5.4-1.el8 | |
ovirt-engine-dwh-grafana-integration-setup | 4.5.4-1.el8 | |
ovirt-engine-dwh-setup | 4.5.4-1.el8 | |
ovirt-engine-extension-aaa-ldap | 1.4.6-1.el8 | |
ovirt-engine-extension-aaa-ldap-setup | 1.4.6-1.el8 | |
ovirt-engine-nodejs-modules | 2.3.9-1.el8 | |
ovirt-engine-ui-extensions | 1.3.5-1.el8 | |
ovirt-hosted-engine-setup | 2.6.5-1.el8 | |
ovirt-imageio-client | 2.4.6-1.el8 | |
ovirt-imageio-common | 2.4.6-1.el8 | |
ovirt-imageio-daemon | 2.4.6-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.2-1.el8 | |
ovirt-openvswitch | 2.15-4.el8 | |
ovirt-openvswitch-devel | 2.15-4.el8 | |
ovirt-openvswitch-ipsec | 2.15-4.el8 | |
ovirt-openvswitch-ovn | 2.15-4.el8 | |
ovirt-openvswitch-ovn-central | 2.15-4.el8 | |
ovirt-openvswitch-ovn-common | 2.15-4.el8 | |
ovirt-openvswitch-ovn-host | 2.15-4.el8 | |
ovirt-openvswitch-ovn-vtep | 2.15-4.el8 | |
ovirt-python-openvswitch | 2.15-4.el8 | |
ovirt-release-host-node | 4.5.2-1.el8 | |
ovirt-web-ui | 1.9.1-1.el8 | |
python3-otopi | 1.10.2-1.el8 | |
python38-ovirt-imageio-client | 2.4.6-1.el8 | |
python38-ovirt-imageio-common | 2.4.6-1.el8 | |
selinux-policy | 3.14.3-105.el8 | |
selinux-policy-devel | 3.14.3-105.el8 | |
selinux-policy-doc | 3.14.3-105.el8 | |
selinux-policy-minimum | 3.14.3-105.el8 | |
selinux-policy-mls | 3.14.3-105.el8 | |
selinux-policy-sandbox | 3.14.3-105.el8 | |
selinux-policy-targeted | 3.14.3-105.el8 | |
unboundid-ldapsdk | 6.0.4-1.el8 | |
unboundid-ldapsdk-javadoc | 6.0.4-1.el8 | |
vdsm | 4.50.2.2-1.el8 | |
vdsm-api | 4.50.2.2-1.el8 | |
vdsm-client | 4.50.2.2-1.el8 | |
vdsm-common | 4.50.2.2-1.el8 | |
vdsm-gluster | 4.50.2.2-1.el8 | |
vdsm-hook-allocate_net | 4.50.2.2-1.el8 | |
vdsm-hook-boot_hostdev | 4.50.2.2-1.el8 | |
vdsm-hook-checkimages | 4.50.2.2-1.el8 | |
vdsm-hook-checkips | 4.50.2.2-1.el8 | |
vdsm-hook-cpuflags | 4.50.2.2-1.el8 | |
vdsm-hook-diskunmap | 4.50.2.2-1.el8 | |
vdsm-hook-ethtool-options | 4.50.2.2-1.el8 | |
vdsm-hook-extnet | 4.50.2.2-1.el8 | |
vdsm-hook-extra-ipv4-addrs | 4.50.2.2-1.el8 | |
vdsm-hook-fakevmstats | 4.50.2.2-1.el8 | |
vdsm-hook-faqemu | 4.50.2.2-1.el8 | |
vdsm-hook-fcoe | 4.50.2.2-1.el8 | |
vdsm-hook-fileinject | 4.50.2.2-1.el8 | |
vdsm-hook-httpsisoboot | 4.50.2.2-1.el8 | |
vdsm-hook-localdisk | 4.50.2.2-1.el8 | |
vdsm-hook-log-console | 4.50.2.2-1.el8 | |
vdsm-hook-log-firmware | 4.50.2.2-1.el8 | |
vdsm-hook-macbind | 4.50.2.2-1.el8 | |
vdsm-hook-nestedvt | 4.50.2.2-1.el8 | |
vdsm-hook-openstacknet | 4.50.2.2-1.el8 | |
vdsm-hook-qemucmdline | 4.50.2.2-1.el8 | |
vdsm-hook-scratchpad | 4.50.2.2-1.el8 | |
vdsm-hook-smbios | 4.50.2.2-1.el8 | |
vdsm-hook-spiceoptions | 4.50.2.2-1.el8 | |
vdsm-hook-vhostmd | 4.50.2.2-1.el8 | |
vdsm-hook-vmfex-dev | 4.50.2.2-1.el8 | |
vdsm-http | 4.50.2.2-1.el8 | |
vdsm-jsonrpc | 4.50.2.2-1.el8 | |
vdsm-network | 4.50.2.2-1.el8 | |
vdsm-python | 4.50.2.2-1.el8 | |
vdsm-yajsonrpc | 4.50.2.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 12.4.0-1.el8 | |
openstack-kolla | 13.3.0-1.el8 | |
openstack-kolla | 14.3.0-1.el8 | |
python-networking-generic-switch-doc | 5.0.1-1.el8 | |
python-oslo-vmware-doc | 3.8.2-1.el8 | |
python-oslo-vmware-doc | 3.9.3-1.el8 | |
python-oslo-vmware-lang | 3.8.2-1.el8 | |
python-oslo-vmware-lang | 3.9.3-1.el8 | |
python3-ironic-lib | 4.6.4-1.el8 | |
python3-networking-generic-switch | 5.0.1-1.el8 | |
python3-networking-generic-switch-tests | 5.0.1-1.el8 | |
python3-oslo-vmware | 3.8.2-1.el8 | |
python3-oslo-vmware | 3.9.3-1.el8 | |
python3-oslo-vmware-tests | 3.8.2-1.el8 | |
python3-oslo-vmware-tests | 3.9.3-1.el8 | |
python3-stevedore | 3.4.1-1.el8 | |
python3-tooz | 2.8.3-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-ovirt-dashboard | 0.16.2-1.el8 | |
mom | 0.6.3-1.el8 | |
otopi-common | 1.10.2-1.el8 | |
otopi-debug-plugins | 1.10.2-1.el8 | |
ovirt-ansible-collection | 2.2.2-1.el8 | |
ovirt-engine-api-metamodel | 1.3.9-1.el8 | |
ovirt-engine-api-metamodel-server | 1.3.9-1.el8 | |
ovirt-engine-api-model | 4.5.11-1.el8 | |
ovirt-engine-dwh | 4.5.4-1.el8 | |
ovirt-engine-dwh-grafana-integration-setup | 4.5.4-1.el8 | |
ovirt-engine-dwh-setup | 4.5.4-1.el8 | |
ovirt-engine-extension-aaa-ldap | 1.4.6-1.el8 | |
ovirt-engine-extension-aaa-ldap-setup | 1.4.6-1.el8 | |
ovirt-engine-nodejs-modules | 2.3.9-1.el8 | |
ovirt-engine-ui-extensions | 1.3.5-1.el8 | |
ovirt-hosted-engine-setup | 2.6.5-1.el8 | |
ovirt-imageio-client | 2.4.6-1.el8 | |
ovirt-imageio-common | 2.4.6-1.el8 | |
ovirt-imageio-daemon | 2.4.6-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.2-1.el8 | |
ovirt-openvswitch | 2.15-4.el8 | |
ovirt-openvswitch-devel | 2.15-4.el8 | |
ovirt-openvswitch-ipsec | 2.15-4.el8 | |
ovirt-openvswitch-ovn | 2.15-4.el8 | |
ovirt-openvswitch-ovn-central | 2.15-4.el8 | |
ovirt-openvswitch-ovn-common | 2.15-4.el8 | |
ovirt-openvswitch-ovn-host | 2.15-4.el8 | |
ovirt-openvswitch-ovn-vtep | 2.15-4.el8 | |
ovirt-python-openvswitch | 2.15-4.el8 | |
ovirt-release-host-node | 4.5.2-1.el8 | |
ovirt-web-ui | 1.9.1-1.el8 | |
python3-otopi | 1.10.2-1.el8 | |
python38-ovirt-imageio-client | 2.4.6-1.el8 | |
python38-ovirt-imageio-common | 2.4.6-1.el8 | |
selinux-policy | 3.14.3-105.el8 | |
selinux-policy-devel | 3.14.3-105.el8 | |
selinux-policy-doc | 3.14.3-105.el8 | |
selinux-policy-minimum | 3.14.3-105.el8 | |
selinux-policy-mls | 3.14.3-105.el8 | |
selinux-policy-sandbox | 3.14.3-105.el8 | |
selinux-policy-targeted | 3.14.3-105.el8 | |
unboundid-ldapsdk | 6.0.4-1.el8 | |
unboundid-ldapsdk-javadoc | 6.0.4-1.el8 | |
vdsm | 4.50.2.2-1.el8 | |
vdsm-api | 4.50.2.2-1.el8 | |
vdsm-client | 4.50.2.2-1.el8 | |
vdsm-common | 4.50.2.2-1.el8 | |
vdsm-gluster | 4.50.2.2-1.el8 | |
vdsm-hook-allocate_net | 4.50.2.2-1.el8 | |
vdsm-hook-boot_hostdev | 4.50.2.2-1.el8 | |
vdsm-hook-checkimages | 4.50.2.2-1.el8 | |
vdsm-hook-checkips | 4.50.2.2-1.el8 | |
vdsm-hook-cpuflags | 4.50.2.2-1.el8 | |
vdsm-hook-diskunmap | 4.50.2.2-1.el8 | |
vdsm-hook-ethtool-options | 4.50.2.2-1.el8 | |
vdsm-hook-extnet | 4.50.2.2-1.el8 | |
vdsm-hook-extra-ipv4-addrs | 4.50.2.2-1.el8 | |
vdsm-hook-fakevmstats | 4.50.2.2-1.el8 | |
vdsm-hook-faqemu | 4.50.2.2-1.el8 | |
vdsm-hook-fcoe | 4.50.2.2-1.el8 | |
vdsm-hook-fileinject | 4.50.2.2-1.el8 | |
vdsm-hook-httpsisoboot | 4.50.2.2-1.el8 | |
vdsm-hook-localdisk | 4.50.2.2-1.el8 | |
vdsm-hook-log-console | 4.50.2.2-1.el8 | |
vdsm-hook-log-firmware | 4.50.2.2-1.el8 | |
vdsm-hook-macbind | 4.50.2.2-1.el8 | |
vdsm-hook-nestedvt | 4.50.2.2-1.el8 | |
vdsm-hook-openstacknet | 4.50.2.2-1.el8 | |
vdsm-hook-qemucmdline | 4.50.2.2-1.el8 | |
vdsm-hook-scratchpad | 4.50.2.2-1.el8 | |
vdsm-hook-smbios | 4.50.2.2-1.el8 | |
vdsm-hook-spiceoptions | 4.50.2.2-1.el8 | |
vdsm-hook-vhostmd | 4.50.2.2-1.el8 | |
vdsm-hook-vmfex-dev | 4.50.2.2-1.el8 | |
vdsm-http | 4.50.2.2-1.el8 | |
vdsm-jsonrpc | 4.50.2.2-1.el8 | |
vdsm-network | 4.50.2.2-1.el8 | |
vdsm-python | 4.50.2.2-1.el8 | |
vdsm-yajsonrpc | 4.50.2.2-1.el8 | |

