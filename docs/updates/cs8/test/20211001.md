## 2021-10-01

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 17.2.0-1.el8 | |
python-cinderclient-doc | 7.2.2-1.el8 | |
python3-cinder | 17.2.0-1.el8 | |
python3-cinder-common | 17.2.0-1.el8 | |
python3-cinder-tests | 17.2.0-1.el8 | |
python3-cinderclient | 7.2.2-1.el8 | |
python3-os-brick | 4.0.4-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 17.2.0-1.el8 | |
python-cinderclient-doc | 7.2.2-1.el8 | |
python3-cinder | 17.2.0-1.el8 | |
python3-cinder-common | 17.2.0-1.el8 | |
python3-cinder-tests | 17.2.0-1.el8 | |
python3-cinderclient | 7.2.2-1.el8 | |
python3-os-brick | 4.0.4-1.el8 | |

