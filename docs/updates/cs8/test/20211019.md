## 2021-10-19

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-glance-tests-tempest | 0.2.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-glance-tests-tempest | 0.2.0-1.el8 | |

