## 2022-10-21

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-node-ng-image-update-placeholder | 4.5.3.1-1.el8 | |
ovirt-release-host-node | 4.5.3.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-node-ng-image-update-placeholder | 4.5.3.1-1.el8 | |
ovirt-release-host-node | 4.5.3.1-1.el8 | |

