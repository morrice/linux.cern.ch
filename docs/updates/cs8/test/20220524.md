## 2022-05-24

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 1.9.0-1.el8 | |
python-oslo-service-doc | 2.6.2-1.el8 | |
python-oslo-utils-doc | 4.10.2-1.el8 | |
python-oslo-utils-lang | 4.10.2-1.el8 | |
python3-heat-tests-tempest | 1.5.0-1.el8 | |
python3-neutron-tests-tempest | 1.9.0-1.el8 | |
python3-oslo-service | 2.6.2-1.el8 | |
python3-oslo-service-tests | 2.6.2-1.el8 | |
python3-oslo-utils | 4.10.2-1.el8 | |
python3-oslo-utils-tests | 4.10.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-tests-tempest-doc | 1.9.0-1.el8 | |
python-oslo-service-doc | 2.6.2-1.el8 | |
python-oslo-utils-doc | 4.10.2-1.el8 | |
python-oslo-utils-lang | 4.10.2-1.el8 | |
python3-heat-tests-tempest | 1.5.0-1.el8 | |
python3-neutron-tests-tempest | 1.9.0-1.el8 | |
python3-oslo-service | 2.6.2-1.el8 | |
python3-oslo-service-tests | 2.6.2-1.el8 | |
python3-oslo-utils | 4.10.2-1.el8 | |
python3-oslo-utils-tests | 4.10.2-1.el8 | |

