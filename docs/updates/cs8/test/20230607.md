## 2023-06-07

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ceilometer-central | 18.1.0-1.el8 | |
openstack-ceilometer-common | 18.1.0-1.el8 | |
openstack-ceilometer-compute | 18.1.0-1.el8 | |
openstack-ceilometer-ipmi | 18.1.0-1.el8 | |
openstack-ceilometer-notification | 18.1.0-1.el8 | |
openstack-ceilometer-polling | 18.1.0-1.el8 | |
python3-ceilometer | 18.1.0-1.el8 | |
python3-ceilometer-tests | 18.1.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ceilometer-central | 18.1.0-1.el8 | |
openstack-ceilometer-common | 18.1.0-1.el8 | |
openstack-ceilometer-compute | 18.1.0-1.el8 | |
openstack-ceilometer-ipmi | 18.1.0-1.el8 | |
openstack-ceilometer-notification | 18.1.0-1.el8 | |
openstack-ceilometer-polling | 18.1.0-1.el8 | |
python3-ceilometer | 18.1.0-1.el8 | |
python3-ceilometer-tests | 18.1.0-1.el8 | |

