## 2021-09-08

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.8-4.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 10.1.0-1.el8 | |
openstack-barbican-api | 10.1.0-1.el8 | |
openstack-barbican-common | 10.1.0-1.el8 | |
openstack-barbican-keystone-listener | 10.1.0-1.el8 | |
openstack-barbican-worker | 10.1.0-1.el8 | |
openstack-heat-api | 14.2.0-1.el8 | |
openstack-heat-api-cfn | 14.2.0-1.el8 | |
openstack-heat-common | 14.2.0-1.el8 | |
openstack-heat-engine | 14.2.0-1.el8 | |
openstack-heat-monolith | 14.2.0-1.el8 | |
openstack-keystone | 17.0.1-1.el8 | |
openstack-keystone-doc | 17.0.1-1.el8 | |
openstack-swift-account | 2.25.2-1.el8 | |
openstack-swift-container | 2.25.2-1.el8 | |
openstack-swift-doc | 2.25.2-1.el8 | |
openstack-swift-object | 2.25.2-1.el8 | |
openstack-swift-proxy | 2.25.2-1.el8 | |
python3-barbican | 10.1.0-1.el8 | |
python3-barbican-tests | 10.1.0-1.el8 | |
python3-heat-tests | 14.2.0-1.el8 | |
python3-keystone | 17.0.1-1.el8 | |
python3-keystone-tests | 17.0.1-1.el8 | |
python3-swift | 2.25.2-1.el8 | |
python3-swift-tests | 2.25.2-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.8-4.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 10.1.0-1.el8 | |
openstack-barbican-api | 10.1.0-1.el8 | |
openstack-barbican-common | 10.1.0-1.el8 | |
openstack-barbican-keystone-listener | 10.1.0-1.el8 | |
openstack-barbican-worker | 10.1.0-1.el8 | |
openstack-heat-api | 14.2.0-1.el8 | |
openstack-heat-api-cfn | 14.2.0-1.el8 | |
openstack-heat-common | 14.2.0-1.el8 | |
openstack-heat-engine | 14.2.0-1.el8 | |
openstack-heat-monolith | 14.2.0-1.el8 | |
openstack-keystone | 17.0.1-1.el8 | |
openstack-keystone-doc | 17.0.1-1.el8 | |
openstack-swift-account | 2.25.2-1.el8 | |
openstack-swift-container | 2.25.2-1.el8 | |
openstack-swift-doc | 2.25.2-1.el8 | |
openstack-swift-object | 2.25.2-1.el8 | |
openstack-swift-proxy | 2.25.2-1.el8 | |
python3-barbican | 10.1.0-1.el8 | |
python3-barbican-tests | 10.1.0-1.el8 | |
python3-heat-tests | 14.2.0-1.el8 | |
python3-keystone | 17.0.1-1.el8 | |
python3-keystone-tests | 17.0.1-1.el8 | |
python3-swift | 2.25.2-1.el8 | |
python3-swift-tests | 2.25.2-1.el8 | |

