## 2022-03-03

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-murano-agent | 6.0.1-1.el8 | |
python-os-client-config-doc | 2.1.0-2.el8 | |
python-oslo-rootwrap-doc | 6.3.1-1.el8 | |
python3-ceilometerclient | 2.9.0-2.el8 | |
python3-glareclient | 0.5.3-2.el8 | |
python3-gnocchiclient | 7.0.4-2.el8 | |
python3-gnocchiclient-tests | 7.0.4-2.el8 | |
python3-os-client-config | 2.1.0-2.el8 | |
python3-oslo-rootwrap | 6.3.1-1.el8 | |
python3-oslo-rootwrap-tests | 6.3.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-murano-agent | 6.0.1-1.el8 | |
python-os-client-config-doc | 2.1.0-2.el8 | |
python-oslo-rootwrap-doc | 6.3.1-1.el8 | |
python3-ceilometerclient | 2.9.0-2.el8 | |
python3-glareclient | 0.5.3-2.el8 | |
python3-gnocchiclient | 7.0.4-2.el8 | |
python3-gnocchiclient-tests | 7.0.4-2.el8 | |
python3-os-client-config | 2.1.0-2.el8 | |
python3-oslo-rootwrap | 6.3.1-1.el8 | |
python3-oslo-rootwrap-tests | 6.3.1-1.el8 | |

