## 2022-11-02

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-7.el8s.cern | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.7-3.el8s | |
centos-release-ovirt45-testing | 8.7-3.el8s | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 12.0.1-3.el8 | |
openstack-barbican | 13.0.1-3.el8 | |
openstack-barbican | 14.0.1-3.el8 | |
openstack-barbican-api | 12.0.1-3.el8 | |
openstack-barbican-api | 13.0.1-3.el8 | |
openstack-barbican-api | 14.0.1-3.el8 | |
openstack-barbican-common | 12.0.1-3.el8 | |
openstack-barbican-common | 13.0.1-3.el8 | |
openstack-barbican-common | 14.0.1-3.el8 | |
openstack-barbican-keystone-listener | 12.0.1-3.el8 | |
openstack-barbican-keystone-listener | 13.0.1-3.el8 | |
openstack-barbican-keystone-listener | 14.0.1-3.el8 | |
openstack-barbican-worker | 12.0.1-3.el8 | |
openstack-barbican-worker | 13.0.1-3.el8 | |
openstack-barbican-worker | 14.0.1-3.el8 | |
python3-barbican | 12.0.1-3.el8 | |
python3-barbican | 13.0.1-3.el8 | |
python3-barbican | 14.0.1-3.el8 | |
python3-barbican-tests | 12.0.1-3.el8 | |
python3-barbican-tests | 13.0.1-3.el8 | |
python3-barbican-tests | 14.0.1-3.el8 | |
python3-stevedore | 3.3.2-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-node-ng-image-update-placeholder | 4.5.3.1-1.el8 | |
ovirt-release-host-node | 4.5.3.1-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-7.el8s.cern | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.7-3.el8s | |
centos-release-ovirt45-testing | 8.7-3.el8s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 12.0.1-3.el8 | |
openstack-barbican | 13.0.1-3.el8 | |
openstack-barbican | 14.0.1-3.el8 | |
openstack-barbican-api | 12.0.1-3.el8 | |
openstack-barbican-api | 13.0.1-3.el8 | |
openstack-barbican-api | 14.0.1-3.el8 | |
openstack-barbican-common | 12.0.1-3.el8 | |
openstack-barbican-common | 13.0.1-3.el8 | |
openstack-barbican-common | 14.0.1-3.el8 | |
openstack-barbican-keystone-listener | 12.0.1-3.el8 | |
openstack-barbican-keystone-listener | 13.0.1-3.el8 | |
openstack-barbican-keystone-listener | 14.0.1-3.el8 | |
openstack-barbican-worker | 12.0.1-3.el8 | |
openstack-barbican-worker | 13.0.1-3.el8 | |
openstack-barbican-worker | 14.0.1-3.el8 | |
python3-barbican | 12.0.1-3.el8 | |
python3-barbican | 13.0.1-3.el8 | |
python3-barbican | 14.0.1-3.el8 | |
python3-barbican-tests | 12.0.1-3.el8 | |
python3-barbican-tests | 13.0.1-3.el8 | |
python3-barbican-tests | 14.0.1-3.el8 | |
python3-stevedore | 3.3.2-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-node-ng-image-update-placeholder | 4.5.3.1-1.el8 | |
ovirt-release-host-node | 4.5.3.1-1.el8 | |

