## 2023-05-03

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-1.el8s.cern | |
CERN-CA-certs | 20230421-2.el8s.cern | |
cern-get-keytab | 1.5.2-1.el8s.cern | |
hepix | 4.10.7-0.el8s.cern | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.9-1.el8s | |
centos-release-ovirt45-testing | 8.9-1.el8s | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 20.1.4-1.el8 | |
openstack-dashboard-theme | 20.1.4-1.el8 | |
openstack-manila | 13.2.0-1.el8 | |
openstack-manila | 14.1.0-1.el8 | |
openstack-manila-doc | 13.2.0-1.el8 | |
openstack-manila-doc | 14.1.0-1.el8 | |
openstack-manila-share | 13.2.0-1.el8 | |
openstack-manila-share | 14.1.0-1.el8 | |
openstack-neutron | 19.7.0-1.el8 | |
openstack-neutron-common | 19.7.0-1.el8 | |
openstack-neutron-linuxbridge | 19.7.0-1.el8 | |
openstack-neutron-macvtap-agent | 19.7.0-1.el8 | |
openstack-neutron-metering-agent | 19.7.0-1.el8 | |
openstack-neutron-ml2 | 19.7.0-1.el8 | |
openstack-neutron-ml2ovn-trace | 19.7.0-1.el8 | |
openstack-neutron-openvswitch | 19.7.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 19.7.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 19.7.0-1.el8 | |
openstack-neutron-rpc-server | 19.7.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 19.7.0-1.el8 | |
openstack-nova | 24.2.1-1.el8 | |
openstack-nova-api | 24.2.1-1.el8 | |
openstack-nova-common | 24.2.1-1.el8 | |
openstack-nova-compute | 24.2.1-1.el8 | |
openstack-nova-conductor | 24.2.1-1.el8 | |
openstack-nova-migration | 24.2.1-1.el8 | |
openstack-nova-novncproxy | 24.2.1-1.el8 | |
openstack-nova-scheduler | 24.2.1-1.el8 | |
openstack-nova-serialproxy | 24.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 24.2.1-1.el8 | |
python-django-horizon-doc | 20.1.4-1.el8 | |
python-manilaclient-doc | 3.0.3-1.el8 | |
python-manilaclient-doc | 3.3.2-1.el8 | |
python3-django-horizon | 20.1.4-1.el8 | |
python3-manila | 13.2.0-1.el8 | |
python3-manila | 14.1.0-1.el8 | |
python3-manila-tests | 13.2.0-1.el8 | |
python3-manila-tests | 14.1.0-1.el8 | |
python3-manilaclient | 3.0.3-1.el8 | |
python3-manilaclient | 3.3.2-1.el8 | |
python3-neutron | 19.7.0-1.el8 | |
python3-neutron-tests | 19.7.0-1.el8 | |
python3-nova | 24.2.1-1.el8 | |
python3-nova-tests | 24.2.1-1.el8 | |
python3-openstacksdk | 0.62.0-1.el8 | |
python3-openstacksdk-tests | 0.62.0-1.el8 | |
python3-ovn-octavia-provider | 1.3.1-1.el8 | |
python3-ovn-octavia-provider-tests | 1.3.1-1.el8 | |
python3-rdo-openvswitch | 3.1-2.el8 | |
rdo-network-scripts-openvswitch | 3.1-2.el8 | |
rdo-openvswitch | 3.1-2.el8 | |
rdo-openvswitch-devel | 3.1-2.el8 | |
rdo-openvswitch-test | 3.1-2.el8 | |
rdo-ovn | 22.12-2.el8 | |
rdo-ovn-central | 22.12-2.el8 | |
rdo-ovn-host | 22.12-2.el8 | |
rdo-ovn-vtep | 22.12-2.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230421-1.el8s.cern | |
CERN-CA-certs | 20230421-2.el8s.cern | |
cern-get-keytab | 1.5.2-1.el8s.cern | |
hepix | 4.10.7-0.el8s.cern | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.9-1.el8s | |
centos-release-ovirt45-testing | 8.9-1.el8s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 20.1.4-1.el8 | |
openstack-dashboard-theme | 20.1.4-1.el8 | |
openstack-manila | 13.2.0-1.el8 | |
openstack-manila | 14.1.0-1.el8 | |
openstack-manila-doc | 13.2.0-1.el8 | |
openstack-manila-doc | 14.1.0-1.el8 | |
openstack-manila-share | 13.2.0-1.el8 | |
openstack-manila-share | 14.1.0-1.el8 | |
openstack-neutron | 19.7.0-1.el8 | |
openstack-neutron-common | 19.7.0-1.el8 | |
openstack-neutron-linuxbridge | 19.7.0-1.el8 | |
openstack-neutron-macvtap-agent | 19.7.0-1.el8 | |
openstack-neutron-metering-agent | 19.7.0-1.el8 | |
openstack-neutron-ml2 | 19.7.0-1.el8 | |
openstack-neutron-ml2ovn-trace | 19.7.0-1.el8 | |
openstack-neutron-openvswitch | 19.7.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 19.7.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 19.7.0-1.el8 | |
openstack-neutron-rpc-server | 19.7.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 19.7.0-1.el8 | |
openstack-nova | 24.2.1-1.el8 | |
openstack-nova-api | 24.2.1-1.el8 | |
openstack-nova-common | 24.2.1-1.el8 | |
openstack-nova-compute | 24.2.1-1.el8 | |
openstack-nova-conductor | 24.2.1-1.el8 | |
openstack-nova-migration | 24.2.1-1.el8 | |
openstack-nova-novncproxy | 24.2.1-1.el8 | |
openstack-nova-scheduler | 24.2.1-1.el8 | |
openstack-nova-serialproxy | 24.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 24.2.1-1.el8 | |
python-django-horizon-doc | 20.1.4-1.el8 | |
python-manilaclient-doc | 3.0.3-1.el8 | |
python-manilaclient-doc | 3.3.2-1.el8 | |
python3-django-horizon | 20.1.4-1.el8 | |
python3-manila | 13.2.0-1.el8 | |
python3-manila | 14.1.0-1.el8 | |
python3-manila-tests | 13.2.0-1.el8 | |
python3-manila-tests | 14.1.0-1.el8 | |
python3-manilaclient | 3.0.3-1.el8 | |
python3-manilaclient | 3.3.2-1.el8 | |
python3-neutron | 19.7.0-1.el8 | |
python3-neutron-tests | 19.7.0-1.el8 | |
python3-nova | 24.2.1-1.el8 | |
python3-nova-tests | 24.2.1-1.el8 | |
python3-openstacksdk | 0.62.0-1.el8 | |
python3-openstacksdk-tests | 0.62.0-1.el8 | |
python3-ovn-octavia-provider | 1.3.1-1.el8 | |
python3-ovn-octavia-provider-tests | 1.3.1-1.el8 | |
python3-rdo-openvswitch | 3.1-2.el8 | |
rdo-network-scripts-openvswitch | 3.1-2.el8 | |
rdo-openvswitch | 3.1-2.el8 | |
rdo-openvswitch-devel | 3.1-2.el8 | |
rdo-openvswitch-test | 3.1-2.el8 | |
rdo-ovn | 22.12-2.el8 | |
rdo-ovn-central | 22.12-2.el8 | |
rdo-ovn-host | 22.12-2.el8 | |
rdo-ovn-vtep | 22.12-2.el8 | |

