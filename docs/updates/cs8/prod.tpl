# Latest production System Updates for CentOS Stream 8 (CS8)

Please verify that your system is up to date, running as root:

```bash
/usr/bin/dnf check-update
```

If the above command shows you available updates apply these, running as root:

```bash
/usr/bin/dnf update
```

or if you only want to apply security updates, run as root:

```bash
/usr/bin/dnf --security update
```

For more information about software repositories please check: [CS8 software repositories](/updates/cs8/)
