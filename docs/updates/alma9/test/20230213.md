## 2023-02-13

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 34.1.43-1.el9_1.1 | |
selinux-policy-doc | 34.1.43-1.el9_1.1 | |
selinux-policy-mls | 34.1.43-1.el9_1.1 | |
selinux-policy-sandbox | 34.1.43-1.el9_1.1 | |
selinux-policy-targeted | 34.1.43-1.el9_1.1 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-devel | 34.1.43-1.el9_1.1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-minimum | 34.1.43-1.el9_1.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 34.1.43-1.el9_1.1 | |
selinux-policy-doc | 34.1.43-1.el9_1.1 | |
selinux-policy-mls | 34.1.43-1.el9_1.1 | |
selinux-policy-sandbox | 34.1.43-1.el9_1.1 | |
selinux-policy-targeted | 34.1.43-1.el9_1.1 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-devel | 34.1.43-1.el9_1.1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy-minimum | 34.1.43-1.el9_1.1 | |

