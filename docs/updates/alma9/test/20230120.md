## 2023-01-20

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-1.al9.cern | |
kmod-openafs | 1.8.9.0-1.5.14.0_162.6.1.el9_1.al9.cern | |
openafs | 1.8.9.0-1.al9.cern | |
openafs-authlibs | 1.8.9.0-1.al9.cern | |
openafs-authlibs-devel | 1.8.9.0-1.al9.cern | |
openafs-client | 1.8.9.0-1.al9.cern | |
openafs-compat | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.6.1.el9_1-1.al9.cern | |
openafs-devel | 1.8.9.0-1.al9.cern | |
openafs-docs | 1.8.9.0-1.al9.cern | |
openafs-kernel-source | 1.8.9.0-1.al9.cern | |
openafs-krb5 | 1.8.9.0-1.al9.cern | |
openafs-server | 1.8.9.0-1.al9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-1.al9.cern | |
kmod-openafs | 1.8.9.0-1.5.14.0_162.6.1.el9_1.al9.cern | |
openafs | 1.8.9.0-1.al9.cern | |
openafs-authlibs | 1.8.9.0-1.al9.cern | |
openafs-authlibs-devel | 1.8.9.0-1.al9.cern | |
openafs-client | 1.8.9.0-1.al9.cern | |
openafs-compat | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.6.1.el9_1-1.al9.cern | |
openafs-devel | 1.8.9.0-1.al9.cern | |
openafs-docs | 1.8.9.0-1.al9.cern | |
openafs-kernel-source | 1.8.9.0-1.al9.cern | |
openafs-krb5 | 1.8.9.0-1.al9.cern | |
openafs-server | 1.8.9.0-1.al9.cern | |

