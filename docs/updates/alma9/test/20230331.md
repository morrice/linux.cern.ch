## 2023-03-31

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tzdata-java | 2023c-1.el9 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>

