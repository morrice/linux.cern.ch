## 2020-02-28

Package | Advisory | Notes
------- | -------- | -----
ppp-2.4.5-34.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0630" target="secadv">RHSA-2020:0630</a> | &nbsp;
ppp-devel-2.4.5-34.el7_7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0630" target="secadv">RHSA-2020:0630</a> | &nbsp;
