## 2016-08-04

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-327.28.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
arc-44-40.6.el7.cern | &nbsp; &nbsp; | &nbsp;
arc-server-44-40.6.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_327.28.2.el7 | &nbsp; &nbsp; | &nbsp;
firefox-45.3.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-abi-whitelists-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-debug-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-debug-devel-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-devel-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-doc-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-headers-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-tools-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-tools-libs-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
kmod-bnx2x-1.712.30-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1559" target="secadv">RHEA-2016:1559</a> | &nbsp;
kmod-bnx2x-firmware-1.712.30-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1559" target="secadv">RHEA-2016:1559</a> | &nbsp;
kmod-ixgbe-4.4.0_k-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1558" target="secadv">RHEA-2016:1558</a> | &nbsp;
perf-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
python-perf-3.10.0-327.28.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1539" target="secadv">RHSA-2016:1539</a> | &nbsp;
