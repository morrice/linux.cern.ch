## 2017-05-23

Package | Advisory | Notes
------- | -------- | -----
ctdb-4.4.4-13.el7_3 | &nbsp; &nbsp; | &nbsp;
ctdb-tests-4.4.4-13.el7_3 | &nbsp; &nbsp; | &nbsp;
kdelibs-4.14.8-6.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1264" target="secadv">RHSA-2017:1264</a> | &nbsp;
kdelibs-apidocs-4.14.8-6.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1264" target="secadv">RHSA-2017:1264</a> | &nbsp;
kdelibs-common-4.14.8-6.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1264" target="secadv">RHSA-2017:1264</a> | &nbsp;
kdelibs-devel-4.14.8-6.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1264" target="secadv">RHSA-2017:1264</a> | &nbsp;
kdelibs-ktexteditor-4.14.8-6.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1264" target="secadv">RHSA-2017:1264</a> | &nbsp;
libsmbclient-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
libsmbclient-devel-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
libtirpc-0.2.4-0.8.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1263" target="secadv">RHSA-2017:1263</a> | &nbsp;
libtirpc-devel-0.2.4-0.8.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1263" target="secadv">RHSA-2017:1263</a> | &nbsp;
libwbclient-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
libwbclient-devel-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
rpcbind-0.2.0-38.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1262" target="secadv">RHSA-2017:1262</a> | &nbsp;
samba-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-client-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-client-libs-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-common-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-common-libs-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-common-tools-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-dc-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-dc-libs-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-devel-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-krb5-printing-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-libs-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-pidl-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-python-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-test-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-test-libs-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-vfs-glusterfs-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-winbind-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-winbind-clients-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-winbind-krb5-locator-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
samba-winbind-modules-4.4.4-13.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1265" target="secadv">RHSA-2017:1265</a> | &nbsp;
