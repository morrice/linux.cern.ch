## 2016-03-01

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.13-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.13-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-0.6-4.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-0.6-5.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-wrappers-1-15.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-wrappers-passwd-1-15.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
glusterfs-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-api-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-api-devel-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-cli-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-client-xlators-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-devel-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-fuse-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-libs-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
glusterfs-rdma-3.7.5-19.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:0323" target="secadv">RHBA-2016:0323</a> | &nbsp;
python-gluster-3.7.5-19.el7 | &nbsp; &nbsp; | &nbsp;
