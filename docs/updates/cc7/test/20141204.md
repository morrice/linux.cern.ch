## 2014-12-04

Package | Advisory | Notes
------- | -------- | -----
firefox-31.3.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;
nss-3.16.2.3-2.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-devel-3.16.2.3-2.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-pkcs11-devel-3.16.2.3-2.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-softokn-3.16.2.3-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-softokn-devel-3.16.2.3-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-softokn-freebl-3.16.2.3-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-softokn-freebl-devel-3.16.2.3-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-sysinit-3.16.2.3-2.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-tools-3.16.2.3-2.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-util-3.16.2.3-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-util-devel-3.16.2.3-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1948" target="secadv">RHSA-2014:1948</a> | &nbsp;
