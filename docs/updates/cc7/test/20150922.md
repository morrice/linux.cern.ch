## 2015-09-22

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.521-1.el7.cern | &nbsp; &nbsp; | &nbsp;
mozilla-prefs-0.5-3.el7.cern | &nbsp; &nbsp; | &nbsp;
libstoraged-2.2.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1798" target="secadv">RHEA-2015:1798</a> | &nbsp;
libstoraged-devel-2.2.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1798" target="secadv">RHEA-2015:1798</a> | &nbsp;
storaged-2.2.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1798" target="secadv">RHEA-2015:1798</a> | &nbsp;
storaged-iscsi-2.2.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1798" target="secadv">RHEA-2015:1798</a> | &nbsp;
storaged-lvm2-2.2.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1798" target="secadv">RHEA-2015:1798</a> | &nbsp;
grub2-2.02-0.17.el7.centos.4.0.1 | &nbsp; &nbsp; | &nbsp;
grub2-efi-2.02-0.17.el7.centos.4.0.1 | &nbsp; &nbsp; | &nbsp;
grub2-efi-modules-2.02-0.17.el7.centos.4.0.1 | &nbsp; &nbsp; | &nbsp;
grub2-tools-2.02-0.17.el7.centos.4.0.1 | &nbsp; &nbsp; | &nbsp;
