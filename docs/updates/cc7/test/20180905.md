## 2018-09-05

Package | Advisory | Notes
------- | -------- | -----
libreoffice-release-1-0 | &nbsp; &nbsp; | &nbsp;
libreoffice-release-1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
python-ovirt-engine-sdk4-4.2.8-1.el7ev | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2634" target="secadv">RHBA-2018:2634</a> | &nbsp;
