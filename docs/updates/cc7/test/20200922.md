## 2020-09-22


Package | Advisory | Notes
------- | -------- | -----
python2-troveclient-3.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-troveclient-doc-3.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
qemu-xen-4.12.1-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.4.75.g93be943e7d-2.el7 | &nbsp; &nbsp; | &nbsp;

