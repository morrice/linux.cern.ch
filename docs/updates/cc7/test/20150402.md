## 2015-04-02

Package | Advisory | Notes
------- | -------- | -----
centos-release-7-1.1503.el7.cern.2.8 | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_229.1.2.el7 | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_229.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_229.el7 | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_229.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
nss-3.16.2.3-5.el7.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.16.2.3-5.el7.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.16.2.3-5.el7.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.16.2.3-5.el7.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.16.2.3-5.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-logos-70.0.6-2.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-31.6.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
tzdata-2015b-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0717" target="secadv">RHEA-2015:0717</a> | &nbsp;
tzdata-java-2015b-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0717" target="secadv">RHEA-2015:0717</a> | &nbsp;
