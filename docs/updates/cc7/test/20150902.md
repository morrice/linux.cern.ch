## 2015-09-02

Package | Advisory | Notes
------- | -------- | -----
lpadmincern-1.3.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
gdk-pixbuf2-2.28.2-5.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1694" target="secadv">RHSA-2015:1694</a> | &nbsp;
gdk-pixbuf2-devel-2.28.2-5.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1694" target="secadv">RHSA-2015:1694</a> | &nbsp;
jakarta-taglibs-standard-1.1.2-14.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1695" target="secadv">RHSA-2015:1695</a> | &nbsp;
jakarta-taglibs-standard-javadoc-1.1.2-14.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1695" target="secadv">RHSA-2015:1695</a> | &nbsp;
nss-softokn-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
nss-softokn-devel-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
nss-softokn-freebl-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
nss-softokn-freebl-devel-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
pacemaker-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-cli-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-cluster-libs-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-cts-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-doc-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-libs-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-libs-devel-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-remote-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pcs-0.9.137-13.el7_1.4 | &nbsp; &nbsp; | &nbsp;
python-clufter-0.9.137-13.el7_1.4 | &nbsp; &nbsp; | &nbsp;
