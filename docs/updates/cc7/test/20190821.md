## 2019-08-21

Package | Advisory | Notes
------- | -------- | -----
bmc-snmp-proxy-1.8.18-7.el7.cern | &nbsp; &nbsp; | &nbsp;
exchange-bmc-os-info-1.8.18-7.el7.cern | &nbsp; &nbsp; | &nbsp;
ipmitool-1.8.18-7.el7.cern | &nbsp; &nbsp; | &nbsp;
ansible-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
