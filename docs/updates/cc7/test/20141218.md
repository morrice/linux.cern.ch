## 2014-12-18

Package | Advisory | Notes
------- | -------- | -----
mailx-12.5-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1999" target="secadv">RHSA-2014:1999</a> | &nbsp;
opencryptoki-3.0-11.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1988" target="secadv">RHBA-2014:1988</a> | &nbsp;
opencryptoki-devel-3.0-11.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1988" target="secadv">RHBA-2014:1988</a> | &nbsp;
opencryptoki-icsftok-3.0-11.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1988" target="secadv">RHBA-2014:1988</a> | &nbsp;
opencryptoki-libs-3.0-11.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1988" target="secadv">RHBA-2014:1988</a> | &nbsp;
opencryptoki-swtok-3.0-11.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1988" target="secadv">RHBA-2014:1988</a> | &nbsp;
opencryptoki-tpmtok-3.0-11.el7_0.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1988" target="secadv">RHBA-2014:1988</a> | &nbsp;
selinux-policy-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
selinux-policy-devel-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
selinux-policy-doc-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
selinux-policy-minimum-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
selinux-policy-mls-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
selinux-policy-sandbox-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
selinux-policy-targeted-3.12.1-153.el7_0.13 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1996" target="secadv">RHBA-2014:1996</a> | &nbsp;
