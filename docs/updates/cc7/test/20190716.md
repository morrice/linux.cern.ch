## 2019-07-16

Package | Advisory | Notes
------- | -------- | -----
NetworkManager-DUID-LLT-0.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
openstack-neutron-fwaas-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
pyparsing-doc-2.1.10-7.el7 | &nbsp; &nbsp; | &nbsp;
python2-pyparsing-2.1.10-7.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-fwaas-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-fwaas-tests-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-varnish6-varnish-6.0.2-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1438" target="secadv">RHEA-2019:1438</a> | &nbsp;
rh-varnish6-varnish-devel-6.0.2-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1438" target="secadv">RHEA-2019:1438</a> | &nbsp;
rh-varnish6-varnish-docs-6.0.2-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1438" target="secadv">RHEA-2019:1438</a> | &nbsp;
rh-varnish6-varnish-libs-6.0.2-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1438" target="secadv">RHEA-2019:1438</a> | &nbsp;
rh-varnish6-varnish-modules-0.15.0-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:1438" target="secadv">RHEA-2019:1438</a> | &nbsp;
nfs-ganesha-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mount-9P-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
