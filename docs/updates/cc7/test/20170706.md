## 2017-07-06

Package | Advisory | Notes
------- | -------- | -----
bind-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-chroot-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-devel-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-libs-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-libs-lite-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-license-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-lite-devel-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-pkcs11-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-pkcs11-devel-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-pkcs11-libs-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-pkcs11-utils-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-sdb-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-sdb-chroot-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
bind-utils-9.9.4-50.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1680" target="secadv">RHSA-2017:1680</a> | &nbsp;
qemu-img-1.5.3-126.el7_3.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1681" target="secadv">RHSA-2017:1681</a> | &nbsp;
qemu-kvm-1.5.3-126.el7_3.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1681" target="secadv">RHSA-2017:1681</a> | &nbsp;
qemu-kvm-common-1.5.3-126.el7_3.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1681" target="secadv">RHSA-2017:1681</a> | &nbsp;
qemu-kvm-tools-1.5.3-126.el7_3.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1681" target="secadv">RHSA-2017:1681</a> | &nbsp;
