## 2020-09-02


Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.11-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.11-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.11-1.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-ovsdbapp-0.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ovsdbapp-tests-0.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ovsdbapp-doc-0.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;

