## 2020-06-29


Package | Advisory | Notes
------- | -------- | -----
openstack-selinux-0.8.22-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-devel-0.8.22-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-selinux-test-0.8.22-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.10.4-1.el7 | &nbsp; &nbsp; | &nbsp;

