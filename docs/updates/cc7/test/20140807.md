## 2014-08-07

Package | Advisory | Notes
------- | -------- | -----
shibboleth-2.5.3-1.2.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.5.3-1.2.el7.cern | &nbsp; &nbsp; | &nbsp;
