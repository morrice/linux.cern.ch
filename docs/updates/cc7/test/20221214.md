## 2022-12-14


Package | Advisory | Notes
------- | -------- | -----
grub2-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-common-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-aa64-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-cdboot-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-cdboot-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-i386-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-pc-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-pc-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-ppc-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-ppc64-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-ppc64le-modules-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-tools-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-tools-extra-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
grub2-tools-minimal-2.02-0.87.0.1.el7.centos.11 | &nbsp; &nbsp; | &nbsp;
pki-base-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-base-java-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-ca-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-javadoc-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-kra-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-server-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-symkey-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
pki-tools-10.5.18-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:8799" target="secadv">RHSA-2022:8799</a> | &nbsp;
tzdata-2022g-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:8785" target="secadv">RHBA-2022:8785</a> | &nbsp;
tzdata-java-2022g-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:8785" target="secadv">RHBA-2022:8785</a> | &nbsp;

