## 2015-09-30

Package | Advisory | Notes
------- | -------- | -----
kde-settings-19-23.5.el7.centos | &nbsp; &nbsp; | &nbsp;
kde-settings-ksplash-19-23.5.el7.centos | &nbsp; &nbsp; | &nbsp;
kde-settings-minimal-19-23.5.el7.centos | &nbsp; &nbsp; | &nbsp;
kde-settings-plasma-19-23.5.el7.centos | &nbsp; &nbsp; | &nbsp;
kde-settings-pulseaudio-19-23.5.el7.centos | &nbsp; &nbsp; | &nbsp;
openldap-2.4.39-7.el7.centos | &nbsp; &nbsp; | &nbsp;
openldap-clients-2.4.39-7.el7.centos | &nbsp; &nbsp; | &nbsp;
openldap-devel-2.4.39-7.el7.centos | &nbsp; &nbsp; | &nbsp;
openldap-servers-2.4.39-7.el7.centos | &nbsp; &nbsp; | &nbsp;
openldap-servers-sql-2.4.39-7.el7.centos | &nbsp; &nbsp; | &nbsp;
qt-settings-19-23.5.el7.centos | &nbsp; &nbsp; | &nbsp;
