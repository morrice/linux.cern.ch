## 2018-02-20

Package | Advisory | Notes
------- | -------- | -----
openstack-glance-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-doc-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-api-7.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-common-7.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-conductor-7.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-inspector-doc-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-2.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-doc-2.2.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-16.1.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-16.1.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python-glance-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-glance-tests-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-inspector-tests-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-tests-7.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-cisco-5.5.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-cisco-tests-tempest-5.5.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tempest-16.1.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python-tempest-tests-16.1.0-4.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-gnfs-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-3.12.6-1.el7 | &nbsp; &nbsp; | &nbsp;
