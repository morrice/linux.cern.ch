## 2016-04-25

Package | Advisory | Notes
------- | -------- | -----
thunderbird-calendar-tweaks-6.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-3.7.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-provider_for_google_calendar-2.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
tzdata-2016d-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:0683" target="secadv">RHEA-2016:0683</a> | &nbsp;
tzdata-java-2016d-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:0683" target="secadv">RHEA-2016:0683</a> | &nbsp;
