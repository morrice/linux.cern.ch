## 2018-06-04

Package | Advisory | Notes
------- | -------- | -----
puppet-kerberos-2.0-2.el7.cern | &nbsp; &nbsp; | &nbsp;
iwl1000-firmware-39.31.5.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl100-firmware-39.31.5.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl105-firmware-18.168.6.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl135-firmware-18.168.6.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl2000-firmware-18.168.6.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl2030-firmware-18.168.6.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl3160-firmware-22.0.7.0-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl3945-firmware-15.32.2.9-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl4965-firmware-228.61.2.24-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl5000-firmware-8.83.5.1_1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl5150-firmware-8.24.2.2-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl6000-firmware-9.221.4.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl6000g2a-firmware-17.168.5.3-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl6000g2b-firmware-17.168.5.2-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl6050-firmware-41.28.5.1-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl7260-firmware-22.0.7.0-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
iwl7265-firmware-22.0.7.0-62.1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
kmod-redhat-megaraid_sas-07.702.06.00_rh2_dup7.5-1.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1670" target="secadv">RHEA-2018:1670</a> | &nbsp;
linux-firmware-20180220-62.1.git6d51311.el7_5 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1771" target="secadv">RHEA-2018:1771</a> | &nbsp;
xmlrpc-client-3.1.3-9.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:1780" target="secadv">RHSA-2018:1780</a> | &nbsp;
xmlrpc-common-3.1.3-9.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:1780" target="secadv">RHSA-2018:1780</a> | &nbsp;
xmlrpc-javadoc-3.1.3-9.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:1780" target="secadv">RHSA-2018:1780</a> | &nbsp;
xmlrpc-server-3.1.3-9.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:1780" target="secadv">RHSA-2018:1780</a> | &nbsp;
