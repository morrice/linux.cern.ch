## 2015-04-29

Package | Advisory | Notes
------- | -------- | -----
389-ds-base-1.3.3.1-16.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0895" target="secadv">RHSA-2015:0895</a> | &nbsp;
389-ds-base-devel-1.3.3.1-16.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0895" target="secadv">RHSA-2015:0895</a> | &nbsp;
389-ds-base-libs-1.3.3.1-16.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0895" target="secadv">RHSA-2015:0895</a> | &nbsp;
tzdata-2015d-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0913" target="secadv">RHEA-2015:0913</a> | &nbsp;
tzdata-java-2015d-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0913" target="secadv">RHEA-2015:0913</a> | &nbsp;
