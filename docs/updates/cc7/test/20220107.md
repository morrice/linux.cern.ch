## 2022-01-07


Package | Advisory | Notes
------- | -------- | -----
libntirpc-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-v4-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
ctdb-4.10.16-17.el7_9 | &nbsp; &nbsp; | &nbsp;
ctdb-tests-4.10.16-17.el7_9 | &nbsp; &nbsp; | &nbsp;
ipa-client-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-client-common-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-common-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-python-compat-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-server-common-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
libsmbclient-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
libsmbclient-devel-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
libwbclient-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
libwbclient-devel-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
log4j-1.2.17-17.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5206" target="secadv">RHSA-2021:5206</a> | &nbsp;
log4j-javadoc-1.2.17-17.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5206" target="secadv">RHSA-2021:5206</a> | &nbsp;
log4j-manual-1.2.17-17.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5206" target="secadv">RHSA-2021:5206</a> | &nbsp;
python2-ipaclient-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
python2-ipalib-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
python2-ipaserver-4.6.8-5.el7.centos.10 | &nbsp; &nbsp; | &nbsp;
samba-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-client-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-client-libs-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-common-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-common-libs-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-common-tools-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-dc-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-dc-libs-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-devel-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-krb5-printing-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-libs-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-pidl-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-python-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-python-test-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-test-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-test-libs-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-vfs-glusterfs-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-winbind-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-winbind-clients-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-winbind-krb5-locator-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
samba-winbind-modules-4.10.16-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:5192" target="secadv">RHSA-2021:5192</a> | &nbsp;
xorg-x11-server-Xdmx-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-Xephyr-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-Xnest-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-Xorg-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-Xvfb-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-Xwayland-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-common-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-devel-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xorg-x11-server-source-1.20.4-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0003" target="secadv">RHSA-2022:0003</a> | &nbsp;
xen-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.4.100.gb9aa1635b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.13.4.6.gd0e2c2762b-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.14.3.33.gc4cf538865-1.el7 | &nbsp; &nbsp; | &nbsp;

