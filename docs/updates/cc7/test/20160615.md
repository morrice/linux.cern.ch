## 2016-06-15

Package | Advisory | Notes
------- | -------- | -----
rh-nodejs4-node-gyp-3.3.1-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-abbrev-1.0.7-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-are-we-there-yet-1.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-aws-sign-0.3.0-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-chmodr-1.0.2-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-chownr-1.0.1-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-cookie-jar-0.3.0-4.2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-forever-agent-0.5.0-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-fstream-ignore-1.0.2-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-fstream-npm-1.0.7-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-ini-1.3.4-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-jju-1.2.1-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-lockfile-1.0.1-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-lru-cache-3.2.0-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-minimatch-3.0.0-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-nopt-3.0.6-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-npmlog-2.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-oauth-sign-0.3.0-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-once-1.3.3-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-osenv-0.1.3-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-promzard-0.3.0-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-require_optional-1.0.0-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1155" target="secadv">RHEA-2016:1155</a> | &nbsp;
rh-nodejs4-nodejs-resolve-from-2.0.0-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1155" target="secadv">RHEA-2016:1155</a> | &nbsp;
rh-nodejs4-nodejs-rimraf-2.5.2-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-semver-5.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-spdx-exceptions-1.0.4-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-spdx-expression-parse-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-tap-stream-0.2.0-6.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-tar-2.2.1-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-tunnel-agent-0.3.0-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-uid-number-0.0.5-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-undefsafe-0.0.3-5.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-which-1.2.0-5.el7 | &nbsp; &nbsp; | &nbsp;
rh-nodejs4-nodejs-wordwrap-1.0.0-5.el7 | &nbsp; &nbsp; | &nbsp;
rh-ror42-rubygem-binding_of_caller-0.7.2-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-binding_of_caller-doc-0.7.2-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
sclo-vagrant1-2.1-6.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-build-2.1-6.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-runtime-2.1-6.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-scldevel-2.1-6.el7 | &nbsp; &nbsp; | &nbsp;
