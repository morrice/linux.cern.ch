## 2015-10-19

Package | Advisory | Notes
------- | -------- | -----
cern-config-users-1.8.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.540-3.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-openstack-kilo-1-2.el7 | &nbsp; &nbsp; | &nbsp;
