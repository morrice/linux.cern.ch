## 2021-06-17


Package | Advisory | Notes
------- | -------- | -----
libntirpc-3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
gupnp-1.0.2-6.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2417" target="secadv">RHSA-2021:2417</a> | &nbsp;
gupnp-devel-1.0.2-6.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2417" target="secadv">RHSA-2021:2417</a> | &nbsp;
gupnp-docs-1.0.2-6.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2417" target="secadv">RHSA-2021:2417</a> | &nbsp;
postgresql-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-contrib-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-devel-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-docs-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-libs-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-plperl-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-plpython-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-pltcl-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-server-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-static-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-test-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;
postgresql-upgrade-9.2.24-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2397" target="secadv">RHSA-2021:2397</a> | &nbsp;

