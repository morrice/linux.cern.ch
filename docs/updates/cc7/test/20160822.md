## 2016-08-22

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-327.28.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_327.28.3.el7 | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-abi-whitelists-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-debug-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-debug-devel-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-devel-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-doc-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-headers-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-tools-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-tools-libs-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
perf-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
python-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-debug-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-devel-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-libs-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-perf-3.10.0-327.28.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1633" target="secadv">RHSA-2016:1633</a> | &nbsp;
python-test-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-tools-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
tkinter-2.7.5-38.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1626" target="secadv">RHSA-2016:1626</a> | &nbsp;
