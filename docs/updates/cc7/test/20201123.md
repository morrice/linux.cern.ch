## 2020-11-23


Package | Advisory | Notes
------- | -------- | -----
openstack-ironic-python-agent-3.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-python-agent-3.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-python-agent-doc-3.6.5-1.el7 | &nbsp; &nbsp; | &nbsp;

