## 2016-01-14

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.9.21-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.9.21-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-sso-cookie-0.5.6-1.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.6-1.el7.cern | &nbsp; &nbsp; | &nbsp;
