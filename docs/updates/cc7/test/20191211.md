## 2019-12-11

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1062.9.1.rt56.1033.el7 | &nbsp; &nbsp; | &nbsp;
rh-python36-python-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-python36-python-debug-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-python36-python-devel-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-python36-python-libs-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-python36-python-test-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-python36-python-tkinter-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-python36-python-tools-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
kernel-azure-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-1062.9.1.el7.azure | &nbsp; &nbsp; | &nbsp;
xen-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.4.28.ge4899550ff-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.1.106.ge10c1fbde8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.8.5.77.gec6c25e467-1.el7 | &nbsp; &nbsp; | &nbsp;
