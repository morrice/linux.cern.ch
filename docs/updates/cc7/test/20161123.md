## 2016-11-23

Package | Advisory | Notes
------- | -------- | -----
libcacard-devel-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
libcacard-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
libcacard-tools-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.3.0-31.0.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
xen-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.6.3-4.el7 | &nbsp; &nbsp; | &nbsp;
