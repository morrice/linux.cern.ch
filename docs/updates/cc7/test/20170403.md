## 2017-04-03

Package | Advisory | Notes
------- | -------- | -----
cvmfs-2.3.5-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cvmfs-devel-2.3.5-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cvmfs-server-2.3.5-1.el7.centos | &nbsp; &nbsp; | &nbsp;
cvmfs-unittests-2.3.5-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xrootd-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-client-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-client-devel-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-client-libs-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-devel-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-doc-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-fuse-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-libs-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-private-devel-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-python-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-selinux-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-server-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-server-devel-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
xrootd-server-libs-4.5.0-2.CERN.el7 | &nbsp; &nbsp; | &nbsp;
