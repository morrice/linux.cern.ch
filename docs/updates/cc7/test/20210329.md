## 2021-03-29


Package | Advisory | Notes
------- | -------- | -----
thunderbird-78.9.0-3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
gnocchi-api-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-common-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-doc-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-metricd-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-statsd-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-api-9.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-common-9.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-conductor-9.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-doc-9.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-rabbitmq-10.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-systemd-2.10.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-tempest-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gnocchi-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gnocchi-tests-4.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-9.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-tests-9.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
podman-1.6.4-29.el7_9 | &nbsp; &nbsp; | &nbsp;
podman-docker-1.6.4-29.el7_9 | &nbsp; &nbsp; | &nbsp;
podman-remote-1.6.4-29.el7_9 | &nbsp; &nbsp; | &nbsp;
firefox-78.9.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-78.9.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;

