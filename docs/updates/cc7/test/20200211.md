## 2020-02-11

Package | Advisory | Notes
------- | -------- | -----
openstack-kolla-7.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-8.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-api-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-common-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-doc-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-engine-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-image-pack-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-sahara-ui-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-doc-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-tests-tempest-0.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-doc-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-tests-unit-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-23.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-tests-tempest-doc-0.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sahara-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sahara-tests-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-webob-1.7.2-2.el7 | &nbsp; &nbsp; | &nbsp;
sahara-image-elements-9.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
devtoolset-9-9.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:4132" target="secadv">RHEA-2019:4132</a> | &nbsp;
devtoolset-9-build-9.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:4132" target="secadv">RHEA-2019:4132</a> | &nbsp;
devtoolset-9-perftools-9.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:4132" target="secadv">RHEA-2019:4132</a> | &nbsp;
devtoolset-9-runtime-9.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:4132" target="secadv">RHEA-2019:4132</a> | &nbsp;
devtoolset-9-toolchain-9.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:4132" target="secadv">RHEA-2019:4132</a> | &nbsp;
sclo-php72-unit-php-1.15.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-unit-php-1.15.0-1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-1062.12.1.el7.azure | &nbsp; &nbsp; | &nbsp;
