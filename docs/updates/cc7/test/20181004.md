## 2018-10-04

Package | Advisory | Notes
------- | -------- | -----
hepix-4.9.4-0.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-azure-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-862.14.4.el7.azure | &nbsp; &nbsp; | &nbsp;
