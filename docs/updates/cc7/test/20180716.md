## 2018-07-16

Package | Advisory | Notes
------- | -------- | -----
gnupg2-2.0.22-5.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2181" target="secadv">RHSA-2018:2181</a> | &nbsp;
gnupg2-smime-2.0.22-5.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2181" target="secadv">RHSA-2018:2181</a> | &nbsp;
python-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
python-debug-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
python-devel-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
python-libs-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
python-test-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
python-tools-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
tkinter-2.7.5-69.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2123" target="secadv">RHSA-2018:2123</a> | &nbsp;
