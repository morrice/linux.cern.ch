## 2017-03-30

Package | Advisory | Notes
------- | -------- | -----
arc-44-40.7.el7.cern | &nbsp; &nbsp; | &nbsp;
arc-server-44-40.7.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-25.0.0.127-1.el7.cern | &nbsp; &nbsp; | &nbsp;
icoutils-0.31.3-1.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0837" target="secadv">RHSA-2017:0837</a> | &nbsp;
openjpeg-1.5.1-16.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0838" target="secadv">RHSA-2017:0838</a> | &nbsp;
openjpeg-devel-1.5.1-16.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0838" target="secadv">RHSA-2017:0838</a> | &nbsp;
openjpeg-libs-1.5.1-16.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0838" target="secadv">RHSA-2017:0838</a> | &nbsp;
selinux-policy-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
selinux-policy-devel-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
selinux-policy-doc-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
selinux-policy-minimum-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
selinux-policy-mls-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
selinux-policy-sandbox-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
selinux-policy-targeted-3.13.1-102.el7_3.16 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0823" target="secadv">RHBA-2017:0823</a> | &nbsp;
tzdata-2017b-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0839" target="secadv">RHBA-2017:0839</a> | &nbsp;
tzdata-java-2017b-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0839" target="secadv">RHBA-2017:0839</a> | &nbsp;
