## 2019-07-03

Package | Advisory | Notes
------- | -------- | -----
koji-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-builder-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-builder-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-hub-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-hub-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-utils-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-vm-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
koji-web-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-cli-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-hub-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-hub-plugins-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-koji-web-1.17.0-6.el7.cern | &nbsp; &nbsp; | &nbsp;
