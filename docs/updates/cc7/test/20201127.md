## 2020-11-27


Package | Advisory | Notes
------- | -------- | -----
python2-oslo-concurrency-3.30.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-concurrency-tests-3.30.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-config-6.11.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-concurrency-doc-3.30.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-concurrency-lang-3.30.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-config-doc-6.11.3-1.el7 | &nbsp; &nbsp; | &nbsp;

