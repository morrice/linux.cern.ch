## 2018-05-29

Package | Advisory | Notes
------- | -------- | -----
perf-3.10.0-862.3.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-862.3.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
cern-rpmverify-4.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-52.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
