## 2017-03-16

Package | Advisory | Notes
------- | -------- | -----
kmod-redhat-megaraid_sas-07.700.00.00-rh1.el7_3 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:0518" target="secadv">RHEA-2017:0518</a> | &nbsp;
thunderbird-45.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.6.0-28.el7_3.6.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.6.0-28.el7_3.6.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.6.0-28.el7_3.6.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.6.0-28.el7_3.6.1 | &nbsp; &nbsp; | &nbsp;
