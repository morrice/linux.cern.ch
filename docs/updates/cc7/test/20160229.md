## 2016-02-29

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.12-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.12-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
