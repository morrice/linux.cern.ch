## 2020-01-28

Package | Advisory | Notes
------- | -------- | -----
openstack-glance-18.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-19.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-doc-18.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-doc-19.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-2.12.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-devel-2.12.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-ipsec-2.12.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-test-2.12.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-18.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-19.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-tests-18.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-tests-19.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-doc-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novajoin-tests-unit-1.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openvswitch-2.12.0-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-4.0.12-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.9.4-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.9.4-2.el7 | &nbsp; &nbsp; | &nbsp;
