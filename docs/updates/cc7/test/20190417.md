## 2019-04-17

Package | Advisory | Notes
------- | -------- | -----
python2-networking-vmware-nsx-14.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-stestr-2.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-stestr-sql-2.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-vmware-nsxlib-14.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-vmware-nsxlib-tests-14.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-vmware-nsx-doc-14.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-stestr-doc-2.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-vmware-nsxlib-doc-14.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-1.7.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-1.7.3-1.el7 | &nbsp; &nbsp; | &nbsp;
mod_auth_mellon-0.14.0-2.el7_6.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0766" target="secadv">RHSA-2019:0766</a> | &nbsp;
mod_auth_mellon-diagnostics-0.14.0-2.el7_6.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0766" target="secadv">RHSA-2019:0766</a> | &nbsp;
