## 2020-05-15


Package | Advisory | Notes
------- | -------- | -----
ansible-2.9.7-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-test-2.9.7-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-16.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-16.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-castellan-1.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystone-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystone-16.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystone-tests-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystone-tests-16.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pyroute2-0.5.6-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.9.9-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.9.9-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-python3-2.9.9-1.el7 | &nbsp; &nbsp; | &nbsp;

