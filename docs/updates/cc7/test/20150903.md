## 2015-09-03

Package | Advisory | Notes
------- | -------- | -----
shibboleth-2.5.5-3.3.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.5.5-3.3.el7.cern | &nbsp; &nbsp; | &nbsp;
selinux-policy-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
selinux-policy-devel-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
selinux-policy-doc-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
selinux-policy-minimum-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
selinux-policy-mls-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
selinux-policy-sandbox-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
selinux-policy-targeted-3.13.1-23.el7_1.17 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1701" target="secadv">RHBA-2015:1701</a> | &nbsp;
