## 2020-09-11


Package | Advisory | Notes
------- | -------- | -----
openstack-nova-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-tests-19.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.8.7-1.el7 | &nbsp; &nbsp; | &nbsp;

