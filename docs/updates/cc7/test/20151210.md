## 2015-12-10

Package | Advisory | Notes
------- | -------- | -----
afs_tools-2.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;
afs_tools_standalone-2.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.554-1.el7.cern | &nbsp; &nbsp; | &nbsp;
