## 2016-09-06

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-23.0.0.162-0.20160831.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-ceph-jewel-1.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-api-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-api-devel-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-cli-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-client-xlators-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-devel-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-fuse-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-libs-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
glusterfs-rdma-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
python-gluster-3.7.9-12.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1810" target="secadv">RHBA-2016:1810</a> | &nbsp;
ipa-admintools-4.2.0-15.0.1.el7.centos.19 | &nbsp; &nbsp; | &nbsp;
ipa-client-4.2.0-15.0.1.el7.centos.19 | &nbsp; &nbsp; | &nbsp;
ipa-python-4.2.0-15.0.1.el7.centos.19 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.2.0-15.0.1.el7.centos.19 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.2.0-15.0.1.el7.centos.19 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.2.0-15.0.1.el7.centos.19 | &nbsp; &nbsp; | &nbsp;
kmod-megaraid_sas-06.811.02.00-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1780" target="secadv">RHEA-2016:1780</a> | &nbsp;
thunderbird-45.3.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
