## 2018-09-07

Package | Advisory | Notes
------- | -------- | -----
arc-44-41.2.el7.cern | &nbsp; &nbsp; | &nbsp;
arc-server-44-41.2.el7.cern | &nbsp; &nbsp; | &nbsp;
rh-postgresql95-postgresql-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-contrib-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-devel-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-docs-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-libs-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-plperl-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-plpython-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-pltcl-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-server-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-static-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
rh-postgresql95-postgresql-test-9.5.14-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2511" target="secadv">RHSA-2018:2511</a> | &nbsp;
