## 2019-11-04

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1062.4.1.rt56.1027.el7 | &nbsp; &nbsp; | &nbsp;
php-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-bcmath-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-cli-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-common-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-dba-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-devel-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-embedded-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-enchant-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-fpm-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-gd-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-intl-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-ldap-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-mbstring-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-mysql-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-mysqlnd-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-odbc-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-pdo-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-pgsql-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-process-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-pspell-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-recode-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-snmp-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-soap-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-xml-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
php-xmlrpc-5.4.16-46.1.el7_7 | &nbsp; &nbsp; | &nbsp;
