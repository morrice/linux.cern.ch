## 2014-08-25

Package | Advisory | Notes
------- | -------- | -----
libguestfs-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-devel-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-gobject-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-gobject-devel-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-gobject-doc-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-java-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-java-devel-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-javadoc-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-man-pages-ja-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-man-pages-uk-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-tools-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
libguestfs-tools-c-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
lua-guestfs-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
ocaml-libguestfs-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
ocaml-libguestfs-devel-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
perl-Sys-Guestfs-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
python-libguestfs-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
ruby-libguestfs-1.22.6-22.el7.centos.0.1 | &nbsp; &nbsp; | &nbsp;
