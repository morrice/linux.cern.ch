## 2020-08-19


Package | Advisory | Notes
------- | -------- | -----
openstack-glance-19.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-glance-doc-19.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-19.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glance-tests-19.0.4-1.el7 | &nbsp; &nbsp; | &nbsp;

