## 2019-04-26

Package | Advisory | Notes
------- | -------- | -----
hepix-4.9.5-3.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-957.12.1.rt56.927.el7 | &nbsp; &nbsp; | &nbsp;
