## 2018-08-17

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-30.0.0.154-1.el7.cern | &nbsp; &nbsp; | &nbsp;
instack-undercloud-8.4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-6.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-doc-6.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-share-6.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-panko-api-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-panko-common-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-panko-doc-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-8.6.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-container-base-8.6.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-containers-8.6.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-devtools-8.6.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-heat-templates-8.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tripleo-8.3.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-5.35.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-tests-5.35.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pankoclient-0.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pankoclient-tests-0.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-6.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-6.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-messaging-doc-5.35.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-panko-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pankoclient-doc-0.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-panko-tests-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tripleoclient-9.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tripleoclient-heat-installer-9.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-atomic-2.9.0-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2456" target="secadv">RHBA-2018:2456</a> | &nbsp;
tuned-profiles-compat-2.9.0-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2456" target="secadv">RHBA-2018:2456</a> | &nbsp;
tuned-profiles-cpu-partitioning-2.9.0-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2456" target="secadv">RHBA-2018:2456</a> | &nbsp;
tuned-profiles-nfv-2.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-guest-2.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-host-2.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-oracle-2.9.0-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2456" target="secadv">RHBA-2018:2456</a> | &nbsp;
tuned-profiles-realtime-2.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-2.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-hana-2.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
