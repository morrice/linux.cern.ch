## 2021-02-10


Package | Advisory | Notes
------- | -------- | -----
podman-1.6.4-27.el7_9 | &nbsp; &nbsp; | &nbsp;
podman-docker-1.6.4-27.el7_9 | &nbsp; &nbsp; | &nbsp;
podman-remote-1.6.4-27.el7_9 | &nbsp; &nbsp; | &nbsp;
libvirt-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-admin-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-bash-completion-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-client-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-config-network-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-config-nwfilter-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-interface-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-libxl-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-lxc-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-network-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-nodedev-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-nwfilter-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-qemu-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-secret-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-core-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-disk-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-gluster-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-iscsi-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-logical-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-mpath-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-rbd-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-driver-storage-scsi-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-kvm-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-lxc-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-daemon-xen-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-devel-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-docs-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-libs-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-lock-sanlock-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-login-shell-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
libvirt-nss-5.1.0-9.xen413.el7 | &nbsp; &nbsp; | &nbsp;
python2-libvirt-5.1.0-2.xen413.el7 | &nbsp; &nbsp; | &nbsp;
qemu-xen-4.13.1-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ovmf-201905-1.git20d2e5a12.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.4.47.gcce7cbd986-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.13.2.59.ge4161938b3-1.el7 | &nbsp; &nbsp; | &nbsp;

