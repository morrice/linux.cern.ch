## 2019-08-15

Package | Advisory | Notes
------- | -------- | -----
python-astroid-1.2.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-logilab-common-0.62.1-1.el7 | &nbsp; &nbsp; | &nbsp;
