## 2023-04-24


Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20230421-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-102.10.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-demo-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-devel-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-headless-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-javadoc-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-jmods-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-src-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-static-libs-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
thunderbird-102.10.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;

