## 2016-11-03

Package | Advisory | Notes
------- | -------- | -----
curl-openssl-7.51.0-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-beta-24.0.0.138-0.beta.el7.cern | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-7.51.0-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-devel-7.51.0-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libsaml9-2.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libsaml-devel-2.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxerces-c-3_1-3.1.4-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxerces-c-devel-3.1.4-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxmltooling7-1.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxmltooling-devel-1.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
opensaml-bin-2.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
opensaml-schemas-2.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-2.6.0-2.1.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.6.0-2.1.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
xerces-c-bin-3.1.4-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
xmltooling-schemas-1.6.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-514.rt56.420.el7 | &nbsp; &nbsp; | &nbsp;
rteval-2.12-1.el7 | &nbsp; &nbsp; | &nbsp;
rteval-common-2.12-1.el7 | &nbsp; &nbsp; | &nbsp;
rt-setup-1.59-5.el7 | &nbsp; &nbsp; | &nbsp;
rt-tests-1.0-6.el7 | &nbsp; &nbsp; | &nbsp;
