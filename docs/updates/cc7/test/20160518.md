## 2016-05-18

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.621-1.el7.cern | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-2.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-build-2.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-runtime-2.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-scldevel-2.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-vagrant-1.8.1-4.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-vagrant-doc-1.8.1-4.el7 | &nbsp; &nbsp; | &nbsp;
firefox-45.1.1-1el7.centos | &nbsp; &nbsp; | &nbsp;
libndp-1.2-6.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1086" target="secadv">RHSA-2016:1086</a> | &nbsp;
libndp-devel-1.2-6.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1086" target="secadv">RHSA-2016:1086</a> | &nbsp;
