## 2015-06-05

Package | Advisory | Notes
------- | -------- | -----
openssl-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-devel-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-libs-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-perl-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-static-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
