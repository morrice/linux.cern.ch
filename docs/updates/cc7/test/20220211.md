## 2022-02-11


Package | Advisory | Notes
------- | -------- | -----
xen-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.4.103.g71e9d0c94d-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.13.4.9.gce49a1d6d8-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.14.4-1.el7 | &nbsp; &nbsp; | &nbsp;

