## 2019-07-12

Package | Advisory | Notes
------- | -------- | -----
tzdata-2019b-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1703" target="secadv">RHBA-2019:1703</a> | &nbsp;
tzdata-java-2019b-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1703" target="secadv">RHBA-2019:1703</a> | &nbsp;
