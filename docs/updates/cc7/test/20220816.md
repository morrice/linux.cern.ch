## 2022-08-16


Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.22.3-1.3.10.0_1160.76.1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
gvfs-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-afc-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-afp-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-archive-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-client-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-devel-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-fuse-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-goa-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-gphoto2-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-mtp-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-smb-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
gvfs-tests-1.36.2-7.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5936" target="secadv">RHBA-2022:5936</a> | &nbsp;
kernel-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-debug-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-devel-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-doc-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-headers-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-tools-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
ksh-20120801-144.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5938" target="secadv">RHBA-2022:5938</a> | &nbsp;
lldpad-1.0.1-7.git036e314.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5940" target="secadv">RHBA-2022:5940</a> | &nbsp;
lldpad-devel-1.0.1-7.git036e314.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5940" target="secadv">RHBA-2022:5940</a> | &nbsp;
microcode_ctl-2.1-73.14.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:5996" target="secadv">RHBA-2022:5996</a> | &nbsp;
perf-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;
python-perf-3.10.0-1160.76.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5937" target="secadv">RHSA-2022:5937</a> | &nbsp;

