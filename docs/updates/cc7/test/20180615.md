## 2018-06-15

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-30.0.0.113-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-3.0.31-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-tty-kraven-2.0-5.el7.cern | &nbsp; &nbsp; | &nbsp;
