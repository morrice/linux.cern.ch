## 2018-08-29

Package | Advisory | Notes
------- | -------- | -----
python2-sphinx-argparse-0.2.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-sushy-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sushy-tests-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-tests-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sushy-doc-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tempestconf-doc-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
bind-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-chroot-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-devel-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-libs-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-libs-lite-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-license-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-lite-devel-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-devel-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-libs-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-utils-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-sdb-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-sdb-chroot-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-utils-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
