## 2020-03-16

Package | Advisory | Notes
------- | -------- | -----
mariadb-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-backup-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-common-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-config-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-devel-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-embedded-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-embedded-devel-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-errmsg-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-gssapi-server-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-libs-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-oqgraph-engine-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-server-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-server-galera-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-server-utils-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
mariadb-test-10.3.20-3.el7.0.0.rdo1 | &nbsp; &nbsp; | &nbsp;
openstack-manila-7.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-doc-7.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-share-7.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-rabbitmq-10.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-7.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-7.4.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-asn1crypto-0.23.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-cffi-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-idna-2.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ipaddress-1.0.18-5.el7 | &nbsp; &nbsp; | &nbsp;
python-cffi-doc-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
ioprocess-1.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ioprocess-1.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
