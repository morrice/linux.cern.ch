## 2020-03-26

Package | Advisory | Notes
------- | -------- | -----
ansible-2.8.10-1.el7.ans | &nbsp; &nbsp; | &nbsp;
openstack-keystone-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-keystone-doc-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstacksdk-0.36.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstacksdk-tests-0.36.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystone-tests-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
centos-release-gluster7-1.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-nfs-ganesha28-1.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-nfs-ganesha30-1.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
glusterfs-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.8.3-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-3.2-4.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-3.2-6.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
