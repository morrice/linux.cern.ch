## 2017-10-03

Package | Advisory | Notes
------- | -------- | -----
dnsmasq-2.76-2.el7_4.2 | &nbsp; &nbsp; | &nbsp;
dnsmasq-utils-2.76-2.el7_4.2 | &nbsp; &nbsp; | &nbsp;
