## 2015-06-25

Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.9-2.3.10.0_229.7.2.el7 | &nbsp; &nbsp; | &nbsp;
gskcrypt64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
fence-agents-compute-4.0.11-13.el7_1 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1144" target="secadv">RHEA-2015:1144</a> | &nbsp;
ntp-4.2.6p5-19.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
