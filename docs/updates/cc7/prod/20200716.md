## 2020-07-16


Package | Advisory | Notes
------- | -------- | -----
puppet-eosclient-2.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-kerberos-2.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
389-ds-base-1.3.10.1-14.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2661" target="secadv">RHBA-2020:2661</a> | &nbsp;
389-ds-base-devel-1.3.10.1-14.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2661" target="secadv">RHBA-2020:2661</a> | &nbsp;
389-ds-base-libs-1.3.10.1-14.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2661" target="secadv">RHBA-2020:2661</a> | &nbsp;
389-ds-base-snmp-1.3.10.1-14.el7_8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2661" target="secadv">RHBA-2020:2661</a> | &nbsp;
dbus-1.10.24-14.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2894" target="secadv">RHSA-2020:2894</a> | &nbsp;
dbus-devel-1.10.24-14.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2894" target="secadv">RHSA-2020:2894</a> | &nbsp;
dbus-doc-1.10.24-14.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2894" target="secadv">RHSA-2020:2894</a> | &nbsp;
dbus-libs-1.10.24-14.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2894" target="secadv">RHSA-2020:2894</a> | &nbsp;
dbus-tests-1.10.24-14.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2894" target="secadv">RHSA-2020:2894</a> | &nbsp;
dbus-x11-1.10.24-14.el7_8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2894" target="secadv">RHSA-2020:2894</a> | &nbsp;
firefox-68.10.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

