## 2015-05-12

Package | Advisory | Notes
------- | -------- | -----
centos-release-7-1.1503.el7.cern.2.10 | &nbsp; &nbsp; | &nbsp;
splunk-6.2.3-264376 | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.2.3-264376 | &nbsp; &nbsp; | &nbsp;
cockpit-0.53-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0941" target="secadv">RHEA-2015:0941</a> | &nbsp;
cockpit-bridge-0.53-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0941" target="secadv">RHEA-2015:0941</a> | &nbsp;
cockpit-doc-0.53-3.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-docker-0.53-3.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-kubernetes-0.53-3.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-pcp-0.53-3.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-selinux-policy-0.53-3.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-shell-0.53-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0941" target="secadv">RHEA-2015:0941</a> | &nbsp;
cockpit-subscriptions-0.53-3.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-ws-0.53-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0941" target="secadv">RHEA-2015:0941</a> | &nbsp;
libssh-0.6.4-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0940" target="secadv">RHEA-2015:0940</a> | &nbsp;
libssh-devel-0.6.4-4.el7 | &nbsp; &nbsp; | &nbsp;
