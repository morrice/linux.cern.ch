## 2022-01-27


Package | Advisory | Notes
------- | -------- | -----
firefox-91.5.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
httpd-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-demo-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-devel-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-headless-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-javadoc-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-jmods-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-src-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
java-11-openjdk-static-libs-11.0.14.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0204" target="secadv">RHSA-2022:0204</a> | &nbsp;
mod_ldap-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-97.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
polkit-0.112-26.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0274" target="secadv">RHSA-2022:0274</a> | &nbsp;
polkit-devel-0.112-26.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0274" target="secadv">RHSA-2022:0274</a> | &nbsp;
polkit-docs-0.112-26.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0274" target="secadv">RHSA-2022:0274</a> | &nbsp;

