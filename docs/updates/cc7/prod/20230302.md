## 2023-03-02


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.83.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
puppet-zoom-1.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-102.8.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
libksba-1.3.0-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0530" target="secadv">RHSA-2023:0530</a> | &nbsp;
libksba-devel-1.3.0-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0530" target="secadv">RHSA-2023:0530</a> | &nbsp;
thunderbird-102.7.1-2.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-102.8.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
tigervnc-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
tigervnc-icons-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
tigervnc-license-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
tigervnc-server-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
tigervnc-server-applet-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
tigervnc-server-minimal-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
tigervnc-server-module-1.8.0-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;

