## 2020-11-12


Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.1.5-13.el7.cern | &nbsp; &nbsp; | &nbsp;
hepix-4.10.1-0.el7.cern | &nbsp; &nbsp; | &nbsp;
hepix-4.10.2-0.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.el7 | &nbsp; &nbsp; | &nbsp;
msktutil-1.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1160.2.1.rt56.1133.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1160.2.2.rt56.1134.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1160.rt56.1131.el7 | &nbsp; &nbsp; | &nbsp;
rteval-2.14-19.el7 | &nbsp; &nbsp; | &nbsp;
rteval-common-2.14-19.el7 | &nbsp; &nbsp; | &nbsp;
rt-tests-1.8-4.el7 | &nbsp; &nbsp; | &nbsp;

