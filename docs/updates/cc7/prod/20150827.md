## 2015-08-27

Package | Advisory | Notes
------- | -------- | -----
nss-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.19.1-5.el7_1.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-3.4.0-0.beta7.el7.cern | &nbsp; &nbsp; | &nbsp;
gskcrypt64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
TIVsm-API64-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-APIcit-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BA-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAcit-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAhdw-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-filepath-7.1.2-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-JBB-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
glusterfs-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-api-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-api-devel-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-cli-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-client-xlators-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-devel-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-fuse-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-libs-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
glusterfs-rdma-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
python-gluster-3.7.1-11.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1496" target="secadv">RHBA-2015:1496</a> | &nbsp;
httpd-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mariadb-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-bench-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-devel-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-embedded-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-embedded-devel-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-libs-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-server-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mariadb-test-5.5.44-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1665" target="secadv">RHSA-2015:1665</a> | &nbsp;
mod_ldap-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-31.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
pam-1.1.8-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1640" target="secadv">RHSA-2015:1640</a> | &nbsp;
pam-devel-1.1.8-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1640" target="secadv">RHSA-2015:1640</a> | &nbsp;
thunderbird-38.2.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
