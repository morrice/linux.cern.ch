## 2023-05-11


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-debug-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-devel-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-doc-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-headers-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-tools-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
libwebp-0.3.0-11.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:2077" target="secadv">RHSA-2023:2077</a> | &nbsp;
libwebp-devel-0.3.0-11.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:2077" target="secadv">RHSA-2023:2077</a> | &nbsp;
libwebp-java-0.3.0-11.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:2077" target="secadv">RHSA-2023:2077</a> | &nbsp;
libwebp-tools-0.3.0-11.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:2077" target="secadv">RHSA-2023:2077</a> | &nbsp;
perf-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;
python-perf-3.10.0-1160.90.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1987" target="secadv">RHSA-2023:1987</a> | &nbsp;

