## 2022-10-27


Package | Advisory | Notes
------- | -------- | -----
389-ds-base-1.3.10.2-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7087" target="secadv">RHSA-2022:7087</a> | &nbsp;
389-ds-base-devel-1.3.10.2-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7087" target="secadv">RHSA-2022:7087</a> | &nbsp;
389-ds-base-libs-1.3.10.2-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7087" target="secadv">RHSA-2022:7087</a> | &nbsp;
389-ds-base-snmp-1.3.10.2-17.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7087" target="secadv">RHSA-2022:7087</a> | &nbsp;
copy-jdk-configs-3.3-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:6851" target="secadv">RHBA-2022:6851</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.352.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7002" target="secadv">RHSA-2022:7002</a> | &nbsp;
java-11-openjdk-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-demo-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-devel-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-headless-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-javadoc-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-jmods-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-src-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
java-11-openjdk-static-libs-11.0.17.0.8-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7008" target="secadv">RHSA-2022:7008</a> | &nbsp;
libksba-1.3.0-6.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7088" target="secadv">RHSA-2022:7088</a> | &nbsp;
libksba-devel-1.3.0-6.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:7088" target="secadv">RHSA-2022:7088</a> | &nbsp;
squid-3.5.20-17.el7_9.8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6815" target="secadv">RHSA-2022:6815</a> | &nbsp;
squid-migration-script-3.5.20-17.el7_9.8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6815" target="secadv">RHSA-2022:6815</a> | &nbsp;
squid-sysvinit-3.5.20-17.el7_9.8 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6815" target="secadv">RHSA-2022:6815</a> | &nbsp;
tzdata-2022e-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7067" target="secadv">RHBA-2022:7067</a> | &nbsp;
tzdata-java-2022e-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:7067" target="secadv">RHBA-2022:7067</a> | &nbsp;

