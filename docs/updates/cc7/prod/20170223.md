## 2017-02-23

Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.0.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.0.0-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.0.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.0.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.0.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cve_2017_6074-0.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cve_2017_6074-0.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_514.6.2.el7 | &nbsp; &nbsp; | &nbsp;
msktutil-1.0-0.git.1.el7.cern | &nbsp; &nbsp; | &nbsp;
msktutil-1.0-0.git.el7.cern | &nbsp; &nbsp; | &nbsp;
pam_afs_session-2.6-2.el7.cern | &nbsp; &nbsp; | &nbsp;
bind-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-chroot-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-devel-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-libs-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-libs-lite-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-license-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-lite-devel-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-pkcs11-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-pkcs11-devel-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-pkcs11-libs-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-pkcs11-utils-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-sdb-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-sdb-chroot-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
bind-utils-9.9.4-38.el7_3.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0276" target="secadv">RHSA-2017:0276</a> | &nbsp;
firefox-45.7.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-abi-whitelists-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-debug-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-debug-devel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-devel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-doc-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-headers-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-tools-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-tools-libs-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
openssl-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-devel-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-libs-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-perl-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
openssl-static-1.0.1e-60.el7_3.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0286" target="secadv">RHSA-2017:0286</a> | &nbsp;
perf-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
python-perf-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
