## 2015-07-30

Package | Advisory | Notes
------- | -------- | -----
bind-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-chroot-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-devel-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-libs-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-libs-lite-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-license-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-lite-devel-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-sdb-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-sdb-chroot-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-utils-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
clutter-1.14.4-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1510" target="secadv">RHSA-2015:1510</a> | &nbsp;
clutter-devel-1.14.4-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1510" target="secadv">RHSA-2015:1510</a> | &nbsp;
clutter-doc-1.14.4-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1510" target="secadv">RHSA-2015:1510</a> | &nbsp;
libcacard-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
libcacard-devel-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
libcacard-tools-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
libuser-0.60-7.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1483" target="secadv">RHSA-2015:1483</a> | &nbsp;
libuser-devel-0.60-7.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1483" target="secadv">RHSA-2015:1483</a> | &nbsp;
libuser-python-0.60-7.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1483" target="secadv">RHSA-2015:1483</a> | &nbsp;
python-chardet-2.2.1-1.el7_1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1474" target="secadv">RHBA-2015:1474</a> | &nbsp;
python-requests-2.6.0-1.el7_1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1475" target="secadv">RHBA-2015:1475</a> | &nbsp;
python-urllib3-1.10.2-1.el7_1 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1473" target="secadv">RHEA-2015:1473</a> | &nbsp;
qemu-img-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
qemu-kvm-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
qemu-kvm-common-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
qemu-kvm-tools-1.5.3-86.el7_1.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1507" target="secadv">RHSA-2015:1507</a> | &nbsp;
