## 2018-08-30

Package | Advisory | Notes
------- | -------- | -----
python2-sphinx-argparse-0.2.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-sushy-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sushy-tests-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-tests-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sushy-doc-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tempestconf-doc-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
bind-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-chroot-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-devel-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-libs-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-libs-lite-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-license-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-lite-devel-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-devel-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-libs-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-pkcs11-utils-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-sdb-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-sdb-chroot-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
bind-utils-9.9.4-61.el7_5.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2570" target="secadv">RHSA-2018:2570</a> | &nbsp;
postgresql-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-contrib-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-devel-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-docs-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-libs-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-plperl-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-plpython-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-pltcl-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-server-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-static-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-test-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
postgresql-upgrade-9.2.24-1.el7_5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2557" target="secadv">RHSA-2018:2557</a> | &nbsp;
ansible-2.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
