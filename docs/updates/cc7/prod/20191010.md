## 2019-10-10

Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1062.1.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
aims2-client-2.28-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.28-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.1.5-7.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1062.1.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1062.1.1.rt56.1024.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1062.1.2.rt56.1025.el7 | &nbsp; &nbsp; | &nbsp;
linux-firmware-20190429-72.gitddde598.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2169" target="secadv">RHSA-2019:2169</a> | &nbsp;
tuned-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-gtk-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-gtk-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-gtk-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-atomic-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-atomic-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-profiles-atomic-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-compat-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-compat-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-profiles-compat-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-cpu-partitioning-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-cpu-partitioning-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-profiles-cpu-partitioning-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-2.8.0-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-guest-2.8.0-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-guest-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-host-2.8.0-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-host-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-oracle-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-oracle-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-profiles-oracle-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-realtime-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-realtime-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-2.8.0-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-hana-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-hana-2.8.0-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-hana-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-utils-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-utils-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-utils-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
tuned-utils-systemtap-2.7.1-5.el7 | &nbsp; &nbsp; | &nbsp;
tuned-utils-systemtap-2.8.0-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2102" target="secadv">RHBA-2017:2102</a> | &nbsp;
tuned-utils-systemtap-2.9.0-1.el7fdp | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-debug-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-debug-devel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-devel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-doc-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-headers-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-tools-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-tools-libs-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
perf-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
python-perf-3.10.0-1062.1.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:2829" target="secadv">RHSA-2019:2829</a> | &nbsp;
