## 2015-06-11

Package | Advisory | Notes
------- | -------- | -----
centos-release-openstack-kilo-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
cloud-init-0.7.5-1.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0041" target="secadv">RHEA-2015:0041</a> | &nbsp;
openssl-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-devel-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-libs-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-perl-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
openssl-static-1.0.1e-42.el7.6 | &nbsp; &nbsp; | &nbsp;
