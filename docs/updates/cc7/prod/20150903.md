## 2015-09-03

Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-0.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-adblock_plus-2.6.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-adblock_plus_ehhelper-1.3.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-add-ons-0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-noscript_security_suite-2.6.9.36-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.60-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.60-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.60-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.60-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.60-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.60-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
lpadmincern-1.3.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
nux-dextop-release-0-5.1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-add-ons-0-3.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-calendar-tweaks-6.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
thunderbird-provider_for_google_calendar-1.0.4-2.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-38.2.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;
gdk-pixbuf2-2.28.2-5.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1694" target="secadv">RHSA-2015:1694</a> | &nbsp;
gdk-pixbuf2-devel-2.28.2-5.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1694" target="secadv">RHSA-2015:1694</a> | &nbsp;
jakarta-taglibs-standard-1.1.2-14.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1695" target="secadv">RHSA-2015:1695</a> | &nbsp;
jakarta-taglibs-standard-javadoc-1.1.2-14.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1695" target="secadv">RHSA-2015:1695</a> | &nbsp;
nss-softokn-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
nss-softokn-devel-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
nss-softokn-freebl-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
nss-softokn-freebl-devel-3.16.2.3-13.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1699" target="secadv">RHSA-2015:1699</a> | &nbsp;
pacemaker-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-cli-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-cluster-libs-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-cts-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-doc-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-libs-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-libs-devel-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pacemaker-remote-1.1.12-22.el7_1.4 | &nbsp; &nbsp; | &nbsp;
pcs-0.9.137-13.el7_1.4 | &nbsp; &nbsp; | &nbsp;
python-clufter-0.9.137-13.el7_1.4 | &nbsp; &nbsp; | &nbsp;
