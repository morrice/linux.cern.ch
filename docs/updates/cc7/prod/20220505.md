## 2022-05-05


Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.26-3.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.26-3.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.26-3.el7.cern | &nbsp; &nbsp; | &nbsp;

