## 2015-02-26

Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20150218-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-0.9.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-7-0.1406.el7.centos.2.6 | &nbsp; &nbsp; | &nbsp;
firefox-31.5.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
libsmbclient-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
libsmbclient-devel-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
libwbclient-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
libwbclient-devel-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-client-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-common-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-dc-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-dc-libs-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-devel-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-libs-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-pidl-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-python-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-test-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-test-devel-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-vfs-glusterfs-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-winbind-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-winbind-clients-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-winbind-krb5-locator-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
samba-winbind-modules-4.1.1-38.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0252" target="secadv">RHSA-2015:0252</a> | &nbsp;
xulrunner-31.5.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xulrunner-devel-31.5.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
