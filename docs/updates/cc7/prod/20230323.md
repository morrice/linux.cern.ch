## 2023-03-23


Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.4.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-2.0.30-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.30-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.30-1.el7.cern | &nbsp; &nbsp; | &nbsp;

