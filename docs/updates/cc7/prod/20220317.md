## 2022-03-17


Package | Advisory | Notes
------- | -------- | -----
devtoolset-10-binutils-2.35-5.el7.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0506" target="secadv">RHBA-2022:0506</a> | &nbsp;
devtoolset-10-binutils-devel-2.35-5.el7.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:0506" target="secadv">RHBA-2022:0506</a> | &nbsp;
firefox-91.7.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;

