## 2015-07-23

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.9.19-2.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.9.19-2.el7.cern | &nbsp; &nbsp; | &nbsp;
CERN-CA-certs-20150721-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-sso-cookie-0.5.4-2.el7.cern | &nbsp; &nbsp; | &nbsp;
elrepo-release-7.0-2.el7.cern | &nbsp; &nbsp; | &nbsp;
nux-dextop-release-0-5.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.4-2.el7.cern | &nbsp; &nbsp; | &nbsp;
bind-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-chroot-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-devel-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-libs-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-libs-lite-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-license-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-lite-devel-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-sdb-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-sdb-chroot-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
bind-utils-9.9.4-18.el7_1.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1443" target="secadv">RHSA-2015:1443</a> | &nbsp;
thunderbird-31.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
