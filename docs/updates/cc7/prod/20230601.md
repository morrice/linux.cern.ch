## 2023-06-01


Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-1.0.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
apr-util-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-devel-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-ldap-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-mysql-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-nss-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-odbc-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-openssl-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-pgsql-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
apr-util-sqlite-1.5.2-6.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3145" target="secadv">RHSA-2023:3145</a> | &nbsp;
emacs-git-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
emacs-git-el-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
firefox-102.11.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
git-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-all-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-bzr-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-cvs-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-daemon-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-email-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-gnome-keyring-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-gui-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-hg-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-instaweb-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-p4-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
git-svn-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
gitk-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
gitweb-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
perl-Git-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
perl-Git-SVN-1.8.3.1-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3263" target="secadv">RHSA-2023:3263</a> | &nbsp;
thunderbird-102.11.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

