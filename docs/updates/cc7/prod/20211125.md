## 2021-11-25


Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-1.6-1.el7.cern | &nbsp; &nbsp; | &nbsp;
rh-ruby27-ruby-2.7.4-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-ruby-devel-2.7.4-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-ruby-doc-2.7.4-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-ruby-libs-2.7.4-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-bigdecimal-2.0.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-bundler-2.2.24-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-did_you_mean-1.4.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-io-console-0.5.6-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-irb-1.2.6-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-json-2.3.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-minitest-5.13.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-net-telnet-0.2.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-openssl-2.1.2-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-power_assert-1.1.7-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-psych-3.1.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-racc-1.4.16-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-rake-13.0.1-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-rdoc-6.2.1.1-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-test-unit-3.3.4-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygem-xmlrpc-0.3.0-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygems-3.1.6-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;
rh-ruby27-rubygems-devel-3.1.6-130.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3559" target="secadv">RHSA-2021:3559</a> | &nbsp;

