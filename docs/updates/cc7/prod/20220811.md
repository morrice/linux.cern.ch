## 2022-08-11


Package | Advisory | Notes
------- | -------- | -----
thunderbird-91.11.0-2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
thunderbird-91.12.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
tuned-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-gtk-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-profiles-atomic-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-profiles-compat-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-profiles-cpu-partitioning-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-profiles-mssql-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-profiles-nfv-2.11.0-11.el7_9 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-guest-2.11.0-11.el7_9 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-nfv-host-2.11.0-11.el7_9 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-oracle-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-profiles-realtime-2.11.0-11.el7_9 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-2.11.0-11.el7_9 | &nbsp; &nbsp; | &nbsp;
tuned-profiles-sap-hana-2.11.0-11.el7_9 | &nbsp; &nbsp; | &nbsp;
tuned-utils-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
tuned-utils-systemtap-2.11.0-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0340" target="secadv">RHBA-2021:0340</a> | &nbsp;
firefox-91.12.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-91.12.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-Xdmx-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-Xephyr-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-Xnest-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-Xorg-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-Xvfb-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-Xwayland-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-common-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-devel-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;
xorg-x11-server-source-1.20.4-18.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:5905" target="secadv">RHSA-2022:5905</a> | &nbsp;

