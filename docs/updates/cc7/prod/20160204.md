## 2016-02-04

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-327.4.5.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
bind-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-chroot-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-devel-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-libs-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-libs-lite-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-license-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-lite-devel-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-pkcs11-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-pkcs11-devel-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-pkcs11-libs-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-pkcs11-utils-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-sdb-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-sdb-chroot-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-utils-9.9.4-29.el7_2.2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0073" target="secadv">RHSA-2016:0073</a> | &nbsp;
firefox-38.6.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
java-1.6.0-openjdk-1.6.0.38-1.13.10.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0067" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.38-1.13.10.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0067" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.38-1.13.10.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0067" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.38-1.13.10.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0067" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.38-1.13.10.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0067" target="secadv">RHSA-2016:0067</a> | &nbsp;
libcacard-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
libcacard-devel-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
libcacard-tools-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
qemu-img-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
qemu-kvm-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
qemu-kvm-common-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
qemu-kvm-tools-1.5.3-105.el7_2.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0083" target="secadv">RHSA-2016:0083</a> | &nbsp;
