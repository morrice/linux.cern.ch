## 2017-02-09

Package | Advisory | Notes
------- | -------- | -----
NetworkManager-DUID-LLT-0.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
NetworkManager-DUID-LLT-0.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
libtiff-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-devel-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-static-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-tools-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
ntp-4.2.6p5-25.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
ntpdate-4.2.6p5-25.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
ntp-doc-4.2.6p5-25.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
ntp-perl-4.2.6p5-25.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
pcs-0.9.152-10.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
sntp-4.2.6p5-25.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
spice-server-0.12.4-20.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0254" target="secadv">RHSA-2017:0254</a> | &nbsp;
spice-server-devel-0.12.4-20.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0254" target="secadv">RHSA-2017:0254</a> | &nbsp;
thunderbird-45.7.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
