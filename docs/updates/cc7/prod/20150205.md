## 2015-02-05

Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.9-2.3.10.0_123.20.1.el7 | &nbsp; &nbsp; | &nbsp;
shibboleth-selinux-0.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-2.9.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
lin_taped-2.9.4-1 | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-abi-whitelists-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-debug-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-debug-devel-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-devel-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-doc-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-headers-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-tools-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-tools-libs-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
libyaml-0.1.4-11.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0100" target="secadv">RHSA-2015:0100</a> | &nbsp;
libyaml-devel-0.1.4-11.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0100" target="secadv">RHSA-2015:0100</a> | &nbsp;
perf-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
python-perf-3.10.0-123.20.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0102" target="secadv">RHSA-2015:0102</a> | &nbsp;
