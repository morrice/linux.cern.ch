## 2023-03-23

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))
openssl-debuginfo | 1.1.1k-9.el8_7 | |
openssl-debugsource | 1.1.1k-9.el8_7 | |
openssl-devel | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))
openssl-libs | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))
openssl-libs-debuginfo | 1.1.1k-9.el8_7 | |
openssl-perl | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el8_7.alma | |
thunderbird-debuginfo | 102.9.0-1.el8_7.alma | |
thunderbird-debugsource | 102.9.0-1.el8_7.alma | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl-static | 1.1.1k-9.el8_7 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el8_7.alma.plus | |
thunderbird-debuginfo | 102.9.0-1.el8_7.alma.plus | |
thunderbird-debugsource | 102.9.0-1.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.9.0-1.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.9.0-1.el8_7.alma.plus | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))
openssl-debuginfo | 1.1.1k-9.el8_7 | |
openssl-debugsource | 1.1.1k-9.el8_7 | |
openssl-devel | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))
openssl-libs | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))
openssl-libs-debuginfo | 1.1.1k-9.el8_7 | |
openssl-perl | 1.1.1k-9.el8_7 | [RHSA-2023:1405](https://access.redhat.com/errata/RHSA-2023:1405) | <div class="adv_s">Security Advisory</div> ([CVE-2022-4304](https://access.redhat.com/security/cve/CVE-2022-4304), [CVE-2022-4450](https://access.redhat.com/security/cve/CVE-2022-4450), [CVE-2023-0215](https://access.redhat.com/security/cve/CVE-2023-0215), [CVE-2023-0286](https://access.redhat.com/security/cve/CVE-2023-0286))

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el8_7.alma | |
thunderbird-debuginfo | 102.9.0-1.el8_7.alma | |
thunderbird-debugsource | 102.9.0-1.el8_7.alma | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openssl-static | 1.1.1k-9.el8_7 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el8_7.alma.plus | |
thunderbird-debuginfo | 102.9.0-1.el8_7.alma.plus | |
thunderbird-debugsource | 102.9.0-1.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.9.0-1.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.9.0-1.el8_7.alma.plus | |

