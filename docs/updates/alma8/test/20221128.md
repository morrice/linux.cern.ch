## 2022-11-28

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release | 8.7-3.al8.cern | |
cern-krb5-conf | 1.3-7.al8.cern | |
cern-krb5-conf-atlas | 1.3-7.al8.cern | |
cern-krb5-conf-cms | 1.3-7.al8.cern | |
cern-krb5-conf-defaults-cernch | 1.3-7.al8.cern | |
cern-krb5-conf-defaults-ipadev | 1.3-7.al8.cern | |
cern-krb5-conf-ipadev | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch-atlas | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch-cms | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch-tn | 1.3-7.al8.cern | |
cern-krb5-conf-realm-ipadev | 1.3-7.al8.cern | |
cern-krb5-conf-tn | 1.3-7.al8.cern | |
cern-linuxsupport-access | 1.8-1.al8.cern | |
cern-wrappers | 1-18.al8.cern | |
cern-wrappers-passwd | 1-18.al8.cern | |
epel-release | 8-11.3.al8.cern | |
pyphonebook | 2.1.4-1.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
almalinux-release | 8.7-3.al8.cern | |
cern-krb5-conf | 1.3-7.al8.cern | |
cern-krb5-conf-atlas | 1.3-7.al8.cern | |
cern-krb5-conf-cms | 1.3-7.al8.cern | |
cern-krb5-conf-defaults-cernch | 1.3-7.al8.cern | |
cern-krb5-conf-defaults-ipadev | 1.3-7.al8.cern | |
cern-krb5-conf-ipadev | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch-atlas | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch-cms | 1.3-7.al8.cern | |
cern-krb5-conf-realm-cernch-tn | 1.3-7.al8.cern | |
cern-krb5-conf-realm-ipadev | 1.3-7.al8.cern | |
cern-krb5-conf-tn | 1.3-7.al8.cern | |
cern-linuxsupport-access | 1.8-1.al8.cern | |
cern-wrappers | 1-18.al8.cern | |
cern-wrappers-passwd | 1-18.al8.cern | |
epel-release | 8-11.3.al8.cern | |
pyphonebook | 2.1.4-1.al8.cern | |

