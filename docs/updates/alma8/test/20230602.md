## 2023-06-02

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.3-1.el8.alma | |
sos-audit | 4.5.3-1.el8.alma | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_477.13.1.el8_8.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
sos | 4.5.3-1.el8.alma | |
sos-audit | 4.5.3-1.el8.alma | |

