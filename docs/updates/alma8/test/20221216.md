## 2022-12-16

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.4-2.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.4-2.al8.cern | |

