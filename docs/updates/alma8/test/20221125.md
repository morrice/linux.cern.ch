## 2022-11-25

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20220329-1.al8.cern | |
cern-gpg-keys | 1.0-1.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20220329-1.al8.cern | |
cern-gpg-keys | 1.0-1.al8.cern | |

