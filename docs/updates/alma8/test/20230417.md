## 2023-04-17

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.10.0-1.el8_7.alma | |
firefox-debuginfo | 102.10.0-1.el8_7.alma | |
firefox-debugsource | 102.10.0-1.el8_7.alma | |
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.10.0-1.el8_7.alma | |
firefox-debuginfo | 102.10.0-1.el8_7.alma | |
firefox-debugsource | 102.10.0-1.el8_7.alma | |
stalld-debuginfo | 1.17.1-1.el8_7 | |
stalld-debugsource | 1.17.1-1.el8_7 | |
vim-debugsource | 8.0.1763-19.el8_6.4 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
mingw32-winpthreads-debuginfo | 5.0.2-2.el8 | |
mingw64-winpthreads-debuginfo | 5.0.2-2.el8 | |

