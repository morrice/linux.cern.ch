## 2023-01-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.al8.cern | |
cern-get-certificate | 0.9.4-5.al8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.6.0-1.el8_7.alma | |
firefox-debuginfo | 102.6.0-1.el8_7.alma | |
firefox-debugsource | 102.6.0-1.el8_7.alma | |
nodejs | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-debuginfo | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-debugsource | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-devel | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-docs | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-full-i18n | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-nodemon | 2.0.20-2.module_el8.7.0+3371+ed8c43db | |
npm | 8.19.2-1.16.18.1.3.module_el8.7.0+3371+ed8c43db | |
prometheus-jmx-exporter | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
prometheus-jmx-exporter-openjdk11 | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
prometheus-jmx-exporter-openjdk17 | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
prometheus-jmx-exporter-openjdk8 | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
thunderbird | 102.6.0-2.el8_7.alma | |
thunderbird-debuginfo | 102.6.0-2.el8_7.alma | |
thunderbird-debugsource | 102.6.0-2.el8_7.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.6.0-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.6.0-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.6.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.6.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.6.0-2.el8_7.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.al8.cern | |
cern-get-certificate | 0.9.4-5.al8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.6.0-1.el8_7.alma | |
firefox-debuginfo | 102.6.0-1.el8_7.alma | |
firefox-debugsource | 102.6.0-1.el8_7.alma | |
nodejs | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-debuginfo | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-debugsource | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-devel | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-docs | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-full-i18n | 16.18.1-3.module_el8.7.0+3371+ed8c43db | |
nodejs-nodemon | 2.0.20-2.module_el8.7.0+3371+ed8c43db | |
npm | 8.19.2-1.16.18.1.3.module_el8.7.0+3371+ed8c43db | |
prometheus-jmx-exporter | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
prometheus-jmx-exporter-openjdk11 | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
prometheus-jmx-exporter-openjdk17 | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
prometheus-jmx-exporter-openjdk8 | 0.12.0-9.el8_7 | [RHSA-2022:9058](https://access.redhat.com/errata/RHSA-2022:9058) | <div class="adv_s">Security Advisory</div> ([CVE-2022-1471](https://access.redhat.com/security/cve/CVE-2022-1471))
thunderbird | 102.6.0-2.el8_7.alma | |
thunderbird-debuginfo | 102.6.0-2.el8_7.alma | |
thunderbird-debugsource | 102.6.0-2.el8_7.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.6.0-2.el8_7.alma.plus | |
thunderbird-debuginfo | 102.6.0-2.el8_7.alma.plus | |
thunderbird-debugsource | 102.6.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp | 102.6.0-2.el8_7.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.6.0-2.el8_7.alma.plus | |

