## 2022-12-08

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-debuginfo | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-debugsource | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-devel | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-docs | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-full-i18n | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-nodemon | 2.0.20-1.module_el8.7.0+3370+40ccb2a8 | |
npm | 8.19.2-1.18.12.1.2.module_el8.7.0+3370+40ccb2a8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nodejs | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-debuginfo | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-debugsource | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-devel | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-docs | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-full-i18n | 18.12.1-2.module_el8.7.0+3370+40ccb2a8 | |
nodejs-nodemon | 2.0.20-1.module_el8.7.0+3370+40ccb2a8 | |
npm | 8.19.2-1.18.12.1.2.module_el8.7.0+3370+40ccb2a8 | |

