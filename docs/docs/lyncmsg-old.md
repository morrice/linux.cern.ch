<!--#include virtual="/linux/layout/header" -->
# Pidgin (Gaim) integration with Microsoft Lync Server
<h2>Pidgin integration with MS Lync Server Instant Messaging </h2>
CERN uses Microsoft Lync Server as the base of instant messaging services. While this platform is
well integrated with Microsoft Windows clients and with Apple Mac OS X ones, integration was lacking for
Linux clients.
<p>
This documentation outlines the setup process allowing Linux clients to benefit from integration with Microsoft Lync
instant messaging.
<p>
While the initial installation of required software is specific to CERN SLC6 and SLC5
Linux distributions, following configuration steps shall be applicable on any modern Linux platform - running at least <a href="www.pidgin.im/">Pidgin 2.7.X</a> and
<a href="http://sipe.sourceforge.net/">Pidgin-SIPE 1.12.0</a> plugin.

<h2>Software installation</h2>

As root on your SLC6 or SLC5 system run:
<pre>
&#35; yum install pidgin-sipe
</pre>

once installation of required software packages finishes, please restart Pidgin.
<h2>Configuration</h2>
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot.png"><img src="../lyncmsg/Screenshot.png" width="300"></a> </td>
    <td>Start pidgin (<b>Menu Applications -> Internet -> Pidgin Internet Messenger</b>)</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot-1.png"><img src="../lyncmsg/Screenshot-1.png" width="300"></a> </td>
    <td>Select <b>Manage accounts</b> from <b>Accounts</b> menu and click <b>Add</b>.</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot-2.png"><img src="../lyncmsg/Screenshot-2.png" width="300"></a> </td>
    <td>In <b>Basic</b> tab<br>
        Select <b>Office Communicator</b> as <b>Protocol</b><br>
        Enter your CERN e-mail <b>Firstname.Surname@cern.ch</b> as <b>Username</b><br>
        Leave <b>Login</b> field empty<br>
        Enter your CERN password as <b>Password</b><br>
        and optionally check <b>Remember password</b> checkbox.
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot-3.png"><img src="../lyncmsg/Screenshot-3.png" width="300"></a> </td>
    <td>In <b>Advanced</b> tab<br>
        Select <b>SSL/TLS</b> as <b>Connection type</b><br>
        Select <b>NTLM</b> as <b>Authentication scheme</b><br>
        Leave other fields empty<br>
        then click on <b>Add/Save</b> to finalize account setup.</td>
   </tr>
 </tbody>
 </table>
<h4>Adding CERN Lync contacts to Pidgin</h4>
<em>Note:</em> Contact search functionality against a Microsoft Lync server requires a CERN-patched pidgin-sipe-1.13.0 pre-release.
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot-4.png"><img src="../lyncmsg/Screenshot-4.png" width="300"></a> </td>
    <td>Select <b>Contact search...</b> from <b>Accounts</b> -> <b>Office Communicator</b> menu and enter search term.</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot-5.png"><img src="../lyncmsg/Screenshot-5.png" width="300"></a> </td>
    <td>Select desired contact and click <b>Add</b>.</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/Screenshot-6.png"><img src="../lyncmsg/Screenshot-6.png" width="300"></a> </td>
    <td>Click <b>Add</b> to add contact to main pidgin window.</td>
   </tr>
  </tbody>
</table>

     <em>Note:</em> While pidgin-sipe plugin supports (to some extent) audio/video calling features of Lync Server (softphone), current pidgin versions on SLC6 and SLC5 do not have support for audio/video compiled in, therefore this feature is not available for now.


