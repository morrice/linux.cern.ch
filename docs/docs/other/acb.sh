#!/bin/bash
# acb
# Jaroslaw.Polok@cern.ch
#---------------------------------------------------------------------------
# Script to use CERN ACB service
# YOU WILL NEED: pppd supporting MS-chap and CBCP !
#
# Documentation:
# ACB service http://wwwcs.cern.ch/
# CERN linux: http://cern.ch/linux/redhat7/documentation/
#
# You will need to know:
# RASPHONE - remote access server phone number
#            0227675500 - callback - dialing from Switzerland
#            0450205500 - callback - dialing from France
#            0800803703 - dialin from Switzerland
#            0800044666 - dialin from France
#
# MYPHONE  - YOUR phone number ;-)
#            as called from CERN
#            0022xxxxxxx  - Switzerland
#            100450xxxxxx - France
#
# MYLOGIN  - YOUR CERN nice login id
#
# /etc/ppp/chap-secrets file contents:
# CERN\\NICELOGIN     RAS-CERN                  NICEPASSWORD 
# RAS-CERN                  CERN\\NICELOGIN     NICEPASSWORD 
#
#
#+----------------- user-dependent settings --------------------------------
RASPHONE="XXXXXXXX"               # CERN RAS phone number, see above
MYPHONE="XXXXXXX"                 # see above 
MYLOGIN="XXXXXXX"                 # CERN NICE login
DEV=/dev/modem                    # adjust if needed
SPEED=115200                      #
TODO=40                           # TIMEOUT for dialout
TOCB=20                           # TIMEOUT for callback
#----------------- debug ? ----------------------------------------------------
PPPDB=debug
CHATDB=-v
#----------------- modem ------------------------------------------------------
MODEMINIT="AT X4 L1 V1 &C0 &D2 S0=0 H0 "
MODEMSTR=$MODEMINIT"DT"$RASPHONE
#----------------- pppd settings ----------------------------------------------
PPPD=/usr/sbin/pppd

PPPDOPTS="lock $PPPDB crtscts modem bsdcomp 15 defaultroute nodetach name CERN\\"$MYLOGIN" remotename RAS-CERN noauth receive-all"
#----------------- chat settings & connect strings ----------------------------
CHAT=/usr/sbin/chat

CHABDO="ABORT BUSY ABORT 'NO ANSWER' ABORT ERROR ABORT 'NO DIAL TONE'"
CHABCB="ABORT ERROR ABORT 'NO DIAL TONE'"
CHATDO="TIMEOUT $TODO $CHABDO '' AT OK '$MODEMSTR' CONNECT ''"
CHATCB="TIMEOUT $TOCB $CHABCB '' AT OK '' RING ATA CONNECT ''"

# ---------- Dialout ----------------------------------------------------------
$PPPD $DEV $SPEED $PPPDOPTS callback $MYPHONE connect "$CHAT $CHATDB $CHATDO"
# ---------- Callback ---------------------------------------------------------
if [ $? = 14 ]; then
$PPPD $DEV $SPEED $PPPDOPTS connect "$CHAT $CHATDB $CHATCB"
fi
