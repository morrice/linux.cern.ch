<!--#include virtual="/linux/layout/header" -->
# Using Kerberos authentication for CERN E-mail services
<h2>Using Kerberos authentication for CERN E-mail services</h2>
CERN uses Microsoft Exchange to provide <a href="http://mmm.cern.ch">E-mail / Collaboration services</a>.
Microsoft Exchange provides multiple user authentication mechanisms: NTLM, Username/Password based and also Kerberos.
<p>
This documentation outlines the setup process allowing Linux clients to use Kerberos based authentication
for CERN E-mail services access.
<p>
While the initial installation of required software is specific to CERN SLC6 and SLC5
Linux distributions, the same functionality shall be applicable on any modern Linux platform -
 <a href="../kerberos-access">configured for CERN Kerberos realm</a>, running at least <a href="http://getthunderbird.com/">Thunderbird 10.X</a> with <a href="https://developer.mozilla.org/En/Integrated_Authentication">Kerberos authentication enabled</a> (and configured for cern.ch domain)</a>, Alpine 2.X (with an SMTP workaround patch, see below) or fetchmail with Kerberos support compiled in..

<h2>Configuration</h2>
<h3>Thunderbird</h3>
<h4>Software installation</h4>

As root on your SLC5, SLC6 or CC7 system run:
<pre>
&#35; yum install mozilla-prefs
</pre>

once installation of required software packages finishes, please restart Thunderbird.<br>
(<em>Note:</em> As of SLC6/5 update of 12.03.2012 mozilla-prefs package is pre-installed on all systems.)
<h4>Setup: New account</h4>
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="../miscshots/thunderbird-2.png"><img src="../miscshots/thunderbird-2.png" width="400"></a> </td>
    <td>After starting thunderbird (Menu -> Applications -> Internet -> Thunderbird E-mail) new e-mail account setup screen will appear<p>
    Enter <b>Firstname</b> <b>Surname</b> in <i>Your name</i><p>
    Enter <b>Firstname.Surname@cern.ch</b> in <i>Email address</i><p>
    Leave <i>Password</i> field <b>empty</b> and click <i>Continue</i>
   </td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../miscshots/thunderbird-3.png"><img src="../miscshots/thunderbird-3.png" width="400"></a> </td>
    <td>Thunderbird autoconfiguration mechanism will find most of needed server settings, except SMTP/IMAP Kerberos authentication which is not default at CERN as of September 2012<p>
    Leave <i>Password</i> field <b>empty</b> and click <i>Manual config</i>
   </td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../miscshots/thunderbird-4.png"><img src="../miscshots/thunderbird-4.png" width="400"></a> </td>
    <td>Manual configuration dialog will appear<p>
    Leave <i>Password</i> field <b>empty</b> and click <i>Re-test</i>
   </td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../miscshots/thunderbird-5.png"><img src="../miscshots/thunderbird-5.png" width="400"></a> </td>
    <td>Thunderbird autoconfiguration mechanism will find Kerberos / GSSAPI for both IMAP and SMTP servers.<p>
    Leave <i>Password</i> field <b>empty</b> and click <i>Create Account</i> to finish configuration.
   </td>
   </tr>

 </tbody>
 </table>
<h4>Setup: Changing existing account authentication methods</h4>
<table>
  <tbody>
   <tr>
    <td> <a target="snapwindow" href="../miscshots/thunderbird-reconf-1.png"><img src="../miscshots/thunderbird-reconf-1.png" width="400"></a> </td>
    <td>After starting thunderbird, select Edit -> Account Settings -> Server Settings<p>
    Change <i>Authentication method</i> to <b>Kerberos / GSSAPI</b><p>
    Click <i>OK</i> to save changes.
   </td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../miscshots/thunderbird-reconf-2.png"><img src="../miscshots/thunderbird-reconf-2.png" width="400"></a> </td>
    <td>Select Edit -> Account Settings -> Outgoing Server (SMTP) -> Edit<p>
    Change <i>Authentication method</i> to <b>Kerberos / GSSAPI</b><p>
    Click <i>OK</i> to save changes.
   </td>
   </tr>

 </tbody>
 </table>

<h3>Alpine</h3>
On SLC5, SLC6 and CC7 systems alpine has been updated to version containing a workaround patch for Kerberos SMTP authentication, please check that alpine version on your system is at least <b>2.02-3</b> by running:
<pre>
rpm -q alpine
</pre>
(if your system is not SLC5/6 you can find the smtp workaround patch <a href="../alpine-2.gssapi_smtp_workaround.patch">here</a>.)
<p>
Before re(starting) alpine please make sure that your <b>~/.pinerc</b> and system-wide <b>/etc/alpine/pine.conf</b> files do not contain a line saying:
<pre>
disable-these-authenticators=GSSAPI,PLAIN
</pre>
if it does: please change that line to read:
<pre>
disable-these-authenticators=PLAIN
</pre>
After starting alpine, you should be no more prompted for the authentication password.

<h3>Fetchmail</h3>
SLC5/SLC6/CC7 fetchmail has the GSSAPI/Kerberos authentication module compiled in:
<pre>
&#35; fetchmail -v -V
This is fetchmail release 6.3.17+GSS+RPA+NTLM+SDPS+SSL+HESIOD+NLS+KRB5.
[...]
</pre>
In order to use Kerberos authentication your <b>~/.fetchmailrc</b> server poll section should be configured as folllows:
<pre>
[...]
poll imap.cern.ch with proto imap auth gssapi:
	user LOGIN with ssl
[...]
</pre>
(substitute LOGIN by your login id)
<h3>Troubleshooting</h3>
After applying settings described above thunderbird/alpine/fetchmail should not ask you for authentication password(s): If a password prompt still appears, please verify that your Kerberos ticket is valid by running:
<pre>
&#35; klist
</pre>
on your system. The output should show a valid ticket with expiry date in the future:
        <pre>
Ticket cache: FILE:/tmp/krb5cc_14213_RZEYN11810
Default principal: jpolok@CERN.CH

Valid starting     Expires            Service principal
08/13/12 14:27:29  08/14/12 14:07:50  krbtgt/CERN.CH@CERN.CH
        renew until 08/18/12 12:44:34
        </pre>
<p>
<em>Note:</em> Above configuration assumes that same account is used for e-mail services and interactive login, if this is not the case local Kerberos ticket acquired for one cannot be used to authenticate other account in CERN Kerberos realm.



