<!--#include virtual="/linux/layout/header" -->
# Pidgin integration with Microsoft Skype for Business (formerly Lync)
<h2>Pidgin integration with Microsoft Skype for Business (formerly Lync) Instant Messaging </h2>
CERN uses Microsoft Lync Server as the base of instant messaging services. While this platform is
well integrated with Microsoft Windows clients and with Apple Mac OS X ones, integration was lacking for
Linux clients.
<p>
This documentation outlines the setup process allowing Linux clients to benefit from integration with Microsoft Lync
instant messaging using Kerberos authentication.
<p>
While the initial installation of required software is specific to CERN SLC5 / SLC6 and CERN CentOS 7
Linux distributions, following configuration steps shall be applicable on any modern Linux platform - running at least <a href="https://www.pidgin.im/">Pidgin 2.7.X</a>,
<a href="http://sipe.sourceforge.net/">Pidgin-SIPE 1.15.0</a> plugin and <a href="/docs/kerberos-access">configured for CERN Kerberos realm</a>.

<h2>Software installation</h2>

As root on your SLC5, SLC6 or CC7 system run:
<pre>
&#35; yum install pidgin pidgin-sipe purple-sipe
</pre>
(it may already be preinstalled on your system: SLC 5.9 and SLC 6.3 versions include pidgin-sipe plugin in default install)

once installation of required software packages finishes, please restart Pidgin.


<h2>Configuration</h2>
<table>
  <tbody>
   <tr>
    <td>&nbsp;</td>
    <td>Start pidgin (<b>Menu Applications -> Internet -> Pidgin Internet Messenger</b>)</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/pidgin1.png"><img src="../lyncmsg/pidgin1.png" width="300"></a> </td>
    <td>Select <b>Add Account</b> ( or if you already have configured pidgin - select <b>Manage accounts</b> from <b>Accounts</b> menu and click <b>Add</b>.)</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/pidgin2.png"><img src="../lyncmsg/pidgin2.png" width="300"></a> </td>
    <td>In <b>Basic</b> tab<br>
        Select <b>Office Communicator</b> as <b>Protocol</b><br>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/pidgin3.png"><img src="../lyncmsg/pidgin3.png" width="300"></a> </td>
    <td>In <b>Basic</b> tab<br>
        Enter your CERN e-mail <b>Firstname.Surname@cern.ch</b> as <b>Username</b><br>
        Leave <b>Login</b> field empty<br>
        Leave <b>Password</b> field empty<br>
        Do not check <b>Remember password</b> checkbox.
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../lyncmsg/pidgin4.png"><img src="../lyncmsg/pidgin4.png" width="300"></a> </td>
    <td>In <b>Advanced</b> tab<br>
        Select <b>Kerberos</b> as <b>Authentication scheme</b><br>
	Check <b>Use Single Sign On</b> checkbox.
        Leave other fields empty<br>
        then click on <b>Add/Save</b> to finalize account setup.</td>
   </tr>
 </tbody>
 </table>
  </tbody>
</table>
<p>
<em>Note:</em> Kerberos authentication for pidgin sipe plugin works only on enterprise internal networks, internet Lync gateway does not provide this functionality - this is the current Microsoft Lync Server limitation.
<p>
<em>Note:</em> While pidgin-sipe plugin supports (to some extent) audio/video calling features of Lync Server (softphone), current pidgin versions on SLC6 and SLC5 do not have support for audio/video compiled in, therefore this feature is not available for now. Audio/Video calling is available on newer pidgin versions but it will not function properly between pidgin and Microsoft Lync clients due to unsupported protocol encryption and incompatible video codecs...


