+++ DRAFT +++ DRAFT +++ DRAFT +++ DRAFT +++ DRAFT +++ DRAFT +++

Proposal for the 

Objectives and scope of the "Linux certification coordination committee"
------------------------------------------------------------------------

* To monitor the Linux certification process and adjust it as
  necessary.

* To implement the CERN Linux certification process. Members of the
  LCCC will
    - jointly take decisions on which vendor release to certify
    - jointly declare start and end of certifications
    - individually ensure that the interests of the CERN Linux user
      communities they are representing are taken into account in the
      certification

* to decide on CERN-wide Linux issues, after identifying and
   consulting with stakeholders outside of this committee.


======================================================================

Proposed (current) Linux certification process:
-----------------------------------------------

Goal:
  Linux Certifications are a tool for managing changes to the
  CERN Linux distribution. They should balance the need for stability
  in the CERN computing environments versus the need to adapt
  (both external and internal) modifications to Linux software and
  CERN-wide configurations.
 

Release policy:
* Mayor (binary incompatible) versions should be certified every 12-18
  month
* Minor (binary compatible) versions should be certified every 6-12
  month [note: was 3-6month, but this met resistance]
* Security updates may be applied at any time.

Implementation:
* Main CERN Linux user communities nominate a candidate to represent
  their interests during a Linux certification. This candidate will
  speak on behalf of his/her user community.

* User communities representatives will define "environments"
  (software+configuration requirements), in order to make their
  dependencies explicit.

* For each certification, representatives will draw their requirements
  of the new version and call for changes necessary for their
  environment.

* LCCC will make sure that conflicting requirements on shared
  facilities are resolved, and that the environments are as uniform as
  possible while fulfilling the needs of their respective users.

* Software packages listed in the environments will have to be tested
  during the certification. Every software package will have somebody
  responsible for reporting test results to LCCC. Software packages
  may list dependencies on other packages and/or configurations.

* During the certification, interim releases will be made to
  accommodate change request from environments and packages. Changes
  between these releases will be documented.

* Representatives will monitor progress on the software packages they
  depend on.

* Representatives will do acceptance tests and report problems

* Once an representative's environment passes the acceptance test,
  certification of that environment is achieved. Once all environments
  are certified, certification is over.

* Cases where certification of a package or environment is not
  possible will be brought to the LCCC's attention as soon as
  possible.

+++ DRAFT +++ DRAFT +++ DRAFT +++ DRAFT +++ DRAFT +++ DRAFT +++


======================================================================

Hot topics / agenda items for the next meeting(s):
--------------------------------------------------

 * "Default experiment software compiler":
   should CERNLIB etc be compiled with gcc-3.2.1, gcc-2.95.2,
   gcc-2.96? (consult with experiment librarians, compiler experts, IT-API)

 * supported Mail clients / Web Browsers under Linux (overlap with
   DTF)

======================================================================
Please see also some previous slides on (Linux) certifications at CERN:

- CLUG 2002 "proposed changes to the certification process"
  http://home.cern.ch/iven/www/presentations/CLUG-2002/Process/

- 7.2.1 post-mortem (highlighting problems)
  http://home.cern.ch/iven/presentations/post-mortem-7.2.1-24.05.02/

- 7.2.1 certification process as presented to UNESCO workshop
  http://home.cern.ch/iven/presentations/unesco-20.04.02/unesco.html

Some other references are available at
http://cern.ch/linux/documentation/certification

and  (of course) the "current status" page for 7.3.1:
  http://cern.ch/linux/redhat73/certification/



 