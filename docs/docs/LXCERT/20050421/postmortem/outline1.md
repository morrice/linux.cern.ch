<html>
<head>
<meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
<title>Timeline 1</title>
</head>
<body text=#000000 bgcolor=#FFFFFF link=#0000CC vlink=#000080 alink=#0000CC>
<p align=left><a href="JavaScript:parent.NavigateAbs(0)"><u>Post-7.3 Timeline</u></a><br><ul><li>July 2003: decision: certify Red Hat 10 beta, goal: November
<li>Sept 2003: RH strategy change (Fedora/Enterprise)
<li>Oct 2003:
<ul><li>LXCERT: continue with 10beta=Fedora
<li>LXCERT: on hold
<li>HEPiX: negotiate wih RH
</ul><li>Feb 2004: negotiations stopped, CEL3
<li>Mar 2004: LXCERT meeting, “real” certification starts
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(1)"><u>Timeline cont.</u></a><br><ul><li>May 2004: LXCERT meeting (still no end date, “experiments need 1 month after full Application Area SW is available”, summer period worries)
<li>June 2004: CEL3 -&gt; SLC3, no real impact on certification
<li>July 2004: LXCERT meeting (still no fixed end date, “end of September” target)
<li>July 2004: LXPLUS test nodes available
<li>Aug-&gt;October: POOL issue
<ul><li>Summer period: (some) production managers not available
</ul><li>Nov 1st 2004: SLC3 certified
<li>Jan 17th 2005: LXPLUS migrated
</ul></p>
<p align=left><a href="JavaScript:parent.NavigateAbs(2)"><u>Issues:</u></a><br><ul><li>[non-issue]: Looong delay (negotiations) before real start (6months) – <u>but:</u> allowed OS to settle
<li>Summer period: absences, conflict with DataChallenges, beam time. <font color=#FF0000>Avoid? </font>(unrealistic for roll-out in autumn)
<li>POOL issue took too long to emerge+address, was blocking whole production chain: <font color=#FF0000>need to 'certify around' identified issues</font>
<li>[non-issue]: LXPLUS migration: delayed by HW unavailability
<li>[non-issue]: non-LHC not reacting (<u>their</u> problem, but influences 7.3 phase-out timeline)
</ul></p>
</body>
</html>