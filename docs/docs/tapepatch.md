<!--#include virtual="/linux/layout/header" -->
# CASTOR tape patch
<h2>CERN SCSI tape kernel patches for CASTOR</h2>

<p>CERN maintains a set of patches for the Linux kernel needed
for CASTOR operations.  Following is a list of these patches with
explanation, in order of decreasing importance.  All recent CERN
kernels carry all these patches.</p>

<h2>SCSI tape patches for SLC3</h2>

<dl>

<dt><a href="../patches/linux-2.4.20-cern-tape_residue_report.patch">linux-2.4.20-cern-tape_residue_report.patch</a></dt>
<dd><p>

This patch fixes tape residue reporting by sacrificing partition support
(which only DATs use anyway).  This only works with synchronous writes and
no read-ahead (the patch takes care of this as well).

</p></dd>

<dt><a href="../patches/linux-2.4.20-cern-tape_ansi_eof.patch">linux-2.4.20-cern-tape_ansi_eof.patch</a></dt>
<dd><p>

This patch fixes tape ANSI EOF reporting.

</p></dd>

<dt><a href="../patches/linux-2.4.20-cern-tape_timeout.patch">linux-2.4.20-cern-tape_timeout.patch</a></dt>
<dd><p>

This patch reduces LONG_TIMEOUT from <em>14000*HZ</em> to <em>3600*HZ</em>.

</p></dd>

<dt><a href="../patches/linux-2.4.21-cern-raw_sense_dump.patch">linux-2.4.21-cern-raw_sense_dump.patch</a></dt>
<dd><p>

This patch enables verbose raw sense bytes dumping.

</p></dd>

<dt><a
href="../patches/linux-2.4.20-cern-symbios_odd_byte_fix.patch">linux-2.4.20-cern-symbios_odd_byte_fix.patch</a></dt>
<dd><p>

This one fixes an unhandled error condition that comes from the
Symbios driver when writing an odd number of bytes.  Experimental,
ugly, but Works For Us(tm).

</p></dd>

<dt><a href="../patches/linux-2.4.20-cern-ibm3590_load_display_cmd.patch">linux-2.4.20-cern-ibm3590_load_display_cmd.patch</a></dt>
<dd><p>

This patch adds support for the Load Display command on IBM3590 drives.

</p></dd>

<dt><a href="../patches/linux-2.4.20-cern-tape_buffer_kbs.patch">linux-2.4.20-cern-tape_buffer_kbs.patch</a></dt>
<dd><p>

This patch is not strictly necessary, it is included for convenience
reasons.  It changes the default tape buffer size from 32K to 256K, that
can be achieved by setting <strong>buffer_kbs=256</strong> module option
when loading the <em>st.o</em> module.

</p></dd>

</dl>

<h2>SCSI tape patches for SLC4 (2.6.9-34.0.1.EL.cern) <strong>*BETA* 2006-06-27</strong></h2>

<p>
<a href="../patches-SLC4/linux-2.6.9-cern-001_tape_residue_report.patch">linux-2.6.9-cern-001_tape_residue_report.patch</a><br>
<a href="../patches-SLC4/linux-2.6.9-cern-002_tape_raw_sense_dump.patch">linux-2.6.9-cern-002_tape_raw_sense_dump.patch</a><br>
<a href="../patches-SLC4/linux-2.6.9-cern-003_tape_timeout.patch">linux-2.6.9-cern-003_tape_timeout.patch</a><br>
<a href="../patches-SLC4/linux-2.6.9-cern-004_tape_symbios_odd_byte_fix.patch">linux-2.6.9-cern-004_tape_symbios_odd_byte_fix.patch</a><br>
<a href="../patches-SLC4/linux-2.6.9-cern-005_tape_ibm3590_load_display_cmd.patch">linux-2.6.9-cern-005_tape_ibm3590_load_display_cmd.patch</a><br>
</p>


