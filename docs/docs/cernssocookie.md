<!--#include virtual="/linux/layout/header" -->
# CERN Single Sign On authentication using scripts/programs

!!! danger ""
    Please note that `cern-get-sso-cookie` has been deprecated in favor of [`auth-get-sso-cookie`](https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie).

<h2>CERN Single Sign on authentication using scripts/programs</h2>
CERN uses <a href="http://login.cern.ch">Microsoft Active Directory Federation Services (ADFS)</a> for Web Single Sign On (SSO) services.
While this method of authentication integrates well with most modern web browsers (Internet Explorer, Firefox, Chrome .. etc), it is not
easy to integrate with command line clients alike wget or curl, or any non-interactive web client.
<p>
This documentation outlines the setup process allowing curl, wget and other non-interactive web clients to access CERN SSO protected web resources.
<p>
While the initial installation of required software is specific to CERN SLC5 and SLC6
Linux distributions, this software can be easily ported to other linux distributions.
<p>
<em>Note:</em>This software is <em>SPECIFIC</em> to CERN SSO installation, it will not work - without modifications - with any other MS ADFS installations. (modifications necessary include changing the library AND changing the setup of ADFS installation).

<h2>Installation</h2>
As root on your  SLC5 or SLC6 system run:
<pre>
&#35; yum install cern-get-sso-cookie
</pre>
following packages will be installed on your system:
<ul>
 <li> CERN SSO authentication cookie grabber (<i>cern-get-sso-cookie</i>)
 <li> CERN SSO authentication library (<i>perl-WWW-CERNSSO-Auth</i>)
 <li> Curl perl bindings (<i>perl-WWW-Curl</i>)
 <li> Kerberos perl bindings (<i>perl-Authen-Krb5</i>)
 <li> CERN CA certificates (<i>CERN-CA-certs</i>/<i>openssl-CERN-CA-certs</i>)
</ul>
<h2>Usage</h2>
cern-get-sso-cookie acquires CERN Single Sign-On cookie using Kerberos credentials or user certificate and stores it in a file
for later usage with tools like curl, wget or others - cookie file is stored in Netscape format understood by most web clients.
<p>
<h3>Kerberos credentials</h3>
In order to use this tool with Kerberos credentials a valid CERN Kerberos ticket must be acquired, for example using <b>kinit</b>.
To check the valididty of Kerberos credentials please use <b>klist</b>.
<p>
Example usage:
<pre>
cern-get-sso-cookie --krb -r -u https://somesite.web.cern.ch/protected -o ~/private/ssocookie.txt

wget --load-cookies ~/private/ssocookie.txt https://somesite.web.cern.ch/protected/documents

curl -L --cookie ~/private/ssocookie.txt --cookie-jar ~/private/ssocookie.txt \
       https://somesite.web.cern.ch/protected/documents
</pre>

<h3>User certificates</h3>
In order to be used with this tool user certificate / key files must be converted to specific formats. In order to do so you may:
Acquire your user certificate at <a href="http://cern.ch/ca">CERN Certification Authority</a>, next
export it from your web browser as myCert.p12 file (Firefox: Edit->Preferences->Advanced->Encryption->View Certificates->Your Certificates->Backup)
<p>
then use following sequence of commands in order to convert it:
<pre>
openssl pkcs12 -clcerts -nokeys -in myCert.p12 -out ~/private/myCert.pem
openssl pkcs12 -nocerts -in myCert.p12 -out ~/private/myCert.tmp.key
openssl rsa -in ~/private/myCert.tmp.key -out ~/private/myCert.key
rm ~/private/myCert.tmp.key
chmod 644 ~/private/myCert.pem
chmod 400 ~/private/myCert.key
</pre>
<em>WARNING</em>: <b>openssl rsa..</b> command removes the passphrase from the private key, please make sure your key file is stored in secure location !
<p>
Example usage:
<pre>
cern-get-sso-cookie --cert ~/private/myCert.pem --key ~/private/myCert.key -r \
                      -u https://somesite.web.cern.ch/protected -o ~/private/ssocookie.txt

curl -L --cookie ~/private/ssocookie.txt --cookie-jar ~/private/ssocookie.txt \
       https://somesite.web.cern.ch/protected/documents
</pre>
<h3>Usage notes</h3>
<p>
Cookie file is created in a format understood by curl/wget or libcurl-based utilities: If you intend to use cookie file with different cookie handling library please use the <i>--reprocess / -r</i> option. See also <i>man cern-get-sso-cookie</i>.
<p>
CERN SSO cookies are created per web site: In order to access protected content on a given site a SSO cookie for that site must be acquired.
<p>
CERN SSO cookies expire within 24 hours.
<p>
<em>WARNING</em>: Always store cookiefile in a private directory: it can be used by anybody to authenticate to CERN SSO as your account !
<p>
Kerberos credentials validity is not checked.
<p>
Certificate format is not checked.
<p>
Certificate key file can be only RSA encrypted (current libcurl limitation)
<p>
Certificate key file can not be password protected.
<p>
If URL given to the script is not SSO protected an empty cookie file is created and no error is reported.



