<!--#include virtual="/linux/layout/header" -->
# Redhat Common @ CERN
<p>
Redhat Common for Scientific Linux 6
<p>
Redhat Common packages are provided here for <em>TEST PURPOSE ONLY</em>.
</p>
</p>
<p>
Please note that <em>ONLY</em> support we provide for Redhat Common packages is related
to installation / packaging problems. This product is provided AS-IS - without support.
</p>
<p>
For everything else, <b>please read the documentation</b>.
</p>

<h2>Documentation</h2>

<h2>Installation</h2>

<ul>
 <li>Scientific Linux 6 (SLC6)<br>
     Save repository information as <a href="http://linuxsoft.cern.ch/cern/rhcommon/slc6-rhcommon.repo">/etc/yum.repos.d/slc6-rhcommon.repo</a> on your system.
</ul>

<div align="right"><a href="mailto:Thomas.Oulevey@cern.ch">Thomas.Oulevey@cern.ch</a></div>

