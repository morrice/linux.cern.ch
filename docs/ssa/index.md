<!--#include virtual="/linux/layout/header" -->
# SSA (Storage Software Appliance)  @ CERN
<p>
Storage Software Appliance packages are provided here for <em>TEST PURPOSE ONLY</em>.
These packages can be installed on top of an existing SLC6 i386/x86_64 installation.
</p>
(information about <a href="http://www.redhat.com/storage/ssa/">SSA (Storage Software Appliance)</a> on Red Hat site)
</p>
<p>
Please note that <em>ONLY</em> support we provide for SSA packages is related
to installation / packaging problems. This product is provided AS-IS - without support.
</p>
<p>
For everything else, <b>please read the documentation</b>.
</p>
<h2>Documentation</h2>
<h4>SSA 3.2 documentation - local copy</h4>
<ul>
 <li><a href="3.2/3.2_Release_Notes">3.2 Release Notes</a>
 <li><a href="3.2/User_Guide/">User Guide</a>
</ul>

<h4>Full SSA documentation on Red Hat site</h4>
The SSA documentation can be found <a href="http://docs.redhat.com/docs/en-US/Red_Hat_Storage_Software_Appliance/index.html">here</a>.
<h2>Installation</h2>

Edit the <i>/etc/yum.repos.d/slc6-ssa.repo</i> file and insert following information in:

<pre>
[slc6-ssa]
name=SSA - Storage Software Appliance repository
baseurl=http://linuxsoft.cern.ch/cern/ssa/3.2/slc6X/$basearch/yum/ssa/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1
</pre>

<p>
To install the SSA run:
</p>
<pre>
yum install glusterfs-fuse glusterfs-core ( glusterfs-rdma ) ( glusterfs-geo-replication ) iperf appliance-base
</pre>

Also available: Amazon Machine Image (AMI) - Virtual Storage Appliance (VSA) for Amazon Web Service - see documentation <a href="http://www.redhat.com/storage/vsa/">here</a>.

To install VSA/AMI run:

<pre>
yum install glusterfs-fuse glusterfs-core ( glusterfs-rdma ) ( glusterfs-geo-replication ) iperf appliance-base appliance-ami
</pre>


<div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>

