# CentOS 7 - AltArch - aarch64

We provide a local mirror and network installation service for CentOS 7 Alternative Architecture: aarch64 (ARM64) platform.

CentOS information about AltArch can be found [here](https://wiki.centos.org/SpecialInterestGroup/AltArch)

## Installation

Network UEFI/PXE boot is provided and works same way than for standard supported architectures: network boot your system and you should see an interactive menu presenting installation options for current CentOS 7 releases for aarch64.

For unattended install following AIMS targets can be used:

* C76_1810_AARCH64 , install path: http://linuxsoft.cern.ch/centos-altarch/7.6.1810/os/aarch64/
* C75_1804_AARCH64 , install path: http://linuxsoft.cern.ch/centos-altarch/7.5.1804/os/aarch64/

Please note that aarch64 network installation has been tested on very limited number of hardware platforms as of 2018.

## Software / Updates

Default installation will include yum repository configuration defining standard CentOS mirrors for updates.

If you wish to pull updates / software from CERN mirror, please edit repository definition files in /etc/yum.repos.d/ changing URLs to point to local mirror at:

http://linuxsoft.cern.ch/centos-altarch/7/NAME/aarch64/

## CERN customization / tools

No CERN customization or tools are provided for CentOS 7 / aarch64.

## Support

No support is provided for CentOS 7 / aarch64