# Software Collections for CERN CentOS 7

<ul>
<li><a href="#scl32">Software Collections 3.2</a>
<li><a href="#devtoolset-8">Developer Toolset 8</a>
<li><a href="#devtoolset-7">Developer Toolset 7</a>
<li><a href="#scl3">Software Collections 3</a>
<li><a href="#devtoolset-6.1">Developer Toolset 6.1</a>
<li><a href="#scl24">Software Collections 2.4</a>
<li><a href="#devtoolset-6">Developer Toolset 6.0</a>
<li><a href="#scl23">Software Collections 2.3</a>
<li><a href="#scl22">Software Collections 2.2</a>
<li><a href="#devtoolset-4">Developer Toolset 4.0</a>
<li><a href="#scl20">Software Collections 2.0</a>
<li><a href="#scl12">Software Collections 1.2</a>
<li><a href="#centosscl">CentOS Community Software Collections</a>
<li><a href="#docs">Documentation</a>
<li><a href="#inst">Installation</a>
</ul>
<hr>
Software Collections provides a set of dynamic programming languages, database servers, and various related packages that are either more recent than their equivalent versions included in the base system or are available for this system for the first time.

<hr><a name="scl32"></a>
<h2>Software Collections 3.2</h2>
Software Collections <b>3.2</b> provides following tools:
<ul>
<li><b>rh-php72</b> - A release of PHP with PEAR 1.10.5 - version <b>7.2.10</b></li>
<li><b>rh-mysql80</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>8.0.13</b> (update)</li>
<li><b>rh-nodejs10</b> - A release of Node.js including V8 engine version 6.6, full N-API support, and stability improvements. - version <b>10.10.0</b></li>
<li><b>rh-nginx114</b> -  A web and proxy server with a focus on high concurrency - version <b>1.14.0</b></li>
<li><b>rh-varnish6</b> - A release of Varnish Cache, a high-performance HTTP reverse proxy - version <b>6.0.0</b></li>
<li><b>rh-git218</b> - A release of Git, a distributed revision control system - version <b>2.18.1</b></li>
</ul>

<hr><a name="devtoolset-8"></a>
<h2>Developer Toolset 8</h2>
Developer Toolset <b>8</b> provides following tools:
<ul>
<li><b>devtoolset-8</b> :
<ul>
<li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>8.2.1</b>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.30</b>
<li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.174</b>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.12</b>
<li><b>gdb</b> - GNU Debugger - version <b>8.2</b>
<li><b>ltrace</b> - A debugging tool to monitor system calls - version <b>0.7.91</b>
<li><b>strace</b> - System calls and signals tracer - version <b>4.24</b>
<li><b>memstomp</b> - A debugging tool to identify calls to library functions - version <b>0.1.5</b>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>3.3</b>
<li><b>valgrind</b> - An instrumentation framework and a number of tools to profile applications - version <b>3.14.0</b>
<li><b>oprofile</b> - System wide profiler - version <b>1.3.0</b>
<li><b>dyninst</b> - A library for instrumenting and working with user-space executables - version <b>9.3.2</b>
<li><b>make</b> - A dependency-tracking build automation tool - version <b>4.2.1</b>
</ul>
</ul>

<hr><a name="devtoolset-7"></a>
<h2>Developer Toolset 7</h2>
Developer Toolset <b>7</b> provides following tools:
<ul>
<li><b>devtoolset-7</b> :
<ul>
<li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>7.2.1</b>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.28</b>
<li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.170</b>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.12</b>
<li><b>gdb</b> - GNU Debugger - version <b>8.0.1</b>
<li><b>ltrace</b> - A debugging tool to monitor system calls - version <b>0.7.91</b>
<li><b>strace</b> - System calls and signals tracer - version <b>4.17</b>
<li><b>memstomp</b> - A debugging tool to identify calls to library functions - version <b>0.1.5</b>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>3.1</b>
<li><b>valgrind</b> - An instrumentation framework and a number of tools to profile applications - version <b>3.13.0</b>
<li><b>oprofile</b> - System wide profiler - version <b>1.2.0</b>
<li><b>dyninst</b> - A library for instrumenting and working with user-space executables - version <b>9.3.2</b>
<li><b>make</b> - A dependency-tracking build automation tool - version <b>4.2.1</b>
</ul>
</ul>

<hr><a name="devtoolset-6.1"></a>
<h2>Developer Toolset 6.1</h2>
Developer Toolset <b>6.1</b> provides following tools:
<ul>
<li><b>devtoolset-6</b> :
<ul>
<li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>6.3.1</b>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.27</b>
<li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.168</b>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.12</b>
<li><b>gdb</b> - GNU Debugger - version <b>7.12.1</b>
<li><b>ltrace</b> - A debugging tool to monitor system calls - version <b>0.7.91</b>
<li><b>strace</b> - System calls and signals tracer - version <b>4.12</b>
<li><b>memstomp</b> - A debugging tool to identify calls to library functions - version <b>0.1.5</b>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>3.0</b>
<li><b>valgrind</b> - An instrumentation framework and a number of tools to profile applications - version <b>3.12.0</b>
<li><b>oprofile</b> - System wide profiler - version <b>1.1.0</b>
<li><b>dyninst</b> - A library for instrumenting and working with user-space executables - version <b>9.2.0</b>
<li><b>make</b> - A dependency-tracking build automation tool - version <b>4.1</b>
</ul>
</ul>

<hr><a name="devtoolset-6"></a>
<h2>Developer Toolset 6.0</h2>
Developer Toolset <b>6.0</b> provides following tools:
<ul>
<li><b>devtoolset-6</b> :
<ul>
<li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>6.2.1</b>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.27</b>
<li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.167</b>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.12</b>
<li><b>gdb</b> - GNU Debugger - version <b>7.12</b>
<li><b>ltrace</b> - A debugging tool to monitor system calls - version <b>0.7.91</b>
<li><b>strace</b> - System calls and signals tracer - version <b>4.12</b>
<li><b>memstomp</b> - A debugging tool to identify calls to library functions - version <b>0.1.5</b>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>3.0</b>
<li><b>valgrind</b> - An instrumentation framework and a number of tools to profile applications - version <b>3.12.0</b>
<li><b>oprofile</b> - System wide profiler - version <b>1.1.0</b>
<li><b>dyninst</b> - A library for instrumenting and working with user-space executables - version <b>9.2.0</b>
</ul>
</ul>

<hr><a name="scl3"></a>
<h2>Software Collections 3</h2>
Software Collections <b>3</b> provides following tools:
<ul>
<li><b>rh-eclipse46</b> - A release of Eclipse integrated development environment - version <b>4.6.3</b></li>
<li><b>rh-perl520</b> - A release of Perl, a high-level programming language - version <b>5.20.1</b></li>
<li><b>rh-perl524</b> - A release of Perl, a high-level programming language - version <b>5.24.0</b></li>
<li><b>rh-php56</b> - A release of PHP with PEAR 1.9.5 - version <b>5.6.25</b></li>
<li><b>rh-php70</b> - A release of PHP with PEAR 1.10 - version <b>7.0.10</b></li>
<li><b>rh-php71</b> - A release of PHP with PEAR 1.10 - version <b>7.1.8</b></li>
<li><b>python27</b> - A release of Python 2.7 with a number of additional utilities - version <b>2.7.13</b></li>
<li><b>rh-python34</b> - A release of Python 3 with a number of additional utilities - version <b>3.4.2</b></li>
<li><b>rh-python35</b> - A release of Python 3 with a number of additional utilities - version <b>3.5.1</b></li>
<li><b>rh-python36</b> - A release of Python 3 with a number of additional utilities - version <b>3.6.3</b></li>
<li><b>rh-ruby22</b> - A release of Ruby 2.2 - version <b>2.2.2</b></li>
<li><b>rh-ruby23</b> - A release of Ruby 2.3 - version <b>2.3.1</b></li>
<li><b>rh-ruby24</b> - A release of Ruby 2.4 - version <b>2.4.0</b></li>
<li><b>rh-ror41</b> - Ruby on Rails -  version <b>4.1.5</b></li>
<li><b>rh-ror42</b> - Ruby on Rails -  version <b>4.2.6</b></li>
<li><b>rh-ror50</b> - Ruby on Rails -  version <b>5.0.1</b></li>
<li><b>rh-scala210</b> - A release of Scala, a general purpose programming language for the Java platform - version <b>2.10.6</b></li>
<li><b>rh-mariadb100</b> - A release of MariaDB, an alternative to MySQL - version <b>10.0.28</b> (update)</li>
<li><b>rh-mariadb101</b> - A release of MariaDB, an alternative to MySQL - version <b>10.1.19</b> (update)</li>
<li><b>rh-mariadb102</b> - A release of MariaDB, an alternative to MySQL - version <b>10.2.8</b></li>
<li><b>rh-mongodb26</b> - High-performance, schema-free document-oriented database - version <b>2.6.9</b></li>
<li><b>rh-mongodb32</b> - High-performance, schema-free document-oriented database - version <b>3.2.10</b></li>
<li><b>rh-mongodb30upg</b> - High-performance, schema-free document-oriented database - a limited version <b>3.0.11</b> providing upgrade path from version <b>2.6</b> to <b>3.2</b>. (update)</li>
<li><b>rh-mysql56</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.6.37</b> (update)</li>
<li><b>rh-mysql57</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.7.19</b> (update)</li>
<li><b>rh-postgresql94</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.4.14</b> (update)</li>
<li><b>rh-postgresql95</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.5.9</b> (update)</li>
<li><b>rh-postgresql96</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.6.5</b></li>
<li><b>rh-nodejs4</b> - A release of Node.js with npm 2.15.1 and support for the SPDY protocol version 3.1. - version <b>4.6.2</b></li>
<li><b>rh-nodejs6</b> - A release of Node.js with npm 3.10.9 - version <b>6.11.3</b> (update)</li>
<li><b>rh-nodejs8</b> - A release of Node.js with npm 5.3.0 - version <b>8.6.0</b></li>
<li><b>rh-nginx18</b> -  A web and proxy server with a focus on high concurrency - version <b>1.8.1</b></li>
<li><b>rh-nginx110</b> -  A web and proxy server with a focus on high concurrency - version <b>1.10.2</b></li>
<li><b>rh-nginx112</b> -  A web and proxy server with a focus on high concurrency - version <b>1.12.1</b></li>
<li><b>httpd24</b> - Apache HTTP Server - version <b>2.4.27</b> (update)</li>
<li><b>rh-varnish4</b> - A release of Varnish Cache, a high-performance HTTP reverse proxy - version <b>4.0.3</b></li>
<li><b>rh-maven33</b> - Java project management and project comprehension tool - version <b>3.3.9</b></li>
<li><b>rh-maven35</b> - Java project management and project comprehension tool - version <b>3.5.0</b></li>
<li><b>rh-passenger40</b> - Phusion Passenger a fast, robust lightweight web and application server - version <b>4.0.50</b></li>
<li><b>rh-git29</b> - A release of Git, a distributed revision control system - version <b>2.9.3</b></li>
<li><b>rh-redis32</b> - A release of Redis 3.2, a persistent key-value database - version <b>3.2.4</b></li>
<li><b>rh-java-common</b> - Common Java libraries and tools used by other collections - version <b>1.1</b></li>
<li><b>v8314</b> - Google JavaScript Engine - version <b>3.14.5.10</b>
</ul>

<hr><a name="scl24"></a>
<h2>Software Collections 2.4</h2>
Software Collections <b>2.4</b> provides following tools:
<ul>
<li><b>rh-eclipse46</b> - A release of Eclipse integrated development environment - version <b>4.6.3</b> (update)</li>
<li><b>rh-perl520</b> - A release of Perl, a high-level programming language - version <b>5.20.1</b> (update) </li>
<li><b>rh-perl524</b> - A release of Perl, a high-level programming language - version <b>5.24.0</b></li>
<li><b>rh-php56</b> - A release of PHP with PEAR 1.9.5 - version <b>5.6.25</b> (update)</li>
<li><b>rh-php70</b> - A release of PHP with PEAR 1.10 - version <b>7.0.10</b></li>
<li><b>python27</b> - A release of Python 2.7 with a number of additional utilities - version <b>2.7.13</b> (update)</li>
<li><b>rh-python34</b> - A release of Python 3 with a number of additional utilities - version <b>3.4.2</b> (update)</li>
<li><b>rh-python35</b> - A release of Python 3 with a number of additional utilities - version <b>3.5.1</b> (update)</li>
<li><b>rh-ruby22</b> - A release of Ruby 2.2 - version <b>2.2.2</b> (update)</li>
<li><b>rh-ruby23</b> - A release of Ruby 2.3 - version <b>2.3.1</b> (update)</li>
<li><b>rh-ruby24</b> - A release of Ruby 2.4 - version <b>2.4.0</b></li>
<li><b>rh-ror41</b> - Ruby on Rails -  version <b>4.1.5</b> (update)</li>
<li><b>rh-ror42</b> - Ruby on Rails -  version <b>4.2.6</b> (update)</li>
<li><b>rh-ror50</b> - Ruby on Rails -  version <b>5.0.1</b></li>
<li><b>rh-scala210</b> - A release of Scala, a general purpose programming language for the Java platform - version <b>2.10.6</b></li>
<li><b>rh-mariadb100</b> - A release of MariaDB, an alternative to MySQL - version <b>10.0.26</b> (update)</li>
<li><b>rh-mariadb101</b> - A release of MariaDB, an alternative to MySQL - version <b>10.1.16</b> (update)</li>
<li><b>rh-mongodb26</b> - High-performance, schema-free document-oriented database - version <b>2.6.9</b></li>
<li><b>rh-mongodb32</b> - High-performance, schema-free document-oriented database - version <b>3.2.10</b> (update)</li>
<li><b>rh-mongodb30upg</b> - High-performance, schema-free document-oriented database - a limited version <b>3.0.11</b> providing upgrade path from version <b>2.6</b> to <b>3.2</b>. (update)</li>
<li><b>rh-mysql56</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.6.34</b> (update)</li>
<li><b>rh-mysql57</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.7.16</b></li>
<li><b>rh-postgresql94</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.4.9</b> (update)</li>
<li><b>rh-postgresql95</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.5.4</b> (update)</li>
<li><b>rh-nodejs4</b> - A release of Node.js with npm 2.15.1 and support for the SPDY protocol version 3.1. - version <b>4.6.2</b> (update)</li>
<li><b>rh-nodejs6</b> - A release of Node.js with npm 3.10.9 - version <b>6.9.1</b></li>
<li><b>rh-nginx18</b> -  A web and proxy server with a focus on high concurrency - version <b>1.8.1</b></li>
<li><b>rh-nginx110</b> -  A web and proxy server with a focus on high concurrency - version <b>1.10.2</b></li>
<li><b>httpd24</b> - Apache HTTP Server - version <b>2.4.25</b> (update)</li>
<li><b>rh-varnish4</b> - A release of Varnish Cache, a high-performance HTTP reverse proxy - version <b>4.0.3</b></li>
<li><b>rh-thermostat16</b> - A monitoring and serviceability tool for OpenJDK - version <b>1.6.4</b> </li>
<li><b>rh-maven33</b> - Java project management and project comprehension tool - version <b>3.3.9</b> (update)</li>
<li><b>rh-passenger40</b> - Phusion Passenger a fast, robust lightweight web and application server - version <b>4.0.50</b> (update)</li>
<li><b>rh-git29</b> - A release of Git, a distributed revision control system - version <b>2.9.3</b></li>
<li><b>rh-redis32</b> - A release of Redis 3.2, a persistent key-value database - version <b>3.2.4</b></li>
<li><b>rh-java-common</b> - Common Java libraries and tools used by other collections - version <b>1.1</b> (updated) </li>
<li><b>v8314</b> - Google JavaScript Engine - version <b>3.14.5.10</b>
</ul>


<hr><a name="scl23"></a>
<h2>Software Collections 2.3</h2>
Software Collections <b>2.3</b> provides following tools:
<ul>
<li><b>rh-eclipse46</b> - A release of Eclipse integrated development environment - version <b>4.6.1</b> (<em>not available yet</em>)</li>
<li><b>rh-perl520</b> - A release of Perl, a high-level programming language - version <b>5.20.1</b> (update) </li>
<li><b>rh-perl524</b> - A release of Perl, a high-level programming language - version <b>5.24.0</b></li>
<li><b>rh-php56</b> - A release of PHP with PEAR 1.9.5 - version <b>5.6.25</b> (update)</li>
<li><b>rh-php70</b> - A release of PHP with PEAR 1.10 - version <b>7.0.10</b></li>
<li><b>python27</b> - A release of Python 2.7 with a number of additional utilities - version <b>2.7.8</b> (update)</li>
<li><b>rh-python34</b> - A release of Python 3 with a number of additional utilities - version <b>3.4.2</b> (update)</li>
<li><b>rh-python35</b> - A release of Python 3 with a number of additional utilities - version <b>3.5.1</b> (update)</li>
<li><b>rh-ruby22</b> - A release of Ruby 2.2 - version <b>2.2.2</b> (update)</li>
<li><b>rh-ruby23</b> - A release of Ruby 2.3 - version <b>2.3.1</b> (update)</li>
<li><b>rh-ror41</b> - Ruby on Rails -  version <b>4.1.5</b> (update)</li>
<li><b>rh-ror42</b> - Ruby on Rails -  version <b>4.2.6</b> (update)</li>
<li><b>rh-mariadb100</b> - A release of MariaDB, an alternative to MySQL - version <b>10.0.26</b> (update)</li>
<li><b>rh-mariadb101</b> - A release of MariaDB, an alternative to MySQL - version <b>10.1.16</b> (update)</li>
<li><b>rh-mongodb26</b> - High-performance, schema-free document-oriented database - version <b>2.6.9</b></li>
<li><b>rh-mongodb32</b> - High-performance, schema-free document-oriented database - version <b>3.2.10</b> (update)</li>
<li><b>rh-mongodb30upg</b> - High-performance, schema-free document-oriented database - a limited version <b>3.0.11</b> providing upgrade path from version <b>2.6</b> to <b>3.2</b>. (update)</li>
<li><b>rh-mysql56</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.6.34</b> (update)</li>
<li><b>rh-mysql57</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.7.16</b></li>
<li><b>rh-postgresql94</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.4.9</b> (update)</li>
<li><b>rh-postgresql95</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.5.4</b> (update)</li>
<li><b>rh-nodejs4</b> - A release of Node.js with npm 2.15.1 and support for the SPDY protocol version 3.1. - version <b>4.4.2</b> (update)</li>
<li><b>rh-nginx18</b> -  A web and proxy server with a focus on high concurrency - version <b>1.8.1</b></li>
<li><b>httpd24</b> - Apache HTTP Server - version <b>2.4.18</b> (update)</li>
<li><b>rh-varnish4</b> - A release of Varnish Cache, a high-performance HTTP reverse proxy - version <b>4.0.3</b></li>
<li><b>rh-thermostat16</b> - A monitoring and serviceability tool for OpenJDK - version <b>1.6.4</b> (<em>not available yet</em>)</li>
<li><b>rh-maven33</b> - Java project management and project comprehension tool - version <b>3.3.9</b> (update)</li>
<li><b>rh-passenger40</b> - Phusion Passenger a fast, robust lightweight web and application server - version <b>4.0.50</b> (update)</li>
<li><b>rh-git29</b> - A release of Git, a distributed revision control system - version <b>2.9.3</b></li>
<li><b>rh-redis32</b> - A release of Redis 3.2, a persistent key-value database - version <b>3.2.4</b></li>
<li><b>rh-java-common</b> - Common Java libraries and tools used by other collections - version <b>1.1</b> (updated) </li>
<li><b>v8314</b> - Google JavaScript Engine - version <b>3.14.5.10</b>
</ul>

<hr><a name="scl22"></a>
<h2>Software Collections 2.2</h2>
Software Collections <b>2.2</b> provides following tools:
<ul>
<li><b>httpd24</b> - Apache HTTP Server - version <b>2.4.18</b></li>
<li><b>python27</b> - An interpreted, interactive, object-oriented programming language - version <b>2.7.8</b> (updated) </li>
<li><b>rh-java-common</b> - Common Java libraries and tools used by other collections - version <b>1.1</b> (updated) </li>
<li><b>rh-mariadb101</b> - A release of MariaDB, an alternative to MySQL - version <b>10.1.14</b></li>
<li><b>rh-maven33</b> - Java project management and project comprehension tool - version <b>3.3.9</b></li>
<li><b>rh-mongodb30upg</b> - High-performance, schema-free document-oriented database - a limited version <b>3.0.11</b> providing upgrade path from version <b>2.6</b> to <b>3.2</b>.</li>
<li><b>rh-mongodb32</b> - High-performance, schema-free document-oriented database - version <b>3.2.6</b></li>
<li><b>rh-nodejs4</b> - A release of Node.js with npm 2.15.1 and support for the SPDY protocol version 3.1. - version <b>4.4.2</b></li>
<li><b>rh-postgresql95</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.5.2</b></li>
<li><b>rh-python35</b> - An interpreted, interactive, object-oriented programming language - version <b>3.5.1</b></li>
<li><b>rh-ror42</b> - Ruby on Rails -  version <b>4.2.6</b></li>
<li><b>rh-ruby23</b> - An interpreter of object-oriented scripting language  - version <b>2.3.0</b></li>
<li><b>thermostat1</b> - A monitoring and serviceability tool for OpenJDK - version <b>1.4.4</b>
</ul>

<hr><a name="devtoolset-4"></a>
<h2>Developer Toolset 4.0</h2>
Developer Toolset <b>4.0</b> provides following tools:
<ul>
<li><b>devtoolset-4</b> :
<ul>
<li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>5.2.1</b>
<li><b>gdb</b> - GNU Debugger - version <b>7.10</b>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.25</b>
<li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.163</b>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.12</b>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>2.8</b>
<li><b>oprofile</b> - System wide profiler - version <b>1.1.0</b>
<li><b>strace</b> - System calls and signals tracer - version <b>4.10</b>
<li><b>eclipse</b> - Integrated Desktop Environment platform - version <b>4.5.0</b>
</ul>
</ul>

<hr><a name="scl20"></a>
<h2>Software Collections 2.0</h2>
Software Collections <b>2.0</b> provides following tools:
<ul>
<li><b>devtoolset-3</b> - Developer Toolset 3.1:
<ul>
<li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>4.9.2</b>
<li><b>gdb</b> - GNU Debugger - version <b>7.8.2</b>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.24</b>
<li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.161</b>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.11</b>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>2.6</b>
<li><b>valgrind</b> - Tool for finding memory management bugs in programs - version <b>3.10</b>
<li><strike><b>oprofile</b> - System wide profiler - version <b>0.9.9</b></strike> Installation is broken. Please use oprofile 1.1.0 from devtoolset-4.
    <li><strike><b>eclipse</b> - Integrated Desktop Environment platform - version <b>4.4.2</b> </strike> Installation is broken. Please use Eclipse 4.5 from devtoolset-4.
    </ul>

<li><b>rh-perl520</b> - A high-level programming language that is commonly used for system administration utilities - version <b>5.20.1</b>
<li><b>php54</b>, <b>php55</b>, <b>rh-php56</b> - Scripting language for creating dynamic web sites - versions <b>5.4.40</b>, <b>5.5.21</b> and <b>5.6.5</b>
<li><b>python27</b>, <b>rh-python34</b> - An interpreted, interactive, object-oriented programming language - versions <b>2.7.8</b>, <b>3.4.2</b></li>
<li><b>rh-ruby22</b> - An interpreter of object-oriented scripting language  - version <b>2.2.2</b></li>
<li><b>rh-ror41</b> - Ruby on Rails -  version <b>4.1.5</b></li>
<li><b>rh-mariadb100</b> - A release of MariaDB, an alternative to MySQL - version <b>10.0.20</b></li>
<li><b>rh-mongodb26</b> - High-performance, schema-free document-oriented database - version <b>2.6.9</b></li>
<li><b>rh-mysql56</b> - A release of MySQL, which provides a number of new features and enhancements - version <b>5.6.26</b></li>
<li><b>rh-postgresql94</b> - A release of PostgreSQL, which provides a number of enhancements - version <b>9.4.4</b></li>
<li><b>nodejs010</b> - A release of Node.js with npm 1.4.28 and support for the SPDY protocol version 3.1.
<li><b>nginx16</b> -  A web and proxy server with a focus on high concurrency - version <b>1.6.2</b></li>
<li><b>httpd24</b> -  superceeded by <a href="#scl22">Software Collections 2.2</a>
<li><b>thermostat1</b> - superceeded by <a href="#scl22">Software Collections 2.2</a>
<li><b>devassist09</b> - DevAssistant - a tool designed to assist developers with creating and setting up projects - version <b>0.9.3</b>
<li><b>maven30</b> - Java project management and project comprehension tool - version <b>3.0.5</b>
<li><b>rh-passenger40</b> - Phusion Passenger a fast, robust lightweight web and application server - version <b>4.0.50</b></li>
<li><b>rh-java-common</b> - Common Java libraries and tools used by other collections - version <b>1.1</b>
<li><b>v8</b> - Google JavaScript Engine - version <b>3.14.5.10</b>
</ul>


<hr><a name="scl12"></a>
<h2>Software Collections 1.2</h2>
Software Collections <b>1.2</b> provides following tools:
<ul>
<li>devtoolset-3</b> - Developer Toolset 3.0:
   <ul>
     <li>superceeded by Developer Toolset 3.1 from <a href="#scl20">Software Collections 2.0</a>
   </ul>
<li><b>perl-516</b> - A high-level programming language that is commonly used for system administration utilities - version <b>5.16.3</b>
<li><b>php54</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>php55</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>python27</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>python33</b> - An interpreted, interactive, object-oriented programming language - version <b>3.3.2</b></li>
<li><b>ruby193</b>, <b>ruby200</b> - An interpreter of object-oriented scripting language  - version <b>1.9.3</b> and <b>2.0.0</b></li>
<li><b>ror40</b> - Ruby on Rails -  version <b>4.0.2</b></li>
<li><b>mariadb55</b> - A release of MariaDB, an alternative to MySQL - version <b>5.5.44</b></li>
<li><b>mongodb24</b> - A cross-platform document-oriented database system - version <b>2.4.9</b></li>
<li><b>mysql55</b> - version <b>5.5.45</b></li>
<li><b>postgresql92</b> - version <b>9.2.13</b></li>
<li><b>nodejs010</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>nginx16</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>httpd24</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>thermostat1</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>git19</b> - version <b>1.9.4</b></li>
<li><b>devassist09</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
<li><b>maven30</b> - superceeded by <a href="#scl20">Software Collections 2.0</a>
</ul>

<hr><a name="centosscl"></a>
<h2>CentOS Community Software Collections</h2>
CentOS Community Software Collections provides following tools:
<ul>
<li><b>vagrant</b> - provides easy to configure, reproducible, and portable work environments - version <b>1.7.4</b></li>
<li><b>sclo-git25</b> - GIT version control system - version <b>2.5.5</b></li>
<li><b>sclo-php54</b> - provides additional php packages for <b>php54</b> collection.</li>
<li><b>sclo-php55</b> - provides additional php packages for <b>php55</b> collection.</li>
<li><b>sclo-php56</b> - provides additional php packages for <b>rh-php56</b> collection.</li>
<li><b>sclo-php70</b> - provides additional php packages for <b>rh-php70</b> collection.</li>
<li><b>sclo-ror42</b> - Ruby on Rails - version <b>4.2</b></li>
<li><b>sclo-httpd24more</b> - Additional packages for <b>httpd24</b> collection.</li>
<li><b>sclo-subversion19</b> - SVN a concurrent version control system - version <b>1.9.3</b></li>
</ul>


<hr><a name="docs"></a>
<h2>Documentation</h2>

<ul>
<li><a href="scl/Red_Hat_Developer_Toolset-8-8.0_Release_Notes-en-US.pdf">Red Hat Developer Toolset 8.0 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-3-3.2_Release_Notes-en-US.pdf">Red Hat Software Collections-3.2 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Developer_Toolset-7-7.0_Release_Notes-en-US.pdf">Red Hat Developer Toolset 7.0 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-3-3.0_Release_Notes-en-US.pdf">Red Hat Software Collections-3.0 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Developer_Toolset-6-6.0_Release_Notes-en-US.pdf">Red Hat Developer Toolset 6.0 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Developer_Toolset-6-User_Guide-en-US.pdf">Red Hat Developer Toolset 6 User Guide (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-2-2.3_Release_Notes-en-US.pdf">Red Hat Software Collections 2.3 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-2-2.2_Release_Notes-en-US.pdf">Red Hat Software Collections 2.2 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Developer_Toolset-4-4.0_Release_Notes-en-US.pdf">Red Hat Developer Toolset 4.0 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Developer_Toolset-3-3.1_Release_Notes-en-US.pdf">Red Hat Developer Toolset 3.1 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Developer_Toolset-3-User_Guide-en-US.pdf">Red Hat Developer Toolset 3 User Guide (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-2-2.0_Release_Notes-en-US.pdf">Red Hat Software Collections 2.0 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-2-Packaging_Guide-en-US.pdf">Red Hat Software Collections 2 Packaging Guide (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-1-1.2_Release_Notes-en-US.pdf">Red Hat Software Collections 1.2 Release Notes (local copy)</a>
<li><a href="scl/Red_Hat_Software_Collections-1-Packaging_Guide-en-US.pdf">Red Hat Software Collections 1 Packaging Guide (local copy)</a>
</ul>

Please <em>note</em>: software collections release life cycle differs from Red Hat Enterprise Linux lifecycle, for details please check:
<a href="https://access.redhat.com/support/policy/updates/rhscl">Red Hat Software Collections Support Policy</a>

<hr><a name="inst"></a>
<h2>Installation</h2>

<h3>CERN CentOS 7 (CC7)</h3>
   Install repository configuration by running as root:
   <pre>
   yum install centos-release-scl
   </pre>
   To install SCL run:
   <pre>
   yum install <b>collection-name</b>
   </pre>


<p>
Note: Each component has it own collection and is independent: <b>pick the ones you need</b>.
</p>

<p>
To test installed environment:
</p>
<pre>
# scl enable [collection1,collection2] bash
e.g:
# scl enable php54 bash
# php --version
PHP 5.4.16 (cli) (built: Oct  9 2013 18:11:47)
Copyright (c) 1997-2013 The PHP Group
Zend Engine v2.4.0, Copyright (c) 1998-2013 Zend Technologies
</pre>


