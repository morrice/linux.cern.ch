<!--&#35;include virtual="/linux/layout/header" -->
# CC7: locmap

<h2>Using locmap at CERN (TEST)</h2>

<h3>About locmap</h3>

locmap replaces the old desktop configuration tool, lcm, based on Quattor for CERN CentOS 7.<br>
locmap uses puppet and selected modules to configure systems outside datacentre.<br>
locmap allows to configure proposed AFS replacement such as cvmfs, eos fuse client and cernbox.<br>

<br>

<h3>Installation</h3>

As root on your system run:

<pre>
&#35; yum install locmap
</pre>
(above command will pull in all dependencies for above packages,<br>
including puppet and available modules. Please note that lcm and associated dependencies will be obsoleted and uninstalled.<br>
<br>

<h3>Usage</h3>

You can access the documentation by tiping:

<pre>
&#35; man locmap
</pre>

You can list enabled modules:

<pre>
&#35; locmap --list
[Available Modules]

Module name : sudo[enabled]
Module name : sendmail[enabled]
Module name : cernbox[disabled]
Module name : ntp[enabled]
Module name : gpg[enabled]
Module name : cvmfs[disabled]
Module name : ssh[enabled]
Module name : lpadmin[enabled]
Module name : nscd[enabled]
Module name : kerberos[enabled]
Module name : eosclient[disabled]
Module name : afs[enabled]

</pre>
You can configure all enabled module with the following command:

<pre>
&#35; locmap --configure all
</pre>

You can configure a new module:

<pre>
&#35; locmap --enable modulename
&#35; locmap --configure modulename
</pre>

You can disable a module:

<pre>
&#35; locmap --disable modulename
</pre>

You can remove root access to the Main Users of LanDB using the `--disallow_root`. It means that, only Resposibles defined in LanDB will have root access.
<pre>
&#35; locmap --disallow_root --configure modulename
&#35; locmap --disallow_root --configure all 
</pre>

<h3>Known issues</h3>
<pre>
 * locmap is not run automatically after installation, you will need to
 run:
    &#35; locmap --configure all

 * If lpadmincern is not installed on your computer before running locmap
 you will need to run :
    &#35; yum --enablerepo=cr --enablerepo={updates,extras,cern}-testing lpadmincern
</pre>
<h3>Support</h3>
For problems related to locmap, contact: <a href="mailto: linux.support@cern.ch">linux.support@cern.ch</a>
<br>
