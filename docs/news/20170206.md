<div class="admonition news_date">2017-02-06</div>

## PXE booting service change

Dear users,

Following the <a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0035318">Windows PXE service change</a>, we would like to announce
that all Windows PXE booting entries (both interactive and AIMS2 targets)
will be removed from current AIMS2/PXE boot server on <a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0035414">06 February 2017</a>
as these will become non-functional.

All systems defined as having operating system <b>WINDOWS</b> (<b>WIN</b>) in
<a href="http://cern.ch/network">Network Database</a> booting PXE will be redirected to Windows PXE server,
all systens having operating system defined as <b>LINUX</b> will be redirected to
Linux PXE server automatically.

Please note that the change in booting service has been implemented
for campus network on 1st of February, and will be implemented on
Computer Center and Technical Networks on 6th of February.

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


