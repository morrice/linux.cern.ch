<div class="admonition news_date">2017-04-10</div>

## Updated cloud / docker images available for SLC5/6 and CC7

Updated SLC5/6 and CC7 cloud and docker TEST images have been made available.

Cloud images:

    CC7 - x86_64 [2017-04-06]
    SLC6 - x86_64 [2017-04-06]
    SLC6 - i686 [2017-04-06]
    SLC5 - x86_64 [2017-04-06]
    SLC5 - i686 [2017-04-06]

available at <a href="https://openstack.cern.ch">https://openstack.cern.ch</a> or from command line:

&#35; openstack image list | grep 2017-04-06

Docker images:

    cc7-base:20170406 (:latest)
    slc6-base:20170406 (:latest)
    slc5-base:20170406 (:latest)

available at:

<a href="https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry">https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc6-base/container_registry</a>
<a href="https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry">https://gitlab.cern.ch/linuxsupport/slc5-base/container_registry</a>

or from command line:

&#35; docker pull gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc6-base:latest
&#35; docker pull gitlab-registry.cern.ch/linuxsupport/slc5-base:latest

(above docker images are also available from <a href="https://hub.docker.com/u/cern/">https://hub.docker.com/u/cern/</a>)

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


