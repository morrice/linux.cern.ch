<div class="admonition news_date">2016-05-30</div>

## Scientific Linux CERN 6.8 available.

Dear Linux users.

We have a pleasure to announce that updated Scientific
Linux CERN 6 (SLC6) version:

            ******************************************************
            SLC 6.8 becomes default SLC6 production version
            ******************************************************

                as of Thursday 30.05.2016

* What is Scientific Linux CERN 6.8 (SLC68)  ?

    - A minor release of SLC6X series with all the
    updates since release of 6.8 (and up to 30.05.2016)
    integrated in the base installation tree.

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

* Installation media images for SLC68 can be found at:

    For 32 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc68/i386/images/">http://linuxsoft.cern.ch/cern/slc68/i386/images/</a>
        (install path: linuxsoft:/cern/slc68/i386/)

    For 64 bit systems:

        <a href="http://linuxsoft.cern.ch/cern/slc68/x86_64/images/">http://linuxsoft.cern.ch/cern/slc68/x86_64/images/</a>
        (install path: linuxsoft:/cern/slc68/x86_64/)


    After downloading media images from above location
    standard installation procedure can be followed:

        <a href="http://cern.ch/linux/scientific6/docs/install">http://cern.ch/linux/scientific6/docs/install</a>


* ISO DVD images can be found at:

        <a href="http://linuxsoft.cern.ch/cern/slc67/iso/">http://linuxsoft.cern.ch/cern/slc68/iso/</a>


* Certification of the SLC 6.8:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


