<div class="admonition news_date">2021-11-17</div>

## CentOS 8.5 testing available

We are pleased to announce that the updated CentOS 8 version 8.5 is available for testing as of Wednesday 2021-11-17.

This version will become the default C8 production version on Wednesday 2021-11-24.

The list of updated packages can be found [here](/updates/c8/test/2021/11/monthly_updates/#2021-11-17).

For information about C8, including installation
instructions please visit [this page](/centos8/).
