<div class="admonition news_date">2016-05-17</div>

## Scientific Linux CERN 6.8 TEST available.

Dear CERN Linux users.

We have a pleasure to announce that:

    TEST version of SLC 6.8 for i386/x86_64
    ***************************************

is available as of today.

* What is Scientific Linux CERN 6 ?

    It is a CERN customized Linux distribution built on
top of common base platform - Scientific Linux -
which was in turn built from freely available Red Hat
Enterprise Linux 6 Server sources by a joint Fermi and CERN
effort.

* What is Scientific Linux CERN 6.8 (SLC68) TEST ?

- TEST minor release of SLC6X series with all the
    updates (up to 17.05.2016) integrated in the base
    installation tree.

* Installation

- via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

- non-PXE installation:

Boot media can be found at:

<a href="http://linuxsoft.cern.ch/cern/slc68/i386/images/">ttp://linuxsoft.cern.ch/cern/slc68/i386/images/</a>
<a href="http://linuxsoft.cern.ch/cern/slc68/x86_64/images/">http://linuxsoft.cern.ch/cern/slc68/x86_64/images/</a>

and corresponding installation paths are:

<a href="http://linuxsoft.cern.ch/cern/slc68/i386/">http://linuxsoft.cern.ch/cern/slc68/i386/</a>
<a href="http://linuxsoft.cern.ch/cern/slc68/x86_64/">http://linuxsoft.cern.ch/cern/slc68/x86_64/</a>

- Automated PXE installations using AIMS2

Boot targets are:

    SLC68_I386
    SLC68_X86_64

* Upgrading from previous SLC6X releases:

    - fully updated SLC 6X systems do not need any upgrade to
    6.8 version, otherwise standard system upgrade procedure
    will bring systems up to date:
        <a href="http://cern.ch/linux/scientific6/docs/quickupdate">http://cern.ch/linux/scientific6/docs/quickupdate</a>

Please note:

* SLC68 will become the default SLC6 production version as of:

                    Monday 30.05.2016
                    *********************

    providing that no show-stopper problems are found.

* SLC68 (yum) updates will be released to production on:

                    Monday 30.05.2016
                    **********************

* Certification of the SLC68:

- as there are no other changes than these listed above
    a formal certification process does not need to be handled
    for this minor release.

* Problem reporting:

Please report problems to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


