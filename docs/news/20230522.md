<div class="admonition news_date">2022-05-18</div>

## AlmaLinux 8.8 is available for testing

We are please to announce that AlmaLinux 8.8 is available for testing as of today.

AlmaLinux 8.8 will be available in production on 2023-05-31.

The list of updated packages can be found [here](/updates/alma8/test/2023/05/monthly_updates/#2023-05-22).

The release notes can be found [here](https://wiki.almalinux.org/release-notes/8.8.html).
