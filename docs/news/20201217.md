<div class="admonition news_date">2020-12-17</div>

## Information on change of end of life for CentOS 8

Dear Linux users,

CERN and Fermilab acknowledge the recent decision to shift focus from CentOS Linux to CentOS Stream, and the sudden change of the end of life of the CentOS 8 release. This may entail significant consequences for the worldwide particle physics community. We are currently investigating together the best path forward. We will keep you informed about any developments in this area during Q1 2021.

For more details on CentOS 8 and CentOS 8 stream, please see <a href="https://linux.cern.ch/centos8">https://linux.cern.ch/centos8</a>

Best regards,
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

