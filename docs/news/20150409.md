<div class="admonition news_date">2015-04-09</div>

## CERN Centos 7.1 becomes default production CC7 release.

Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

            *********************************************
            CC 7.1 becomes default CC7 production version
            *********************************************

                      as of Thursday 09.04.2015

For information about CC71, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

What happened to Scientific Linux CERN (SLC) ?

- SLC 6 and SLC5 remain supported Linux versions at CERN
- Next major release: 7 is based on CentOS

please see: <a href="http://cern.ch/linux/nextversion">http://cern.ch/linux/nextversion</a>
for more information.

Updating CERN CentOS 7.0 to 7.1:

All updates are released to production repositories as of 09.04.2015,
in order to update your system please run as root:

&#35; yum clean all
&#35; yum update


Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


