<div class="admonition news_date">2017-05-15</div>

## Phaseout of SMB v1 protocol at CERN

Dear Linux users.

Server Message Block (SMB) version 1 protocol used by Linux Samba suite to access
Microsoft Windows shares and Microsoft DFS filesystem is being disabled on
CERN systems, between 15 and 18 of May 2017, please see:
<p>
<a href="https://cern.service-now.com/service-portal?id=outage&from=CSP-Service-Status-Board&n=OTG0037584">Disabling SMB v1 protocol CERN-wide (SSB article)</a>
</p>
and
<p>
<a href="http://linux.web.cern.ch/linux/docs/phaseoutsmb1">Phaseout of SMB v1 protocol at CERN (Linux systems)</a>
</p>
Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


