<div class="admonition news_date">2023-05-19</div>

## Red Hat Enterprise Linux 8.8 is available for testing

We are please to announce that Red Hat Enterprise Linux 8.8 is available for testing as of today.

Red Hat Enterprise Linux 8.8 will be available in production on 2023-05-31.

The list of updated packages can be found [here](/updates/rhel8/test/2023/05/monthly_updates/#2023-05-19).

The release notes can be found [here](/rhel/rhel8/Red_Hat_Enterprise_Linux-8-8.8_Release_Notes-en-US.pdf).
