<div class="admonition news_date">2020-04-14</div>

## CERN CentOS 7.8 testing available.

Dear Linux users.

We are pleased to announce the updated CERN CentOS Linux (CC7) TEST version:

            *******************************
            CC 7.8 is available for testing
            *******************************
                as of Tuesday 14.04.2020.

For information about CC7, including installation instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

Please note that install media is not yet available.
We will send an additional email when ready.

* Updating CERN CentOS 7.7 to 7.8

CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    --enablerepo=epel-testing \
    update

* Agile infrastructure

As a reminder, all puppet configured machines within the
QA environment will receive automatically those updates.

--
Ben Morrice for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


