# Software repositories

Install software repository definitions on your system, by running as root:

<pre>
&#35; wget http://cern.ch/linux/rhel/repofiles/rhel7.repo -O /etc/yum.repos.d/rhel.repo
</pre>

Direct download: [rhel7.repo](repofiles/rhel7.repo)
To update your system run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

You may also want to consider adding the 'CERN' repository

<pre>
&#35; curl -o /etc/yum.repos.d/rhel7-cern.repo https://linux.web.cern.ch/rhel/repofiles/rhel7-cern.repo
&#35; curl -o /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 https://linuxsoft.cern.ch/internal/repos/RPM-GPG-KEY-kojiv2
</pre>
