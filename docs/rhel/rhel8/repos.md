# Software repositories

Install software repository definitions on your system, by running as root:

<pre>
&#35; curl -o /etc/yum.repos.d/rhel8.repo https://linux.web.cern.ch/rhel/repofiles/rhel8.repo
</pre>

Direct download: [rhel8.repo](repofiles/rhel8.repo)
To update your system run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

You may also want to consider adding the 'CERN' repository

<pre>
&#35; dnf --repofrompath=cern9el,http://linuxsoft.cern.ch/internal/repos/cern8el-stable/x86_64/os --repo=cern8el install cern-release
</pre>
