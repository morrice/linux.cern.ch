# Software repositories

Install software repository definitions on your system, by running as root:

<pre>
&#35; curl -o /etc/yum.repos.d/rhel9.repo https://linux.web.cern.ch/rhel/repofiles/rhel9.repo
</pre>

Direct download: [rhel9.repo](repofiles/rhel9.repo)
To update your system run as root:

<pre>
&#35; /usr/bin/yum update
</pre>

You may also want to consider adding the 'CERN' repository

<pre>
&#35; dnf --repofrompath=cern9el,http://linuxsoft.cern.ch/internal/repos/cern9el-stable/x86_64/os --repo=cern9el install cern-release
</pre>
