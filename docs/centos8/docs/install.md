# CentOS Stream 8 - Install instructions

The following methods are currently available to install CentOS Stream 8

* AIMS interactive - select the menu item `Install CentOS Stream 8 (CS8) 64-bit system`
* AIMS host registration - define your host to use the `CS8_X64_64` (Stream)
* OpenStack VM - boot from the image `CS8 - x86_64 [YYYY-MM-DD` (Stream)
* ai-bs puppet managed VM - use `ai-bs --cs8`
* docker image - use `docker pull gitlab-registry.cern.ch/linuxsupport/cs8-base:latest`
* Boot ISO: https://linuxsoft.cern.ch/cern/centos/s8/BaseOS/x86_64/os/images/boot.iso

For a detailed installation of a Linux Desktop, check the [step by step guide](/centos8/docs/stepbystep)
