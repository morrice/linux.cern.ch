# Linux web

This repo follows <https://how-to.docs.cern.ch/advanced/alternative/>.

* `linux` is the production site and uses the `linux-paas` OKD4 project.
  * `linux-qa` and uses the `linux-qa-paas` OKD4 project.

* We have a `CNAME` registered that we then use for a redirect, i.e. linux-qa.cern.ch -> linux-qa.web.cern.ch, to replicate the old website behaviour.
  * Redirection is done with a simple nginx redirection, deployed with `oc create -f nginx-redirect-(qa/prod).yaml`.

* For <https://linux-qa.web.cern.ch> **and** <https://linux.web.cern.ch> we use [KB0006299](https://cern.service-now.com/service-portal?id=kb_article&n=KB0006299) solution for a Let's encrypt certificate.

* This repo adds some useful redirections to ease with any old URL format. Check `nginx-default-cfg/redirect.conf` file for more details. This directory and its contents follow the [documentation](https://how-to.docs.cern.ch/advanced/gitbook_on_webeos_to_mkdocs_on_openshift/#b-customise-the-nginx-server-configuration-that-is-serving-your-documentation-on-openshift)

## Customisations

* `/theme` directory is used as the [Material Design](https://github.com/squidfunk/mkdocs-material) `custom_dir` for [overriding the theme](https://squidfunk.github.io/mkdocs-material/customization/).

* We use https://gitlab.cern.ch/linuxsupport/websites/updates-autogeneration-plugin/ for autogenerating, ordering and adding to our navigation all distribution updates.

## CI/CD

Instead of relying on the general use case <https://how-to.docs.cern.ch>, we deployed the more complex version IT-CDA-WF provided. Check [their docs for reference](https://how-to.docs.cern.ch/advanced/alternative/).

## Archive

Following a similar setup (except for the `nginx-redirect` as this one is not necessary) we have prepared <https://gitlab.cern.ch/linuxsupport/websites/linux-archive.web.cern.ch> for archiving old content. This helps reducing the building time, as for example splitting old SLC6 updates in a different project.

## Upgrading or updating MkDocs configuration

As of August 2021 we still use <https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-container> which is now considered unmaintained.

If we ever update the image version, we need to follow certain steps:

* Check the MkDocs and Material design theme version and edit them in  `.gitlab-ci.yml`
* Check if there is any incompatibility of such version with the website by using a feature branch.
* More than probably you will find styling not to be working properly.
  * Update the `theme/partials/{footer,header}.html` content with the corresponding version of the Material design you got from the previous Dockerfile
    * For Material design `7.1.7` we copy <https://raw.githubusercontent.com/squidfunk/mkdocs-material/7.1.7/src/partials/header.html> and adapt as comments mention. Do a diff to check what needs to be moved or added, basically the repository link and a warning about the <https://linux-archive.web.cern.ch>
    * You may need to update our custom CSS in `docs/stylesheets/extra.css`, normally class names or certain sizes.
* Do what you just did to <https://gitlab.cern.ch/linuxsupport/websites/linux-archive.web.cern.ch>

GitLab CI will take care of deploying everything.
